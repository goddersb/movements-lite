﻿Public Class MT210

    Public Property ClientSwiftCode As String
    Public Property BenSwiftCode As String
    Public Property OrderRef As String
    Public Property BrokerAccountNo As String
    Public Property RelatedOrderRef As String
    Public Property SettleDate As String
    Public Property Amount As Double?
    Public Property Ccy As String
    Public Property OrderSwiftCode As String
    Public Property BenSubBankSwift As String
    Public Property SwiftUrgent As Boolean
    Public Property Email As String
    Public Property Ftp As Boolean?

End Class