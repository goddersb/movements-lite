﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmStandingOrderRequest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gpbStandingOrders = New System.Windows.Forms.GroupBox()
        Me.lblSwiftRef = New System.Windows.Forms.Label()
        Me.txtSwiftRef = New System.Windows.Forms.TextBox()
        Me.lblTemplate = New System.Windows.Forms.Label()
        Me.cboTemplate = New System.Windows.Forms.ComboBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.cboStatus = New System.Windows.Forms.ComboBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.cboInterval = New System.Windows.Forms.ComboBox()
        Me.lblInterval = New System.Windows.Forms.Label()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.cboCCY = New System.Windows.Forms.ComboBox()
        Me.lblCurrency = New System.Windows.Forms.Label()
        Me.cboPortfolio = New System.Windows.Forms.ComboBox()
        Me.lblPortfolio = New System.Windows.Forms.Label()
        Me.cboPayType = New System.Windows.Forms.ComboBox()
        Me.lblPayType = New System.Windows.Forms.Label()
        Me.lblStandingOrders = New System.Windows.Forms.Label()
        Me.gpbStandingOrders.SuspendLayout()
        Me.SuspendLayout()
        '
        'gpbStandingOrders
        '
        Me.gpbStandingOrders.Controls.Add(Me.lblSwiftRef)
        Me.gpbStandingOrders.Controls.Add(Me.txtSwiftRef)
        Me.gpbStandingOrders.Controls.Add(Me.lblTemplate)
        Me.gpbStandingOrders.Controls.Add(Me.cboTemplate)
        Me.gpbStandingOrders.Controls.Add(Me.btnSave)
        Me.gpbStandingOrders.Controls.Add(Me.lblEndDate)
        Me.gpbStandingOrders.Controls.Add(Me.dtpEndDate)
        Me.gpbStandingOrders.Controls.Add(Me.lblStartDate)
        Me.gpbStandingOrders.Controls.Add(Me.dtpStartDate)
        Me.gpbStandingOrders.Controls.Add(Me.cboStatus)
        Me.gpbStandingOrders.Controls.Add(Me.lblStatus)
        Me.gpbStandingOrders.Controls.Add(Me.cboInterval)
        Me.gpbStandingOrders.Controls.Add(Me.lblInterval)
        Me.gpbStandingOrders.Controls.Add(Me.txtAmount)
        Me.gpbStandingOrders.Controls.Add(Me.lblAmount)
        Me.gpbStandingOrders.Controls.Add(Me.cboCCY)
        Me.gpbStandingOrders.Controls.Add(Me.lblCurrency)
        Me.gpbStandingOrders.Controls.Add(Me.cboPortfolio)
        Me.gpbStandingOrders.Controls.Add(Me.lblPortfolio)
        Me.gpbStandingOrders.Controls.Add(Me.cboPayType)
        Me.gpbStandingOrders.Controls.Add(Me.lblPayType)
        Me.gpbStandingOrders.Location = New System.Drawing.Point(12, 40)
        Me.gpbStandingOrders.Name = "gpbStandingOrders"
        Me.gpbStandingOrders.Size = New System.Drawing.Size(685, 404)
        Me.gpbStandingOrders.TabIndex = 0
        Me.gpbStandingOrders.TabStop = False
        Me.gpbStandingOrders.Text = "Standing Order Request"
        '
        'lblSwiftRef
        '
        Me.lblSwiftRef.Location = New System.Drawing.Point(13, 213)
        Me.lblSwiftRef.Name = "lblSwiftRef"
        Me.lblSwiftRef.Size = New System.Drawing.Size(68, 18)
        Me.lblSwiftRef.TabIndex = 202
        Me.lblSwiftRef.Text = "Swift Ref:"
        Me.lblSwiftRef.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSwiftRef
        '
        Me.txtSwiftRef.Location = New System.Drawing.Point(87, 213)
        Me.txtSwiftRef.Name = "txtSwiftRef"
        Me.txtSwiftRef.Size = New System.Drawing.Size(566, 20)
        Me.txtSwiftRef.TabIndex = 201
        '
        'lblTemplate
        '
        Me.lblTemplate.Location = New System.Drawing.Point(13, 171)
        Me.lblTemplate.Name = "lblTemplate"
        Me.lblTemplate.Size = New System.Drawing.Size(68, 18)
        Me.lblTemplate.TabIndex = 200
        Me.lblTemplate.Text = "Template:"
        Me.lblTemplate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboTemplate
        '
        Me.cboTemplate.BackColor = System.Drawing.Color.White
        Me.cboTemplate.FormattingEnabled = True
        Me.cboTemplate.Location = New System.Drawing.Point(87, 171)
        Me.cboTemplate.Name = "cboTemplate"
        Me.cboTemplate.Size = New System.Drawing.Size(566, 21)
        Me.cboTemplate.TabIndex = 199
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.Location = New System.Drawing.Point(275, 339)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(123, 44)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblEndDate
        '
        Me.lblEndDate.AutoSize = True
        Me.lblEndDate.Location = New System.Drawing.Point(461, 258)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(55, 13)
        Me.lblEndDate.TabIndex = 198
        Me.lblEndDate.Text = "End Date:"
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndDate.Location = New System.Drawing.Point(522, 252)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(131, 20)
        Me.dtpEndDate.TabIndex = 6
        '
        'lblStartDate
        '
        Me.lblStartDate.AutoSize = True
        Me.lblStartDate.Location = New System.Drawing.Point(22, 259)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(58, 13)
        Me.lblStartDate.TabIndex = 196
        Me.lblStartDate.Text = "Start Date:"
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpStartDate.Location = New System.Drawing.Point(86, 254)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(131, 20)
        Me.dtpStartDate.TabIndex = 5
        '
        'cboStatus
        '
        Me.cboStatus.BackColor = System.Drawing.Color.White
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(522, 294)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(131, 21)
        Me.cboStatus.TabIndex = 8
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(464, 298)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(40, 13)
        Me.lblStatus.TabIndex = 194
        Me.lblStatus.Text = "Status:"
        '
        'cboInterval
        '
        Me.cboInterval.BackColor = System.Drawing.Color.White
        Me.cboInterval.FormattingEnabled = True
        Me.cboInterval.Location = New System.Drawing.Point(86, 295)
        Me.cboInterval.Name = "cboInterval"
        Me.cboInterval.Size = New System.Drawing.Size(131, 21)
        Me.cboInterval.TabIndex = 7
        '
        'lblInterval
        '
        Me.lblInterval.AutoSize = True
        Me.lblInterval.Location = New System.Drawing.Point(36, 302)
        Me.lblInterval.Name = "lblInterval"
        Me.lblInterval.Size = New System.Drawing.Size(45, 13)
        Me.lblInterval.TabIndex = 192
        Me.lblInterval.Text = "Interval:"
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(87, 130)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(131, 20)
        Me.txtAmount.TabIndex = 3
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmount
        '
        Me.lblAmount.AutoSize = True
        Me.lblAmount.Location = New System.Drawing.Point(35, 133)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(46, 13)
        Me.lblAmount.TabIndex = 190
        Me.lblAmount.Text = "Amount:"
        '
        'cboCCY
        '
        Me.cboCCY.BackColor = System.Drawing.Color.White
        Me.cboCCY.FormattingEnabled = True
        Me.cboCCY.Location = New System.Drawing.Point(522, 130)
        Me.cboCCY.Name = "cboCCY"
        Me.cboCCY.Size = New System.Drawing.Size(131, 21)
        Me.cboCCY.TabIndex = 4
        '
        'lblCurrency
        '
        Me.lblCurrency.AutoSize = True
        Me.lblCurrency.Location = New System.Drawing.Point(464, 133)
        Me.lblCurrency.Name = "lblCurrency"
        Me.lblCurrency.Size = New System.Drawing.Size(52, 13)
        Me.lblCurrency.TabIndex = 189
        Me.lblCurrency.Text = "Currency:"
        '
        'cboPortfolio
        '
        Me.cboPortfolio.BackColor = System.Drawing.Color.White
        Me.cboPortfolio.FormattingEnabled = True
        Me.cboPortfolio.Location = New System.Drawing.Point(87, 88)
        Me.cboPortfolio.Name = "cboPortfolio"
        Me.cboPortfolio.Size = New System.Drawing.Size(566, 21)
        Me.cboPortfolio.TabIndex = 2
        '
        'lblPortfolio
        '
        Me.lblPortfolio.AutoSize = True
        Me.lblPortfolio.Location = New System.Drawing.Point(32, 95)
        Me.lblPortfolio.Name = "lblPortfolio"
        Me.lblPortfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblPortfolio.TabIndex = 186
        Me.lblPortfolio.Text = "Portfolio:"
        '
        'cboPayType
        '
        Me.cboPayType.BackColor = System.Drawing.Color.White
        Me.cboPayType.FormattingEnabled = True
        Me.cboPayType.Location = New System.Drawing.Point(87, 46)
        Me.cboPayType.Name = "cboPayType"
        Me.cboPayType.Size = New System.Drawing.Size(566, 21)
        Me.cboPayType.TabIndex = 1
        '
        'lblPayType
        '
        Me.lblPayType.AutoSize = True
        Me.lblPayType.Location = New System.Drawing.Point(25, 49)
        Me.lblPayType.Name = "lblPayType"
        Me.lblPayType.Size = New System.Drawing.Size(55, 13)
        Me.lblPayType.TabIndex = 131
        Me.lblPayType.Text = "Pay Type:"
        '
        'lblStandingOrders
        '
        Me.lblStandingOrders.AutoSize = True
        Me.lblStandingOrders.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStandingOrders.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblStandingOrders.Location = New System.Drawing.Point(237, 9)
        Me.lblStandingOrders.Name = "lblStandingOrders"
        Me.lblStandingOrders.Size = New System.Drawing.Size(235, 24)
        Me.lblStandingOrders.TabIndex = 45
        Me.lblStandingOrders.Text = "Standing Order Request"
        '
        'FrmStandingOrderRequest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(709, 456)
        Me.Controls.Add(Me.lblStandingOrders)
        Me.Controls.Add(Me.gpbStandingOrders)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmStandingOrderRequest"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Standing Order Request"
        Me.gpbStandingOrders.ResumeLayout(False)
        Me.gpbStandingOrders.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gpbStandingOrders As GroupBox
    Friend WithEvents lblStandingOrders As Label
    Friend WithEvents cboPayType As ComboBox
    Friend WithEvents lblPayType As Label
    Friend WithEvents cboPortfolio As ComboBox
    Friend WithEvents lblPortfolio As Label
    Friend WithEvents txtAmount As TextBox
    Friend WithEvents lblAmount As Label
    Friend WithEvents cboCCY As ComboBox
    Friend WithEvents lblCurrency As Label
    Friend WithEvents cboInterval As ComboBox
    Friend WithEvents lblInterval As Label
    Friend WithEvents cboStatus As ComboBox
    Friend WithEvents lblStatus As Label
    Friend WithEvents lblStartDate As Label
    Friend WithEvents dtpStartDate As DateTimePicker
    Friend WithEvents lblEndDate As Label
    Friend WithEvents dtpEndDate As DateTimePicker
    Friend WithEvents btnSave As Button
    Friend WithEvents lblTemplate As Label
    Friend WithEvents cboTemplate As ComboBox
    Friend WithEvents txtSwiftRef As TextBox
    Friend WithEvents lblSwiftRef As Label
End Class
