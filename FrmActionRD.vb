﻿Public Class FrmActionRD

    Private Function ValidateConfirm() As Boolean
        ValidateConfirm = True
        If (Rb1.Checked Or Rb3.Checked) And (ClsRD.StatusID = cStatusCancelled) Then
            MsgBox("You cannot action this transaction as it has already been cancelled", vbCritical, "Cannot edit")
            ValidateConfirm = False
        ElseIf Rb1.Checked And (ClsRD.StatusID <> cStatusReady And ClsRD.StatusID <> cStatusPendingAuth And ClsRD.StatusID <> cStatusPendingReview) Then
            MsgBox("You cannot edit this transaction as it has already been sent to SWIFT", vbCritical, "Cannot edit")
            ValidateConfirm = False
        End If
    End Function

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Dim varResponse As Integer

        ClsRD.SelectedAuthoriseOption = 0
        If ValidateConfirm() Then
            If Rb1.Checked Then
                varResponse = MsgBox("Are you sure you want to edit this transaction?", vbQuestion + vbYesNo, "edit transaction")
                If varResponse = vbYes Then
                    ClsRD.SelectedAuthoriseOption = 1
                End If
            ElseIf Rb2.Checked Then
                varResponse = MsgBox("Are you sure you want to copy this transaction?", vbQuestion + vbYesNo, "copy transaction")
                If varResponse = vbYes Then
                    ClsRD.SelectedAuthoriseOption = 2
                End If
            ElseIf Rb3.Checked Then
                varResponse = MsgBox("Are you sure you want to cancel this transaction (This might also send a cancellation SWIFT if required)?", vbQuestion + vbYesNo, "cancel transaction")
                If varResponse = vbYes Then
                    ClsRD.SelectedAuthoriseOption = 3
                End If
            ElseIf Rb4.Checked Then
                varResponse = MsgBox("PLEASE NOTE: This FOP transaction will be sent to IMS AFTER you manually confirm." & vbNewLine & "Are you sure you want to manually confirm this transaction?", vbQuestion + vbYesNo, "manually confirm transaction")

                If varResponse = vbYes Then
                    ClsRD.SelectedAuthoriseOption = 4
                End If
            ElseIf Rb5.Checked Then
                If ClsRD.StatusID = 10 Then
                    varResponse = MsgBox("Are you sure you want to re-send this transaction to IMS?", vbQuestion + vbYesNo, "Re-Send to IMS?")
                Else
                    varResponse = MsgBox("This transaction's status is not on IMS Error so you cannot re-send it?", vbExclamation, "No Re-send required")
                End If
                If varResponse = vbYes Then
                    ClsRD.SelectedAuthoriseOption = 6
                End If
            ElseIf Rb6.Checked Then
                varResponse = MsgBox("PLEASE NOTE: This FOP transaction will NOT SEND to IMS." & vbNewLine & "Are you sure you want to manually confirm this transaction?", vbQuestion + vbYesNo, "manually confirm transaction")

                If varResponse = vbYes Then
                    ClsRD.SelectedAuthoriseOption = 7
                End If
            End If

            If ClsRD.SelectedAuthoriseOption <> 1 Then
                ClsIMS.ActionMovementRD(ClsRD.SelectedAuthoriseOption, ClsRD.SelectedTransactionID, ClsIMS.FullUserName)
            End If

            If varResponse = vbYes Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub FrmActionRD_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtRDAuthType.Text = IIf(ClsRD.RecType = 1, "Receive Free", "Deliver Free")
        txtRDAuthID.Text = ClsRD.SelectedTransactionID
        txtRDAuthPortfolio.Text = ClsRD.PortfolioName
        txtRDAuthCCY.Text = ClsRD.CCYName
        txtRDAuthAmount.Text = ClsRD.Amount
        dtRDAuthTransDate.Value = ClsRD.TransDate
        dtRDAuthSettleDate.Value = ClsRD.SettleDate
        txtRDAuthInstrument.Text = ClsRD.InstrName
        txtRDAuthISIN.Text = ClsRD.InstrISIN
        txtRDAuthBroker.Text = ClsRD.BrokerName
        txtRDAuthClearer.Text = ClsRD.ClearerName
        txtRDAuthDelivAgent.Text = ClsRD.AgentName
        txtRDAuthDelivAgentAccountNo.Text = ClsRD.AgentAccountNo
    End Sub
End Class