﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLoadInstruments
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtTickerSearch = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CmdLoadInstrument = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboType = New System.Windows.Forms.ComboBox()
        Me.txtDesc = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboIssuer = New System.Windows.Forms.ComboBox()
        Me.ChkActive = New System.Windows.Forms.CheckBox()
        Me.ChkApproved = New System.Windows.Forms.CheckBox()
        Me.ChkESMA = New System.Windows.Forms.CheckBox()
        Me.lblPricingScenarios = New System.Windows.Forms.Label()
        Me.cboPricingScenarios = New System.Windows.Forms.ComboBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txtCFICode = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txttax = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtComments = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cboSector = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cboOffMarket = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cboGroup = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboSubType = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTicker = New System.Windows.Forms.TextBox()
        Me.lblTradedCcy = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboTradedCCY = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboIssuedCtry = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtISIN = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboMarket = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CboCCY = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.TabFund = New System.Windows.Forms.TabPage()
        Me.TabFundamentals = New System.Windows.Forms.TabControl()
        Me.tbpBond = New System.Windows.Forms.TabPage()
        Me.ChkTaxCalc = New System.Windows.Forms.CheckBox()
        Me.ChkPerpetual = New System.Windows.Forms.CheckBox()
        Me.ChkCapGuaranteed = New System.Windows.Forms.CheckBox()
        Me.ChkCpnGuaranteed = New System.Windows.Forms.CheckBox()
        Me.ChkDCC = New System.Windows.Forms.CheckBox()
        Me.ChkSinkable = New System.Windows.Forms.CheckBox()
        Me.ChkConvertible = New System.Windows.Forms.CheckBox()
        Me.txtDiscountFactor = New System.Windows.Forms.TextBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.txtMinCapGauranteed = New System.Windows.Forms.TextBox()
        Me.Label99 = New System.Windows.Forms.Label()
        Me.cboCalcType = New System.Windows.Forms.ComboBox()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.cboCouponType = New System.Windows.Forms.ComboBox()
        Me.cboInflationLinked = New System.Windows.Forms.ComboBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.cboRedemptionOptions = New System.Windows.Forms.ComboBox()
        Me.cboBondType = New System.Windows.Forms.ComboBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.cbotier = New System.Windows.Forms.ComboBox()
        Me.cboRelatedIndex = New System.Windows.Forms.ComboBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.cboTradedBy = New System.Windows.Forms.ComboBox()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.txtCpnDate = New System.Windows.Forms.TextBox()
        Me.txtGuarantor = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.txtMinDenomination = New System.Windows.Forms.TextBox()
        Me.cboBondYieldType = New System.Windows.Forms.ComboBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.cboIssueType = New System.Windows.Forms.ComboBox()
        Me.txtMinTradingQty = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.cboIntCalc2 = New System.Windows.Forms.ComboBox()
        Me.txtDailyQty = New System.Windows.Forms.TextBox()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.cboCollateralType = New System.Windows.Forms.ComboBox()
        Me.txtMaxAllowedQty = New System.Windows.Forms.TextBox()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.cboMoodys = New System.Windows.Forms.ComboBox()
        Me.cboInNewMonth = New System.Windows.Forms.ComboBox()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.cboSP = New System.Windows.Forms.ComboBox()
        Me.cboInDayOff = New System.Windows.Forms.ComboBox()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.cboFitch = New System.Windows.Forms.ComboBox()
        Me.cboIntCalc1 = New System.Windows.Forms.ComboBox()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.cboCalcTerms = New System.Windows.Forms.ComboBox()
        Me.cboLongCoupon = New System.Windows.Forms.ComboBox()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.cboTaxCalc = New System.Windows.Forms.ComboBox()
        Me.TabFundamentalsOther = New System.Windows.Forms.TabPage()
        Me.txtPriceDecimals = New System.Windows.Forms.TextBox()
        Me.Label93 = New System.Windows.Forms.Label()
        Me.TextBox11 = New System.Windows.Forms.TextBox()
        Me.Label94 = New System.Windows.Forms.Label()
        Me.TextBox12 = New System.Windows.Forms.TextBox()
        Me.Label95 = New System.Windows.Forms.Label()
        Me.TextBox13 = New System.Windows.Forms.TextBox()
        Me.Label96 = New System.Windows.Forms.Label()
        Me.TextBox14 = New System.Windows.Forms.TextBox()
        Me.Label97 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.txtQtyDecimals = New System.Windows.Forms.TextBox()
        Me.Label92 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.TabLinks = New System.Windows.Forms.TabPage()
        Me.cboExternalFeedModel = New System.Windows.Forms.ComboBox()
        Me.lblTickSize = New System.Windows.Forms.Label()
        Me.txtTickSize = New System.Windows.Forms.TextBox()
        Me.chkBogException = New System.Windows.Forms.CheckBox()
        Me.txtMainTitle = New System.Windows.Forms.TextBox()
        Me.txtSedol = New System.Windows.Forms.TextBox()
        Me.txtCusip = New System.Windows.Forms.TextBox()
        Me.txtExportLink2 = New System.Windows.Forms.TextBox()
        Me.txtExportLink1 = New System.Windows.Forms.TextBox()
        Me.txtLink5 = New System.Windows.Forms.TextBox()
        Me.txtLink4 = New System.Windows.Forms.TextBox()
        Me.txtLink3 = New System.Windows.Forms.TextBox()
        Me.txtLink2 = New System.Windows.Forms.TextBox()
        Me.txtLink1 = New System.Windows.Forms.TextBox()
        Me.txtProfileLink = New System.Windows.Forms.TextBox()
        Me.lblMainTitle = New System.Windows.Forms.Label()
        Me.lblSedol = New System.Windows.Forms.Label()
        Me.lblCusip = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.cboBBGPriceSource = New System.Windows.Forms.ComboBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.cboPriceQuoteType = New System.Windows.Forms.ComboBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.cboBBGType = New System.Windows.Forms.ComboBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.TabMain = New System.Windows.Forms.TabPage()
        Me.lblCommDerivInd = New System.Windows.Forms.Label()
        Me.cboCommDerivInd = New System.Windows.Forms.ComboBox()
        Me.lblDeliveryType = New System.Windows.Forms.Label()
        Me.cboDeliveryType = New System.Windows.Forms.ComboBox()
        Me.cboStyle = New System.Windows.Forms.ComboBox()
        Me.cboOptionType = New System.Windows.Forms.ComboBox()
        Me.cboSetDays = New System.Windows.Forms.ComboBox()
        Me.txtEndOfInvPeriod = New System.Windows.Forms.TextBox()
        Me.txtStartofInvPeriod = New System.Windows.Forms.TextBox()
        Me.chkMidCost = New System.Windows.Forms.CheckBox()
        Me.cboEquityType = New System.Windows.Forms.ComboBox()
        Me.gbxtraInfo = New System.Windows.Forms.GroupBox()
        Me.blLowPrice = New System.Windows.Forms.Label()
        Me.txtLowPrice = New System.Windows.Forms.TextBox()
        Me.lblPriceHigh = New System.Windows.Forms.Label()
        Me.txtPriceHigh = New System.Windows.Forms.TextBox()
        Me.lblPriceClose = New System.Windows.Forms.Label()
        Me.txtPriceClose = New System.Windows.Forms.TextBox()
        Me.lblDerivitiveDelType = New System.Windows.Forms.Label()
        Me.txtDerivitiveDelType = New System.Windows.Forms.TextBox()
        Me.lblMFIDComplexInstrInd = New System.Windows.Forms.Label()
        Me.txtMFIDComplexInstrInd = New System.Windows.Forms.TextBox()
        Me.lblMFIDIndicator = New System.Windows.Forms.Label()
        Me.txtMFIDIndicator = New System.Windows.Forms.TextBox()
        Me.lblCompToParentRel = New System.Windows.Forms.Label()
        Me.txtCompToParentRel = New System.Windows.Forms.TextBox()
        Me.lblDayCount = New System.Windows.Forms.Label()
        Me.txtDayCount = New System.Windows.Forms.TextBox()
        Me.lblBBG_ID = New System.Windows.Forms.Label()
        Me.txtBBG_ID = New System.Windows.Forms.TextBox()
        Me.ChkReCapitalised = New System.Windows.Forms.CheckBox()
        Me.ChkListed = New System.Windows.Forms.CheckBox()
        Me.txtFreefield4 = New System.Windows.Forms.TextBox()
        Me.txtPeriod = New System.Windows.Forms.TextBox()
        Me.txtMultiplier = New System.Windows.Forms.TextBox()
        Me.txtCalcDate = New System.Windows.Forms.TextBox()
        Me.txtBeta = New System.Windows.Forms.TextBox()
        Me.txtLimitAdjFactor = New System.Windows.Forms.TextBox()
        Me.txtFreefield3 = New System.Windows.Forms.TextBox()
        Me.txtFreefield2 = New System.Windows.Forms.TextBox()
        Me.txtFreefield1 = New System.Windows.Forms.TextBox()
        Me.txtIssuedFV = New System.Windows.Forms.TextBox()
        Me.txtOutstandingFV = New System.Windows.Forms.TextBox()
        Me.txtPremium = New System.Windows.Forms.TextBox()
        Me.txtStockExchangeCode = New System.Windows.Forms.TextBox()
        Me.txtCurrentCoupon = New System.Windows.Forms.TextBox()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.txtEntranceValue = New System.Windows.Forms.TextBox()
        Me.txtMaturityDate = New System.Windows.Forms.TextBox()
        Me.txtInitDate = New System.Windows.Forms.TextBox()
        Me.txtIssueDate = New System.Windows.Forms.TextBox()
        Me.lblMultiplier = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.cboInstrumentCategory = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.lblTitleType = New System.Windows.Forms.Label()
        Me.cboTitleType = New System.Windows.Forms.ComboBox()
        Me.lblCouponCcy = New System.Windows.Forms.Label()
        Me.cboCouponCCY = New System.Windows.Forms.ComboBox()
        Me.lblTaxPolicy = New System.Windows.Forms.Label()
        Me.cboTaxPolicy = New System.Windows.Forms.ComboBox()
        Me.lblPortfolioLink = New System.Windows.Forms.Label()
        Me.cboPortfolioLink = New System.Windows.Forms.ComboBox()
        Me.lblPeriod = New System.Windows.Forms.Label()
        Me.cboPeriod = New System.Windows.Forms.ComboBox()
        Me.lblIssuedFV = New System.Windows.Forms.Label()
        Me.lblOutstandingFV = New System.Windows.Forms.Label()
        Me.lblPremium = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblCurrentCoupon = New System.Windows.Forms.Label()
        Me.lblInterest = New System.Windows.Forms.Label()
        Me.lblEntranceValue = New System.Windows.Forms.Label()
        Me.lblMaturityDate = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblIssueDate = New System.Windows.Forms.Label()
        Me.TabInstrument = New System.Windows.Forms.TabControl()
        Me.TabSSI = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cboIndustrySector = New System.Windows.Forms.ComboBox()
        Me.Label106 = New System.Windows.Forms.Label()
        Me.cboIndustryGroups = New System.Windows.Forms.ComboBox()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.cboIndustries = New System.Windows.Forms.ComboBox()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.cboIndustrySubs = New System.Windows.Forms.ComboBox()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.txtCashSettlementBroker = New System.Windows.Forms.TextBox()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.txtClearingAgentCodePOS = New System.Windows.Forms.TextBox()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.txtAgentCodePOS = New System.Windows.Forms.TextBox()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.cboCashClearingAgt = New System.Windows.Forms.ComboBox()
        Me.Label104 = New System.Windows.Forms.Label()
        Me.cboSecClearingAgt2 = New System.Windows.Forms.ComboBox()
        Me.Label103 = New System.Windows.Forms.Label()
        Me.cboSecClearingAgt1 = New System.Windows.Forms.ComboBox()
        Me.Label102 = New System.Windows.Forms.Label()
        Me.cboCpty = New System.Windows.Forms.ComboBox()
        Me.Label101 = New System.Windows.Forms.Label()
        Me.cboPOS = New System.Windows.Forms.ComboBox()
        Me.btnInsertInstrument = New System.Windows.Forms.Button()
        Me.btnClearForm = New System.Windows.Forms.Button()
        Me.gbxOption = New System.Windows.Forms.GroupBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtUnderlyingShortCut = New System.Windows.Forms.TextBox()
        Me.txtUnderlyingName = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtDerivTCode = New System.Windows.Forms.TextBox()
        Me.ttpMain = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtExternalFeedModel = New System.Windows.Forms.TextBox()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabFund.SuspendLayout()
        Me.TabFundamentals.SuspendLayout()
        Me.tbpBond.SuspendLayout()
        Me.TabFundamentalsOther.SuspendLayout()
        Me.TabLinks.SuspendLayout()
        Me.TabMain.SuspendLayout()
        Me.gbxtraInfo.SuspendLayout()
        Me.TabInstrument.SuspendLayout()
        Me.TabSSI.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.gbxOption.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtTickerSearch
        '
        Me.txtTickerSearch.Location = New System.Drawing.Point(60, 16)
        Me.txtTickerSearch.Name = "txtTickerSearch"
        Me.txtTickerSearch.Size = New System.Drawing.Size(219, 20)
        Me.txtTickerSearch.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(159, 24)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Load Instrument"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox2.Controls.Add(Me.CmdLoadInstrument)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtTickerSearch)
        Me.GroupBox2.Location = New System.Drawing.Point(16, 40)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(375, 46)
        Me.GroupBox2.TabIndex = 80
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Get Data Criteria"
        '
        'CmdLoadInstrument
        '
        Me.CmdLoadInstrument.Enabled = False
        Me.CmdLoadInstrument.Location = New System.Drawing.Point(283, 14)
        Me.CmdLoadInstrument.Name = "CmdLoadInstrument"
        Me.CmdLoadInstrument.Size = New System.Drawing.Size(75, 23)
        Me.CmdLoadInstrument.TabIndex = 79
        Me.CmdLoadInstrument.Text = "Load"
        Me.CmdLoadInstrument.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 78
        Me.Label2.Text = "Ticker:"
        '
        'cboType
        '
        Me.cboType.FormattingEnabled = True
        Me.cboType.Location = New System.Drawing.Point(91, 44)
        Me.cboType.Name = "cboType"
        Me.cboType.Size = New System.Drawing.Size(148, 21)
        Me.cboType.TabIndex = 77
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(862, 33)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(415, 85)
        Me.txtDesc.TabIndex = 81
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox1.Controls.Add(Me.cboIssuer)
        Me.GroupBox1.Controls.Add(Me.ChkActive)
        Me.GroupBox1.Controls.Add(Me.ChkApproved)
        Me.GroupBox1.Controls.Add(Me.ChkESMA)
        Me.GroupBox1.Controls.Add(Me.lblPricingScenarios)
        Me.GroupBox1.Controls.Add(Me.cboPricingScenarios)
        Me.GroupBox1.Controls.Add(Me.Label43)
        Me.GroupBox1.Controls.Add(Me.txtCFICode)
        Me.GroupBox1.Controls.Add(Me.Label42)
        Me.GroupBox1.Controls.Add(Me.txttax)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.txtComments)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.cboSector)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.cboOffMarket)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.cboGroup)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cboSubType)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtTicker)
        Me.GroupBox1.Controls.Add(Me.lblTradedCcy)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.cboTradedCCY)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.cboIssuedCtry)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtISIN)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.cboMarket)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.CboCCY)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtDesc)
        Me.GroupBox1.Controls.Add(Me.cboType)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 93)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1298, 228)
        Me.GroupBox1.TabIndex = 82
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Get Data Criteria"
        '
        'cboIssuer
        '
        Me.cboIssuer.FormattingEnabled = True
        Me.cboIssuer.Location = New System.Drawing.Point(91, 142)
        Me.cboIssuer.Name = "cboIssuer"
        Me.cboIssuer.Size = New System.Drawing.Size(430, 21)
        Me.cboIssuer.TabIndex = 120
        '
        'ChkActive
        '
        Me.ChkActive.AutoSize = True
        Me.ChkActive.Location = New System.Drawing.Point(768, 206)
        Me.ChkActive.Name = "ChkActive"
        Me.ChkActive.Size = New System.Drawing.Size(56, 17)
        Me.ChkActive.TabIndex = 119
        Me.ChkActive.Text = "Active"
        Me.ChkActive.UseVisualStyleBackColor = True
        '
        'ChkApproved
        '
        Me.ChkApproved.AutoSize = True
        Me.ChkApproved.Location = New System.Drawing.Point(626, 206)
        Me.ChkApproved.Name = "ChkApproved"
        Me.ChkApproved.Size = New System.Drawing.Size(72, 17)
        Me.ChkApproved.TabIndex = 118
        Me.ChkApproved.Text = "Approved"
        Me.ChkApproved.UseVisualStyleBackColor = True
        '
        'ChkESMA
        '
        Me.ChkESMA.AutoSize = True
        Me.ChkESMA.Location = New System.Drawing.Point(245, 119)
        Me.ChkESMA.Name = "ChkESMA"
        Me.ChkESMA.Size = New System.Drawing.Size(73, 17)
        Me.ChkESMA.TabIndex = 117
        Me.ChkESMA.Text = "On ESMA"
        Me.ChkESMA.UseVisualStyleBackColor = True
        '
        'lblPricingScenarios
        '
        Me.lblPricingScenarios.AutoSize = True
        Me.lblPricingScenarios.Location = New System.Drawing.Point(528, 154)
        Me.lblPricingScenarios.Name = "lblPricingScenarios"
        Me.lblPricingScenarios.Size = New System.Drawing.Size(92, 13)
        Me.lblPricingScenarios.TabIndex = 116
        Me.lblPricingScenarios.Text = "Pricing Scenarios:"
        '
        'cboPricingScenarios
        '
        Me.cboPricingScenarios.FormattingEnabled = True
        Me.cboPricingScenarios.Location = New System.Drawing.Point(626, 151)
        Me.cboPricingScenarios.Name = "cboPricingScenarios"
        Me.cboPricingScenarios.Size = New System.Drawing.Size(224, 21)
        Me.cboPricingScenarios.TabIndex = 115
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(566, 181)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(54, 13)
        Me.Label43.TabIndex = 114
        Me.Label43.Text = "CFI Code:"
        '
        'txtCFICode
        '
        Me.txtCFICode.Location = New System.Drawing.Point(626, 178)
        Me.txtCFICode.Name = "txtCFICode"
        Me.txtCFICode.Size = New System.Drawing.Size(148, 20)
        Me.txtCFICode.TabIndex = 113
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(589, 128)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(28, 13)
        Me.Label42.TabIndex = 112
        Me.Label42.Text = "Tax:"
        '
        'txttax
        '
        Me.txttax.Location = New System.Drawing.Point(626, 125)
        Me.txttax.Name = "txttax"
        Me.txttax.Size = New System.Drawing.Size(148, 20)
        Me.txttax.TabIndex = 111
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(859, 119)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(56, 13)
        Me.Label17.TabIndex = 110
        Me.Label17.Text = "Comments"
        '
        'txtComments
        '
        Me.txtComments.Location = New System.Drawing.Point(862, 135)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.Size = New System.Drawing.Size(415, 85)
        Me.txtComments.TabIndex = 109
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(579, 101)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(41, 13)
        Me.Label16.TabIndex = 108
        Me.Label16.Text = "Sector:"
        '
        'cboSector
        '
        Me.cboSector.FormattingEnabled = True
        Me.cboSector.Location = New System.Drawing.Point(626, 98)
        Me.cboSector.Name = "cboSector"
        Me.cboSector.Size = New System.Drawing.Size(224, 21)
        Me.cboSector.TabIndex = 107
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(557, 74)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(63, 13)
        Me.Label15.TabIndex = 106
        Me.Label15.Text = "OFF Market"
        '
        'cboOffMarket
        '
        Me.cboOffMarket.FormattingEnabled = True
        Me.cboOffMarket.Location = New System.Drawing.Point(626, 71)
        Me.cboOffMarket.Name = "cboOffMarket"
        Me.cboOffMarket.Size = New System.Drawing.Size(224, 21)
        Me.cboOffMarket.TabIndex = 105
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(581, 49)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(39, 13)
        Me.Label14.TabIndex = 104
        Me.Label14.Text = "Group:"
        '
        'cboGroup
        '
        Me.cboGroup.FormattingEnabled = True
        Me.cboGroup.Location = New System.Drawing.Point(626, 46)
        Me.cboGroup.Name = "cboGroup"
        Me.cboGroup.Size = New System.Drawing.Size(224, 21)
        Me.cboGroup.TabIndex = 103
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(567, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(53, 13)
        Me.Label13.TabIndex = 102
        Me.Label13.Text = "SubType:"
        '
        'cboSubType
        '
        Me.cboSubType.FormattingEnabled = True
        Me.cboSubType.Location = New System.Drawing.Point(626, 20)
        Me.cboSubType.Name = "cboSubType"
        Me.cboSubType.Size = New System.Drawing.Size(224, 21)
        Me.cboSubType.TabIndex = 101
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(49, 72)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 13)
        Me.Label12.TabIndex = 97
        Me.Label12.Text = "Ticker:"
        '
        'txtTicker
        '
        Me.txtTicker.Location = New System.Drawing.Point(91, 70)
        Me.txtTicker.Name = "txtTicker"
        Me.txtTicker.Size = New System.Drawing.Size(430, 20)
        Me.txtTicker.TabIndex = 96
        '
        'lblTradedCcy
        '
        Me.lblTradedCcy.AutoSize = True
        Me.lblTradedCcy.Location = New System.Drawing.Point(19, 193)
        Me.lblTradedCcy.Name = "lblTradedCcy"
        Me.lblTradedCcy.Size = New System.Drawing.Size(68, 13)
        Me.lblTradedCcy.TabIndex = 95
        Me.lblTradedCcy.Text = "Traded CCY:"
        Me.lblTradedCcy.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(5, 168)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 13)
        Me.Label10.TabIndex = 93
        Me.Label10.Text = "Issued Country:"
        '
        'cboTradedCCY
        '
        Me.cboTradedCCY.FormattingEnabled = True
        Me.cboTradedCCY.Location = New System.Drawing.Point(91, 191)
        Me.cboTradedCCY.Name = "cboTradedCCY"
        Me.cboTradedCCY.Size = New System.Drawing.Size(148, 21)
        Me.cboTradedCCY.TabIndex = 92
        Me.cboTradedCCY.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(49, 143)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(38, 13)
        Me.Label9.TabIndex = 91
        Me.Label9.Text = "Issuer:"
        '
        'cboIssuedCtry
        '
        Me.cboIssuedCtry.FormattingEnabled = True
        Me.cboIssuedCtry.Location = New System.Drawing.Point(91, 166)
        Me.cboIssuedCtry.Name = "cboIssuedCtry"
        Me.cboIssuedCtry.Size = New System.Drawing.Size(148, 21)
        Me.cboIssuedCtry.TabIndex = 90
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(55, 120)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(31, 13)
        Me.Label8.TabIndex = 89
        Me.Label8.Text = "ISIN:"
        '
        'txtISIN
        '
        Me.txtISIN.Location = New System.Drawing.Point(91, 118)
        Me.txtISIN.Name = "txtISIN"
        Me.txtISIN.Size = New System.Drawing.Size(148, 20)
        Me.txtISIN.TabIndex = 88
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(44, 95)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 87
        Me.Label7.Text = "Market:"
        '
        'cboMarket
        '
        Me.cboMarket.FormattingEnabled = True
        Me.cboMarket.Location = New System.Drawing.Point(91, 93)
        Me.cboMarket.Name = "cboMarket"
        Me.cboMarket.Size = New System.Drawing.Size(430, 21)
        Me.cboMarket.TabIndex = 86
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(405, 49)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 13)
        Me.Label6.TabIndex = 85
        Me.Label6.Text = "CCY:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(49, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 84
        Me.Label5.Text = "Type:"
        '
        'CboCCY
        '
        Me.CboCCY.FormattingEnabled = True
        Me.CboCCY.Location = New System.Drawing.Point(442, 44)
        Me.CboCCY.Name = "CboCCY"
        Me.CboCCY.Size = New System.Drawing.Size(79, 21)
        Me.CboCCY.TabIndex = 83
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(859, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 82
        Me.Label4.Text = "Description"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(49, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 78
        Me.Label3.Text = "Name:"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(91, 20)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(430, 20)
        Me.txtName.TabIndex = 0
        '
        'TabFund
        '
        Me.TabFund.BackColor = System.Drawing.Color.FloralWhite
        Me.TabFund.Controls.Add(Me.TabFundamentals)
        Me.TabFund.Location = New System.Drawing.Point(4, 22)
        Me.TabFund.Name = "TabFund"
        Me.TabFund.Padding = New System.Windows.Forms.Padding(3)
        Me.TabFund.Size = New System.Drawing.Size(1290, 422)
        Me.TabFund.TabIndex = 2
        Me.TabFund.Text = "Fundamentals"
        '
        'TabFundamentals
        '
        Me.TabFundamentals.Controls.Add(Me.tbpBond)
        Me.TabFundamentals.Controls.Add(Me.TabFundamentalsOther)
        Me.TabFundamentals.Location = New System.Drawing.Point(40, 28)
        Me.TabFundamentals.Name = "TabFundamentals"
        Me.TabFundamentals.SelectedIndex = 0
        Me.TabFundamentals.Size = New System.Drawing.Size(1078, 382)
        Me.TabFundamentals.TabIndex = 175
        '
        'tbpBond
        '
        Me.tbpBond.BackColor = System.Drawing.Color.OldLace
        Me.tbpBond.Controls.Add(Me.ChkTaxCalc)
        Me.tbpBond.Controls.Add(Me.ChkPerpetual)
        Me.tbpBond.Controls.Add(Me.ChkCapGuaranteed)
        Me.tbpBond.Controls.Add(Me.ChkCpnGuaranteed)
        Me.tbpBond.Controls.Add(Me.ChkDCC)
        Me.tbpBond.Controls.Add(Me.ChkSinkable)
        Me.tbpBond.Controls.Add(Me.ChkConvertible)
        Me.tbpBond.Controls.Add(Me.txtDiscountFactor)
        Me.tbpBond.Controls.Add(Me.Label100)
        Me.tbpBond.Controls.Add(Me.txtMinCapGauranteed)
        Me.tbpBond.Controls.Add(Me.Label99)
        Me.tbpBond.Controls.Add(Me.cboCalcType)
        Me.tbpBond.Controls.Add(Me.Label83)
        Me.tbpBond.Controls.Add(Me.cboCouponType)
        Me.tbpBond.Controls.Add(Me.cboInflationLinked)
        Me.tbpBond.Controls.Add(Me.Label61)
        Me.tbpBond.Controls.Add(Me.Label84)
        Me.tbpBond.Controls.Add(Me.Label60)
        Me.tbpBond.Controls.Add(Me.cboRedemptionOptions)
        Me.tbpBond.Controls.Add(Me.cboBondType)
        Me.tbpBond.Controls.Add(Me.Label85)
        Me.tbpBond.Controls.Add(Me.Label63)
        Me.tbpBond.Controls.Add(Me.cbotier)
        Me.tbpBond.Controls.Add(Me.cboRelatedIndex)
        Me.tbpBond.Controls.Add(Me.Label86)
        Me.tbpBond.Controls.Add(Me.Label62)
        Me.tbpBond.Controls.Add(Me.cboTradedBy)
        Me.tbpBond.Controls.Add(Me.Label68)
        Me.tbpBond.Controls.Add(Me.txtCpnDate)
        Me.tbpBond.Controls.Add(Me.txtGuarantor)
        Me.tbpBond.Controls.Add(Me.Label82)
        Me.tbpBond.Controls.Add(Me.Label67)
        Me.tbpBond.Controls.Add(Me.Label81)
        Me.tbpBond.Controls.Add(Me.txtMinDenomination)
        Me.tbpBond.Controls.Add(Me.cboBondYieldType)
        Me.tbpBond.Controls.Add(Me.Label78)
        Me.tbpBond.Controls.Add(Me.Label66)
        Me.tbpBond.Controls.Add(Me.cboIssueType)
        Me.tbpBond.Controls.Add(Me.txtMinTradingQty)
        Me.tbpBond.Controls.Add(Me.Label79)
        Me.tbpBond.Controls.Add(Me.Label65)
        Me.tbpBond.Controls.Add(Me.cboIntCalc2)
        Me.tbpBond.Controls.Add(Me.txtDailyQty)
        Me.tbpBond.Controls.Add(Me.Label80)
        Me.tbpBond.Controls.Add(Me.Label64)
        Me.tbpBond.Controls.Add(Me.cboCollateralType)
        Me.tbpBond.Controls.Add(Me.txtMaxAllowedQty)
        Me.tbpBond.Controls.Add(Me.Label75)
        Me.tbpBond.Controls.Add(Me.cboMoodys)
        Me.tbpBond.Controls.Add(Me.cboInNewMonth)
        Me.tbpBond.Controls.Add(Me.Label69)
        Me.tbpBond.Controls.Add(Me.Label76)
        Me.tbpBond.Controls.Add(Me.cboSP)
        Me.tbpBond.Controls.Add(Me.cboInDayOff)
        Me.tbpBond.Controls.Add(Me.Label70)
        Me.tbpBond.Controls.Add(Me.Label77)
        Me.tbpBond.Controls.Add(Me.cboFitch)
        Me.tbpBond.Controls.Add(Me.cboIntCalc1)
        Me.tbpBond.Controls.Add(Me.Label71)
        Me.tbpBond.Controls.Add(Me.Label72)
        Me.tbpBond.Controls.Add(Me.cboCalcTerms)
        Me.tbpBond.Controls.Add(Me.cboLongCoupon)
        Me.tbpBond.Controls.Add(Me.Label74)
        Me.tbpBond.Controls.Add(Me.Label73)
        Me.tbpBond.Controls.Add(Me.cboTaxCalc)
        Me.tbpBond.Location = New System.Drawing.Point(4, 22)
        Me.tbpBond.Name = "tbpBond"
        Me.tbpBond.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpBond.Size = New System.Drawing.Size(1070, 356)
        Me.tbpBond.TabIndex = 0
        Me.tbpBond.Text = "Bond"
        '
        'ChkTaxCalc
        '
        Me.ChkTaxCalc.AutoSize = True
        Me.ChkTaxCalc.Location = New System.Drawing.Point(762, 190)
        Me.ChkTaxCalc.Name = "ChkTaxCalc"
        Me.ChkTaxCalc.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ChkTaxCalc.Size = New System.Drawing.Size(99, 17)
        Me.ChkTaxCalc.TabIndex = 185
        Me.ChkTaxCalc.Text = "Tax Calculation"
        Me.ChkTaxCalc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkTaxCalc.UseVisualStyleBackColor = True
        '
        'ChkPerpetual
        '
        Me.ChkPerpetual.AutoSize = True
        Me.ChkPerpetual.Location = New System.Drawing.Point(901, 305)
        Me.ChkPerpetual.Name = "ChkPerpetual"
        Me.ChkPerpetual.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ChkPerpetual.Size = New System.Drawing.Size(94, 17)
        Me.ChkPerpetual.TabIndex = 184
        Me.ChkPerpetual.Text = "Perpetual Flag"
        Me.ChkPerpetual.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkPerpetual.UseVisualStyleBackColor = True
        '
        'ChkCapGuaranteed
        '
        Me.ChkCapGuaranteed.AutoSize = True
        Me.ChkCapGuaranteed.Location = New System.Drawing.Point(878, 282)
        Me.ChkCapGuaranteed.Name = "ChkCapGuaranteed"
        Me.ChkCapGuaranteed.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ChkCapGuaranteed.Size = New System.Drawing.Size(117, 17)
        Me.ChkCapGuaranteed.TabIndex = 183
        Me.ChkCapGuaranteed.Text = "Capital Guaranteed"
        Me.ChkCapGuaranteed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkCapGuaranteed.UseVisualStyleBackColor = True
        '
        'ChkCpnGuaranteed
        '
        Me.ChkCpnGuaranteed.AutoSize = True
        Me.ChkCpnGuaranteed.Location = New System.Drawing.Point(873, 259)
        Me.ChkCpnGuaranteed.Name = "ChkCpnGuaranteed"
        Me.ChkCpnGuaranteed.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ChkCpnGuaranteed.Size = New System.Drawing.Size(122, 17)
        Me.ChkCpnGuaranteed.TabIndex = 182
        Me.ChkCpnGuaranteed.Text = "Coupon Guaranteed"
        Me.ChkCpnGuaranteed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkCpnGuaranteed.UseVisualStyleBackColor = True
        '
        'ChkDCC
        '
        Me.ChkDCC.AutoSize = True
        Me.ChkDCC.Location = New System.Drawing.Point(756, 305)
        Me.ChkDCC.Name = "ChkDCC"
        Me.ChkDCC.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ChkDCC.Size = New System.Drawing.Size(108, 17)
        Me.ChkDCC.TabIndex = 181
        Me.ChkDCC.Text = "Use Custom DCC"
        Me.ChkDCC.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkDCC.UseVisualStyleBackColor = True
        '
        'ChkSinkable
        '
        Me.ChkSinkable.AutoSize = True
        Me.ChkSinkable.Location = New System.Drawing.Point(797, 282)
        Me.ChkSinkable.Name = "ChkSinkable"
        Me.ChkSinkable.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ChkSinkable.Size = New System.Drawing.Size(67, 17)
        Me.ChkSinkable.TabIndex = 180
        Me.ChkSinkable.Text = "Sinkable"
        Me.ChkSinkable.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkSinkable.UseVisualStyleBackColor = True
        '
        'ChkConvertible
        '
        Me.ChkConvertible.AutoSize = True
        Me.ChkConvertible.Location = New System.Drawing.Point(785, 259)
        Me.ChkConvertible.Name = "ChkConvertible"
        Me.ChkConvertible.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ChkConvertible.Size = New System.Drawing.Size(79, 17)
        Me.ChkConvertible.TabIndex = 179
        Me.ChkConvertible.Text = "Convertible"
        Me.ChkConvertible.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkConvertible.UseVisualStyleBackColor = True
        '
        'txtDiscountFactor
        '
        Me.txtDiscountFactor.Location = New System.Drawing.Point(847, 219)
        Me.txtDiscountFactor.Name = "txtDiscountFactor"
        Me.txtDiscountFactor.Size = New System.Drawing.Size(148, 20)
        Me.txtDiscountFactor.TabIndex = 178
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Location = New System.Drawing.Point(756, 222)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(85, 13)
        Me.Label100.TabIndex = 177
        Me.Label100.Text = "Discount Factor:"
        '
        'txtMinCapGauranteed
        '
        Me.txtMinCapGauranteed.Location = New System.Drawing.Point(847, 155)
        Me.txtMinCapGauranteed.Name = "txtMinCapGauranteed"
        Me.txtMinCapGauranteed.Size = New System.Drawing.Size(148, 20)
        Me.txtMinCapGauranteed.TabIndex = 176
        '
        'Label99
        '
        Me.Label99.AutoSize = True
        Me.Label99.Location = New System.Drawing.Point(722, 158)
        Me.Label99.Name = "Label99"
        Me.Label99.Size = New System.Drawing.Size(119, 13)
        Me.Label99.TabIndex = 175
        Me.Label99.Text = "Min Cap Guaranteed %:"
        '
        'cboCalcType
        '
        Me.cboCalcType.FormattingEnabled = True
        Me.cboCalcType.Location = New System.Drawing.Point(106, 30)
        Me.cboCalcType.Name = "cboCalcType"
        Me.cboCalcType.Size = New System.Drawing.Size(148, 21)
        Me.cboCalcType.TabIndex = 123
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Location = New System.Drawing.Point(759, 128)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(82, 13)
        Me.Label83.TabIndex = 174
        Me.Label83.Text = "Inflation Linked:"
        '
        'cboCouponType
        '
        Me.cboCouponType.FormattingEnabled = True
        Me.cboCouponType.Location = New System.Drawing.Point(106, 57)
        Me.cboCouponType.Name = "cboCouponType"
        Me.cboCouponType.Size = New System.Drawing.Size(267, 21)
        Me.cboCouponType.TabIndex = 121
        '
        'cboInflationLinked
        '
        Me.cboInflationLinked.FormattingEnabled = True
        Me.cboInflationLinked.Location = New System.Drawing.Point(847, 125)
        Me.cboInflationLinked.Name = "cboInflationLinked"
        Me.cboInflationLinked.Size = New System.Drawing.Size(148, 21)
        Me.cboInflationLinked.TabIndex = 173
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(29, 60)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(74, 13)
        Me.Label61.TabIndex = 122
        Me.Label61.Text = "Coupon Type:"
        '
        'Label84
        '
        Me.Label84.AutoSize = True
        Me.Label84.Location = New System.Drawing.Point(732, 98)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(109, 13)
        Me.Label84.TabIndex = 172
        Me.Label84.Text = "Redemtption Options:"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(41, 33)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(58, 13)
        Me.Label60.TabIndex = 124
        Me.Label60.Text = "Calc Type:"
        '
        'cboRedemptionOptions
        '
        Me.cboRedemptionOptions.FormattingEnabled = True
        Me.cboRedemptionOptions.Location = New System.Drawing.Point(847, 95)
        Me.cboRedemptionOptions.Name = "cboRedemptionOptions"
        Me.cboRedemptionOptions.Size = New System.Drawing.Size(148, 21)
        Me.cboRedemptionOptions.TabIndex = 171
        '
        'cboBondType
        '
        Me.cboBondType.FormattingEnabled = True
        Me.cboBondType.Location = New System.Drawing.Point(106, 84)
        Me.cboBondType.Name = "cboBondType"
        Me.cboBondType.Size = New System.Drawing.Size(267, 21)
        Me.cboBondType.TabIndex = 125
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Location = New System.Drawing.Point(813, 71)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(28, 13)
        Me.Label85.TabIndex = 170
        Me.Label85.Text = "Tier:"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(37, 87)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(62, 13)
        Me.Label63.TabIndex = 126
        Me.Label63.Text = "Bond Type:"
        '
        'cbotier
        '
        Me.cbotier.FormattingEnabled = True
        Me.cbotier.Location = New System.Drawing.Point(847, 65)
        Me.cbotier.Name = "cbotier"
        Me.cbotier.Size = New System.Drawing.Size(148, 21)
        Me.cbotier.TabIndex = 169
        '
        'cboRelatedIndex
        '
        Me.cboRelatedIndex.FormattingEnabled = True
        Me.cboRelatedIndex.Location = New System.Drawing.Point(106, 111)
        Me.cboRelatedIndex.Name = "cboRelatedIndex"
        Me.cboRelatedIndex.Size = New System.Drawing.Size(185, 21)
        Me.cboRelatedIndex.TabIndex = 127
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Location = New System.Drawing.Point(782, 41)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(59, 13)
        Me.Label86.TabIndex = 168
        Me.Label86.Text = "Traded By:"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(27, 114)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(73, 13)
        Me.Label62.TabIndex = 128
        Me.Label62.Text = "RelatedIndex:"
        '
        'cboTradedBy
        '
        Me.cboTradedBy.FormattingEnabled = True
        Me.cboTradedBy.Location = New System.Drawing.Point(847, 38)
        Me.cboTradedBy.Name = "cboTradedBy"
        Me.cboTradedBy.Size = New System.Drawing.Size(148, 21)
        Me.cboTradedBy.TabIndex = 167
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(41, 141)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(57, 13)
        Me.Label68.TabIndex = 129
        Me.Label68.Text = "Guarantor:"
        '
        'txtCpnDate
        '
        Me.txtCpnDate.Location = New System.Drawing.Point(524, 68)
        Me.txtCpnDate.Name = "txtCpnDate"
        Me.txtCpnDate.Size = New System.Drawing.Size(148, 20)
        Me.txtCpnDate.TabIndex = 166
        '
        'txtGuarantor
        '
        Me.txtGuarantor.Location = New System.Drawing.Point(106, 138)
        Me.txtGuarantor.Name = "txtGuarantor"
        Me.txtGuarantor.Size = New System.Drawing.Size(269, 20)
        Me.txtGuarantor.TabIndex = 130
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(423, 71)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(93, 13)
        Me.Label82.TabIndex = 165
        Me.Label82.Text = "Ref Coupon Date:"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(5, 167)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(95, 13)
        Me.Label67.TabIndex = 131
        Me.Label67.Text = "Min Denomination:"
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(430, 317)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(88, 13)
        Me.Label81.TabIndex = 164
        Me.Label81.Text = "Bond Yield Type:"
        '
        'txtMinDenomination
        '
        Me.txtMinDenomination.Location = New System.Drawing.Point(106, 164)
        Me.txtMinDenomination.Name = "txtMinDenomination"
        Me.txtMinDenomination.Size = New System.Drawing.Size(148, 20)
        Me.txtMinDenomination.TabIndex = 132
        '
        'cboBondYieldType
        '
        Me.cboBondYieldType.FormattingEnabled = True
        Me.cboBondYieldType.Location = New System.Drawing.Point(524, 314)
        Me.cboBondYieldType.Name = "cboBondYieldType"
        Me.cboBondYieldType.Size = New System.Drawing.Size(148, 21)
        Me.cboBondYieldType.TabIndex = 163
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(453, 290)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(62, 13)
        Me.Label78.TabIndex = 162
        Me.Label78.Text = "Issue Type:"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(13, 193)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(85, 13)
        Me.Label66.TabIndex = 133
        Me.Label66.Text = "Min Trading Qty:"
        '
        'cboIssueType
        '
        Me.cboIssueType.FormattingEnabled = True
        Me.cboIssueType.Location = New System.Drawing.Point(524, 287)
        Me.cboIssueType.Name = "cboIssueType"
        Me.cboIssueType.Size = New System.Drawing.Size(148, 21)
        Me.cboIssueType.TabIndex = 161
        Me.cboIssueType.Tag = "Miss"
        '
        'txtMinTradingQty
        '
        Me.txtMinTradingQty.Location = New System.Drawing.Point(106, 190)
        Me.txtMinTradingQty.Name = "txtMinTradingQty"
        Me.txtMinTradingQty.Size = New System.Drawing.Size(148, 20)
        Me.txtMinTradingQty.TabIndex = 134
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(416, 236)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(100, 13)
        Me.Label79.TabIndex = 160
        Me.Label79.Text = "Interest Calculation:"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(48, 219)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(52, 13)
        Me.Label65.TabIndex = 135
        Me.Label65.Text = "Daily Qty:"
        '
        'cboIntCalc2
        '
        Me.cboIntCalc2.FormattingEnabled = True
        Me.cboIntCalc2.Location = New System.Drawing.Point(524, 233)
        Me.cboIntCalc2.Name = "cboIntCalc2"
        Me.cboIntCalc2.Size = New System.Drawing.Size(148, 21)
        Me.cboIntCalc2.TabIndex = 159
        Me.cboIntCalc2.Tag = "Miss"
        '
        'txtDailyQty
        '
        Me.txtDailyQty.Location = New System.Drawing.Point(106, 216)
        Me.txtDailyQty.Name = "txtDailyQty"
        Me.txtDailyQty.Size = New System.Drawing.Size(148, 20)
        Me.txtDailyQty.TabIndex = 136
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(435, 263)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(80, 13)
        Me.Label80.TabIndex = 158
        Me.Label80.Text = "Collateral Type:"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(11, 245)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(89, 13)
        Me.Label64.TabIndex = 137
        Me.Label64.Text = "Max Allowed Qty:"
        '
        'cboCollateralType
        '
        Me.cboCollateralType.FormattingEnabled = True
        Me.cboCollateralType.Location = New System.Drawing.Point(524, 260)
        Me.cboCollateralType.Name = "cboCollateralType"
        Me.cboCollateralType.Size = New System.Drawing.Size(148, 21)
        Me.cboCollateralType.TabIndex = 157
        Me.cboCollateralType.Tag = "Miss"
        '
        'txtMaxAllowedQty
        '
        Me.txtMaxAllowedQty.Location = New System.Drawing.Point(106, 242)
        Me.txtMaxAllowedQty.Name = "txtMaxAllowedQty"
        Me.txtMaxAllowedQty.Size = New System.Drawing.Size(148, 20)
        Me.txtMaxAllowedQty.TabIndex = 138
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(441, 209)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(77, 13)
        Me.Label75.TabIndex = 156
        Me.Label75.Text = "In New Month:"
        '
        'cboMoodys
        '
        Me.cboMoodys.FormattingEnabled = True
        Me.cboMoodys.Location = New System.Drawing.Point(106, 268)
        Me.cboMoodys.Name = "cboMoodys"
        Me.cboMoodys.Size = New System.Drawing.Size(91, 21)
        Me.cboMoodys.TabIndex = 139
        '
        'cboInNewMonth
        '
        Me.cboInNewMonth.FormattingEnabled = True
        Me.cboInNewMonth.Location = New System.Drawing.Point(524, 206)
        Me.cboInNewMonth.Name = "cboInNewMonth"
        Me.cboInNewMonth.Size = New System.Drawing.Size(148, 21)
        Me.cboInNewMonth.TabIndex = 155
        Me.cboInNewMonth.Tag = "Miss"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(55, 271)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(47, 13)
        Me.Label69.TabIndex = 140
        Me.Label69.Text = "Moodys:"
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(460, 155)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(58, 13)
        Me.Label76.TabIndex = 154
        Me.Label76.Text = "In Day Off:"
        '
        'cboSP
        '
        Me.cboSP.FormattingEnabled = True
        Me.cboSP.Location = New System.Drawing.Point(106, 295)
        Me.cboSP.Name = "cboSP"
        Me.cboSP.Size = New System.Drawing.Size(91, 21)
        Me.cboSP.TabIndex = 141
        '
        'cboInDayOff
        '
        Me.cboInDayOff.FormattingEnabled = True
        Me.cboInDayOff.Location = New System.Drawing.Point(524, 152)
        Me.cboInDayOff.Name = "cboInDayOff"
        Me.cboInDayOff.Size = New System.Drawing.Size(148, 21)
        Me.cboInDayOff.TabIndex = 153
        Me.cboInDayOff.Tag = "Miss"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(70, 298)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(30, 13)
        Me.Label70.TabIndex = 142
        Me.Label70.Text = "S+P:"
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(418, 182)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(100, 13)
        Me.Label77.TabIndex = 152
        Me.Label77.Text = "Interest Calculation:"
        '
        'cboFitch
        '
        Me.cboFitch.FormattingEnabled = True
        Me.cboFitch.Location = New System.Drawing.Point(106, 322)
        Me.cboFitch.Name = "cboFitch"
        Me.cboFitch.Size = New System.Drawing.Size(91, 21)
        Me.cboFitch.TabIndex = 143
        '
        'cboIntCalc1
        '
        Me.cboIntCalc1.FormattingEnabled = True
        Me.cboIntCalc1.Location = New System.Drawing.Point(524, 179)
        Me.cboIntCalc1.Name = "cboIntCalc1"
        Me.cboIntCalc1.Size = New System.Drawing.Size(148, 21)
        Me.cboIntCalc1.TabIndex = 151
        Me.cboIntCalc1.Tag = "Miss"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(67, 322)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(33, 13)
        Me.Label71.TabIndex = 144
        Me.Label71.Text = "Fitch:"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(444, 127)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(74, 13)
        Me.Label72.TabIndex = 150
        Me.Label72.Text = "Long Coupon:"
        '
        'cboCalcTerms
        '
        Me.cboCalcTerms.FormattingEnabled = True
        Me.cboCalcTerms.Location = New System.Drawing.Point(524, 95)
        Me.cboCalcTerms.Name = "cboCalcTerms"
        Me.cboCalcTerms.Size = New System.Drawing.Size(148, 21)
        Me.cboCalcTerms.TabIndex = 145
        '
        'cboLongCoupon
        '
        Me.cboLongCoupon.FormattingEnabled = True
        Me.cboLongCoupon.Location = New System.Drawing.Point(524, 122)
        Me.cboLongCoupon.Name = "cboLongCoupon"
        Me.cboLongCoupon.Size = New System.Drawing.Size(148, 21)
        Me.cboLongCoupon.TabIndex = 149
        Me.cboLongCoupon.Tag = "Miss"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(424, 98)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(94, 13)
        Me.Label74.TabIndex = 146
        Me.Label74.Text = "Calculation Terms:"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(435, 41)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(83, 13)
        Me.Label73.TabIndex = 148
        Me.Label73.Text = "Tax Calculation:"
        '
        'cboTaxCalc
        '
        Me.cboTaxCalc.FormattingEnabled = True
        Me.cboTaxCalc.Location = New System.Drawing.Point(524, 38)
        Me.cboTaxCalc.Name = "cboTaxCalc"
        Me.cboTaxCalc.Size = New System.Drawing.Size(148, 21)
        Me.cboTaxCalc.TabIndex = 147
        Me.cboTaxCalc.Tag = "Miss"
        '
        'TabFundamentalsOther
        '
        Me.TabFundamentalsOther.BackColor = System.Drawing.Color.OldLace
        Me.TabFundamentalsOther.Controls.Add(Me.txtPriceDecimals)
        Me.TabFundamentalsOther.Controls.Add(Me.Label93)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox11)
        Me.TabFundamentalsOther.Controls.Add(Me.Label94)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox12)
        Me.TabFundamentalsOther.Controls.Add(Me.Label95)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox13)
        Me.TabFundamentalsOther.Controls.Add(Me.Label96)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox14)
        Me.TabFundamentalsOther.Controls.Add(Me.Label97)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox15)
        Me.TabFundamentalsOther.Controls.Add(Me.Label98)
        Me.TabFundamentalsOther.Controls.Add(Me.txtQtyDecimals)
        Me.TabFundamentalsOther.Controls.Add(Me.Label92)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox6)
        Me.TabFundamentalsOther.Controls.Add(Me.Label91)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox5)
        Me.TabFundamentalsOther.Controls.Add(Me.Label90)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox4)
        Me.TabFundamentalsOther.Controls.Add(Me.Label89)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox3)
        Me.TabFundamentalsOther.Controls.Add(Me.Label88)
        Me.TabFundamentalsOther.Controls.Add(Me.TextBox2)
        Me.TabFundamentalsOther.Controls.Add(Me.Label87)
        Me.TabFundamentalsOther.Location = New System.Drawing.Point(4, 22)
        Me.TabFundamentalsOther.Name = "TabFundamentalsOther"
        Me.TabFundamentalsOther.Padding = New System.Windows.Forms.Padding(3)
        Me.TabFundamentalsOther.Size = New System.Drawing.Size(1070, 356)
        Me.TabFundamentalsOther.TabIndex = 1
        Me.TabFundamentalsOther.Text = "Other"
        '
        'txtPriceDecimals
        '
        Me.txtPriceDecimals.Location = New System.Drawing.Point(520, 208)
        Me.txtPriceDecimals.Name = "txtPriceDecimals"
        Me.txtPriceDecimals.Size = New System.Drawing.Size(246, 20)
        Me.txtPriceDecimals.TabIndex = 118
        '
        'Label93
        '
        Me.Label93.AutoSize = True
        Me.Label93.Location = New System.Drawing.Point(434, 211)
        Me.Label93.Name = "Label93"
        Me.Label93.Size = New System.Drawing.Size(80, 13)
        Me.Label93.TabIndex = 117
        Me.Label93.Text = "Price Decimals:"
        '
        'TextBox11
        '
        Me.TextBox11.Location = New System.Drawing.Point(520, 142)
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New System.Drawing.Size(246, 20)
        Me.TextBox11.TabIndex = 116
        '
        'Label94
        '
        Me.Label94.AutoSize = True
        Me.Label94.Location = New System.Drawing.Point(478, 145)
        Me.Label94.Name = "Label94"
        Me.Label94.Size = New System.Drawing.Size(36, 13)
        Me.Label94.TabIndex = 115
        Me.Label94.Text = "Label:"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New System.Drawing.Point(520, 116)
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New System.Drawing.Size(246, 20)
        Me.TextBox12.TabIndex = 114
        '
        'Label95
        '
        Me.Label95.AutoSize = True
        Me.Label95.Location = New System.Drawing.Point(478, 119)
        Me.Label95.Name = "Label95"
        Me.Label95.Size = New System.Drawing.Size(36, 13)
        Me.Label95.TabIndex = 113
        Me.Label95.Text = "Label:"
        '
        'TextBox13
        '
        Me.TextBox13.Location = New System.Drawing.Point(520, 90)
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New System.Drawing.Size(246, 20)
        Me.TextBox13.TabIndex = 112
        '
        'Label96
        '
        Me.Label96.AutoSize = True
        Me.Label96.Location = New System.Drawing.Point(457, 93)
        Me.Label96.Name = "Label96"
        Me.Label96.Size = New System.Drawing.Size(57, 13)
        Me.Label96.TabIndex = 111
        Me.Label96.Text = "NoStocks:"
        '
        'TextBox14
        '
        Me.TextBox14.Location = New System.Drawing.Point(520, 64)
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New System.Drawing.Size(246, 20)
        Me.TextBox14.TabIndex = 110
        '
        'Label97
        '
        Me.Label97.AutoSize = True
        Me.Label97.Location = New System.Drawing.Point(478, 68)
        Me.Label97.Name = "Label97"
        Me.Label97.Size = New System.Drawing.Size(36, 13)
        Me.Label97.TabIndex = 109
        Me.Label97.Text = "Label:"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(520, 38)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(246, 20)
        Me.TextBox15.TabIndex = 108
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.Location = New System.Drawing.Point(478, 42)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(36, 13)
        Me.Label98.TabIndex = 107
        Me.Label98.Text = "Label:"
        '
        'txtQtyDecimals
        '
        Me.txtQtyDecimals.Location = New System.Drawing.Point(135, 205)
        Me.txtQtyDecimals.Name = "txtQtyDecimals"
        Me.txtQtyDecimals.Size = New System.Drawing.Size(246, 20)
        Me.txtQtyDecimals.TabIndex = 106
        '
        'Label92
        '
        Me.Label92.AutoSize = True
        Me.Label92.Location = New System.Drawing.Point(34, 208)
        Me.Label92.Name = "Label92"
        Me.Label92.Size = New System.Drawing.Size(95, 13)
        Me.Label92.TabIndex = 105
        Me.Label92.Text = "Quantity Decimals:"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(135, 139)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(246, 20)
        Me.TextBox6.TabIndex = 104
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Location = New System.Drawing.Point(93, 142)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(36, 13)
        Me.Label91.TabIndex = 103
        Me.Label91.Text = "Label:"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(135, 113)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(246, 20)
        Me.TextBox5.TabIndex = 102
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Location = New System.Drawing.Point(93, 116)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(36, 13)
        Me.Label90.TabIndex = 101
        Me.Label90.Text = "Label:"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(135, 87)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(246, 20)
        Me.TextBox4.TabIndex = 100
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Location = New System.Drawing.Point(93, 90)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(36, 13)
        Me.Label89.TabIndex = 99
        Me.Label89.Text = "Label:"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(135, 61)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(246, 20)
        Me.TextBox3.TabIndex = 98
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.Location = New System.Drawing.Point(93, 64)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(36, 13)
        Me.Label88.TabIndex = 97
        Me.Label88.Text = "Label:"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(135, 35)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(246, 20)
        Me.TextBox2.TabIndex = 96
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.Location = New System.Drawing.Point(49, 38)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(80, 13)
        Me.Label87.TabIndex = 95
        Me.Label87.Text = "Price/Earnings:"
        '
        'TabLinks
        '
        Me.TabLinks.BackColor = System.Drawing.Color.OldLace
        Me.TabLinks.Controls.Add(Me.cboExternalFeedModel)
        Me.TabLinks.Controls.Add(Me.lblTickSize)
        Me.TabLinks.Controls.Add(Me.txtTickSize)
        Me.TabLinks.Controls.Add(Me.chkBogException)
        Me.TabLinks.Controls.Add(Me.txtMainTitle)
        Me.TabLinks.Controls.Add(Me.txtSedol)
        Me.TabLinks.Controls.Add(Me.txtCusip)
        Me.TabLinks.Controls.Add(Me.txtExternalFeedModel)
        Me.TabLinks.Controls.Add(Me.txtExportLink2)
        Me.TabLinks.Controls.Add(Me.txtExportLink1)
        Me.TabLinks.Controls.Add(Me.txtLink5)
        Me.TabLinks.Controls.Add(Me.txtLink4)
        Me.TabLinks.Controls.Add(Me.txtLink3)
        Me.TabLinks.Controls.Add(Me.txtLink2)
        Me.TabLinks.Controls.Add(Me.txtLink1)
        Me.TabLinks.Controls.Add(Me.txtProfileLink)
        Me.TabLinks.Controls.Add(Me.lblMainTitle)
        Me.TabLinks.Controls.Add(Me.lblSedol)
        Me.TabLinks.Controls.Add(Me.lblCusip)
        Me.TabLinks.Controls.Add(Me.Label57)
        Me.TabLinks.Controls.Add(Me.Label58)
        Me.TabLinks.Controls.Add(Me.Label59)
        Me.TabLinks.Controls.Add(Me.Label53)
        Me.TabLinks.Controls.Add(Me.cboBBGPriceSource)
        Me.TabLinks.Controls.Add(Me.Label51)
        Me.TabLinks.Controls.Add(Me.cboPriceQuoteType)
        Me.TabLinks.Controls.Add(Me.Label52)
        Me.TabLinks.Controls.Add(Me.cboBBGType)
        Me.TabLinks.Controls.Add(Me.Label48)
        Me.TabLinks.Controls.Add(Me.Label49)
        Me.TabLinks.Controls.Add(Me.Label50)
        Me.TabLinks.Controls.Add(Me.Label45)
        Me.TabLinks.Controls.Add(Me.Label46)
        Me.TabLinks.Controls.Add(Me.Label47)
        Me.TabLinks.Location = New System.Drawing.Point(4, 22)
        Me.TabLinks.Margin = New System.Windows.Forms.Padding(2)
        Me.TabLinks.Name = "TabLinks"
        Me.TabLinks.Padding = New System.Windows.Forms.Padding(2)
        Me.TabLinks.Size = New System.Drawing.Size(1290, 422)
        Me.TabLinks.TabIndex = 1
        Me.TabLinks.Text = "Links"
        '
        'cboExternalFeedModel
        '
        Me.cboExternalFeedModel.FormattingEnabled = True
        Me.cboExternalFeedModel.Location = New System.Drawing.Point(423, 94)
        Me.cboExternalFeedModel.Name = "cboExternalFeedModel"
        Me.cboExternalFeedModel.Size = New System.Drawing.Size(423, 21)
        Me.cboExternalFeedModel.TabIndex = 142
        '
        'lblTickSize
        '
        Me.lblTickSize.AutoSize = True
        Me.lblTickSize.Location = New System.Drawing.Point(363, 261)
        Me.lblTickSize.Name = "lblTickSize"
        Me.lblTickSize.Size = New System.Drawing.Size(54, 13)
        Me.lblTickSize.TabIndex = 141
        Me.lblTickSize.Text = "Tick Size:"
        Me.lblTickSize.Visible = False
        '
        'txtTickSize
        '
        Me.txtTickSize.Location = New System.Drawing.Point(423, 258)
        Me.txtTickSize.Name = "txtTickSize"
        Me.txtTickSize.Size = New System.Drawing.Size(148, 20)
        Me.txtTickSize.TabIndex = 140
        Me.txtTickSize.Visible = False
        '
        'chkBogException
        '
        Me.chkBogException.AutoSize = True
        Me.chkBogException.Location = New System.Drawing.Point(423, 284)
        Me.chkBogException.Name = "chkBogException"
        Me.chkBogException.Size = New System.Drawing.Size(122, 17)
        Me.chkBogException.TabIndex = 139
        Me.chkBogException.Text = "Exception from BOG"
        Me.chkBogException.UseVisualStyleBackColor = True
        '
        'txtMainTitle
        '
        Me.txtMainTitle.Location = New System.Drawing.Point(423, 172)
        Me.txtMainTitle.Name = "txtMainTitle"
        Me.txtMainTitle.Size = New System.Drawing.Size(148, 20)
        Me.txtMainTitle.TabIndex = 138
        '
        'txtSedol
        '
        Me.txtSedol.Location = New System.Drawing.Point(423, 146)
        Me.txtSedol.Name = "txtSedol"
        Me.txtSedol.Size = New System.Drawing.Size(148, 20)
        Me.txtSedol.TabIndex = 136
        '
        'txtCusip
        '
        Me.txtCusip.Location = New System.Drawing.Point(423, 120)
        Me.txtCusip.Name = "txtCusip"
        Me.txtCusip.Size = New System.Drawing.Size(148, 20)
        Me.txtCusip.TabIndex = 134
        '
        'txtExportLink2
        '
        Me.txtExportLink2.Location = New System.Drawing.Point(423, 68)
        Me.txtExportLink2.Name = "txtExportLink2"
        Me.txtExportLink2.Size = New System.Drawing.Size(148, 20)
        Me.txtExportLink2.TabIndex = 130
        '
        'txtExportLink1
        '
        Me.txtExportLink1.Location = New System.Drawing.Point(423, 42)
        Me.txtExportLink1.Name = "txtExportLink1"
        Me.txtExportLink1.Size = New System.Drawing.Size(148, 20)
        Me.txtExportLink1.TabIndex = 128
        '
        'txtLink5
        '
        Me.txtLink5.Location = New System.Drawing.Point(118, 169)
        Me.txtLink5.Name = "txtLink5"
        Me.txtLink5.Size = New System.Drawing.Size(148, 20)
        Me.txtLink5.TabIndex = 112
        '
        'txtLink4
        '
        Me.txtLink4.Location = New System.Drawing.Point(118, 143)
        Me.txtLink4.Name = "txtLink4"
        Me.txtLink4.Size = New System.Drawing.Size(148, 20)
        Me.txtLink4.TabIndex = 110
        '
        'txtLink3
        '
        Me.txtLink3.Location = New System.Drawing.Point(118, 117)
        Me.txtLink3.Name = "txtLink3"
        Me.txtLink3.Size = New System.Drawing.Size(148, 20)
        Me.txtLink3.TabIndex = 108
        '
        'txtLink2
        '
        Me.txtLink2.Location = New System.Drawing.Point(118, 91)
        Me.txtLink2.Name = "txtLink2"
        Me.txtLink2.Size = New System.Drawing.Size(148, 20)
        Me.txtLink2.TabIndex = 106
        '
        'txtLink1
        '
        Me.txtLink1.Location = New System.Drawing.Point(118, 65)
        Me.txtLink1.Name = "txtLink1"
        Me.txtLink1.Size = New System.Drawing.Size(148, 20)
        Me.txtLink1.TabIndex = 104
        '
        'txtProfileLink
        '
        Me.txtProfileLink.Location = New System.Drawing.Point(118, 39)
        Me.txtProfileLink.Name = "txtProfileLink"
        Me.txtProfileLink.Size = New System.Drawing.Size(148, 20)
        Me.txtProfileLink.TabIndex = 102
        '
        'lblMainTitle
        '
        Me.lblMainTitle.AutoSize = True
        Me.lblMainTitle.Location = New System.Drawing.Point(318, 176)
        Me.lblMainTitle.Name = "lblMainTitle"
        Me.lblMainTitle.Size = New System.Drawing.Size(99, 13)
        Me.lblMainTitle.TabIndex = 137
        Me.lblMainTitle.Text = "Underlying product:"
        '
        'lblSedol
        '
        Me.lblSedol.AutoSize = True
        Me.lblSedol.Location = New System.Drawing.Point(374, 149)
        Me.lblSedol.Name = "lblSedol"
        Me.lblSedol.Size = New System.Drawing.Size(46, 13)
        Me.lblSedol.TabIndex = 135
        Me.lblSedol.Text = "SEDOL:"
        '
        'lblCusip
        '
        Me.lblCusip.AutoSize = True
        Me.lblCusip.Location = New System.Drawing.Point(378, 123)
        Me.lblCusip.Name = "lblCusip"
        Me.lblCusip.Size = New System.Drawing.Size(42, 13)
        Me.lblCusip.TabIndex = 133
        Me.lblCusip.Text = "CUSIP:"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(310, 97)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(107, 13)
        Me.Label57.TabIndex = 131
        Me.Label57.Text = "External Feed Model:"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(345, 71)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(72, 13)
        Me.Label58.TabIndex = 129
        Me.Label58.Text = "Export Link 2:"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(345, 45)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(72, 13)
        Me.Label59.TabIndex = 127
        Me.Label59.Text = "Export Link 1:"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(293, 234)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(124, 13)
        Me.Label53.TabIndex = 126
        Me.Label53.Text = "Bloomberg Price Source:"
        '
        'cboBBGPriceSource
        '
        Me.cboBBGPriceSource.FormattingEnabled = True
        Me.cboBBGPriceSource.Location = New System.Drawing.Point(423, 231)
        Me.cboBBGPriceSource.Name = "cboBBGPriceSource"
        Me.cboBBGPriceSource.Size = New System.Drawing.Size(148, 21)
        Me.cboBBGPriceSource.TabIndex = 125
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(19, 256)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(93, 13)
        Me.Label51.TabIndex = 124
        Me.Label51.Text = "Price Quote Type:"
        '
        'cboPriceQuoteType
        '
        Me.cboPriceQuoteType.FormattingEnabled = True
        Me.cboPriceQuoteType.Location = New System.Drawing.Point(118, 253)
        Me.cboPriceQuoteType.Name = "cboPriceQuoteType"
        Me.cboPriceQuoteType.Size = New System.Drawing.Size(148, 21)
        Me.cboPriceQuoteType.TabIndex = 123
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(25, 229)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(87, 13)
        Me.Label52.TabIndex = 122
        Me.Label52.Text = "Bloomberg Type:"
        '
        'cboBBGType
        '
        Me.cboBBGType.FormattingEnabled = True
        Me.cboBBGType.Location = New System.Drawing.Point(118, 226)
        Me.cboBBGType.Name = "cboBBGType"
        Me.cboBBGType.Size = New System.Drawing.Size(148, 21)
        Me.cboBBGType.TabIndex = 121
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(73, 172)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(39, 13)
        Me.Label48.TabIndex = 111
        Me.Label48.Text = "Link 5:"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(73, 146)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(39, 13)
        Me.Label49.TabIndex = 109
        Me.Label49.Text = "Link 4:"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(73, 120)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(39, 13)
        Me.Label50.TabIndex = 107
        Me.Label50.Text = "Link 3:"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(73, 94)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(39, 13)
        Me.Label45.TabIndex = 105
        Me.Label45.Text = "Link 2:"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(73, 68)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(39, 13)
        Me.Label46.TabIndex = 103
        Me.Label46.Text = "Link 1:"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(53, 42)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(62, 13)
        Me.Label47.TabIndex = 101
        Me.Label47.Text = "Profile Link:"
        '
        'TabMain
        '
        Me.TabMain.BackColor = System.Drawing.Color.OldLace
        Me.TabMain.Controls.Add(Me.lblCommDerivInd)
        Me.TabMain.Controls.Add(Me.cboCommDerivInd)
        Me.TabMain.Controls.Add(Me.lblDeliveryType)
        Me.TabMain.Controls.Add(Me.cboDeliveryType)
        Me.TabMain.Controls.Add(Me.cboStyle)
        Me.TabMain.Controls.Add(Me.cboOptionType)
        Me.TabMain.Controls.Add(Me.cboSetDays)
        Me.TabMain.Controls.Add(Me.txtEndOfInvPeriod)
        Me.TabMain.Controls.Add(Me.txtStartofInvPeriod)
        Me.TabMain.Controls.Add(Me.chkMidCost)
        Me.TabMain.Controls.Add(Me.cboEquityType)
        Me.TabMain.Controls.Add(Me.gbxtraInfo)
        Me.TabMain.Controls.Add(Me.ChkReCapitalised)
        Me.TabMain.Controls.Add(Me.ChkListed)
        Me.TabMain.Controls.Add(Me.txtFreefield4)
        Me.TabMain.Controls.Add(Me.txtPeriod)
        Me.TabMain.Controls.Add(Me.txtMultiplier)
        Me.TabMain.Controls.Add(Me.txtCalcDate)
        Me.TabMain.Controls.Add(Me.txtBeta)
        Me.TabMain.Controls.Add(Me.txtLimitAdjFactor)
        Me.TabMain.Controls.Add(Me.txtFreefield3)
        Me.TabMain.Controls.Add(Me.txtFreefield2)
        Me.TabMain.Controls.Add(Me.txtFreefield1)
        Me.TabMain.Controls.Add(Me.txtIssuedFV)
        Me.TabMain.Controls.Add(Me.txtOutstandingFV)
        Me.TabMain.Controls.Add(Me.txtPremium)
        Me.TabMain.Controls.Add(Me.txtStockExchangeCode)
        Me.TabMain.Controls.Add(Me.txtCurrentCoupon)
        Me.TabMain.Controls.Add(Me.txtInterest)
        Me.TabMain.Controls.Add(Me.txtEntranceValue)
        Me.TabMain.Controls.Add(Me.txtMaturityDate)
        Me.TabMain.Controls.Add(Me.txtInitDate)
        Me.TabMain.Controls.Add(Me.txtIssueDate)
        Me.TabMain.Controls.Add(Me.lblMultiplier)
        Me.TabMain.Controls.Add(Me.Label40)
        Me.TabMain.Controls.Add(Me.cboInstrumentCategory)
        Me.TabMain.Controls.Add(Me.Label37)
        Me.TabMain.Controls.Add(Me.Label38)
        Me.TabMain.Controls.Add(Me.Label39)
        Me.TabMain.Controls.Add(Me.Label36)
        Me.TabMain.Controls.Add(Me.Label33)
        Me.TabMain.Controls.Add(Me.Label34)
        Me.TabMain.Controls.Add(Me.Label35)
        Me.TabMain.Controls.Add(Me.lblTitleType)
        Me.TabMain.Controls.Add(Me.cboTitleType)
        Me.TabMain.Controls.Add(Me.lblCouponCcy)
        Me.TabMain.Controls.Add(Me.cboCouponCCY)
        Me.TabMain.Controls.Add(Me.lblTaxPolicy)
        Me.TabMain.Controls.Add(Me.cboTaxPolicy)
        Me.TabMain.Controls.Add(Me.lblPortfolioLink)
        Me.TabMain.Controls.Add(Me.cboPortfolioLink)
        Me.TabMain.Controls.Add(Me.lblPeriod)
        Me.TabMain.Controls.Add(Me.cboPeriod)
        Me.TabMain.Controls.Add(Me.lblIssuedFV)
        Me.TabMain.Controls.Add(Me.lblOutstandingFV)
        Me.TabMain.Controls.Add(Me.lblPremium)
        Me.TabMain.Controls.Add(Me.Label24)
        Me.TabMain.Controls.Add(Me.lblCurrentCoupon)
        Me.TabMain.Controls.Add(Me.lblInterest)
        Me.TabMain.Controls.Add(Me.lblEntranceValue)
        Me.TabMain.Controls.Add(Me.lblMaturityDate)
        Me.TabMain.Controls.Add(Me.Label19)
        Me.TabMain.Controls.Add(Me.lblIssueDate)
        Me.TabMain.Location = New System.Drawing.Point(4, 22)
        Me.TabMain.Margin = New System.Windows.Forms.Padding(2)
        Me.TabMain.Name = "TabMain"
        Me.TabMain.Padding = New System.Windows.Forms.Padding(2)
        Me.TabMain.Size = New System.Drawing.Size(1290, 422)
        Me.TabMain.TabIndex = 0
        Me.TabMain.Text = "Main Info"
        '
        'lblCommDerivInd
        '
        Me.lblCommDerivInd.AutoSize = True
        Me.lblCommDerivInd.Location = New System.Drawing.Point(356, 355)
        Me.lblCommDerivInd.Name = "lblCommDerivInd"
        Me.lblCommDerivInd.Size = New System.Drawing.Size(85, 13)
        Me.lblCommDerivInd.TabIndex = 172
        Me.lblCommDerivInd.Text = "Comm Deriv Ind:"
        Me.lblCommDerivInd.Visible = False
        '
        'cboCommDerivInd
        '
        Me.cboCommDerivInd.FormattingEnabled = True
        Me.cboCommDerivInd.Location = New System.Drawing.Point(446, 352)
        Me.cboCommDerivInd.Name = "cboCommDerivInd"
        Me.cboCommDerivInd.Size = New System.Drawing.Size(148, 21)
        Me.cboCommDerivInd.TabIndex = 171
        Me.cboCommDerivInd.Visible = False
        '
        'lblDeliveryType
        '
        Me.lblDeliveryType.AutoSize = True
        Me.lblDeliveryType.Location = New System.Drawing.Point(365, 275)
        Me.lblDeliveryType.Name = "lblDeliveryType"
        Me.lblDeliveryType.Size = New System.Drawing.Size(75, 13)
        Me.lblDeliveryType.TabIndex = 170
        Me.lblDeliveryType.Text = "Delivery Type:"
        Me.lblDeliveryType.Visible = False
        '
        'cboDeliveryType
        '
        Me.cboDeliveryType.FormattingEnabled = True
        Me.cboDeliveryType.Location = New System.Drawing.Point(446, 272)
        Me.cboDeliveryType.Name = "cboDeliveryType"
        Me.cboDeliveryType.Size = New System.Drawing.Size(148, 21)
        Me.cboDeliveryType.TabIndex = 169
        Me.cboDeliveryType.Visible = False
        '
        'cboStyle
        '
        Me.cboStyle.FormattingEnabled = True
        Me.cboStyle.Location = New System.Drawing.Point(446, 325)
        Me.cboStyle.Name = "cboStyle"
        Me.cboStyle.Size = New System.Drawing.Size(148, 21)
        Me.cboStyle.TabIndex = 168
        Me.cboStyle.Visible = False
        '
        'cboOptionType
        '
        Me.cboOptionType.Enabled = False
        Me.cboOptionType.FormattingEnabled = True
        Me.cboOptionType.Location = New System.Drawing.Point(401, 299)
        Me.cboOptionType.Name = "cboOptionType"
        Me.cboOptionType.Size = New System.Drawing.Size(193, 21)
        Me.cboOptionType.TabIndex = 167
        Me.cboOptionType.Visible = False
        '
        'cboSetDays
        '
        Me.cboSetDays.FormattingEnabled = True
        Me.cboSetDays.Location = New System.Drawing.Point(105, 378)
        Me.cboSetDays.Name = "cboSetDays"
        Me.cboSetDays.Size = New System.Drawing.Size(193, 21)
        Me.cboSetDays.TabIndex = 166
        Me.cboSetDays.Visible = False
        '
        'txtEndOfInvPeriod
        '
        Me.txtEndOfInvPeriod.Location = New System.Drawing.Point(105, 352)
        Me.txtEndOfInvPeriod.Name = "txtEndOfInvPeriod"
        Me.txtEndOfInvPeriod.Size = New System.Drawing.Size(193, 20)
        Me.txtEndOfInvPeriod.TabIndex = 165
        Me.txtEndOfInvPeriod.Visible = False
        '
        'txtStartofInvPeriod
        '
        Me.txtStartofInvPeriod.Location = New System.Drawing.Point(105, 326)
        Me.txtStartofInvPeriod.Name = "txtStartofInvPeriod"
        Me.txtStartofInvPeriod.Size = New System.Drawing.Size(193, 20)
        Me.txtStartofInvPeriod.TabIndex = 164
        Me.txtStartofInvPeriod.Visible = False
        '
        'chkMidCost
        '
        Me.chkMidCost.AutoSize = True
        Me.chkMidCost.Location = New System.Drawing.Point(446, 249)
        Me.chkMidCost.Name = "chkMidCost"
        Me.chkMidCost.Size = New System.Drawing.Size(67, 17)
        Me.chkMidCost.TabIndex = 163
        Me.chkMidCost.Text = "Mid Cost"
        Me.chkMidCost.UseVisualStyleBackColor = True
        Me.chkMidCost.Visible = False
        '
        'cboEquityType
        '
        Me.cboEquityType.FormattingEnabled = True
        Me.cboEquityType.Location = New System.Drawing.Point(105, 299)
        Me.cboEquityType.Name = "cboEquityType"
        Me.cboEquityType.Size = New System.Drawing.Size(193, 21)
        Me.cboEquityType.TabIndex = 162
        Me.cboEquityType.Visible = False
        '
        'gbxtraInfo
        '
        Me.gbxtraInfo.Controls.Add(Me.blLowPrice)
        Me.gbxtraInfo.Controls.Add(Me.txtLowPrice)
        Me.gbxtraInfo.Controls.Add(Me.lblPriceHigh)
        Me.gbxtraInfo.Controls.Add(Me.txtPriceHigh)
        Me.gbxtraInfo.Controls.Add(Me.lblPriceClose)
        Me.gbxtraInfo.Controls.Add(Me.txtPriceClose)
        Me.gbxtraInfo.Controls.Add(Me.lblDerivitiveDelType)
        Me.gbxtraInfo.Controls.Add(Me.txtDerivitiveDelType)
        Me.gbxtraInfo.Controls.Add(Me.lblMFIDComplexInstrInd)
        Me.gbxtraInfo.Controls.Add(Me.txtMFIDComplexInstrInd)
        Me.gbxtraInfo.Controls.Add(Me.lblMFIDIndicator)
        Me.gbxtraInfo.Controls.Add(Me.txtMFIDIndicator)
        Me.gbxtraInfo.Controls.Add(Me.lblCompToParentRel)
        Me.gbxtraInfo.Controls.Add(Me.txtCompToParentRel)
        Me.gbxtraInfo.Controls.Add(Me.lblDayCount)
        Me.gbxtraInfo.Controls.Add(Me.txtDayCount)
        Me.gbxtraInfo.Controls.Add(Me.lblBBG_ID)
        Me.gbxtraInfo.Controls.Add(Me.txtBBG_ID)
        Me.gbxtraInfo.Location = New System.Drawing.Point(924, 23)
        Me.gbxtraInfo.Name = "gbxtraInfo"
        Me.gbxtraInfo.Size = New System.Drawing.Size(348, 283)
        Me.gbxtraInfo.TabIndex = 161
        Me.gbxtraInfo.TabStop = False
        Me.gbxtraInfo.Text = "Extra Info"
        '
        'blLowPrice
        '
        Me.blLowPrice.AutoSize = True
        Me.blLowPrice.Location = New System.Drawing.Point(124, 237)
        Me.blLowPrice.Name = "blLowPrice"
        Me.blLowPrice.Size = New System.Drawing.Size(57, 13)
        Me.blLowPrice.TabIndex = 166
        Me.blLowPrice.Text = "Price Low:"
        '
        'txtLowPrice
        '
        Me.txtLowPrice.Location = New System.Drawing.Point(187, 233)
        Me.txtLowPrice.Name = "txtLowPrice"
        Me.txtLowPrice.ReadOnly = True
        Me.txtLowPrice.Size = New System.Drawing.Size(148, 20)
        Me.txtLowPrice.TabIndex = 165
        '
        'lblPriceHigh
        '
        Me.lblPriceHigh.AutoSize = True
        Me.lblPriceHigh.Location = New System.Drawing.Point(124, 210)
        Me.lblPriceHigh.Name = "lblPriceHigh"
        Me.lblPriceHigh.Size = New System.Drawing.Size(59, 13)
        Me.lblPriceHigh.TabIndex = 164
        Me.lblPriceHigh.Text = "Price High:"
        '
        'txtPriceHigh
        '
        Me.txtPriceHigh.Location = New System.Drawing.Point(187, 207)
        Me.txtPriceHigh.Name = "txtPriceHigh"
        Me.txtPriceHigh.ReadOnly = True
        Me.txtPriceHigh.Size = New System.Drawing.Size(148, 20)
        Me.txtPriceHigh.TabIndex = 163
        '
        'lblPriceClose
        '
        Me.lblPriceClose.AutoSize = True
        Me.lblPriceClose.Location = New System.Drawing.Point(118, 184)
        Me.lblPriceClose.Name = "lblPriceClose"
        Me.lblPriceClose.Size = New System.Drawing.Size(63, 13)
        Me.lblPriceClose.TabIndex = 162
        Me.lblPriceClose.Text = "Price Close:"
        '
        'txtPriceClose
        '
        Me.txtPriceClose.Location = New System.Drawing.Point(187, 181)
        Me.txtPriceClose.Name = "txtPriceClose"
        Me.txtPriceClose.ReadOnly = True
        Me.txtPriceClose.Size = New System.Drawing.Size(148, 20)
        Me.txtPriceClose.TabIndex = 161
        '
        'lblDerivitiveDelType
        '
        Me.lblDerivitiveDelType.AutoSize = True
        Me.lblDerivitiveDelType.Location = New System.Drawing.Point(54, 158)
        Me.lblDerivitiveDelType.Name = "lblDerivitiveDelType"
        Me.lblDerivitiveDelType.Size = New System.Drawing.Size(126, 13)
        Me.lblDerivitiveDelType.TabIndex = 160
        Me.lblDerivitiveDelType.Text = "Derivative Delivery Type:"
        Me.lblDerivitiveDelType.Visible = False
        '
        'txtDerivitiveDelType
        '
        Me.txtDerivitiveDelType.Location = New System.Drawing.Point(187, 155)
        Me.txtDerivitiveDelType.Name = "txtDerivitiveDelType"
        Me.txtDerivitiveDelType.ReadOnly = True
        Me.txtDerivitiveDelType.Size = New System.Drawing.Size(148, 20)
        Me.txtDerivitiveDelType.TabIndex = 159
        Me.txtDerivitiveDelType.WordWrap = False
        '
        'lblMFIDComplexInstrInd
        '
        Me.lblMFIDComplexInstrInd.AutoSize = True
        Me.lblMFIDComplexInstrInd.Location = New System.Drawing.Point(6, 129)
        Me.lblMFIDComplexInstrInd.Name = "lblMFIDComplexInstrInd"
        Me.lblMFIDComplexInstrInd.Size = New System.Drawing.Size(175, 13)
        Me.lblMFIDComplexInstrInd.TabIndex = 158
        Me.lblMFIDComplexInstrInd.Text = "MFID Complex Instrument Indicator:"
        '
        'txtMFIDComplexInstrInd
        '
        Me.txtMFIDComplexInstrInd.Location = New System.Drawing.Point(187, 126)
        Me.txtMFIDComplexInstrInd.Name = "txtMFIDComplexInstrInd"
        Me.txtMFIDComplexInstrInd.ReadOnly = True
        Me.txtMFIDComplexInstrInd.Size = New System.Drawing.Size(148, 20)
        Me.txtMFIDComplexInstrInd.TabIndex = 157
        '
        'lblMFIDIndicator
        '
        Me.lblMFIDIndicator.AutoSize = True
        Me.lblMFIDIndicator.Location = New System.Drawing.Point(65, 103)
        Me.lblMFIDIndicator.Name = "lblMFIDIndicator"
        Me.lblMFIDIndicator.Size = New System.Drawing.Size(115, 13)
        Me.lblMFIDIndicator.TabIndex = 156
        Me.lblMFIDIndicator.Text = "MFID FIRDS Indicator:"
        '
        'txtMFIDIndicator
        '
        Me.txtMFIDIndicator.Location = New System.Drawing.Point(187, 100)
        Me.txtMFIDIndicator.Name = "txtMFIDIndicator"
        Me.txtMFIDIndicator.ReadOnly = True
        Me.txtMFIDIndicator.Size = New System.Drawing.Size(148, 20)
        Me.txtMFIDIndicator.TabIndex = 155
        '
        'lblCompToParentRel
        '
        Me.lblCompToParentRel.AutoSize = True
        Me.lblCompToParentRel.Location = New System.Drawing.Point(19, 75)
        Me.lblCompToParentRel.Name = "lblCompToParentRel"
        Me.lblCompToParentRel.Size = New System.Drawing.Size(161, 13)
        Me.lblCompToParentRel.TabIndex = 154
        Me.lblCompToParentRel.Text = "Company to Parent Relationship:"
        '
        'txtCompToParentRel
        '
        Me.txtCompToParentRel.Location = New System.Drawing.Point(187, 72)
        Me.txtCompToParentRel.Name = "txtCompToParentRel"
        Me.txtCompToParentRel.ReadOnly = True
        Me.txtCompToParentRel.Size = New System.Drawing.Size(148, 20)
        Me.txtCompToParentRel.TabIndex = 153
        '
        'lblDayCount
        '
        Me.lblDayCount.AutoSize = True
        Me.lblDayCount.Location = New System.Drawing.Point(120, 46)
        Me.lblDayCount.Name = "lblDayCount"
        Me.lblDayCount.Size = New System.Drawing.Size(60, 13)
        Me.lblDayCount.TabIndex = 152
        Me.lblDayCount.Text = "Day Count:"
        '
        'txtDayCount
        '
        Me.txtDayCount.Location = New System.Drawing.Point(187, 43)
        Me.txtDayCount.Name = "txtDayCount"
        Me.txtDayCount.ReadOnly = True
        Me.txtDayCount.Size = New System.Drawing.Size(148, 20)
        Me.txtDayCount.TabIndex = 151
        '
        'lblBBG_ID
        '
        Me.lblBBG_ID.AutoSize = True
        Me.lblBBG_ID.Location = New System.Drawing.Point(107, 20)
        Me.lblBBG_ID.Name = "lblBBG_ID"
        Me.lblBBG_ID.Size = New System.Drawing.Size(74, 13)
        Me.lblBBG_ID.TabIndex = 150
        Me.lblBBG_ID.Text = "Bloomberg ID:"
        '
        'txtBBG_ID
        '
        Me.txtBBG_ID.Location = New System.Drawing.Point(187, 17)
        Me.txtBBG_ID.Name = "txtBBG_ID"
        Me.txtBBG_ID.ReadOnly = True
        Me.txtBBG_ID.Size = New System.Drawing.Size(148, 20)
        Me.txtBBG_ID.TabIndex = 149
        '
        'ChkReCapitalised
        '
        Me.ChkReCapitalised.AutoSize = True
        Me.ChkReCapitalised.Location = New System.Drawing.Point(764, 174)
        Me.ChkReCapitalised.Name = "ChkReCapitalised"
        Me.ChkReCapitalised.Size = New System.Drawing.Size(90, 17)
        Me.ChkReCapitalised.TabIndex = 148
        Me.ChkReCapitalised.Text = "Recapitalised"
        Me.ChkReCapitalised.UseVisualStyleBackColor = True
        '
        'ChkListed
        '
        Me.ChkListed.AutoSize = True
        Me.ChkListed.Location = New System.Drawing.Point(764, 151)
        Me.ChkListed.Name = "ChkListed"
        Me.ChkListed.Size = New System.Drawing.Size(154, 17)
        Me.ChkListed.TabIndex = 147
        Me.ChkListed.Text = "Listed - In Stock Exchange"
        Me.ChkListed.UseVisualStyleBackColor = True
        '
        'txtFreefield4
        '
        Me.txtFreefield4.Location = New System.Drawing.Point(764, 123)
        Me.txtFreefield4.Name = "txtFreefield4"
        Me.txtFreefield4.Size = New System.Drawing.Size(148, 20)
        Me.txtFreefield4.TabIndex = 146
        '
        'txtPeriod
        '
        Me.txtPeriod.Location = New System.Drawing.Point(251, 118)
        Me.txtPeriod.Name = "txtPeriod"
        Me.txtPeriod.Size = New System.Drawing.Size(47, 20)
        Me.txtPeriod.TabIndex = 145
        Me.txtPeriod.Visible = False
        '
        'txtMultiplier
        '
        Me.txtMultiplier.Location = New System.Drawing.Point(446, 69)
        Me.txtMultiplier.Name = "txtMultiplier"
        Me.txtMultiplier.Size = New System.Drawing.Size(148, 20)
        Me.txtMultiplier.TabIndex = 144
        '
        'txtCalcDate
        '
        Me.txtCalcDate.Location = New System.Drawing.Point(764, 286)
        Me.txtCalcDate.Name = "txtCalcDate"
        Me.txtCalcDate.Size = New System.Drawing.Size(148, 20)
        Me.txtCalcDate.TabIndex = 140
        '
        'txtBeta
        '
        Me.txtBeta.Location = New System.Drawing.Point(764, 257)
        Me.txtBeta.Name = "txtBeta"
        Me.txtBeta.Size = New System.Drawing.Size(148, 20)
        Me.txtBeta.TabIndex = 138
        '
        'txtLimitAdjFactor
        '
        Me.txtLimitAdjFactor.Location = New System.Drawing.Point(764, 228)
        Me.txtLimitAdjFactor.Name = "txtLimitAdjFactor"
        Me.txtLimitAdjFactor.Size = New System.Drawing.Size(148, 20)
        Me.txtLimitAdjFactor.TabIndex = 136
        '
        'txtFreefield3
        '
        Me.txtFreefield3.Location = New System.Drawing.Point(764, 95)
        Me.txtFreefield3.Name = "txtFreefield3"
        Me.txtFreefield3.Size = New System.Drawing.Size(148, 20)
        Me.txtFreefield3.TabIndex = 130
        '
        'txtFreefield2
        '
        Me.txtFreefield2.Location = New System.Drawing.Point(764, 66)
        Me.txtFreefield2.Name = "txtFreefield2"
        Me.txtFreefield2.Size = New System.Drawing.Size(148, 20)
        Me.txtFreefield2.TabIndex = 128
        '
        'txtFreefield1
        '
        Me.txtFreefield1.Location = New System.Drawing.Point(764, 40)
        Me.txtFreefield1.Name = "txtFreefield1"
        Me.txtFreefield1.Size = New System.Drawing.Size(148, 20)
        Me.txtFreefield1.TabIndex = 126
        '
        'txtIssuedFV
        '
        Me.txtIssuedFV.Location = New System.Drawing.Point(446, 216)
        Me.txtIssuedFV.Name = "txtIssuedFV"
        Me.txtIssuedFV.Size = New System.Drawing.Size(148, 20)
        Me.txtIssuedFV.TabIndex = 114
        '
        'txtOutstandingFV
        '
        Me.txtOutstandingFV.Location = New System.Drawing.Point(446, 187)
        Me.txtOutstandingFV.Name = "txtOutstandingFV"
        Me.txtOutstandingFV.Size = New System.Drawing.Size(148, 20)
        Me.txtOutstandingFV.TabIndex = 112
        '
        'txtPremium
        '
        Me.txtPremium.Location = New System.Drawing.Point(446, 130)
        Me.txtPremium.Name = "txtPremium"
        Me.txtPremium.Size = New System.Drawing.Size(148, 20)
        Me.txtPremium.TabIndex = 110
        '
        'txtStockExchangeCode
        '
        Me.txtStockExchangeCode.Location = New System.Drawing.Point(446, 101)
        Me.txtStockExchangeCode.Name = "txtStockExchangeCode"
        Me.txtStockExchangeCode.Size = New System.Drawing.Size(148, 20)
        Me.txtStockExchangeCode.TabIndex = 108
        '
        'txtCurrentCoupon
        '
        Me.txtCurrentCoupon.Location = New System.Drawing.Point(446, 40)
        Me.txtCurrentCoupon.Name = "txtCurrentCoupon"
        Me.txtCurrentCoupon.Size = New System.Drawing.Size(148, 20)
        Me.txtCurrentCoupon.TabIndex = 106
        '
        'txtInterest
        '
        Me.txtInterest.Location = New System.Drawing.Point(105, 219)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.Size = New System.Drawing.Size(193, 20)
        Me.txtInterest.TabIndex = 104
        '
        'txtEntranceValue
        '
        Me.txtEntranceValue.Location = New System.Drawing.Point(105, 149)
        Me.txtEntranceValue.Name = "txtEntranceValue"
        Me.txtEntranceValue.Size = New System.Drawing.Size(193, 20)
        Me.txtEntranceValue.TabIndex = 102
        '
        'txtMaturityDate
        '
        Me.txtMaturityDate.Location = New System.Drawing.Point(105, 92)
        Me.txtMaturityDate.Name = "txtMaturityDate"
        Me.txtMaturityDate.Size = New System.Drawing.Size(193, 20)
        Me.txtMaturityDate.TabIndex = 100
        '
        'txtInitDate
        '
        Me.txtInitDate.Location = New System.Drawing.Point(105, 66)
        Me.txtInitDate.Name = "txtInitDate"
        Me.txtInitDate.Size = New System.Drawing.Size(193, 20)
        Me.txtInitDate.TabIndex = 98
        '
        'txtIssueDate
        '
        Me.txtIssueDate.Location = New System.Drawing.Point(105, 40)
        Me.txtIssueDate.Name = "txtIssueDate"
        Me.txtIssueDate.Size = New System.Drawing.Size(193, 20)
        Me.txtIssueDate.TabIndex = 96
        '
        'lblMultiplier
        '
        Me.lblMultiplier.AutoSize = True
        Me.lblMultiplier.Location = New System.Drawing.Point(389, 72)
        Me.lblMultiplier.Name = "lblMultiplier"
        Me.lblMultiplier.Size = New System.Drawing.Size(51, 13)
        Me.lblMultiplier.TabIndex = 143
        Me.lblMultiplier.Text = "Multiplier:"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(654, 204)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(104, 13)
        Me.Label40.TabIndex = 142
        Me.Label40.Text = "Instrument Category:"
        '
        'cboInstrumentCategory
        '
        Me.cboInstrumentCategory.FormattingEnabled = True
        Me.cboInstrumentCategory.Location = New System.Drawing.Point(764, 201)
        Me.cboInstrumentCategory.Name = "cboInstrumentCategory"
        Me.cboInstrumentCategory.Size = New System.Drawing.Size(148, 21)
        Me.cboInstrumentCategory.TabIndex = 141
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(670, 289)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(88, 13)
        Me.Label37.TabIndex = 139
        Me.Label37.Text = "Calculation Date:"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(726, 260)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(32, 13)
        Me.Label38.TabIndex = 137
        Me.Label38.Text = "Beta:"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(673, 231)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(85, 13)
        Me.Label39.TabIndex = 135
        Me.Label39.Text = "Limit Adj. Factor:"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(619, 126)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(142, 13)
        Me.Label36.TabIndex = 131
        Me.Label36.Text = "FreeField4/BBG Description:"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(635, 98)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(123, 13)
        Me.Label33.TabIndex = 129
        Me.Label33.Text = "FreeField3/BBG Source:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(669, 69)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(89, 13)
        Me.Label34.TabIndex = 127
        Me.Label34.Text = "FreeField2/IBAN:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(637, 43)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(121, 13)
        Me.Label35.TabIndex = 125
        Me.Label35.Text = "FreeField1/Account No:"
        '
        'lblTitleType
        '
        Me.lblTitleType.AutoSize = True
        Me.lblTitleType.Location = New System.Drawing.Point(383, 163)
        Me.lblTitleType.Name = "lblTitleType"
        Me.lblTitleType.Size = New System.Drawing.Size(57, 13)
        Me.lblTitleType.TabIndex = 124
        Me.lblTitleType.Text = "Title Type:"
        '
        'cboTitleType
        '
        Me.cboTitleType.FormattingEnabled = True
        Me.cboTitleType.Location = New System.Drawing.Point(446, 160)
        Me.cboTitleType.Name = "cboTitleType"
        Me.cboTitleType.Size = New System.Drawing.Size(148, 21)
        Me.cboTitleType.TabIndex = 123
        '
        'lblCouponCcy
        '
        Me.lblCouponCcy.AutoSize = True
        Me.lblCouponCcy.Location = New System.Drawing.Point(33, 181)
        Me.lblCouponCcy.Name = "lblCouponCcy"
        Me.lblCouponCcy.Size = New System.Drawing.Size(68, 13)
        Me.lblCouponCcy.TabIndex = 122
        Me.lblCouponCcy.Text = "Option Type:"
        '
        'cboCouponCCY
        '
        Me.cboCouponCCY.FormattingEnabled = True
        Me.cboCouponCCY.Location = New System.Drawing.Point(105, 178)
        Me.cboCouponCCY.Name = "cboCouponCCY"
        Me.cboCouponCCY.Size = New System.Drawing.Size(193, 21)
        Me.cboCouponCCY.TabIndex = 121
        '
        'lblTaxPolicy
        '
        Me.lblTaxPolicy.AutoSize = True
        Me.lblTaxPolicy.Location = New System.Drawing.Point(40, 275)
        Me.lblTaxPolicy.Name = "lblTaxPolicy"
        Me.lblTaxPolicy.Size = New System.Drawing.Size(59, 13)
        Me.lblTaxPolicy.TabIndex = 120
        Me.lblTaxPolicy.Text = "Tax Policy:"
        '
        'cboTaxPolicy
        '
        Me.cboTaxPolicy.FormattingEnabled = True
        Me.cboTaxPolicy.Location = New System.Drawing.Point(105, 272)
        Me.cboTaxPolicy.Name = "cboTaxPolicy"
        Me.cboTaxPolicy.Size = New System.Drawing.Size(193, 21)
        Me.cboTaxPolicy.TabIndex = 119
        '
        'lblPortfolioLink
        '
        Me.lblPortfolioLink.AutoSize = True
        Me.lblPortfolioLink.Location = New System.Drawing.Point(28, 248)
        Me.lblPortfolioLink.Name = "lblPortfolioLink"
        Me.lblPortfolioLink.Size = New System.Drawing.Size(71, 13)
        Me.lblPortfolioLink.TabIndex = 118
        Me.lblPortfolioLink.Text = "Portfolio Link:"
        '
        'cboPortfolioLink
        '
        Me.cboPortfolioLink.FormattingEnabled = True
        Me.cboPortfolioLink.Location = New System.Drawing.Point(105, 245)
        Me.cboPortfolioLink.Name = "cboPortfolioLink"
        Me.cboPortfolioLink.Size = New System.Drawing.Size(193, 21)
        Me.cboPortfolioLink.TabIndex = 117
        '
        'lblPeriod
        '
        Me.lblPeriod.AutoSize = True
        Me.lblPeriod.Location = New System.Drawing.Point(59, 121)
        Me.lblPeriod.Name = "lblPeriod"
        Me.lblPeriod.Size = New System.Drawing.Size(40, 13)
        Me.lblPeriod.TabIndex = 116
        Me.lblPeriod.Text = "Period:"
        '
        'cboPeriod
        '
        Me.cboPeriod.FormattingEnabled = True
        Me.cboPeriod.Location = New System.Drawing.Point(105, 118)
        Me.cboPeriod.Name = "cboPeriod"
        Me.cboPeriod.Size = New System.Drawing.Size(140, 21)
        Me.cboPeriod.TabIndex = 115
        '
        'lblIssuedFV
        '
        Me.lblIssuedFV.AutoSize = True
        Me.lblIssuedFV.Location = New System.Drawing.Point(374, 219)
        Me.lblIssuedFV.Name = "lblIssuedFV"
        Me.lblIssuedFV.Size = New System.Drawing.Size(66, 13)
        Me.lblIssuedFV.TabIndex = 113
        Me.lblIssuedFV.Text = "Issued F.V. :"
        '
        'lblOutstandingFV
        '
        Me.lblOutstandingFV.AutoSize = True
        Me.lblOutstandingFV.Location = New System.Drawing.Point(348, 190)
        Me.lblOutstandingFV.Name = "lblOutstandingFV"
        Me.lblOutstandingFV.Size = New System.Drawing.Size(92, 13)
        Me.lblOutstandingFV.TabIndex = 111
        Me.lblOutstandingFV.Text = "Outstanding F. V.:"
        '
        'lblPremium
        '
        Me.lblPremium.AutoSize = True
        Me.lblPremium.Location = New System.Drawing.Point(390, 133)
        Me.lblPremium.Name = "lblPremium"
        Me.lblPremium.Size = New System.Drawing.Size(50, 13)
        Me.lblPremium.TabIndex = 109
        Me.lblPremium.Text = "Premium:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(323, 104)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(117, 13)
        Me.Label24.TabIndex = 107
        Me.Label24.Text = "Stock Exchange Code:"
        '
        'lblCurrentCoupon
        '
        Me.lblCurrentCoupon.AutoSize = True
        Me.lblCurrentCoupon.Location = New System.Drawing.Point(356, 43)
        Me.lblCurrentCoupon.Name = "lblCurrentCoupon"
        Me.lblCurrentCoupon.Size = New System.Drawing.Size(84, 13)
        Me.lblCurrentCoupon.TabIndex = 105
        Me.lblCurrentCoupon.Text = "Current Coupon:"
        '
        'lblInterest
        '
        Me.lblInterest.AutoSize = True
        Me.lblInterest.Location = New System.Drawing.Point(54, 223)
        Me.lblInterest.Name = "lblInterest"
        Me.lblInterest.Size = New System.Drawing.Size(45, 13)
        Me.lblInterest.TabIndex = 103
        Me.lblInterest.Text = "Interest:"
        '
        'lblEntranceValue
        '
        Me.lblEntranceValue.AutoSize = True
        Me.lblEntranceValue.Location = New System.Drawing.Point(16, 152)
        Me.lblEntranceValue.Name = "lblEntranceValue"
        Me.lblEntranceValue.Size = New System.Drawing.Size(83, 13)
        Me.lblEntranceValue.TabIndex = 101
        Me.lblEntranceValue.Text = "Entrance Value:"
        '
        'lblMaturityDate
        '
        Me.lblMaturityDate.AutoSize = True
        Me.lblMaturityDate.Location = New System.Drawing.Point(26, 95)
        Me.lblMaturityDate.Name = "lblMaturityDate"
        Me.lblMaturityDate.Size = New System.Drawing.Size(73, 13)
        Me.lblMaturityDate.TabIndex = 99
        Me.lblMaturityDate.Text = "Maturity Date:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(49, 69)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(50, 13)
        Me.Label19.TabIndex = 97
        Me.Label19.Text = "Init Date:"
        '
        'lblIssueDate
        '
        Me.lblIssueDate.AutoSize = True
        Me.lblIssueDate.Location = New System.Drawing.Point(40, 43)
        Me.lblIssueDate.Name = "lblIssueDate"
        Me.lblIssueDate.Size = New System.Drawing.Size(61, 13)
        Me.lblIssueDate.TabIndex = 95
        Me.lblIssueDate.Text = "Issue Date:"
        '
        'TabInstrument
        '
        Me.TabInstrument.Controls.Add(Me.TabMain)
        Me.TabInstrument.Controls.Add(Me.TabLinks)
        Me.TabInstrument.Controls.Add(Me.TabFund)
        Me.TabInstrument.Controls.Add(Me.TabSSI)
        Me.TabInstrument.Location = New System.Drawing.Point(16, 341)
        Me.TabInstrument.Margin = New System.Windows.Forms.Padding(2)
        Me.TabInstrument.Name = "TabInstrument"
        Me.TabInstrument.SelectedIndex = 0
        Me.TabInstrument.Size = New System.Drawing.Size(1298, 448)
        Me.TabInstrument.TabIndex = 83
        '
        'TabSSI
        '
        Me.TabSSI.BackColor = System.Drawing.Color.OldLace
        Me.TabSSI.Controls.Add(Me.GroupBox3)
        Me.TabSSI.Controls.Add(Me.Label112)
        Me.TabSSI.Controls.Add(Me.txtCashSettlementBroker)
        Me.TabSSI.Controls.Add(Me.Label111)
        Me.TabSSI.Controls.Add(Me.txtClearingAgentCodePOS)
        Me.TabSSI.Controls.Add(Me.Label110)
        Me.TabSSI.Controls.Add(Me.txtAgentCodePOS)
        Me.TabSSI.Controls.Add(Me.Label105)
        Me.TabSSI.Controls.Add(Me.cboCashClearingAgt)
        Me.TabSSI.Controls.Add(Me.Label104)
        Me.TabSSI.Controls.Add(Me.cboSecClearingAgt2)
        Me.TabSSI.Controls.Add(Me.Label103)
        Me.TabSSI.Controls.Add(Me.cboSecClearingAgt1)
        Me.TabSSI.Controls.Add(Me.Label102)
        Me.TabSSI.Controls.Add(Me.cboCpty)
        Me.TabSSI.Controls.Add(Me.Label101)
        Me.TabSSI.Controls.Add(Me.cboPOS)
        Me.TabSSI.Location = New System.Drawing.Point(4, 22)
        Me.TabSSI.Name = "TabSSI"
        Me.TabSSI.Size = New System.Drawing.Size(1290, 422)
        Me.TabSSI.TabIndex = 3
        Me.TabSSI.Text = "SSI"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboIndustrySector)
        Me.GroupBox3.Controls.Add(Me.Label106)
        Me.GroupBox3.Controls.Add(Me.cboIndustryGroups)
        Me.GroupBox3.Controls.Add(Me.Label107)
        Me.GroupBox3.Controls.Add(Me.cboIndustries)
        Me.GroupBox3.Controls.Add(Me.Label108)
        Me.GroupBox3.Controls.Add(Me.cboIndustrySubs)
        Me.GroupBox3.Controls.Add(Me.Label109)
        Me.GroupBox3.Location = New System.Drawing.Point(535, 44)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(482, 223)
        Me.GroupBox3.TabIndex = 149
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Industries GICS"
        '
        'cboIndustrySector
        '
        Me.cboIndustrySector.FormattingEnabled = True
        Me.cboIndustrySector.Location = New System.Drawing.Point(109, 29)
        Me.cboIndustrySector.Name = "cboIndustrySector"
        Me.cboIndustrySector.Size = New System.Drawing.Size(303, 21)
        Me.cboIndustrySector.TabIndex = 135
        '
        'Label106
        '
        Me.Label106.AutoSize = True
        Me.Label106.Location = New System.Drawing.Point(22, 32)
        Me.Label106.Name = "Label106"
        Me.Label106.Size = New System.Drawing.Size(81, 13)
        Me.Label106.TabIndex = 136
        Me.Label106.Text = "Industry Sector:"
        '
        'cboIndustryGroups
        '
        Me.cboIndustryGroups.FormattingEnabled = True
        Me.cboIndustryGroups.Location = New System.Drawing.Point(109, 56)
        Me.cboIndustryGroups.Name = "cboIndustryGroups"
        Me.cboIndustryGroups.Size = New System.Drawing.Size(303, 21)
        Me.cboIndustryGroups.TabIndex = 137
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Location = New System.Drawing.Point(19, 59)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(84, 13)
        Me.Label107.TabIndex = 138
        Me.Label107.Text = "Industry Groups:"
        '
        'cboIndustries
        '
        Me.cboIndustries.FormattingEnabled = True
        Me.cboIndustries.Location = New System.Drawing.Point(109, 83)
        Me.cboIndustries.Name = "cboIndustries"
        Me.cboIndustries.Size = New System.Drawing.Size(303, 21)
        Me.cboIndustries.TabIndex = 139
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Location = New System.Drawing.Point(48, 86)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(55, 13)
        Me.Label108.TabIndex = 140
        Me.Label108.Text = "Industries:"
        '
        'cboIndustrySubs
        '
        Me.cboIndustrySubs.FormattingEnabled = True
        Me.cboIndustrySubs.Location = New System.Drawing.Point(109, 110)
        Me.cboIndustrySubs.Name = "cboIndustrySubs"
        Me.cboIndustrySubs.Size = New System.Drawing.Size(303, 21)
        Me.cboIndustrySubs.TabIndex = 141
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Location = New System.Drawing.Point(29, 113)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(74, 13)
        Me.Label109.TabIndex = 142
        Me.Label109.Text = "Industry Subs:"
        '
        'Label112
        '
        Me.Label112.AutoSize = True
        Me.Label112.Location = New System.Drawing.Point(47, 250)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(121, 13)
        Me.Label112.TabIndex = 147
        Me.Label112.Text = "Cash Settlement Broker:"
        '
        'txtCashSettlementBroker
        '
        Me.txtCashSettlementBroker.Location = New System.Drawing.Point(174, 247)
        Me.txtCashSettlementBroker.Name = "txtCashSettlementBroker"
        Me.txtCashSettlementBroker.Size = New System.Drawing.Size(303, 20)
        Me.txtCashSettlementBroker.TabIndex = 148
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.Location = New System.Drawing.Point(14, 197)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(154, 13)
        Me.Label111.TabIndex = 145
        Me.Label111.Text = "Clearing Agent's Code on POS:"
        '
        'txtClearingAgentCodePOS
        '
        Me.txtClearingAgentCodePOS.Location = New System.Drawing.Point(174, 194)
        Me.txtClearingAgentCodePOS.Name = "txtClearingAgentCodePOS"
        Me.txtClearingAgentCodePOS.Size = New System.Drawing.Size(303, 20)
        Me.txtClearingAgentCodePOS.TabIndex = 146
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(55, 144)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(113, 13)
        Me.Label110.TabIndex = 143
        Me.Label110.Text = "Agent's Code on POS:"
        '
        'txtAgentCodePOS
        '
        Me.txtAgentCodePOS.Location = New System.Drawing.Point(174, 141)
        Me.txtAgentCodePOS.Name = "txtAgentCodePOS"
        Me.txtAgentCodePOS.Size = New System.Drawing.Size(303, 20)
        Me.txtAgentCodePOS.TabIndex = 144
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Location = New System.Drawing.Point(62, 223)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(106, 13)
        Me.Label105.TabIndex = 134
        Me.Label105.Text = "Cash Clearing Agent:"
        '
        'cboCashClearingAgt
        '
        Me.cboCashClearingAgt.FormattingEnabled = True
        Me.cboCashClearingAgt.Location = New System.Drawing.Point(174, 220)
        Me.cboCashClearingAgt.Name = "cboCashClearingAgt"
        Me.cboCashClearingAgt.Size = New System.Drawing.Size(303, 21)
        Me.cboCashClearingAgt.TabIndex = 133
        '
        'Label104
        '
        Me.Label104.AutoSize = True
        Me.Label104.Location = New System.Drawing.Point(40, 170)
        Me.Label104.Name = "Label104"
        Me.Label104.Size = New System.Drawing.Size(128, 13)
        Me.Label104.TabIndex = 132
        Me.Label104.Text = "Securities Clearing Agent:"
        '
        'cboSecClearingAgt2
        '
        Me.cboSecClearingAgt2.FormattingEnabled = True
        Me.cboSecClearingAgt2.Location = New System.Drawing.Point(174, 167)
        Me.cboSecClearingAgt2.Name = "cboSecClearingAgt2"
        Me.cboSecClearingAgt2.Size = New System.Drawing.Size(303, 21)
        Me.cboSecClearingAgt2.TabIndex = 131
        '
        'Label103
        '
        Me.Label103.AutoSize = True
        Me.Label103.Location = New System.Drawing.Point(40, 117)
        Me.Label103.Name = "Label103"
        Me.Label103.Size = New System.Drawing.Size(128, 13)
        Me.Label103.TabIndex = 130
        Me.Label103.Text = "Securities Clearing Agent:"
        '
        'cboSecClearingAgt1
        '
        Me.cboSecClearingAgt1.FormattingEnabled = True
        Me.cboSecClearingAgt1.Location = New System.Drawing.Point(174, 114)
        Me.cboSecClearingAgt1.Name = "cboSecClearingAgt1"
        Me.cboSecClearingAgt1.Size = New System.Drawing.Size(303, 21)
        Me.cboSecClearingAgt1.TabIndex = 129
        '
        'Label102
        '
        Me.Label102.AutoSize = True
        Me.Label102.Location = New System.Drawing.Point(98, 90)
        Me.Label102.Name = "Label102"
        Me.Label102.Size = New System.Drawing.Size(70, 13)
        Me.Label102.TabIndex = 128
        Me.Label102.Text = "Counterparty:"
        '
        'cboCpty
        '
        Me.cboCpty.FormattingEnabled = True
        Me.cboCpty.Location = New System.Drawing.Point(174, 87)
        Me.cboCpty.Name = "cboCpty"
        Me.cboCpty.Size = New System.Drawing.Size(303, 21)
        Me.cboCpty.TabIndex = 127
        '
        'Label101
        '
        Me.Label101.AutoSize = True
        Me.Label101.Location = New System.Drawing.Point(35, 63)
        Me.Label101.Name = "Label101"
        Me.Label101.Size = New System.Drawing.Size(133, 13)
        Me.Label101.TabIndex = 126
        Me.Label101.Text = "Place of Settlement (POS):"
        '
        'cboPOS
        '
        Me.cboPOS.FormattingEnabled = True
        Me.cboPOS.Location = New System.Drawing.Point(174, 60)
        Me.cboPOS.Name = "cboPOS"
        Me.cboPOS.Size = New System.Drawing.Size(303, 21)
        Me.cboPOS.TabIndex = 125
        '
        'btnInsertInstrument
        '
        Me.btnInsertInstrument.Enabled = False
        Me.btnInsertInstrument.Location = New System.Drawing.Point(1094, 794)
        Me.btnInsertInstrument.Name = "btnInsertInstrument"
        Me.btnInsertInstrument.Size = New System.Drawing.Size(220, 27)
        Me.btnInsertInstrument.TabIndex = 84
        Me.btnInsertInstrument.Text = "Load Instrument to IMS"
        Me.btnInsertInstrument.UseVisualStyleBackColor = True
        '
        'btnClearForm
        '
        Me.btnClearForm.Enabled = False
        Me.btnClearForm.Location = New System.Drawing.Point(902, 794)
        Me.btnClearForm.Name = "btnClearForm"
        Me.btnClearForm.Size = New System.Drawing.Size(165, 27)
        Me.btnClearForm.TabIndex = 85
        Me.btnClearForm.Text = "Clear Form"
        Me.btnClearForm.UseVisualStyleBackColor = True
        '
        'gbxOption
        '
        Me.gbxOption.BackColor = System.Drawing.Color.OldLace
        Me.gbxOption.Controls.Add(Me.Label20)
        Me.gbxOption.Controls.Add(Me.txtUnderlyingShortCut)
        Me.gbxOption.Controls.Add(Me.txtUnderlyingName)
        Me.gbxOption.Controls.Add(Me.Label18)
        Me.gbxOption.Controls.Add(Me.Label11)
        Me.gbxOption.Controls.Add(Me.txtDerivTCode)
        Me.gbxOption.Location = New System.Drawing.Point(421, 40)
        Me.gbxOption.Name = "gbxOption"
        Me.gbxOption.Size = New System.Drawing.Size(787, 47)
        Me.gbxOption.TabIndex = 86
        Me.gbxOption.TabStop = False
        Me.gbxOption.Text = "Underlying Instrument Details"
        Me.gbxOption.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(476, 19)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(50, 13)
        Me.Label20.TabIndex = 84
        Me.Label20.Text = "Shortcut:"
        '
        'txtUnderlyingShortCut
        '
        Me.txtUnderlyingShortCut.Enabled = False
        Me.txtUnderlyingShortCut.Location = New System.Drawing.Point(532, 16)
        Me.txtUnderlyingShortCut.Name = "txtUnderlyingShortCut"
        Me.txtUnderlyingShortCut.Size = New System.Drawing.Size(233, 20)
        Me.txtUnderlyingShortCut.TabIndex = 83
        '
        'txtUnderlyingName
        '
        Me.txtUnderlyingName.Enabled = False
        Me.txtUnderlyingName.Location = New System.Drawing.Point(237, 16)
        Me.txtUnderlyingName.Name = "txtUnderlyingName"
        Me.txtUnderlyingName.Size = New System.Drawing.Size(226, 20)
        Me.txtUnderlyingName.TabIndex = 82
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(193, 19)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(38, 13)
        Me.Label18.TabIndex = 81
        Me.Label18.Text = "Name:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(15, 19)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 13)
        Me.Label11.TabIndex = 80
        Me.Label11.Text = "T Code:"
        '
        'txtDerivTCode
        '
        Me.txtDerivTCode.Enabled = False
        Me.txtDerivTCode.Location = New System.Drawing.Point(66, 16)
        Me.txtDerivTCode.Name = "txtDerivTCode"
        Me.txtDerivTCode.Size = New System.Drawing.Size(111, 20)
        Me.txtDerivTCode.TabIndex = 80
        '
        'ttpMain
        '
        Me.ttpMain.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        '
        'txtExternalFeedModel
        '
        Me.txtExternalFeedModel.Location = New System.Drawing.Point(858, 94)
        Me.txtExternalFeedModel.Name = "txtExternalFeedModel"
        Me.txtExternalFeedModel.Size = New System.Drawing.Size(148, 20)
        Me.txtExternalFeedModel.TabIndex = 132
        Me.txtExternalFeedModel.Visible = False
        '
        'FrmLoadInstruments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1320, 828)
        Me.Controls.Add(Me.gbxOption)
        Me.Controls.Add(Me.btnClearForm)
        Me.Controls.Add(Me.btnInsertInstrument)
        Me.Controls.Add(Me.TabInstrument)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FrmLoadInstruments"
        Me.Text = "Load Instruments"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabFund.ResumeLayout(False)
        Me.TabFundamentals.ResumeLayout(False)
        Me.tbpBond.ResumeLayout(False)
        Me.tbpBond.PerformLayout()
        Me.TabFundamentalsOther.ResumeLayout(False)
        Me.TabFundamentalsOther.PerformLayout()
        Me.TabLinks.ResumeLayout(False)
        Me.TabLinks.PerformLayout()
        Me.TabMain.ResumeLayout(False)
        Me.TabMain.PerformLayout()
        Me.gbxtraInfo.ResumeLayout(False)
        Me.gbxtraInfo.PerformLayout()
        Me.TabInstrument.ResumeLayout(False)
        Me.TabSSI.ResumeLayout(False)
        Me.TabSSI.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.gbxOption.ResumeLayout(False)
        Me.gbxOption.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtTickerSearch As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents CmdLoadInstrument As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents cboType As ComboBox
    Friend WithEvents txtDesc As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cboTradedCCY As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents cboIssuedCtry As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtISIN As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cboMarket As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents CboCCY As ComboBox
    Friend WithEvents lblTradedCcy As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtTicker As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cboSector As ComboBox
    Friend WithEvents Label15 As Label
    Friend WithEvents cboOffMarket As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents cboGroup As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents cboSubType As ComboBox
    Friend WithEvents Label17 As Label
    Friend WithEvents txtComments As TextBox
    Friend WithEvents lblPricingScenarios As Label
    Friend WithEvents cboPricingScenarios As ComboBox
    Friend WithEvents Label43 As Label
    Friend WithEvents txtCFICode As TextBox
    Friend WithEvents Label42 As Label
    Friend WithEvents txttax As TextBox
    Friend WithEvents TabFund As TabPage
    Friend WithEvents TabFundamentals As TabControl
    Friend WithEvents tbpBond As TabPage
    Friend WithEvents txtDiscountFactor As TextBox
    Friend WithEvents Label100 As Label
    Friend WithEvents txtMinCapGauranteed As TextBox
    Friend WithEvents Label99 As Label
    Friend WithEvents cboCalcType As ComboBox
    Friend WithEvents Label83 As Label
    Friend WithEvents cboCouponType As ComboBox
    Friend WithEvents cboInflationLinked As ComboBox
    Friend WithEvents Label61 As Label
    Friend WithEvents Label84 As Label
    Friend WithEvents Label60 As Label
    Friend WithEvents cboRedemptionOptions As ComboBox
    Friend WithEvents cboBondType As ComboBox
    Friend WithEvents Label85 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents cbotier As ComboBox
    Friend WithEvents cboRelatedIndex As ComboBox
    Friend WithEvents Label86 As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents cboTradedBy As ComboBox
    Friend WithEvents Label68 As Label
    Friend WithEvents txtCpnDate As TextBox
    Friend WithEvents txtGuarantor As TextBox
    Friend WithEvents Label82 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents Label81 As Label
    Friend WithEvents txtMinDenomination As TextBox
    Friend WithEvents cboBondYieldType As ComboBox
    Friend WithEvents Label78 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents cboIssueType As ComboBox
    Friend WithEvents txtMinTradingQty As TextBox
    Friend WithEvents Label79 As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents cboIntCalc2 As ComboBox
    Friend WithEvents txtDailyQty As TextBox
    Friend WithEvents Label80 As Label
    Friend WithEvents Label64 As Label
    Friend WithEvents cboCollateralType As ComboBox
    Friend WithEvents txtMaxAllowedQty As TextBox
    Friend WithEvents Label75 As Label
    Friend WithEvents cboMoodys As ComboBox
    Friend WithEvents cboInNewMonth As ComboBox
    Friend WithEvents Label69 As Label
    Friend WithEvents Label76 As Label
    Friend WithEvents cboSP As ComboBox
    Friend WithEvents cboInDayOff As ComboBox
    Friend WithEvents Label70 As Label
    Friend WithEvents Label77 As Label
    Friend WithEvents cboFitch As ComboBox
    Friend WithEvents cboIntCalc1 As ComboBox
    Friend WithEvents Label71 As Label
    Friend WithEvents Label72 As Label
    Friend WithEvents cboCalcTerms As ComboBox
    Friend WithEvents cboLongCoupon As ComboBox
    Friend WithEvents Label74 As Label
    Friend WithEvents Label73 As Label
    Friend WithEvents cboTaxCalc As ComboBox
    Friend WithEvents TabFundamentalsOther As TabPage
    Friend WithEvents txtPriceDecimals As TextBox
    Friend WithEvents Label93 As Label
    Friend WithEvents TextBox11 As TextBox
    Friend WithEvents Label94 As Label
    Friend WithEvents TextBox12 As TextBox
    Friend WithEvents Label95 As Label
    Friend WithEvents TextBox13 As TextBox
    Friend WithEvents Label96 As Label
    Friend WithEvents TextBox14 As TextBox
    Friend WithEvents Label97 As Label
    Friend WithEvents TextBox15 As TextBox
    Friend WithEvents Label98 As Label
    Friend WithEvents txtQtyDecimals As TextBox
    Friend WithEvents Label92 As Label
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents Label91 As Label
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents Label90 As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents Label89 As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Label88 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label87 As Label
    Friend WithEvents TabLinks As TabPage
    Friend WithEvents txtMainTitle As TextBox
    Friend WithEvents txtSedol As TextBox
    Friend WithEvents txtCusip As TextBox
    Friend WithEvents txtExportLink2 As TextBox
    Friend WithEvents txtExportLink1 As TextBox
    Friend WithEvents txtLink5 As TextBox
    Friend WithEvents txtLink4 As TextBox
    Friend WithEvents txtLink3 As TextBox
    Friend WithEvents txtLink2 As TextBox
    Friend WithEvents txtLink1 As TextBox
    Friend WithEvents txtProfileLink As TextBox
    Friend WithEvents lblMainTitle As Label
    Friend WithEvents lblSedol As Label
    Friend WithEvents lblCusip As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents Label53 As Label
    Friend WithEvents cboBBGPriceSource As ComboBox
    Friend WithEvents Label51 As Label
    Friend WithEvents cboPriceQuoteType As ComboBox
    Friend WithEvents Label52 As Label
    Friend WithEvents cboBBGType As ComboBox
    Friend WithEvents Label48 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Label50 As Label
    Friend WithEvents Label45 As Label
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents TabMain As TabPage
    Friend WithEvents txtPeriod As TextBox
    Friend WithEvents txtMultiplier As TextBox
    Friend WithEvents txtCalcDate As TextBox
    Friend WithEvents txtBeta As TextBox
    Friend WithEvents txtLimitAdjFactor As TextBox
    Friend WithEvents txtFreefield3 As TextBox
    Friend WithEvents txtFreefield2 As TextBox
    Friend WithEvents txtFreefield1 As TextBox
    Friend WithEvents txtIssuedFV As TextBox
    Friend WithEvents txtOutstandingFV As TextBox
    Friend WithEvents txtPremium As TextBox
    Friend WithEvents txtStockExchangeCode As TextBox
    Friend WithEvents txtInterest As TextBox
    Friend WithEvents txtEntranceValue As TextBox
    Friend WithEvents txtMaturityDate As TextBox
    Friend WithEvents txtInitDate As TextBox
    Friend WithEvents txtIssueDate As TextBox
    Friend WithEvents lblMultiplier As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents cboInstrumentCategory As ComboBox
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents lblTitleType As Label
    Friend WithEvents cboTitleType As ComboBox
    Friend WithEvents lblCouponCcy As Label
    Friend WithEvents cboCouponCCY As ComboBox
    Friend WithEvents lblTaxPolicy As Label
    Friend WithEvents cboTaxPolicy As ComboBox
    Friend WithEvents lblPortfolioLink As Label
    Friend WithEvents cboPortfolioLink As ComboBox
    Friend WithEvents lblPeriod As Label
    Friend WithEvents cboPeriod As ComboBox
    Friend WithEvents lblIssuedFV As Label
    Friend WithEvents lblOutstandingFV As Label
    Friend WithEvents lblPremium As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents lblCurrentCoupon As Label
    Friend WithEvents lblInterest As Label
    Friend WithEvents lblEntranceValue As Label
    Friend WithEvents lblMaturityDate As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents lblIssueDate As Label
    Friend WithEvents TabInstrument As TabControl
    Friend WithEvents txtFreefield4 As TextBox
    Friend WithEvents ChkESMA As CheckBox
    Friend WithEvents txtCurrentCoupon As TextBox
    Friend WithEvents ChkActive As CheckBox
    Friend WithEvents ChkApproved As CheckBox
    Friend WithEvents ChkReCapitalised As CheckBox
    Friend WithEvents ChkListed As CheckBox
    Friend WithEvents ChkTaxCalc As CheckBox
    Friend WithEvents ChkPerpetual As CheckBox
    Friend WithEvents ChkCapGuaranteed As CheckBox
    Friend WithEvents ChkCpnGuaranteed As CheckBox
    Friend WithEvents ChkDCC As CheckBox
    Friend WithEvents ChkSinkable As CheckBox
    Friend WithEvents ChkConvertible As CheckBox
    Friend WithEvents TabSSI As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents cboIndustrySector As ComboBox
    Friend WithEvents Label106 As Label
    Friend WithEvents cboIndustryGroups As ComboBox
    Friend WithEvents Label107 As Label
    Friend WithEvents cboIndustries As ComboBox
    Friend WithEvents Label108 As Label
    Friend WithEvents cboIndustrySubs As ComboBox
    Friend WithEvents Label109 As Label
    Friend WithEvents Label112 As Label
    Friend WithEvents txtCashSettlementBroker As TextBox
    Friend WithEvents Label111 As Label
    Friend WithEvents txtClearingAgentCodePOS As TextBox
    Friend WithEvents Label110 As Label
    Friend WithEvents txtAgentCodePOS As TextBox
    Friend WithEvents Label105 As Label
    Friend WithEvents cboCashClearingAgt As ComboBox
    Friend WithEvents Label104 As Label
    Friend WithEvents cboSecClearingAgt2 As ComboBox
    Friend WithEvents Label103 As Label
    Friend WithEvents cboSecClearingAgt1 As ComboBox
    Friend WithEvents Label102 As Label
    Friend WithEvents cboCpty As ComboBox
    Friend WithEvents Label101 As Label
    Friend WithEvents cboPOS As ComboBox
    Friend WithEvents cboIssuer As ComboBox
    Friend WithEvents btnInsertInstrument As Button
    Friend WithEvents btnClearForm As Button
    Friend WithEvents chkBogException As CheckBox
    Friend WithEvents lblBBG_ID As Label
    Friend WithEvents txtBBG_ID As TextBox
    Friend WithEvents lblDayCount As Label
    Friend WithEvents txtDayCount As TextBox
    Friend WithEvents lblCompToParentRel As Label
    Friend WithEvents txtCompToParentRel As TextBox
    Friend WithEvents lblMFIDIndicator As Label
    Friend WithEvents txtMFIDIndicator As TextBox
    Friend WithEvents lblMFIDComplexInstrInd As Label
    Friend WithEvents txtMFIDComplexInstrInd As TextBox
    Friend WithEvents txtDerivitiveDelType As TextBox
    Friend WithEvents lblDerivitiveDelType As Label
    Friend WithEvents gbxtraInfo As GroupBox
    Friend WithEvents lblPriceClose As Label
    Friend WithEvents txtPriceClose As TextBox
    Friend WithEvents lblPriceHigh As Label
    Friend WithEvents txtPriceHigh As TextBox
    Friend WithEvents txtLowPrice As TextBox
    Friend WithEvents blLowPrice As Label
    Friend WithEvents cboEquityType As ComboBox
    Friend WithEvents chkMidCost As CheckBox
    Friend WithEvents txtEndOfInvPeriod As TextBox
    Friend WithEvents txtStartofInvPeriod As TextBox
    Friend WithEvents cboSetDays As ComboBox
    Friend WithEvents cboOptionType As ComboBox
    Friend WithEvents cboStyle As ComboBox
    Friend WithEvents cboDeliveryType As ComboBox
    Friend WithEvents lblDeliveryType As Label
    Friend WithEvents gbxOption As GroupBox
    Friend WithEvents txtDerivTCode As TextBox
    Friend WithEvents txtTickSize As TextBox
    Friend WithEvents lblTickSize As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents txtUnderlyingShortCut As TextBox
    Friend WithEvents txtUnderlyingName As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents lblCommDerivInd As Label
    Friend WithEvents cboCommDerivInd As ComboBox
    Friend WithEvents ttpMain As ToolTip
    Friend WithEvents cboExternalFeedModel As ComboBox
    Friend WithEvents txtExternalFeedModel As TextBox
End Class
