﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTRS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTRS))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.gpbTrsDetails = New System.Windows.Forms.GroupBox()
        Me.grbTrsMain = New System.Windows.Forms.GroupBox()
        Me.gpbXml = New System.Windows.Forms.GroupBox()
        Me.btnGenerateXmlFile = New System.Windows.Forms.Button()
        Me.gpbSelectDate = New System.Windows.Forms.GroupBox()
        Me.btnRefreshGrid = New System.Windows.Forms.Button()
        Me.dtpReportDate = New System.Windows.Forms.DateTimePicker()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.tcrTRS = New System.Windows.Forms.TabControl()
        Me.tbpTRS = New System.Windows.Forms.TabPage()
        Me.dgvTrs = New System.Windows.Forms.DataGridView()
        Me.tbpXML = New System.Windows.Forms.TabPage()
        Me.btnSetFileName = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rtbXml = New System.Windows.Forms.RichTextBox()
        Me.lblTrsFileName = New System.Windows.Forms.Label()
        Me.txtFileName = New System.Windows.Forms.TextBox()
        Me.btnSubmit = New System.Windows.Forms.Button()
        Me.lblTRS = New System.Windows.Forms.Label()
        Me.sfdXml = New System.Windows.Forms.SaveFileDialog()
        Me.gpbTrsDetails.SuspendLayout()
        Me.grbTrsMain.SuspendLayout()
        Me.gpbXml.SuspendLayout()
        Me.gpbSelectDate.SuspendLayout()
        Me.tcrTRS.SuspendLayout()
        Me.tbpTRS.SuspendLayout()
        CType(Me.dgvTrs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpXML.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gpbTrsDetails
        '
        Me.gpbTrsDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gpbTrsDetails.Controls.Add(Me.grbTrsMain)
        Me.gpbTrsDetails.Controls.Add(Me.tcrTRS)
        Me.gpbTrsDetails.Controls.Add(Me.lblTRS)
        Me.gpbTrsDetails.Location = New System.Drawing.Point(12, 12)
        Me.gpbTrsDetails.Name = "gpbTrsDetails"
        Me.gpbTrsDetails.Size = New System.Drawing.Size(1677, 653)
        Me.gpbTrsDetails.TabIndex = 45
        Me.gpbTrsDetails.TabStop = False
        Me.gpbTrsDetails.Text = "TRS Details"
        '
        'grbTrsMain
        '
        Me.grbTrsMain.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbTrsMain.BackColor = System.Drawing.Color.OldLace
        Me.grbTrsMain.Controls.Add(Me.gpbXml)
        Me.grbTrsMain.Controls.Add(Me.gpbSelectDate)
        Me.grbTrsMain.Controls.Add(Me.CmdExportToExcel)
        Me.grbTrsMain.Location = New System.Drawing.Point(130, 19)
        Me.grbTrsMain.Name = "grbTrsMain"
        Me.grbTrsMain.Size = New System.Drawing.Size(1531, 79)
        Me.grbTrsMain.TabIndex = 80
        Me.grbTrsMain.TabStop = False
        '
        'gpbXml
        '
        Me.gpbXml.Controls.Add(Me.btnGenerateXmlFile)
        Me.gpbXml.Location = New System.Drawing.Point(326, 16)
        Me.gpbXml.Name = "gpbXml"
        Me.gpbXml.Size = New System.Drawing.Size(175, 55)
        Me.gpbXml.TabIndex = 80
        Me.gpbXml.TabStop = False
        Me.gpbXml.Text = "Previous TRS submissions"
        '
        'btnGenerateXmlFile
        '
        Me.btnGenerateXmlFile.Location = New System.Drawing.Point(57, 14)
        Me.btnGenerateXmlFile.Name = "btnGenerateXmlFile"
        Me.btnGenerateXmlFile.Size = New System.Drawing.Size(63, 35)
        Me.btnGenerateXmlFile.TabIndex = 79
        Me.btnGenerateXmlFile.Text = "View"
        Me.btnGenerateXmlFile.UseVisualStyleBackColor = True
        '
        'gpbSelectDate
        '
        Me.gpbSelectDate.BackColor = System.Drawing.Color.OldLace
        Me.gpbSelectDate.Controls.Add(Me.btnRefreshGrid)
        Me.gpbSelectDate.Controls.Add(Me.dtpReportDate)
        Me.gpbSelectDate.Location = New System.Drawing.Point(6, 16)
        Me.gpbSelectDate.MinimumSize = New System.Drawing.Size(314, 55)
        Me.gpbSelectDate.Name = "gpbSelectDate"
        Me.gpbSelectDate.Size = New System.Drawing.Size(314, 55)
        Me.gpbSelectDate.TabIndex = 77
        Me.gpbSelectDate.TabStop = False
        Me.gpbSelectDate.Text = "Select Report Date"
        '
        'btnRefreshGrid
        '
        Me.btnRefreshGrid.Location = New System.Drawing.Point(181, 12)
        Me.btnRefreshGrid.Name = "btnRefreshGrid"
        Me.btnRefreshGrid.Size = New System.Drawing.Size(100, 35)
        Me.btnRefreshGrid.TabIndex = 1
        Me.btnRefreshGrid.Text = "Refresh Grid"
        Me.btnRefreshGrid.UseVisualStyleBackColor = True
        '
        'dtpReportDate
        '
        Me.dtpReportDate.Location = New System.Drawing.Point(19, 19)
        Me.dtpReportDate.Name = "dtpReportDate"
        Me.dtpReportDate.Size = New System.Drawing.Size(144, 20)
        Me.dtpReportDate.TabIndex = 0
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(1486, 19)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 78
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'tcrTRS
        '
        Me.tcrTRS.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tcrTRS.Controls.Add(Me.tbpTRS)
        Me.tcrTRS.Controls.Add(Me.tbpXML)
        Me.tcrTRS.Location = New System.Drawing.Point(10, 104)
        Me.tcrTRS.Name = "tcrTRS"
        Me.tcrTRS.SelectedIndex = 0
        Me.tcrTRS.Size = New System.Drawing.Size(1661, 543)
        Me.tcrTRS.TabIndex = 79
        '
        'tbpTRS
        '
        Me.tbpTRS.Controls.Add(Me.dgvTrs)
        Me.tbpTRS.Location = New System.Drawing.Point(4, 22)
        Me.tbpTRS.Name = "tbpTRS"
        Me.tbpTRS.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpTRS.Size = New System.Drawing.Size(1653, 517)
        Me.tbpTRS.TabIndex = 0
        Me.tbpTRS.Text = "TRS Details"
        Me.tbpTRS.UseVisualStyleBackColor = True
        '
        'dgvTrs
        '
        Me.dgvTrs.AllowUserToAddRows = False
        Me.dgvTrs.AllowUserToOrderColumns = True
        Me.dgvTrs.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvTrs.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvTrs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvTrs.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvTrs.Location = New System.Drawing.Point(6, 6)
        Me.dgvTrs.Name = "dgvTrs"
        Me.dgvTrs.Size = New System.Drawing.Size(1641, 508)
        Me.dgvTrs.TabIndex = 77
        '
        'tbpXML
        '
        Me.tbpXML.BackColor = System.Drawing.Color.Gainsboro
        Me.tbpXML.Controls.Add(Me.btnSetFileName)
        Me.tbpXML.Controls.Add(Me.Panel1)
        Me.tbpXML.Controls.Add(Me.lblTrsFileName)
        Me.tbpXML.Controls.Add(Me.txtFileName)
        Me.tbpXML.Controls.Add(Me.btnSubmit)
        Me.tbpXML.Location = New System.Drawing.Point(4, 22)
        Me.tbpXML.Name = "tbpXML"
        Me.tbpXML.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpXML.Size = New System.Drawing.Size(1653, 517)
        Me.tbpXML.TabIndex = 1
        Me.tbpXML.Text = "TRS XML Preview"
        '
        'btnSetFileName
        '
        Me.btnSetFileName.Location = New System.Drawing.Point(424, 8)
        Me.btnSetFileName.Name = "btnSetFileName"
        Me.btnSetFileName.Size = New System.Drawing.Size(125, 23)
        Me.btnSetFileName.TabIndex = 7
        Me.btnSetFileName.Text = "Set TRS File Name"
        Me.btnSetFileName.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.rtbXml)
        Me.Panel1.Location = New System.Drawing.Point(19, 48)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1622, 420)
        Me.Panel1.TabIndex = 6
        '
        'rtbXml
        '
        Me.rtbXml.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbXml.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.rtbXml.Location = New System.Drawing.Point(-1, 0)
        Me.rtbXml.Name = "rtbXml"
        Me.rtbXml.ReadOnly = True
        Me.rtbXml.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.rtbXml.Size = New System.Drawing.Size(1621, 418)
        Me.rtbXml.TabIndex = 5
        Me.rtbXml.Text = ""
        '
        'lblTrsFileName
        '
        Me.lblTrsFileName.AutoSize = True
        Me.lblTrsFileName.Location = New System.Drawing.Point(16, 13)
        Me.lblTrsFileName.Name = "lblTrsFileName"
        Me.lblTrsFileName.Size = New System.Drawing.Size(82, 13)
        Me.lblTrsFileName.TabIndex = 5
        Me.lblTrsFileName.Text = "TRS File Name:"
        '
        'txtFileName
        '
        Me.txtFileName.Location = New System.Drawing.Point(104, 10)
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.Size = New System.Drawing.Size(314, 20)
        Me.txtFileName.TabIndex = 3
        '
        'btnSubmit
        '
        Me.btnSubmit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSubmit.Location = New System.Drawing.Point(1526, 474)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(121, 37)
        Me.btnSubmit.TabIndex = 1
        Me.btnSubmit.Text = "Generate TRS report"
        Me.btnSubmit.UseVisualStyleBackColor = True
        Me.btnSubmit.Visible = False
        '
        'lblTRS
        '
        Me.lblTRS.AutoSize = True
        Me.lblTRS.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTRS.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblTRS.Location = New System.Drawing.Point(6, 16)
        Me.lblTRS.Name = "lblTRS"
        Me.lblTRS.Size = New System.Drawing.Size(118, 24)
        Me.lblTRS.TabIndex = 45
        Me.lblTRS.Text = "TRS Report"
        '
        'FrmTRS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1701, 678)
        Me.Controls.Add(Me.gpbTrsDetails)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmTRS"
        Me.Text = "Submit TRS Report"
        Me.gpbTrsDetails.ResumeLayout(False)
        Me.gpbTrsDetails.PerformLayout()
        Me.grbTrsMain.ResumeLayout(False)
        Me.gpbXml.ResumeLayout(False)
        Me.gpbSelectDate.ResumeLayout(False)
        Me.tcrTRS.ResumeLayout(False)
        Me.tbpTRS.ResumeLayout(False)
        CType(Me.dgvTrs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpXML.ResumeLayout(False)
        Me.tbpXML.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gpbTrsDetails As GroupBox
    Friend WithEvents lblTRS As Label
    Friend WithEvents gpbSelectDate As GroupBox
    Friend WithEvents dtpReportDate As DateTimePicker
    Friend WithEvents btnRefreshGrid As Button
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents tcrTRS As TabControl
    Friend WithEvents tbpTRS As TabPage
    Friend WithEvents dgvTrs As DataGridView
    Friend WithEvents tbpXML As TabPage
    Friend WithEvents grbTrsMain As GroupBox
    Friend WithEvents gpbXml As GroupBox
    Friend WithEvents btnGenerateXmlFile As Button
    Friend WithEvents btnSubmit As Button
    Friend WithEvents sfdXml As SaveFileDialog
    Friend WithEvents lblTrsFileName As Label
    Friend WithEvents txtFileName As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents rtbXml As RichTextBox
    Friend WithEvents btnSetFileName As Button
End Class
