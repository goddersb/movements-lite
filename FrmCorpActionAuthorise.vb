﻿Public Class FrmCorpActionAuthorise
    Private Sub FrmCorpActionAuthorise_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCAAuthID.Text = ClsCA.ID
        txtCAAuthName.Text = ClsCA.InstrumentName
        txtCAAuthISIN.Text = ClsCA.ISIN
        txtCAAuthEvent.Text = ClsCA.CAEvent
        txtCAAuthMV.Text = ClsCA.MV
    End Sub

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Dim varResponse As Integer

        ClsCA.SelectedAuthoriseOption = 0
        If CARb1.Checked Then
            varResponse = MsgBox("Are you sure you want to archive this event?", vbQuestion + vbYesNo, "Are you sure?")
            If varResponse = vbYes Then
                ClsCA.SelectedAuthoriseOption = 1
            End If
        ElseIf CARb2.Checked Then
            ClsCA.SelectedAuthoriseOption = 2
            Dim NewCAT As New FrmCorpActionTerms
            NewCAT.ShowDialog()
        End If

        If ClsCA.SelectedAuthoriseOption <> 2 Then
            ClsIMS.ActionMovementCA(ClsCA.SelectedAuthoriseOption, ClsCA.ID, ClsIMS.FullUserName)
        End If

        If varResponse = vbYes Then
            Me.Close()
        End If
    End Sub
End Class