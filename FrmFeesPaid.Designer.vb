﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFeesPaid
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmFeesPaid))
        Me.GrpFSDetails = New System.Windows.Forms.GroupBox()
        Me.lblFPortfolio = New System.Windows.Forms.Label()
        Me.cboFPortfolio = New System.Windows.Forms.ComboBox()
        Me.lbllastcrystalised = New System.Windows.Forms.Label()
        Me.ChkShowEstimated = New System.Windows.Forms.CheckBox()
        Me.dtFTo = New System.Windows.Forms.DateTimePicker()
        Me.cmdGetFees = New System.Windows.Forms.Button()
        Me.lblFTo = New System.Windows.Forms.Label()
        Me.cboFDate = New System.Windows.Forms.ComboBox()
        Me.lblFDate = New System.Windows.Forms.Label()
        Me.lblFfrom = New System.Windows.Forms.Label()
        Me.cboFDateYear = New System.Windows.Forms.ComboBox()
        Me.lblFDateYear = New System.Windows.Forms.Label()
        Me.dtFFrom = New System.Windows.Forms.DateTimePicker()
        Me.cboFType = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboFView = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblforcastMsg = New System.Windows.Forms.Label()
        Me.ChkSelectAllClientOnly = New System.Windows.Forms.CheckBox()
        Me.ChkSelectAll = New System.Windows.Forms.CheckBox()
        Me.dgvF = New System.Windows.Forms.DataGridView()
        Me.GroupBoxAction = New System.Windows.Forms.GroupBox()
        Me.CmdUnAuthFee = New System.Windows.Forms.Button()
        Me.CmdAuthFee = New System.Windows.Forms.Button()
        Me.CmdPay = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.cboFilterStatus = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboFilterRM1 = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboFilterRM2 = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboFilterDestination = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboFilterDesk = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboFilterAmt = New System.Windows.Forms.ComboBox()
        Me.cmdClearFilter = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboFilterRef = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboFilterAccount = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboFilterType = New System.Windows.Forms.ComboBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboFilterTemplate = New System.Windows.Forms.ComboBox()
        Me.GroupBoxFilter = New System.Windows.Forms.GroupBox()
        Me.lblFilterPortfolio = New System.Windows.Forms.Label()
        Me.cboFilterPortfolio = New System.Windows.Forms.ComboBox()
        Me.cmdMultrees = New System.Windows.Forms.Button()
        Me.GrpFSDetails.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxAction.SuspendLayout()
        Me.GroupBoxFilter.SuspendLayout()
        Me.SuspendLayout()
        '
        'GrpFSDetails
        '
        Me.GrpFSDetails.BackColor = System.Drawing.Color.OldLace
        Me.GrpFSDetails.Controls.Add(Me.lblFPortfolio)
        Me.GrpFSDetails.Controls.Add(Me.cboFPortfolio)
        Me.GrpFSDetails.Controls.Add(Me.lbllastcrystalised)
        Me.GrpFSDetails.Controls.Add(Me.ChkShowEstimated)
        Me.GrpFSDetails.Controls.Add(Me.dtFTo)
        Me.GrpFSDetails.Controls.Add(Me.cmdGetFees)
        Me.GrpFSDetails.Controls.Add(Me.lblFTo)
        Me.GrpFSDetails.Controls.Add(Me.cboFDate)
        Me.GrpFSDetails.Controls.Add(Me.lblFDate)
        Me.GrpFSDetails.Controls.Add(Me.lblFfrom)
        Me.GrpFSDetails.Controls.Add(Me.cboFDateYear)
        Me.GrpFSDetails.Controls.Add(Me.lblFDateYear)
        Me.GrpFSDetails.Controls.Add(Me.dtFFrom)
        Me.GrpFSDetails.Controls.Add(Me.cboFType)
        Me.GrpFSDetails.Controls.Add(Me.Label1)
        Me.GrpFSDetails.Controls.Add(Me.cboFView)
        Me.GrpFSDetails.Controls.Add(Me.Label3)
        Me.GrpFSDetails.Controls.Add(Me.lblpaymentTitle)
        Me.GrpFSDetails.Location = New System.Drawing.Point(12, 12)
        Me.GrpFSDetails.Name = "GrpFSDetails"
        Me.GrpFSDetails.Size = New System.Drawing.Size(592, 167)
        Me.GrpFSDetails.TabIndex = 13
        Me.GrpFSDetails.TabStop = False
        Me.GrpFSDetails.Text = "Fee Selection Criteria"
        '
        'lblFPortfolio
        '
        Me.lblFPortfolio.AutoSize = True
        Me.lblFPortfolio.Location = New System.Drawing.Point(216, 22)
        Me.lblFPortfolio.Name = "lblFPortfolio"
        Me.lblFPortfolio.Size = New System.Drawing.Size(47, 13)
        Me.lblFPortfolio.TabIndex = 147
        Me.lblFPortfolio.Text = "PF Only:"
        Me.lblFPortfolio.Visible = False
        '
        'cboFPortfolio
        '
        Me.cboFPortfolio.FormattingEnabled = True
        Me.cboFPortfolio.Location = New System.Drawing.Point(272, 19)
        Me.cboFPortfolio.Name = "cboFPortfolio"
        Me.cboFPortfolio.Size = New System.Drawing.Size(149, 21)
        Me.cboFPortfolio.TabIndex = 146
        Me.cboFPortfolio.Visible = False
        '
        'lbllastcrystalised
        '
        Me.lbllastcrystalised.AutoSize = True
        Me.lbllastcrystalised.ForeColor = System.Drawing.Color.Maroon
        Me.lbllastcrystalised.Location = New System.Drawing.Point(391, 140)
        Me.lbllastcrystalised.Name = "lbllastcrystalised"
        Me.lbllastcrystalised.Size = New System.Drawing.Size(0, 13)
        Me.lbllastcrystalised.TabIndex = 145
        Me.lbllastcrystalised.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'ChkShowEstimated
        '
        Me.ChkShowEstimated.AutoSize = True
        Me.ChkShowEstimated.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkShowEstimated.Location = New System.Drawing.Point(438, 111)
        Me.ChkShowEstimated.Name = "ChkShowEstimated"
        Me.ChkShowEstimated.Size = New System.Drawing.Size(128, 17)
        Me.ChkShowEstimated.TabIndex = 144
        Me.ChkShowEstimated.Text = "Show Estimated Fees"
        Me.ChkShowEstimated.UseVisualStyleBackColor = True
        '
        'dtFTo
        '
        Me.dtFTo.Location = New System.Drawing.Point(271, 108)
        Me.dtFTo.Name = "dtFTo"
        Me.dtFTo.Size = New System.Drawing.Size(150, 20)
        Me.dtFTo.TabIndex = 141
        Me.dtFTo.Visible = False
        '
        'cmdGetFees
        '
        Me.cmdGetFees.Location = New System.Drawing.Point(445, 51)
        Me.cmdGetFees.Name = "cmdGetFees"
        Me.cmdGetFees.Size = New System.Drawing.Size(121, 48)
        Me.cmdGetFees.TabIndex = 135
        Me.cmdGetFees.Text = "Search Fees"
        Me.cmdGetFees.UseVisualStyleBackColor = True
        '
        'lblFTo
        '
        Me.lblFTo.AutoSize = True
        Me.lblFTo.Location = New System.Drawing.Point(242, 111)
        Me.lblFTo.Name = "lblFTo"
        Me.lblFTo.Size = New System.Drawing.Size(23, 13)
        Me.lblFTo.TabIndex = 143
        Me.lblFTo.Text = "To:"
        Me.lblFTo.Visible = False
        '
        'cboFDate
        '
        Me.cboFDate.FormattingEnabled = True
        Me.cboFDate.Location = New System.Drawing.Point(65, 79)
        Me.cboFDate.Name = "cboFDate"
        Me.cboFDate.Size = New System.Drawing.Size(161, 21)
        Me.cboFDate.TabIndex = 131
        '
        'lblFDate
        '
        Me.lblFDate.AutoSize = True
        Me.lblFDate.Location = New System.Drawing.Point(26, 83)
        Me.lblFDate.Name = "lblFDate"
        Me.lblFDate.Size = New System.Drawing.Size(33, 13)
        Me.lblFDate.TabIndex = 132
        Me.lblFDate.Text = "Date:"
        '
        'lblFfrom
        '
        Me.lblFfrom.AutoSize = True
        Me.lblFfrom.Location = New System.Drawing.Point(30, 110)
        Me.lblFfrom.Name = "lblFfrom"
        Me.lblFfrom.Size = New System.Drawing.Size(33, 13)
        Me.lblFfrom.TabIndex = 142
        Me.lblFfrom.Text = "From:"
        Me.lblFfrom.Visible = False
        '
        'cboFDateYear
        '
        Me.cboFDateYear.FormattingEnabled = True
        Me.cboFDateYear.Location = New System.Drawing.Point(272, 79)
        Me.cboFDateYear.Name = "cboFDateYear"
        Me.cboFDateYear.Size = New System.Drawing.Size(150, 21)
        Me.cboFDateYear.TabIndex = 133
        '
        'lblFDateYear
        '
        Me.lblFDateYear.AutoSize = True
        Me.lblFDateYear.Location = New System.Drawing.Point(234, 81)
        Me.lblFDateYear.Name = "lblFDateYear"
        Me.lblFDateYear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblFDateYear.Size = New System.Drawing.Size(32, 13)
        Me.lblFDateYear.TabIndex = 134
        Me.lblFDateYear.Text = "Year:"
        '
        'dtFFrom
        '
        Me.dtFFrom.Location = New System.Drawing.Point(65, 108)
        Me.dtFFrom.Name = "dtFFrom"
        Me.dtFFrom.Size = New System.Drawing.Size(161, 20)
        Me.dtFFrom.TabIndex = 140
        Me.dtFFrom.Visible = False
        '
        'cboFType
        '
        Me.cboFType.FormattingEnabled = True
        Me.cboFType.Location = New System.Drawing.Point(65, 51)
        Me.cboFType.Name = "cboFType"
        Me.cboFType.Size = New System.Drawing.Size(161, 21)
        Me.cboFType.TabIndex = 136
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 137
        Me.Label1.Text = "Type:"
        '
        'cboFView
        '
        Me.cboFView.FormattingEnabled = True
        Me.cboFView.Location = New System.Drawing.Point(271, 51)
        Me.cboFView.Name = "cboFView"
        Me.cboFView.Size = New System.Drawing.Size(151, 21)
        Me.cboFView.TabIndex = 138
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(233, 54)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 139
        Me.Label3.Text = "View:"
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(6, 16)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(104, 24)
        Me.lblpaymentTitle.TabIndex = 118
        Me.lblpaymentTitle.Text = "Fees Paid"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox1.Controls.Add(Me.lblforcastMsg)
        Me.GroupBox1.Controls.Add(Me.ChkSelectAllClientOnly)
        Me.GroupBox1.Controls.Add(Me.ChkSelectAll)
        Me.GroupBox1.Controls.Add(Me.dgvF)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 185)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1871, 690)
        Me.GroupBox1.TabIndex = 111
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Fee Results"
        '
        'lblforcastMsg
        '
        Me.lblforcastMsg.AutoSize = True
        Me.lblforcastMsg.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblforcastMsg.ForeColor = System.Drawing.Color.Maroon
        Me.lblforcastMsg.Location = New System.Drawing.Point(1269, 15)
        Me.lblforcastMsg.Name = "lblforcastMsg"
        Me.lblforcastMsg.Size = New System.Drawing.Size(596, 13)
        Me.lblforcastMsg.TabIndex = 124
        Me.lblforcastMsg.Text = "PLEASE NOTE: THIS DATE SELECTION INCLUDES FORCASTING OF FEES FROM TODAY ONWARDS"
        Me.lblforcastMsg.Visible = False
        '
        'ChkSelectAllClientOnly
        '
        Me.ChkSelectAllClientOnly.AutoSize = True
        Me.ChkSelectAllClientOnly.Location = New System.Drawing.Point(82, 15)
        Me.ChkSelectAllClientOnly.Name = "ChkSelectAllClientOnly"
        Me.ChkSelectAllClientOnly.Size = New System.Drawing.Size(15, 14)
        Me.ChkSelectAllClientOnly.TabIndex = 113
        Me.ChkSelectAllClientOnly.UseVisualStyleBackColor = True
        Me.ChkSelectAllClientOnly.Visible = False
        '
        'ChkSelectAll
        '
        Me.ChkSelectAll.AutoSize = True
        Me.ChkSelectAll.Location = New System.Drawing.Point(50, 15)
        Me.ChkSelectAll.Name = "ChkSelectAll"
        Me.ChkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.ChkSelectAll.TabIndex = 112
        Me.ChkSelectAll.UseVisualStyleBackColor = True
        Me.ChkSelectAll.Visible = False
        '
        'dgvF
        '
        Me.dgvF.AllowUserToResizeRows = False
        Me.dgvF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvF.Location = New System.Drawing.Point(6, 35)
        Me.dgvF.MultiSelect = False
        Me.dgvF.Name = "dgvF"
        Me.dgvF.Size = New System.Drawing.Size(1859, 636)
        Me.dgvF.TabIndex = 111
        '
        'GroupBoxAction
        '
        Me.GroupBoxAction.BackColor = System.Drawing.Color.OldLace
        Me.GroupBoxAction.Controls.Add(Me.CmdUnAuthFee)
        Me.GroupBoxAction.Controls.Add(Me.CmdAuthFee)
        Me.GroupBoxAction.Controls.Add(Me.CmdPay)
        Me.GroupBoxAction.Location = New System.Drawing.Point(1381, 12)
        Me.GroupBoxAction.Name = "GroupBoxAction"
        Me.GroupBoxAction.Size = New System.Drawing.Size(256, 167)
        Me.GroupBoxAction.TabIndex = 113
        Me.GroupBoxAction.TabStop = False
        Me.GroupBoxAction.Text = "Action on selected fees"
        Me.GroupBoxAction.Visible = False
        '
        'CmdUnAuthFee
        '
        Me.CmdUnAuthFee.Location = New System.Drawing.Point(41, 65)
        Me.CmdUnAuthFee.Name = "CmdUnAuthFee"
        Me.CmdUnAuthFee.Size = New System.Drawing.Size(179, 34)
        Me.CmdUnAuthFee.TabIndex = 116
        Me.CmdUnAuthFee.Text = "Un-Authorise Selected Fees"
        Me.CmdUnAuthFee.UseVisualStyleBackColor = True
        '
        'CmdAuthFee
        '
        Me.CmdAuthFee.Location = New System.Drawing.Point(41, 19)
        Me.CmdAuthFee.Name = "CmdAuthFee"
        Me.CmdAuthFee.Size = New System.Drawing.Size(179, 37)
        Me.CmdAuthFee.TabIndex = 115
        Me.CmdAuthFee.Text = "Authorise Selected Fees"
        Me.CmdAuthFee.UseVisualStyleBackColor = True
        '
        'CmdPay
        '
        Me.CmdPay.Location = New System.Drawing.Point(41, 111)
        Me.CmdPay.Name = "CmdPay"
        Me.CmdPay.Size = New System.Drawing.Size(179, 33)
        Me.CmdPay.TabIndex = 114
        Me.CmdPay.Text = "Pay Selected Fees"
        Me.CmdPay.UseVisualStyleBackColor = True
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.DefaultExt = "xlsx"
        Me.SaveFileDialog1.FileName = "Movements.xlsx"
        Me.SaveFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*"
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(629, 16)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(127, 56)
        Me.CmdFilter.TabIndex = 113
        Me.CmdFilter.Text = "Filter"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'cboFilterStatus
        '
        Me.cboFilterStatus.FormattingEnabled = True
        Me.cboFilterStatus.Location = New System.Drawing.Point(81, 27)
        Me.cboFilterStatus.Name = "cboFilterStatus"
        Me.cboFilterStatus.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterStatus.TabIndex = 116
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(35, 30)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 117
        Me.Label5.Text = "Status:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(238, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(33, 13)
        Me.Label8.TabIndex = 119
        Me.Label8.Text = "RM1:"
        '
        'cboFilterRM1
        '
        Me.cboFilterRM1.FormattingEnabled = True
        Me.cboFilterRM1.Location = New System.Drawing.Point(277, 27)
        Me.cboFilterRM1.Name = "cboFilterRM1"
        Me.cboFilterRM1.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterRM1.TabIndex = 118
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(238, 59)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(33, 13)
        Me.Label6.TabIndex = 121
        Me.Label6.Text = "RM2:"
        '
        'cboFilterRM2
        '
        Me.cboFilterRM2.FormattingEnabled = True
        Me.cboFilterRM2.Location = New System.Drawing.Point(277, 56)
        Me.cboFilterRM2.Name = "cboFilterRM2"
        Me.cboFilterRM2.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterRM2.TabIndex = 120
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 59)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 123
        Me.Label7.Text = "Destination:"
        '
        'cboFilterDestination
        '
        Me.cboFilterDestination.FormattingEnabled = True
        Me.cboFilterDestination.Location = New System.Drawing.Point(81, 56)
        Me.cboFilterDestination.Name = "cboFilterDestination"
        Me.cboFilterDestination.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterDestination.TabIndex = 122
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 125
        Me.Label2.Text = "Desk:"
        '
        'cboFilterDesk
        '
        Me.cboFilterDesk.FormattingEnabled = True
        Me.cboFilterDesk.Location = New System.Drawing.Point(81, 83)
        Me.cboFilterDesk.Name = "cboFilterDesk"
        Me.cboFilterDesk.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterDesk.TabIndex = 124
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(243, 86)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(28, 13)
        Me.Label4.TabIndex = 127
        Me.Label4.Text = "Amt:"
        '
        'cboFilterAmt
        '
        Me.cboFilterAmt.FormattingEnabled = True
        Me.cboFilterAmt.Location = New System.Drawing.Point(277, 83)
        Me.cboFilterAmt.Name = "cboFilterAmt"
        Me.cboFilterAmt.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterAmt.TabIndex = 126
        '
        'cmdClearFilter
        '
        Me.cmdClearFilter.Location = New System.Drawing.Point(629, 79)
        Me.cmdClearFilter.Name = "cmdClearFilter"
        Me.cmdClearFilter.Size = New System.Drawing.Size(127, 23)
        Me.cmdClearFilter.TabIndex = 128
        Me.cmdClearFilter.Text = "Clear Filter"
        Me.cmdClearFilter.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(434, 30)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(27, 13)
        Me.Label9.TabIndex = 130
        Me.Label9.Text = "Ref:"
        '
        'cboFilterRef
        '
        Me.cboFilterRef.FormattingEnabled = True
        Me.cboFilterRef.Location = New System.Drawing.Point(473, 27)
        Me.cboFilterRef.Name = "cboFilterRef"
        Me.cboFilterRef.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterRef.TabIndex = 129
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(434, 57)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(29, 13)
        Me.Label10.TabIndex = 132
        Me.Label10.Text = "Acc:"
        '
        'cboFilterAccount
        '
        Me.cboFilterAccount.FormattingEnabled = True
        Me.cboFilterAccount.Location = New System.Drawing.Point(473, 54)
        Me.cboFilterAccount.Name = "cboFilterAccount"
        Me.cboFilterAccount.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterAccount.TabIndex = 131
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(434, 84)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 134
        Me.Label11.Text = "Type:"
        '
        'cboFilterType
        '
        Me.cboFilterType.FormattingEnabled = True
        Me.cboFilterType.Location = New System.Drawing.Point(473, 81)
        Me.cboFilterType.Name = "cboFilterType"
        Me.cboFilterType.Size = New System.Drawing.Size(147, 21)
        Me.cboFilterType.TabIndex = 133
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(712, 108)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(44, 45)
        Me.CmdExportToExcel.TabIndex = 116
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(21, 114)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(54, 13)
        Me.Label12.TabIndex = 138
        Me.Label12.Text = "Template:"
        '
        'cboFilterTemplate
        '
        Me.cboFilterTemplate.FormattingEnabled = True
        Me.cboFilterTemplate.Location = New System.Drawing.Point(81, 111)
        Me.cboFilterTemplate.Name = "cboFilterTemplate"
        Me.cboFilterTemplate.Size = New System.Drawing.Size(539, 21)
        Me.cboFilterTemplate.TabIndex = 137
        '
        'GroupBoxFilter
        '
        Me.GroupBoxFilter.BackColor = System.Drawing.Color.OldLace
        Me.GroupBoxFilter.Controls.Add(Me.cmdMultrees)
        Me.GroupBoxFilter.Controls.Add(Me.lblFilterPortfolio)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterPortfolio)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterTemplate)
        Me.GroupBoxFilter.Controls.Add(Me.Label12)
        Me.GroupBoxFilter.Controls.Add(Me.CmdExportToExcel)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterType)
        Me.GroupBoxFilter.Controls.Add(Me.Label11)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterAccount)
        Me.GroupBoxFilter.Controls.Add(Me.Label10)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterRef)
        Me.GroupBoxFilter.Controls.Add(Me.Label9)
        Me.GroupBoxFilter.Controls.Add(Me.cmdClearFilter)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterAmt)
        Me.GroupBoxFilter.Controls.Add(Me.Label4)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterDesk)
        Me.GroupBoxFilter.Controls.Add(Me.Label2)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterDestination)
        Me.GroupBoxFilter.Controls.Add(Me.Label7)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterRM2)
        Me.GroupBoxFilter.Controls.Add(Me.Label6)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterRM1)
        Me.GroupBoxFilter.Controls.Add(Me.Label8)
        Me.GroupBoxFilter.Controls.Add(Me.Label5)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterStatus)
        Me.GroupBoxFilter.Controls.Add(Me.CmdFilter)
        Me.GroupBoxFilter.Location = New System.Drawing.Point(610, 12)
        Me.GroupBoxFilter.Name = "GroupBoxFilter"
        Me.GroupBoxFilter.Size = New System.Drawing.Size(765, 167)
        Me.GroupBoxFilter.TabIndex = 114
        Me.GroupBoxFilter.TabStop = False
        Me.GroupBoxFilter.Text = "Filter Grid Criteria"
        Me.GroupBoxFilter.Visible = False
        '
        'lblFilterPortfolio
        '
        Me.lblFilterPortfolio.AutoSize = True
        Me.lblFilterPortfolio.Location = New System.Drawing.Point(27, 141)
        Me.lblFilterPortfolio.Name = "lblFilterPortfolio"
        Me.lblFilterPortfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblFilterPortfolio.TabIndex = 143
        Me.lblFilterPortfolio.Text = "Portfolio:"
        '
        'cboFilterPortfolio
        '
        Me.cboFilterPortfolio.FormattingEnabled = True
        Me.cboFilterPortfolio.Location = New System.Drawing.Point(81, 138)
        Me.cboFilterPortfolio.Name = "cboFilterPortfolio"
        Me.cboFilterPortfolio.Size = New System.Drawing.Size(539, 21)
        Me.cboFilterPortfolio.TabIndex = 142
        '
        'cmdMultrees
        '
        Me.cmdMultrees.Image = CType(resources.GetObject("cmdMultrees.Image"), System.Drawing.Image)
        Me.cmdMultrees.Location = New System.Drawing.Point(629, 108)
        Me.cmdMultrees.Name = "cmdMultrees"
        Me.cmdMultrees.Size = New System.Drawing.Size(44, 45)
        Me.cmdMultrees.TabIndex = 144
        Me.cmdMultrees.UseVisualStyleBackColor = True
        '
        'FrmFeesPaid
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1897, 882)
        Me.Controls.Add(Me.GroupBoxFilter)
        Me.Controls.Add(Me.GroupBoxAction)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GrpFSDetails)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmFeesPaid"
        Me.Text = "FrmFeesPaid"
        Me.GrpFSDetails.ResumeLayout(False)
        Me.GrpFSDetails.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxAction.ResumeLayout(False)
        Me.GroupBoxFilter.ResumeLayout(False)
        Me.GroupBoxFilter.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GrpFSDetails As GroupBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgvF As DataGridView
    Friend WithEvents GroupBoxAction As GroupBox
    Friend WithEvents CmdAuthFee As Button
    Friend WithEvents CmdPay As Button
    Friend WithEvents ChkSelectAll As CheckBox
    Friend WithEvents CmdUnAuthFee As Button
    Friend WithEvents lblpaymentTitle As Label
    Friend WithEvents ChkSelectAllClientOnly As CheckBox
    Friend WithEvents lblforcastMsg As Label
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents dtFTo As DateTimePicker
    Friend WithEvents cmdGetFees As Button
    Friend WithEvents lblFTo As Label
    Friend WithEvents cboFDate As ComboBox
    Friend WithEvents lblFDate As Label
    Friend WithEvents lblFfrom As Label
    Friend WithEvents cboFDateYear As ComboBox
    Friend WithEvents lblFDateYear As Label
    Friend WithEvents dtFFrom As DateTimePicker
    Friend WithEvents cboFType As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboFView As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents CmdFilter As Button
    Friend WithEvents cboFilterStatus As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents cboFilterRM1 As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cboFilterRM2 As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cboFilterDestination As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cboFilterDesk As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cboFilterAmt As ComboBox
    Friend WithEvents cmdClearFilter As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents cboFilterRef As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cboFilterAccount As ComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents cboFilterType As ComboBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents Label12 As Label
    Friend WithEvents cboFilterTemplate As ComboBox
    Friend WithEvents GroupBoxFilter As GroupBox
    Friend WithEvents ChkShowEstimated As CheckBox
    Friend WithEvents lblFilterPortfolio As Label
    Friend WithEvents cboFilterPortfolio As ComboBox
    Friend WithEvents lbllastcrystalised As Label
    Friend WithEvents lblFPortfolio As Label
    Friend WithEvents cboFPortfolio As ComboBox
    Friend WithEvents cmdMultrees As Button
End Class
