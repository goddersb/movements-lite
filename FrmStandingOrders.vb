﻿Public Class FrmStandingOrders

    Dim SICConn As SqlClient.SqlConnection
    Dim dstStandingOrders As New DataSet
    Dim dicStandingOrders As Dictionary(Of String, String)
    Dim blnFormLoaded As Boolean = False

    Public Sub New()

        InitializeComponent()
        SICConn = ClsIMS.GetNewOpenConnection

    End Sub

    Private Sub FrmStandingOrders_Load(sender As Object, e As EventArgs) Handles Me.Load

        Try
            ClsIMS.PopulateComboboxWithData(cboPRPortfolio, "SELECT 1, Portfolio FROM vwDolfinPaymentPortfolio ORDER BY Portfolio ASC")
            cboPRPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboPRPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems

            ClsIMS.PopulateComboboxWithData(cboCCY, "SELECT 1, CR_Name1 from vwDolfinPaymentCurrencies group by CR_Name1 order by CR_Name1")
            cboCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboCCY.AutoCompleteSource = AutoCompleteSource.ListItems

            ModRMS.DoubleBuffered(dgvStandingOrders, True)
            dicStandingOrders = New Dictionary(Of String, String)
            dicStandingOrders.Add("PortfolioName", "")
            dicStandingOrders.Add("CurrencyName", "")

            LoadGrid(dicStandingOrders)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameStandingOrders, $"{Err.Description} - error loading the standing orders form.")
        End Try

    End Sub

    Private Sub LoadGrid(ByVal dictionary As Dictionary(Of String, String))

        Dim _strColSql As String = ""
        Dim _clsIMSData As New ClsIMSData

        Try
            Cursor = Cursors.WaitCursor
            dgvStandingOrders.DataSource = Nothing

            dstStandingOrders = _clsIMSData.GetStandingOrders(dictionary)
            dgvStandingOrders.AutoGenerateColumns = True
            dgvStandingOrders.DataSource = dstStandingOrders.Tables("StandingOrders")

            dgvStandingOrders.Columns("PortfolioName").Width = 270
            dgvStandingOrders.Columns("PortfolioName").HeaderText = "Portfolio Name"
            dgvStandingOrders.Columns("TemplateName").Width = 490
            dgvStandingOrders.Columns("TemplateName").HeaderText = "Template Name"
            dgvStandingOrders.Columns("PaymentType").Width = 200
            dgvStandingOrders.Columns("PaymentType").HeaderText = "Payment Type"
            dgvStandingOrders.Columns("Currency").Width = 80
            dgvStandingOrders.Columns("Start Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvStandingOrders.Columns("Next Run Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvStandingOrders.Columns("End Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvStandingOrders.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvStandingOrders.Columns("Amount").DefaultCellStyle.Format = "n2"
            dgvStandingOrders.Columns("Interval").Width = 90
            dgvStandingOrders.Columns("Status").Width = 90
            dgvStandingOrders.Columns(10).Visible = False

            blnFormLoaded = True

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvStandingOrders.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameStandingOrders, $"{Err.Description}- Problem With viewing standing orders grid.")
        Finally
            _clsIMSData = Nothing
        End Try

    End Sub

    Private Sub cmdPRSearch_Click(sender As Object, e As EventArgs) Handles cmdPRSearch.Click

        Try
            Cursor = Cursors.WaitCursor
            dicStandingOrders = New Dictionary(Of String, String)
            dicStandingOrders.Add("PortfolioName", Me.cboPRPortfolio.Text)
            dicStandingOrders.Add("CurrencyName", Me.cboCCY.Text)
            LoadGrid(dicStandingOrders)
            Cursor = Cursors.Default

        Catch ex As Exception
            Cursor = Cursors.Default
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameStandingOrders, $"{Err.Description} - Problem With filtering on the standing orders grid.")
        End Try

    End Sub

    Private Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click

        cboPRPortfolio.Text = ""
        cboCCY.Text = ""
        cboCCY.Text = ""
        Me.btnReset.Enabled = False
        Me.cmdPRSearch.Enabled = False
        dicStandingOrders = New Dictionary(Of String, String)
        dicStandingOrders.Add("PortfolioName", "")
        dicStandingOrders.Add("CurrencyName", "")
        LoadGrid(dicStandingOrders)

    End Sub

    Private Sub dgvStandingOrders_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvStandingOrders.CellFormatting

        If dgvStandingOrders.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "Status" Then
            FormatGridStatus(dgvStandingOrders, e.RowIndex, e.ColumnIndex)
        End If

    End Sub

    Public Sub FormatGridStatus(ByRef dgv As DataGridView, ByRef varRow As Integer, Optional ByRef varCol As Integer = 1)

        Try
            If Not IsDBNull(dgv.Rows(varRow).Cells(varCol).Value) Then
                If dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusActive) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkGreen
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusExpired) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkRed
                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameStandingOrders, $"{Err.Description} - Problem With applying formatting to grid.")
        End Try

    End Sub

    Private Sub btnNewStandingOrder_Click(sender As Object, e As EventArgs) Handles btnNewStandingOrder.Click

        NewStandingOrderRequest = New FrmStandingOrderRequest
        NewStandingOrderRequest.ShowDialog()
        ModRMS.DoubleBuffered(dgvStandingOrders, True)
        dicStandingOrders = New Dictionary(Of String, String)
        dicStandingOrders.Add("PortfolioName", "")
        dicStandingOrders.Add("CurrencyName", "")

        LoadGrid(dicStandingOrders)

    End Sub

    Private Sub dgvStandingOrders_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvStandingOrders.CellDoubleClick

        Dim intStandingOrderId As Integer
        Dim intResponse As Integer

        Try
            If e.ColumnIndex = -1 Then
                intResponse = MsgBox("Do you wish to edit the selected Standing Order?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Edit Standing Order")
                If intResponse = vbYes Then
                    intStandingOrderId = dgvStandingOrders.Rows(e.RowIndex).Cells("SO_ID").Value
                    Dim StandingOrders As New ClsStandingOrder

                    With StandingOrders
                        .StandingOrderId = CInt(intStandingOrderId)
                        .PortfolioName = dgvStandingOrders.Rows(e.RowIndex).Cells("PortfolioName").Value
                        .TemplateName = dgvStandingOrders.Rows(e.RowIndex).Cells("TemplateName").Value
                        .PaymentType = dgvStandingOrders.Rows(e.RowIndex).Cells("PaymentType").Value
                        .Currency = dgvStandingOrders.Rows(e.RowIndex).Cells("Currency").Value
                        .Amount = CDbl(dgvStandingOrders.Rows(e.RowIndex).Cells("Amount").Value)
                        .StartDate = CDate(dgvStandingOrders.Rows(e.RowIndex).Cells("Start Date").Value)
                        .EndDate = CDate(dgvStandingOrders.Rows(e.RowIndex).Cells("End Date").Value)
                        .Interval = dgvStandingOrders.Rows(e.RowIndex).Cells("Interval").Value
                        .Status = dgvStandingOrders.Rows(e.RowIndex).Cells("Status").Value
                        .SwiftRef = dgvStandingOrders.Rows(e.RowIndex).Cells("SwiftRef").Value
                        .LoadEditForm()
                    End With
                End If
            End If

            dicStandingOrders = New Dictionary(Of String, String)
            dicStandingOrders.Add("PortfolioName", "")
            dicStandingOrders.Add("CurrencyName", "")

            LoadGrid(dicStandingOrders)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameStandingOrders, $"{Err.Description} - Problem selecting standing order {intStandingOrderId} to edit.")
        End Try

    End Sub

    Private Sub cboPRPortfolio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPRPortfolio.SelectedIndexChanged
        If Me.cboPRPortfolio.Text <> "" And blnFormLoaded = True Then
            Me.btnReset.Enabled = True
            Me.cmdPRSearch.Enabled = True
        End If
    End Sub

    Private Sub cboCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCCY.SelectedIndexChanged
        If Me.cboCCY.Text <> "" And blnFormLoaded = True Then
            Me.btnReset.Enabled = True
            Me.cmdPRSearch.Enabled = True
        End If
    End Sub

    Private Sub CmdExcelPR_Click(sender As Object, e As EventArgs) Handles CmdExcelPR.Click

        Cursor = Cursors.WaitCursor
        ExprtGridToExcelFrmForm(dgvStandingOrders, 0)
        'ClsPay.ExportGridToExcel(dgvStandingOrders)
        Cursor = Cursors.Default

    End Sub

    Private Sub dgvStandingOrders_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvStandingOrders.UserDeletingRow

        Dim intStandingOrderId As Integer = 0
        Try
            intStandingOrderId = IIf(IsDBNull(dgvStandingOrders.SelectedRows(0).Cells("SO_ID").Value), 0, dgvStandingOrders.SelectedRows(0).Cells("SO_ID").Value)
            Dim varResponse As Integer = MsgBox($"Are you sure you want to delete Standing order {Chr(13)}" & IIf(IsDBNull(dgvStandingOrders.SelectedRows(0).Cells(0).Value), 0, dgvStandingOrders.SelectedRows(0).Cells(0).Value) & "?", vbYesNo + vbQuestion, "Are you sure")
            If varResponse = vbYes Then
                ClsIMS.ExecuteString(SICConn, $"DELETE DolfinPaymentStandingOrders WHERE SO_ID = {intStandingOrderId}")
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameStandingOrders, $"user deleted - delete DolfinPaymentStandingOrders SO_ID {intStandingOrderId}.")
            Else
                e.Cancel = True
            End If
            dgvStandingOrders.Refresh()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameStandingOrders, Err.Description & $" - Problem with deleting DolfinPaymentStandingOrders varid {intStandingOrderId}.")
        End Try

    End Sub

End Class