﻿Imports System.Data.SqlClient
Imports System.IO

Public Class FrmTreasuryManagement
    Dim PortfolioCode As Integer = 0
    Dim BrokerCode As Integer = 0
    Dim CurrencyCode As Integer = 0
    Dim CurrentBal As Double = 0
    Dim ExtraAmtToPay As Double = 0
    Dim TCode As Integer = 0
    Dim FileName As String = ""
    Dim IsFileSucessful As Boolean = False
    Dim FileWait As Integer = 10
    Dim i As Integer = 0
    Dim IdxLoop As Integer = 10

    Private Sub FrmTreasuryManagement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateComboboxWithData(cboPortfolio, "SELECT PF_Code = -1, Portfolio = '_ALL' 
                                                        UNION ALL SELECT PF_Code, Portfolio FROM dbo.vwDolfinPaymentFlowTreasuryPortfolioList WHERE BranchCode = " & ClsIMS.UserLocationCode & " ORDER BY Portfolio")
        cboPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems

        ClsIMS.PopulateComboboxWithData(cboBroker, "SELECT B_Code = -1, B_Name1 = '_ALL' 
                                            UNION ALL SELECT B_Code, B_Name1 FROM dbo.vwDolfinPaymentFlowTreasuryBrokerList ORDER BY B_Name1")
        cboBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBroker.AutoCompleteSource = AutoCompleteSource.ListItems

        ClsIMS.PopulateComboboxWithData(cboCurrency, "SELECT CR_Code = -1, CR_Name1 = '_ALL' 
                                            UNION ALL SELECT CR_Code,CR_Name1 FROM vwDolfinPaymentSELECTAccount sa1 WHERE BranchCode = " & ClsIMS.UserLocationCode & " and b_class In (1,25,37,39) And exists(SELECT pf_code FROM vwDolfinPaymentSELECTAccount sa2 WHERE b_class in (1,25,37,39) And sa1.pf_code = sa2.pf_code GROUP BY pf_code having count(pf_code) > 1) GROUP BY CR_Code,CR_Name1 ORDER BY CR_Name1")

        ClsIMS.PopulateComboboxWithData(cboBalType, "SELECT BalType = -2, BalName = 'All' 
                                                    UNION ALL SELECT BalType = -1, BalName = 'Neg' 
                                                    UNION ALL SELECT BalType = 0, BalName = 'Par' 
                                                    UNION ALL SELECT BalType = 1, BalName = 'Pos' ")

        cboCurrency.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboCurrency.AutoCompleteSource = AutoCompleteSource.ListItems

        cboBroker.Text = "_ALL"
        cboPortfolio.Text = "_ALL"
        cboCurrency.Text = "_ALL"
        chkHasMoney.Checked = True
        cboBalType.Text = "Neg"
        'LoadBalanceData()
        ShowAutoTreasuryStatus()
    End Sub

    Private Sub LoadBalanceData()
        Dim SqlString As String = ""
        Dim ds As DataSet
        Dim dgvView As DataView

        Me.dgvBalance.DataSource = Nothing
        Me.dgvBalance.Rows.Clear()
        Me.dgvBalance.Columns.Clear()

        SqlString = "EXEC dbo.DolfinPaymentGetPortfolioBalances '" + cboPortfolio.SelectedValue.ToString + "', '" + cboBroker.SelectedValue.ToString + "', '" + cboCurrency.SelectedValue.ToString + "', '" + Format(dtpTransDate.Value, "yyyy-MM-dd") + "', '" + cboBalType.SelectedValue.ToString + "', '" + IIf(chkHasMoney.Checked = False, 0, 1).ToString + "'"
        Try
            ds = ClsIMS.GetTransactionData(SqlString)
            With Me.dgvBalance
                .AutoGenerateColumns = True
                .DataSource = ds.Tables(0)
                .CurrentCell = Nothing
                .RowHeadersWidth = 25
                dgvBalance.Columns("BranchCode").Visible = False
                dgvBalance.Columns("BranchLocation").Visible = False
                With .Columns("Portfolio")
                    .Width = 200
                    .ReadOnly = True
                    .Name = "PortfolioName"
                End With
                With .Columns("Broker")
                    .Width = 100
                    .ReadOnly = True
                    .Name = "BrokerName"
                End With
                With .Columns("CCY")
                    .Width = 40
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Name = "CurrencyName"
                End With
                With .Columns("Balance")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .DefaultCellStyle.Format = "#,##0.00"
                    .Width = 95
                    .Name = "CurrBal"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns("T_Code")
                    .Visible = False
                End With
                With .Columns("PfCcyBalance")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .DefaultCellStyle.Format = "#,##0.00"
                    .Name = "PfCurrBal"
                    .HeaderText = "Pf Ccy Bal"
                    .Width = 95
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns("PF_Code")
                    .Visible = False
                    .Name = "PfCode"
                End With
                With .Columns("B_Code")
                    .Visible = False
                    .Name = "BCode"
                End With
                With .Columns("CR_Code")
                    .Visible = False
                    .Name = "CrCode"
                End With

                With .Columns("HasMoney") ' Balance
                    .Width = 50
                    .ReadOnly = True
                    .Name = "HasEnoughBalance"
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .HeaderText = "Has Bal"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With

                Dim txt As New DataGridViewTextBoxColumn()
                .Columns.Add(txt)
                With .Columns(12) ' Extra amount
                    .Width = 80
                    ' .DefaultCellStyle.Format = "#,##0.00"
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .DefaultCellStyle.NullValue = "0.00"
                    .DefaultCellStyle.Format = "#,##0.00"
                    .Name = "ExtraAmt"
                    .ValueType = GetType(Double)
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                txt.HeaderText = "Target Bal"
                txt.ReadOnly = False

                Dim cmd As New DataGridViewButtonColumn()
                .Columns.Add(cmd)
                cmd.HeaderText = ""
                cmd.Text = "Get Moves"
                cmd.Name = "cmdProcess"
                cmd.UseColumnTextForButtonValue = True
                'button
                .Columns(13).Width = 70

                dgvView = New DataView(ds.Tables(0))
                dgvView.RowFilter = "BranchCode = " & ClsIMS.UserLocationCode
                .DataSource = dgvView
            End With

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            cmdSubmitMoves.Enabled = False
            ds = Nothing
        End Try
    End Sub

    Private Sub LoadFileStatusData()
        Dim SqlString As String = ""
        Me.timerCheckTreasuryStatus.DataSource = Nothing
        Me.timerCheckTreasuryStatus.Rows.Clear()
        Me.timerCheckTreasuryStatus.Columns.Clear()

        SqlString = "SELECT * FROM dbo.vwDolfinPaymentTreasuryMoveStatus WHERE FileName = '" + FileName + "'"

        Dim ds As DataSet
        Dim dr As SqlDataReader
        Try
            ds = ClsIMS.GetTransactionData(SqlString)
            With Me.timerCheckTreasuryStatus
                .AutoGenerateColumns = True
                .DataSource = ds.Tables(0)
                .CurrentCell = Nothing
                With .Columns(0)
                    .Width = 70
                    .ReadOnly = True
                End With
                With .Columns(1)
                    .Width = 70
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns(2)
                    .Width = 150
                    .ReadOnly = True
                    '.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
            End With
            SqlString = "SELECT IsFileSuccessful = CASE WHEN COUNT(*) = MIN(FL.SucceededRecs)  THEN 1 ELSE 0 END
                    FROM  dbo.vwDolfinPaymentTreasuryMoveStatus M
                        INNER JOIN dbo.IFT_FileLog FL
                        ON M.FileName = FL.FileName COLLATE SQL_Latin1_General_CP1253_CI_AS
                     WHERE FL.FileName = '" + FileName + "'"
            dr = ClsIMS.GetSQLDataWithThrow(SqlString)

            If dr.HasRows Then
                dr.Read()
                IsFileSucessful = dr.GetValue(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            dr = Nothing
            ds = Nothing
        End Try
    End Sub

    Private Sub LoadTreasuryMoves(PFCode As Integer, BrCode As Integer, CcyCode As Integer, ExtraAmountToPay As Double, BalanceDate As Date, TitleCode As Integer)
        Dim SqlString As String = ""
        Me.dgvMoves.DataSource = Nothing
        Me.dgvMoves.Rows.Clear()
        Me.dgvMoves.Columns.Clear()

        SqlString = "EXEC dbo.DolfinPaymentGetPortfolioTreasuryMoves " & PFCode.ToString() &
                        " , " & BrCode.ToString &
                        " , " & CcyCode.ToString &
                        " , " & Math.Abs(ExtraAmountToPay).ToString & ' Only pass in the extra amount to pay as the current balance needs to be read from the database.
                        " , '" & Format(BalanceDate, "yyyy-MM-dd") & "'" &
                        " , '" & TitleCode.ToString & "'" &
                        " , 0" ' Display the moves

        Dim dr As SqlDataReader
        Dim dt As New DataTable

        Try
            dr = ClsIMS.GetSQLDataWithThrow(SqlString)
            If dr.HasRows Then
                dt.Load(dr)
                'If dt.Rows.Count = 1 Then
                '    Throw New System.Exception("Invalid move detected:")
                'Else
                With Me.dgvMoves
                    .AutoGenerateColumns = True
                    .DataSource = dt
                    .CurrentCell = Nothing
                    With .Columns(0)
                        .Width = 75
                        .ReadOnly = True
                    End With

                    With .Columns(1)
                        .Width = 120
                        .ReadOnly = True
                    End With
                    With .Columns(2)
                        .Width = 100
                        .ReadOnly = True
                    End With
                    With .Columns(3)
                        .Width = 100
                        .ReadOnly = True
                    End With
                    With .Columns(3)
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        .DefaultCellStyle.NullValue = "0.00"
                        .DefaultCellStyle.Format = "#,##0.00"
                    End With
                    With .Columns(4)
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        .DefaultCellStyle.NullValue = "0.00"
                        .DefaultCellStyle.Format = "#,##0.00"
                    End With
                    With .Columns(5)
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        .DefaultCellStyle.NullValue = "0.00"
                        .DefaultCellStyle.Format = "#,##0.00"
                    End With
                    With .Columns(7)
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        .DefaultCellStyle.NullValue = "0.00"
                        .DefaultCellStyle.Format = "#,##0.00"
                    End With
                    With .Columns(8)
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        .DefaultCellStyle.NullValue = "0.00"
                        .DefaultCellStyle.Format = "#,##0.00"
                    End With
                    With .Columns(9)
                        .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        .DefaultCellStyle.NullValue = "0.00"
                        .DefaultCellStyle.Format = "#,##0.00"
                    End With
                    .Columns(10).Visible = False
                    .Columns(11).Visible = False
                End With

                cmdSubmitMoves.Enabled = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            dr = Nothing
            dt = Nothing
        End Try
    End Sub

    Private Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        cboBroker.Text = "_ALL"
        cboPortfolio.Text = "_ALL"
        cboCurrency.Text = "_ALL"
        cboBalType.Text = "Neg"
        chkHasMoney.Checked = True
        LoadBalanceData()
        ClearMoves()
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        ClearFileStatusData()
        ClearMoves()
        LoadBalanceData()
    End Sub

    Private Sub dgvBalance_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBalance.CellContentClick
        Try
            If dgvBalance.Columns(e.ColumnIndex).Name = "cmdProcess" Then
                ClearMoves()
                With dgvBalance
                    ' Variable assignment to be use later in the file creation
                    PortfolioCode = .Item("PfCode", e.RowIndex).Value
                    BrokerCode = .Item("BCode", e.RowIndex).Value
                    CurrencyCode = .Item("CrCode", e.RowIndex).Value
                    CurrentBal = .Item("CurrBal", e.RowIndex).Value
                    ExtraAmtToPay = .Item("ExtraAmt", e.RowIndex).Value
                    TCode = .Item("T_Code", e.RowIndex).Value
                End With
                If ExtraAmtToPay < 0 Or ExtraAmtToPay < CurrentBal Or dgvBalance.Item("PfCurrBal", e.RowIndex).Value < ExtraAmtToPay Then
                    ' Invalid target balance
                    MsgBox("Target balance must be:" + Chr(13) + Chr(13) + "Greater than the current balance" + Chr(13) + "Less than or equal to portfolio currency balance" + Chr(13) + "Equal or greater than 0", vbCritical, "Target Balance Error")
                Else
                    ' Show the moves
                    LoadTreasuryMoves(PortfolioCode, BrokerCode, CurrencyCode, ExtraAmtToPay, dtpTransDate.Value, TCode)
                End If
            End If
        Catch

        End Try
    End Sub

    Private Sub dgvBalance_SelectionChanged(sender As Object, e As EventArgs) Handles dgvBalance.SelectionChanged
        ClearMoves()
    End Sub

    Private Sub ClearMoves()
        dgvMoves.DataSource = Nothing
        dgvMoves.Rows.Clear()
        dgvMoves.Columns.Clear()
        cmdSubmitMoves.Enabled = False
        lblRequestStatus.Text = ""
    End Sub
    Private Sub ClearFileStatusData()
        timerCheckTreasuryStatus.DataSource = Nothing
        timerCheckTreasuryStatus.Rows.Clear()
        timerCheckTreasuryStatus.Columns.Clear()
        cmdSubmitMoves.Enabled = False
        txtFileName.Text = ""
    End Sub

    Public Sub AddTreasuryMoveRequest(ByVal PortfolioCode As Integer, ByVal BrokerCode As Integer, ByVal CurrencyCode As Integer, ByVal TCode As Integer, ByVal TransactionDate As DateTime, TargetBal As Decimal)
        ' TODO: Add treasury move
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = ClsIMS.GetCurrentConnection
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "dbo.DolfinPaymentAddTreasuryMoveRequest"
            Cmd.Parameters.Add("@PortfolioCode", SqlDbType.Int).Value = PortfolioCode
            Cmd.Parameters.Add("@BrokerCode ", SqlDbType.Int).Value = BrokerCode
            Cmd.Parameters.Add("@CurrencyCode", SqlDbType.Int).Value = CurrencyCode
            Cmd.Parameters.Add("@TCode", SqlDbType.Int).Value = TCode
            Cmd.Parameters.Add("@TransactionDate", SqlDbType.DateTime).Value = Format(TransactionDate, "yyyy-MM-dd")
            Cmd.Parameters.Add("@TargetBal", SqlDbType.Money).Value = TargetBal
            Cmd.Parameters.Add("@RequestSrc", SqlDbType.TinyInt).Value = 2
            Cmd.ExecuteNonQuery()
        Catch ex As SqlException
            Throw ex
        End Try
    End Sub

    Private Sub ClearFileData()
        Me.timerCheckTreasuryStatus.DataSource = Nothing
        Me.timerCheckTreasuryStatus.Rows.Clear()
        Me.timerCheckTreasuryStatus.Columns.Clear()
    End Sub
    Private Sub cmdSubmitMoves_Click(sender As Object, e As EventArgs) Handles cmdSubmitMoves.Click
        cmdSubmitMoves.Enabled = False ' Disable it staright away so resubmitting the same move is disabled
        Try
            AddTreasuryMoveRequest(PortfolioCode, BrokerCode, CurrencyCode, TCode, dtpTransDate.Value, Math.Abs(ExtraAmtToPay))
            MessageBox.Show("Request added successfully. It will be processed when treasury runs.", "Request Added", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Request Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, ex.Message.ToString & " - error in cmdSubmitMoves")
        Finally
            ClearMoves()
            ClearFileData()
        End Try
    End Sub
    Public Sub UpdateTreasuryMoveStatus(ByVal varTreasuyMoveID As Integer, ByVal varFileName As String, ByVal varMoveStatus As Byte)
        ' TODO: Update treasury move status proc
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = ClsIMS.GetCurrentConnection
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "dbo.DolfinPaymentUpdateTreasuryRequestAndFile"
            Cmd.Parameters.Add("@TreasuryMoveID", SqlDbType.Int).Value = varTreasuyMoveID
            Cmd.Parameters.Add("@FileName", SqlDbType.VarChar).Value = varFileName
            Cmd.Parameters.Add("@MoveStatus", SqlDbType.Int).Value = varMoveStatus
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub CreateTextFile(ByVal x As String, FILE_FULL_PATH As String)
        Dim objWriter As New System.IO.StreamWriter(FILE_FULL_PATH, True)
        Try
            If System.IO.File.Exists(FILE_FULL_PATH) = False Then
                System.IO.File.Create(FILE_FULL_PATH).Dispose()
            End If
            objWriter.WriteLine(x)
            objWriter.Close()
        Catch
            MsgBox("Error creating the treasury movement file.  Please make sure you have the rights to access the file location.", vbOKOnly, "File creation error...")
        Finally
            objWriter.Close()
            objWriter = Nothing
        End Try
    End Sub

    Private Sub timerMoveStatus_Tick(sender As Object, e As EventArgs) Handles timerMoveStatus.Tick
        Dim Answer As Integer
        Dim IsMailSent As Boolean
        If i < FileWait And IsFileSucessful = False Then
            i += 1
            cmdGetFileStatus.Enabled = False
            LoadFileStatusData()
        Else
            If IsFileSucessful = True Then ' Then just stop the refresh of the dgvMoveStatus
                timerMoveStatus.Stop()
                timerMoveStatus.Enabled = False
                FileName = ""
                LoadBalanceData()
                ClearMoves()
                ClearFileStatusData()
                cmdGetFileStatus.Enabled = True
            ElseIf i >= FileWait And IsFileSucessful = False Then
                timerMoveStatus.Stop()
                timerMoveStatus.Enabled = False
                Answer = MsgBox("Do you want to continue waiting for the file result?", vbYesNo, "Wait for file status?")
                If Answer = vbYes Then
                    i = 0
                    timerMoveStatus.Start()
                    timerMoveStatus.Enabled = True
                    cmdGetFileStatus.Enabled = False
                Else
                    timerMoveStatus.Stop()
                    timerMoveStatus.Enabled = False
                    IsMailSent = ClsIMS.SendEmail(ClsIMS.GetMyEmailAddress, "Please check file: " + FileName, "Treasury did not complete successfully for file: " + FileName)
                    LoadBalanceData()
                    ClearMoves()
                    ClearFileStatusData()
                    cmdGetFileStatus.Enabled = True
                    FileName = ""
                End If
            End If
        End If
    End Sub

    Private Sub cmdGetFileStatus_Click(sender As Object, e As EventArgs) Handles cmdGetFileStatus.Click
        FileName = txtFileName.Text
        Dim dr As SqlDataReader
        Dim SqlString As String = "SELECT FileName
                                    , RequestCode	
                                    , NoOfRecInFile	
                                    , NoOfRecInIMS
                                    , IsFileSuccessful  = (SELECT TRY_IsFileSuccessful FROM dbo.DolfinPaymentTreasuryRequest WHERE TRY_RequestCode =  RequestCode)
                                    , TreasuryHasError = ISNULL((SELECT FE_ExportBuffer FROM [dbo].[DolfinPaymentFileExport] WHERE FE_Key = 'Treasury has error'), 0)
                                    , IsTreasuryOn = ISNULL((SELECT FE_ExportBuffer FROM [dbo].[DolfinPaymentFileExport] WHERE FE_Name = 'Auto Treasury'), 0)
                                FROM dbo.vwDolfinPaymentTreasuryMoveStatusCount
                                WHERE RequestCode = (SELECT MAX(TRY_RequestCode) FROM dbo.DolfinPaymentTreasuryRequest)"

        Try
            dr = ClsIMS.GetSQLDataWithThrow(SqlString)
            dr.Read()
            FileName = IIf(FileName <> "", FileName, dr("FileName")) ' Makes sure file name is not blank
            txtFileName.Text = FileName
            LoadFileStatusData()
            If (dr("TreasuryHasError") = True Or dr("IsTreasuryOn") = False) And dr("NoOfRecInFile") = dr("NoOfRecInIMS") Then ' If treasury has error
                Dim result As Integer = MessageBox.Show("The last file is successful and Treasury is off." + Chr(13) + " Do you want to switch on auto Treasury?", "Treasury Off", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)
                If result = DialogResult.Yes Then
                    ' TODO: Update auto treasury - SP needs to be added to the right directory
                    UpdateTreasuryMoveStatus(0, FileName, 2)
                    ClsIMS.ExecuteStringWithThrow(ClsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentClearTreasuryError 0") ' Clear has error
                    ClsIMS.ExecuteStringWithThrow(ClsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentSetTreasuryInProgress 0") ' Clear in progress flag
                    ClsIMS.ExecuteStringWithThrow(ClsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentSetAutomaticTreasury 1") ' Enables treasury
                    txtFileName.Text = ""
                Else
                    UpdateTreasuryMoveStatus(0, FileName, 2)
                    ClsIMS.ExecuteStringWithThrow(ClsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentClearTreasuryError 0") ' Clear has error
                    ClsIMS.ExecuteStringWithThrow(ClsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentSetTreasuryInProgress 0") ' Clear in progress flag
                    txtFileName.Text = ""
                End If
                ClearFileData()
                ShowAutoTreasuryStatus()
            End If
            FileName = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "File Error Status", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Finally
            FileName = ""
            dr = Nothing
        End Try
    End Sub

    Private Sub cboBalType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBalType.SelectedIndexChanged
        If cboBalType.Text = "Pos" Or cboBalType.Text = "Par" Or cboBalType.Text = "All" Then
            chkHasMoney.Checked = False
        End If
    End Sub

    Private Sub chkHasMoney_CheckedChanged(sender As Object, e As EventArgs) Handles chkHasMoney.CheckedChanged
        If chkHasMoney.Checked = True Then
            If cboBalType.Text <> "Neg" Then
                cboBalType.SelectedValue = -1
            End If
        End If
    End Sub

    Private Sub dtpTransDate_ValueChanged(sender As Object, e As EventArgs) Handles dtpTransDate.ValueChanged
        ClearMoves()
    End Sub

    Private Sub dgvBalance_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvBalance.DataError
        If dgvBalance.Item("ExtraAmt", e.RowIndex).Value = Nothing Then
            dgvBalance.CancelEdit()
        End If
    End Sub
    Private Sub resetGetMoveVariables()
        PortfolioCode = 0
        BrokerCode = 0
        CurrencyCode = 0
        CurrentBal = 0
        ExtraAmtToPay = 0
        TCode = 0
        i = 0
    End Sub

    Private Sub cboBroker_Leave(sender As Object, e As EventArgs) Handles cboBroker.Leave
        If cboBroker.SelectedIndex = -1 Then
            cboBroker.SelectedValue = 0
        End If
    End Sub
    Private Sub cboPortfolio_Leave(sender As Object, e As EventArgs) Handles cboPortfolio.Leave
        If cboPortfolio.SelectedIndex = -1 Then
            cboPortfolio.SelectedValue = 0
        End If
    End Sub

    Private Sub cboCurrency_Leave(sender As Object, e As EventArgs) Handles cboCurrency.Leave
        If cboCurrency.SelectedIndex = -1 Then
            cboCurrency.SelectedValue = 0
        End If
    End Sub

    Private Sub cboBalType_Leave(sender As Object, e As EventArgs) Handles cboBalType.Leave
        If cboBalType.SelectedIndex = -1 Then
            cboBalType.SelectedValue = 0
        End If
    End Sub

    Private Sub ColorBalances(ByVal dgv As DataGridView, ByVal varRow As Integer, ByVal varCol As Integer)
        If Not IsDBNull(dgv.Rows(varRow).Cells(varCol).Value) Then
            If IsNumeric(dgv.Rows(varRow).Cells(varCol).Value) Then
                If CDbl(dgv.Rows(varRow).Cells(varCol).Value) < 0 Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkRed
                Else
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.Black
                End If
            End If
        End If
    End Sub

    Private Sub dgvBalance_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvBalance.CellFormatting
        ColorBalances(dgvBalance, e.RowIndex, e.ColumnIndex)
    End Sub

    Private Sub dgvMoves_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvMoves.CellFormatting
        ColorBalances(dgvMoves, e.RowIndex, e.ColumnIndex)
    End Sub

    Private Sub dgvMoveStatus_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles timerCheckTreasuryStatus.CellFormatting
        ColorBalances(timerCheckTreasuryStatus, e.RowIndex, e.ColumnIndex)
    End Sub

    Private Sub chkTreasuryAuto_CheckedChanged(sender As Object, e As EventArgs) Handles chkTreasuryAuto.CheckedChanged
        Try
            chkTreasuryAuto.FlatStyle = FlatStyle.Flat
            ClsIMS.ExecuteStringWithThrow(ClsIMS.GetCurrentConnection(), "EXEC dbo.DolfinPaymentSetAutomaticTreasury " + IIf(chkTreasuryAuto.Checked = True, "1", "0").ToString) ' Disables treasury automation 
            ShowAutoTreasuryStatus()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, "Automatic Balancing: Unable to get treasury status.", 0)
            MessageBox.Show(ex.Message.ToString, "Treasury Status Error", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cmdSubmitMoves.Enabled = False
        End Try
    End Sub

    Public Sub ShowAutoTreasuryStatus()
        Dim SqlString As String = ""
        Dim IsSet As Boolean = 0
        Dim dr As SqlDataReader

        SqlString = "SELECT AutoTreasury = FE_ExportBuffer 
                    FROM dbo.DolfinPaymentFileExport 
                    WHERE FE_Name = 'Auto Treasury'"
        Try
            dr = ClsIMS.GetSQLDataWithThrow(SqlString)
            If dr.HasRows Then
                dr.Read()
                IsSet = dr("AutoTreasury")

                If IsSet = True Then
                    With chkTreasuryAuto
                        .Checked = True
                        .Text = "Auto Treasury On"
                        .BackColor = Color.Green
                        .FlatStyle = FlatStyle.Popup
                    End With

                Else
                    With chkTreasuryAuto
                        .Checked = False
                        .Text = "Auto Treasury Off"
                        .BackColor = Color.Red
                        .FlatStyle = FlatStyle.Popup
                    End With
                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, "Unable to get treasury status.", 0)
        Finally
            dr = Nothing
        End Try
    End Sub

    Public Function IsTreasuryInProgress() As Boolean
        Dim SqlString As String = ""
        Dim InProgress As Boolean = 1
        Dim dr As SqlDataReader

        SqlString = "SELECT IsTreasuryInProgress = FE_ExportBuffer 
                    FROM dbo.DolfinPaymentFileExport 
                    WHERE FE_Name = 'Treasury Status'"

        Try
            dr = ClsIMS.GetSQLDataWithThrow(SqlString)
            If dr.HasRows Then
                dr.Read()
                InProgress = dr("IsTreasuryInProgress")
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, "Unable to get treasury status.", 0)
        Finally
            dr = Nothing
        End Try
        Return InProgress
    End Function

    Private Sub timerGetTreasuryStatus_Tick(sender As Object, e As EventArgs) Handles timerGetTreasuryStatus.Tick
        Dim IsInProgress As Boolean = True
        lblRequestStatus.Text = "Checking in progress treasury process for: " + IdxLoop.ToString + " time/s"
        If IdxLoop < 501 Then
            If IsTreasuryInProgress() = False Then
                MessageBox.Show("You can now submit treasury moves manually.")
                cmdSubmitMoves.Enabled = True
                timerGetTreasuryStatus.Stop()
                timerGetTreasuryStatus.Enabled = False
                lblRequestStatus.Text = ""
                Exit Sub
            End If
        Else
            MessageBox.Show("Unable to retrieve treasury progress in time.  Please check with OPs.")
            timerGetTreasuryStatus.Stop()
            timerGetTreasuryStatus.Enabled = False
            lblRequestStatus.Text = ""
            Exit Sub
        End If
        IdxLoop += IdxLoop
    End Sub
End Class