﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaymentrequestAuthorise
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtPRAuthAmount = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPRAuthOrderRef = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPRAuthPortfolio = New System.Windows.Forms.TextBox()
        Me.lblauthportfolio = New System.Windows.Forms.Label()
        Me.txtPRAuthCCY = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.txtPRAuthID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PRRb1 = New System.Windows.Forms.RadioButton()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.PRRb2 = New System.Windows.Forms.RadioButton()
        Me.PRRb3 = New System.Windows.Forms.RadioButton()
        Me.PRRb5 = New System.Windows.Forms.RadioButton()
        Me.PRRb4 = New System.Windows.Forms.RadioButton()
        Me.PRRB7 = New System.Windows.Forms.RadioButton()
        Me.PRRb6 = New System.Windows.Forms.RadioButton()
        Me.PRRb8 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPRAuthAmount)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtPRAuthOrderRef)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtPRAuthPortfolio)
        Me.GroupBox1.Controls.Add(Me.lblauthportfolio)
        Me.GroupBox1.Controls.Add(Me.txtPRAuthCCY)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblportfoliofee)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 48)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(371, 134)
        Me.GroupBox1.TabIndex = 64
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Summary Details"
        '
        'txtPRAuthAmount
        '
        Me.txtPRAuthAmount.Enabled = False
        Me.txtPRAuthAmount.Location = New System.Drawing.Point(70, 97)
        Me.txtPRAuthAmount.Name = "txtPRAuthAmount"
        Me.txtPRAuthAmount.Size = New System.Drawing.Size(125, 20)
        Me.txtPRAuthAmount.TabIndex = 90
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 100)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 89
        Me.Label3.Text = "Amount:"
        '
        'txtPRAuthOrderRef
        '
        Me.txtPRAuthOrderRef.Enabled = False
        Me.txtPRAuthOrderRef.Location = New System.Drawing.Point(70, 71)
        Me.txtPRAuthOrderRef.Name = "txtPRAuthOrderRef"
        Me.txtPRAuthOrderRef.Size = New System.Drawing.Size(125, 20)
        Me.txtPRAuthOrderRef.TabIndex = 88
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 87
        Me.Label1.Text = "Order Ref:"
        '
        'txtPRAuthPortfolio
        '
        Me.txtPRAuthPortfolio.Enabled = False
        Me.txtPRAuthPortfolio.Location = New System.Drawing.Point(70, 19)
        Me.txtPRAuthPortfolio.Name = "txtPRAuthPortfolio"
        Me.txtPRAuthPortfolio.Size = New System.Drawing.Size(291, 20)
        Me.txtPRAuthPortfolio.TabIndex = 86
        '
        'lblauthportfolio
        '
        Me.lblauthportfolio.AutoSize = True
        Me.lblauthportfolio.Location = New System.Drawing.Point(16, 22)
        Me.lblauthportfolio.Name = "lblauthportfolio"
        Me.lblauthportfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblauthportfolio.TabIndex = 85
        Me.lblauthportfolio.Text = "Portfolio:"
        '
        'txtPRAuthCCY
        '
        Me.txtPRAuthCCY.Enabled = False
        Me.txtPRAuthCCY.Location = New System.Drawing.Point(70, 45)
        Me.txtPRAuthCCY.Name = "txtPRAuthCCY"
        Me.txtPRAuthCCY.Size = New System.Drawing.Size(72, 20)
        Me.txtPRAuthCCY.TabIndex = 84
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 48)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "CCY:"
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Enabled = False
        Me.lblportfoliofee.Location = New System.Drawing.Point(179, 45)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 76
        '
        'txtPRAuthID
        '
        Me.txtPRAuthID.AcceptsReturn = True
        Me.txtPRAuthID.AcceptsTab = True
        Me.txtPRAuthID.BackColor = System.Drawing.Color.FloralWhite
        Me.txtPRAuthID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPRAuthID.Enabled = False
        Me.txtPRAuthID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPRAuthID.Location = New System.Drawing.Point(12, 10)
        Me.txtPRAuthID.Multiline = True
        Me.txtPRAuthID.Name = "txtPRAuthID"
        Me.txtPRAuthID.Size = New System.Drawing.Size(64, 29)
        Me.txtPRAuthID.TabIndex = 50
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(78, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(251, 24)
        Me.Label2.TabIndex = 63
        Me.Label2.Text = "Payment Request Options"
        '
        'PRRb1
        '
        Me.PRRb1.AutoSize = True
        Me.PRRb1.Location = New System.Drawing.Point(55, 228)
        Me.PRRb1.Name = "PRRb1"
        Me.PRRb1.Size = New System.Drawing.Size(179, 17)
        Me.PRRb1.TabIndex = 61
        Me.PRRb1.Text = "View Outgoing Swift (if available)"
        Me.PRRb1.UseVisualStyleBackColor = True
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(12, 414)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(373, 23)
        Me.cmdContinue.TabIndex = 60
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'PRRb2
        '
        Me.PRRb2.AutoSize = True
        Me.PRRb2.Location = New System.Drawing.Point(55, 251)
        Me.PRRb2.Name = "PRRb2"
        Me.PRRb2.Size = New System.Drawing.Size(194, 17)
        Me.PRRb2.TabIndex = 65
        Me.PRRb2.Text = "View Confirmation Swift (if available)"
        Me.PRRb2.UseVisualStyleBackColor = True
        '
        'PRRb3
        '
        Me.PRRb3.AutoSize = True
        Me.PRRb3.Location = New System.Drawing.Point(55, 275)
        Me.PRRb3.Name = "PRRb3"
        Me.PRRb3.Size = New System.Drawing.Size(241, 17)
        Me.PRRb3.TabIndex = 66
        Me.PRRb3.Text = "Cancel Payment Request (pending items only)"
        Me.PRRb3.UseVisualStyleBackColor = True
        '
        'PRRb5
        '
        Me.PRRb5.AutoSize = True
        Me.PRRb5.Checked = True
        Me.PRRb5.Location = New System.Drawing.Point(55, 321)
        Me.PRRb5.Name = "PRRb5"
        Me.PRRb5.Size = New System.Drawing.Size(135, 17)
        Me.PRRb5.TabIndex = 67
        Me.PRRb5.TabStop = True
        Me.PRRb5.Text = "View Payment Request"
        Me.PRRb5.UseVisualStyleBackColor = True
        '
        'PRRb4
        '
        Me.PRRb4.AutoSize = True
        Me.PRRb4.Location = New System.Drawing.Point(55, 298)
        Me.PRRb4.Name = "PRRb4"
        Me.PRRb4.Size = New System.Drawing.Size(136, 17)
        Me.PRRb4.TabIndex = 68
        Me.PRRb4.Text = "Copy Payment Request"
        Me.PRRb4.UseVisualStyleBackColor = True
        '
        'PRRB7
        '
        Me.PRRB7.AutoSize = True
        Me.PRRB7.Location = New System.Drawing.Point(56, 205)
        Me.PRRB7.Name = "PRRB7"
        Me.PRRB7.Size = New System.Drawing.Size(156, 17)
        Me.PRRB7.TabIndex = 70
        Me.PRRB7.Text = "Authorise Payment Request"
        Me.PRRB7.UseVisualStyleBackColor = True
        '
        'PRRb6
        '
        Me.PRRb6.AutoSize = True
        Me.PRRb6.Location = New System.Drawing.Point(56, 344)
        Me.PRRb6.Name = "PRRb6"
        Me.PRRb6.Size = New System.Drawing.Size(98, 17)
        Me.PRRb6.TabIndex = 71
        Me.PRRb6.Text = "View Audit Trail"
        Me.PRRb6.UseVisualStyleBackColor = True
        '
        'PRRb8
        '
        Me.PRRb8.AutoSize = True
        Me.PRRb8.Location = New System.Drawing.Point(56, 367)
        Me.PRRb8.Name = "PRRb8"
        Me.PRRb8.Size = New System.Drawing.Size(159, 17)
        Me.PRRb8.TabIndex = 72
        Me.PRRb8.Text = "Re-Send Authorisation Email"
        Me.PRRb8.UseVisualStyleBackColor = True
        '
        'FrmPaymentrequestAuthorise
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(405, 456)
        Me.Controls.Add(Me.PRRb8)
        Me.Controls.Add(Me.PRRb6)
        Me.Controls.Add(Me.PRRB7)
        Me.Controls.Add(Me.PRRb4)
        Me.Controls.Add(Me.PRRb5)
        Me.Controls.Add(Me.PRRb3)
        Me.Controls.Add(Me.PRRb2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PRRb1)
        Me.Controls.Add(Me.cmdContinue)
        Me.Controls.Add(Me.txtPRAuthID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmPaymentrequestAuthorise"
        Me.Text = "Payment Request Authorise"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtPRAuthPortfolio As TextBox
    Friend WithEvents lblauthportfolio As Label
    Friend WithEvents txtPRAuthCCY As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents PRRb1 As RadioButton
    Friend WithEvents cmdContinue As Button
    Friend WithEvents txtPRAuthAmount As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtPRAuthOrderRef As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents PRRb2 As RadioButton
    Friend WithEvents PRRb3 As RadioButton
    Friend WithEvents txtPRAuthID As TextBox
    Friend WithEvents PRRb5 As RadioButton
    Friend WithEvents PRRb4 As RadioButton
    Friend WithEvents PRRB7 As RadioButton
    Friend WithEvents PRRb6 As RadioButton
    Friend WithEvents PRRb8 As RadioButton
End Class
