﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaymentRequestViewSwift
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtSwiftView = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'txtSwiftView
        '
        Me.txtSwiftView.AcceptsReturn = True
        Me.txtSwiftView.AcceptsTab = True
        Me.txtSwiftView.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.txtSwiftView.Location = New System.Drawing.Point(12, 12)
        Me.txtSwiftView.Multiline = True
        Me.txtSwiftView.Name = "txtSwiftView"
        Me.txtSwiftView.ReadOnly = True
        Me.txtSwiftView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSwiftView.Size = New System.Drawing.Size(555, 621)
        Me.txtSwiftView.TabIndex = 34
        '
        'FrmPaymentRequestViewSwift
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(579, 645)
        Me.Controls.Add(Me.txtSwiftView)
        Me.Name = "FrmPaymentRequestViewSwift"
        Me.Text = "FrmViewSwift"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtSwiftView As TextBox
End Class
