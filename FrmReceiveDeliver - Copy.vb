﻿Public Class FrmReceiveDeliver
    Private Function ValidatedForm() As Boolean
        ValidatedForm = True
        If TabRD.SelectedTab.Name = "TabPageReceive" Then
            If cboRPortfolio.Text = "" Then
                MsgBox("Please enter portfolio", vbExclamation, "Enter Portfolio")
                ValidatedForm = False
            ElseIf cboRCCY.Text = "" Then
                MsgBox("Please enter Currency", vbExclamation, "Enter Currency")
                ValidatedForm = False
            ElseIf Not IsNumeric(txtRAmount.Text) Then
                MsgBox("Please enter a valid amount", vbExclamation, "Enter valid amount")
                ValidatedForm = False
            ElseIf CboRInstrument.Text = "" And cboRISIN.Text = "" Then
                MsgBox("Please enter either an instrument or an ISIN", vbExclamation, "Enter Instrument")
                ValidatedForm = False
            ElseIf cboRBroker.Text = "" Then
                MsgBox("Please enter a Broker", vbExclamation, "Enter Broker")
                ValidatedForm = False
            ElseIf cboRClearer.Text = "" Then
                MsgBox("Please enter a clearer", vbExclamation, "Enter Clearer")
                ValidatedForm = False
            ElseIf cboRDelivAgent.Text = "" Then
                MsgBox("Please enter a Delivery Agent", vbExclamation, "Enter Delivery Agent")
                ValidatedForm = False
            ElseIf txtRDelivAgentAccountNo.Text = "" Then
                MsgBox("Please enter a Delivery Agent's Account Number", vbExclamation, "Enter Delivery Agent's Account Number")
                ValidatedForm = False
            ElseIf cboRPlaceSettle.Text = "" Then
                MsgBox("Please enter a Place of Settlement", vbExclamation, "Enter place of settlement")
                ValidatedForm = False
            End If
        ElseIf TabRD.SelectedTab.Name = "TabPageDeliver" Then
            If txtDAmount.Text = "" Then
                MsgBox("Please enter a valid amount", vbExclamation, "Enter valid amount")
                ValidatedForm = False
            ElseIf cboDReceivAgent.SelectedValue = "" Then
                MsgBox("Please enter a Receiving Agent", vbExclamation, "Enter Receiving Agent")
                ValidatedForm = False
            ElseIf txtDReceivAgentAccountNo.Text = "" Then
                MsgBox("Please enter a Receiving Agent's Account Number", vbExclamation, "Enter Receiving Agent's Account Number")
                ValidatedForm = False
            ElseIf cboDPlaceSettle.Text = "" Then
                MsgBox("Please enter a Place of Settlement", vbExclamation, "Enter place of settlement")
                ValidatedForm = False
            End If
        End If
    End Function

    Private Sub ClearRForm()
        RRB1.Checked = True
        cboRPortfolio.Text = ""
        CboRInstrument.Text = ""
        cboRISIN.Text = ""
        dtRTransDate.Value = Now
        dtRSettleDate.Value = Now
        txtRAmount.Text = ""
        CboRCCY.Text = ""
        cboRBroker.Text = ""
        cboRCashAcc.Text = ""
        cboRClearer.Text = ""
        cboRDelivAgent.Text = ""
        txtRDelivAgentAccountNo.Text = ""
        cboRPlaceSettle.Text = ""
        txtRInstructions.Text = ""
        ChkRSendIMS.Checked = True
    End Sub

    Private Sub ClearDForm()
        txtDAmount.Text = ""
        cboDBroker.Text = ""
        cboDCashAcc.Text = ""
        cboDClearer.Text = ""
        cboDReceivAgent.Text = ""
        txtDReceivAgentAccountNo.Text = ""
        cboDPlaceSettle.Text = ""
        txtDInstructions.Text = ""
        ChkDSendIMS.Checked = True
    End Sub

    Private Sub EditRD()
        PopulateComboBoxes()
        cboRPortfolio.SelectedValue = ClsRD.PortfolioCode
        If IsNumeric(cboRPortfolio.SelectedValue) Then
            Call PopulateCbo(CboRCCY, "Select CR_Code, CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboRPortfolio.SelectedValue & " group by CR_Code, CR_Name1 order by CR_Name1")
        End If
        CboRCCY.SelectedValue = ClsRD.CCYCode
        PopulateRInstAndCashAccount()
        CboRInstrument.SelectedValue = ClsRD.InstrCode
        cboRBroker.SelectedValue = ClsRD.BrokerCode
        If IsNumeric(cboRBroker.SelectedValue) Then
            Call PopulateCbo(cboRClearer, "Select ClearerCode,ClearerName from vwDolfinPaymentBrokerClearer where isnull(ClearerSwiftAddress,'')<> '' and isnull(ClearerNo,'')<> '' and BrokerCode = " & cboRBroker.SelectedValue & " group by ClearerCode,ClearerName")
        End If
        cboRCashAcc.SelectedValue = ClsRD.CashAccountCode
        cboRClearer.SelectedValue = ClsRD.ClearerCode
        dtRTransDate.Value = ClsRD.TransDate
        dtRSettleDate.Value = ClsRD.SettleDate
        cboRDelivAgent.SelectedValue = ClsRD.Agent
        txtRDelivAgentAccountNo.Text = ClsRD.AgentAccountNo
        txtRAmount.Text = ClsRD.Amount
        cboRPlaceSettle.SelectedValue = ClsRD.PlaceofSettlement
        txtRInstructions.Text = ClsRD.Instructions
        ChkDSendIMS.Checked = ClsRD.SendIMS
        ChangeformRD(False)
    End Sub

    Public Sub ChangeformRD(ByVal varAdd As Boolean)
        If varAdd Then
            cmdRDRefresh.Text = "Refresh Transactions"
            cmdAddRD.Text = "Add Transaction"
            'txtID.BackColor = Color.LightSteelBlue
            Me.BackColor = Color.LightSteelBlue
            GrpTransType.BackColor = Color.LightSteelBlue
            RDBox.BackColor = Color.LightSteelBlue
            TabPageReceive.BackColor = Color.LightSteelBlue
        Else
            cmdAddRD.Text = "Click to confirm edit"
            Me.BackColor = Color.FromKnownColor(KnownColor.Control)
            GrpTransType.BackColor = Color.FromKnownColor(KnownColor.Control)
            RDBox.BackColor = Color.FromKnownColor(KnownColor.Control)
            TabPageReceive.BackColor = Color.FromKnownColor(KnownColor.Control)
        End If
    End Sub

    Private Sub FrmReceiveDeliver_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadForm()
    End Sub

    Private Sub PopulateComboBoxes()
        cboRBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRBroker.AutoCompleteSource = AutoCompleteSource.ListItems
        cboRCashAcc.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRCashAcc.AutoCompleteSource = AutoCompleteSource.ListItems
        cboRClearer.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRClearer.AutoCompleteSource = AutoCompleteSource.ListItems
        CboRInstrument.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboRInstrument.AutoCompleteSource = AutoCompleteSource.ListItems
        RRB1.Checked = True
        cboRISIN.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRISIN.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboRBroker, "Select BrokerCode, BrokerName from vwDolfinPaymentBrokerClearer where isnull(BrokerSwiftAddress,'')<>'' and isnull(BrokerAccountNo,'')<>'' group by BrokerCode, BrokerName order by BrokerName")
        cboRBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRBroker.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboRPortfolio, "Select pf_Code,Portfolio from vwDolfinPaymentPortfolio order by Portfolio")
        cboRPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(CboRCCY, "Select CR_Code, CR_Name1 from vwDolfinPaymentSelectAccount group by CR_Code, CR_Name1 order by CR_Name1")
        CboRCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboRCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboRDelivAgent, "select SC_SwiftCode, isnull(SC_BankName,'') + '      (' + isnull(SC_SwiftCode,'') + ')' from vwDolfinPaymentSwiftCodes where isnull(SC_SwiftCode,'')<>'' and isnull(SC_BankName,'')<>'' Order by SC_BankName")
        cboRDelivAgent.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRDelivAgent.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboRPlaceSettle, "select SC_SwiftCode, isnull(SC_BankName,'') + '      (' + isnull(SC_SwiftCode,'') + ')' from vwDolfinPaymentSwiftCodes where isnull(SC_SwiftCode,'')<>'' and isnull(SC_BankName,'')<>'' Order by SC_BankName")
        cboRPlaceSettle.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRPlaceSettle.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboDPortfolio, "Select pf_Code,Portfolio from vwDolfinPaymentPortfolio order by Portfolio")
        cboDPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboDPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboDReceivAgent, "select SC_SwiftCode, isnull(SC_BankName,'') + '      (' + isnull(SC_SwiftCode,'') + ')' from vwDolfinPaymentSwiftCodes where isnull(SC_SwiftCode,'')<>'' and isnull(SC_BankName,'')<>'' Order by SC_BankName")
        cboDReceivAgent.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboDReceivAgent.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboDPlaceSettle, "select SC_SwiftCode, isnull(SC_BankName,'') + '      (' + isnull(SC_SwiftCode,'') + ')' from vwDolfinPaymentSwiftCodes where isnull(SC_SwiftCode,'')<>'' and isnull(SC_BankName,'')<>'' Order by SC_BankName")
        cboDPlaceSettle.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboDPlaceSettle.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboDBroker, "Select BrokerCode, BrokerName from vwDolfinPaymentBrokerClearer where isnull(BrokerSwiftAddress,'')<>'' and isnull(BrokerAccountNo,'')<>'' group by BrokerCode, BrokerName order by BrokerName")
        cboRBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRBroker.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub LoadForm()
        ModRMS.DoubleBuffered(dgvRD, True)
        ModRMS.DoubleBuffered(dgvD, True)
        PopulateComboBoxes()
        If CboFilterStatus.Items.Count = 0 Then
            ClsIMS.PopulatecbostatusTypes(CboFilterStatus)
        End If
        If cboSwiftCheck.Items.Count = 0 Then
            ClsIMS.PopulatecboSwiftCheck(cboSwiftCheck)
        End If
        cboSwiftCheck.SelectedValue = 0

        lblRMsg.Text = "Please Note:" & vbNewLine & "1. Only instruments with ISIN populated in IMS are available to be selected" & vbNewLine &
        "2. Only broker acccounts set up with a clearer, SWIFT Address and broker account number are available to be selected"
        RefreshGridsRD()
        ClsRD.ClearClass()
    End Sub

    Private Sub PopulateCbo(ByRef cbo As ComboBox, ByVal varSQL As String)
        Dim TempValue As String = ""
        TempValue = cbo.Text
        ClsIMS.PopulateComboboxWithData(cbo, varSQL)
        If TempValue = "" Then
            cbo.SelectedIndex = -1
        Else
            cbo.Text = TempValue
        End If
    End Sub


    Private Sub PopulateRClassFromForm()
        ClsRD.RecType = 1
        ClsRD.IMSTrCode = 0
        ClsRD.PortfolioCode = cboRPortfolio.SelectedValue
        ClsRD.CCYCode = CboRCCY.SelectedValue
        If CboRInstrument.Text <> "" Then
            ClsRD.InstrCode = CboRInstrument.SelectedValue
        Else
            ClsRD.InstrCode = cboRISIN.SelectedValue
        End If
        ClsRD.BrokerCode = cboRBroker.SelectedValue
        ClsRD.CashAccountCode = cboRCashAcc.SelectedValue
        ClsRD.ClearerCode = cboRClearer.SelectedValue
        ClsRD.TransDate = dtRTransDate.Value
        ClsRD.SettleDate = dtRSettleDate.Value
        ClsRD.Agent = cboRDelivAgent.SelectedValue
        ClsRD.AgentAccountNo = txtRDelivAgentAccountNo.Text
        ClsRD.PlaceofSettlement = cboRPlaceSettle.SelectedValue
        ClsRD.Instructions = txtRInstructions.Text
        ClsRD.Amount = txtRAmount.Text
        ClsRD.SendIMS = ChkRSendIMS.Checked
    End Sub


    Private Sub cmdAddRD_Click(sender As Object, e As EventArgs) Handles cmdAddRD.Click
        If TabRD.SelectedTab.Name = "TabPageReceive" Or TabRD.SelectedTab.Name = "TabPageDeliver" Then
            If ValidatedForm() Then
                If TabRD.SelectedTab.Name = "TabPageReceive" Then
                    PopulateRClassFromForm()
                    If ClsRD.SelectedTransactionID <> 0 Then
                        ClsIMS.UpdatePaymentTableRD(False)
                        ChangeformRD(True)
                    Else
                        ClsIMS.UpdatePaymentTableRD(True)
                    End If

                    ClsRD.ClearClass()
                    ClearRForm()
                ElseIf TabRD.SelectedTab.Name = "TabPageDeliver" Then
                    ClsRD.RecType = 2
                    ClsRD.IMSTrCode = dgvD.Rows(dgvD.SelectedCells(0).RowIndex).Cells("TrCode").Value
                    ClsRD.PortfolioCode = cboDPortfolio.SelectedValue
                    ClsRD.TransDate = dtDTransDate.Value
                    ClsRD.SettleDate = dtDSettleDate.Value
                    ClsRD.Amount = txtDAmount.Text
                    ClsRD.Agent = cboDReceivAgent.SelectedValue
                    ClsRD.AgentAccountNo = txtDReceivAgentAccountNo.Text
                    ClsRD.BrokerCode = cboDBroker.SelectedValue
                    ClsRD.CashAccountCode = cboDCashAcc.SelectedValue
                    ClsRD.ClearerCode = cboDClearer.SelectedValue
                    ClsRD.PlaceofSettlement = cboDPlaceSettle.SelectedValue
                    ClsRD.Instructions = txtDInstructions.Text
                    ClsRD.SendIMS = ChkDSendIMS.Checked
                    ClsIMS.UpdatePaymentTableRD(True)
                    ClsRD.ClearClass()
                    ClearDForm()
                End If
            End If
        End If
        RefreshGridsRD()
    End Sub

    Public Sub PopulatePreviewSWIFT()
        Dim Lst As New Dictionary(Of String, String)
        Dim PaymentTypeID As Integer = 0

        If dgvD.SelectedCells.Count = 0 Then
            Lst.Add("OrderRef", "RECEIV0000000")
            Lst.Add("TransDate", dtRTransDate.Value.Date)
            Lst.Add("SettleDate", dtRSettleDate.Value.Date)
            Lst.Add("Amount", IIf(IsNumeric(txtRAmount.Text), txtRAmount.Text, 0))
            If CboRInstrument.Text = "" And cboRISIN.Text <> "" Then
                Lst.Add("InstrCode", cboRISIN.SelectedValue)
            ElseIf CboRInstrument.Text <> "" Then
                Lst.Add("InstrCode", CboRInstrument.SelectedValue)
            End If

            Lst.Add("BrokerCode", IIf(cboRBroker.Text <> "", cboRBroker.SelectedValue, 0))
            Lst.Add("CashAccCode", IIf(cboRCashAcc.Text <> "", cboRCashAcc.SelectedValue, 0))
            Lst.Add("ClearerCode", IIf(cboRClearer.Text <> "", cboRClearer.SelectedValue, 0))
            Lst.Add("Agent", IIf(cboRDelivAgent.Text <> "", cboRDelivAgent.SelectedValue, 0))
            Lst.Add("AgentAccountNo", IIf(txtRDelivAgentAccountNo.Text <> "", txtRDelivAgentAccountNo.Text, ""))
            Lst.Add("PlaceofSettlement", IIf(cboRPlaceSettle.SelectedValue <> "", cboRPlaceSettle.SelectedValue, ""))
            Lst.Add("Instructions", IIf(txtRInstructions.Text <> "", txtRInstructions.Text, ""))
            txtSWIFT.Text = ClsIMS.GetSwiftFileData540(Lst)
        Else
            Lst.Add("OrderRef", "DELIVE0000000")
            Lst.Add("PortfolioCode", cboDPortfolio.SelectedValue)
            Lst.Add("Agent", IIf(cboDReceivAgent.Text <> "", cboDReceivAgent.SelectedValue, 0))
            Lst.Add("AgentAccountNo", IIf(txtDReceivAgentAccountNo.Text <> "", txtDReceivAgentAccountNo.Text, ""))
            Lst.Add("PlaceofSettlement", IIf(cboRPlaceSettle.SelectedValue <> "", cboRPlaceSettle.SelectedValue, ""))
            Lst.Add("Instructions", IIf(txtRInstructions.Text <> "", txtRInstructions.Text, ""))
            txtSWIFT.Text = ClsIMS.GetSwiftFileData542(Lst)
        End If

    End Sub

    Public Function GetFilterWhereClause() As String
        GetFilterWhereClause = ""
        If CboFilterStatus.Text <> "" Then
            GetFilterWhereClause = "where (status = '" & CboFilterStatus.Text & "')"
        End If
    End Function

    Private Sub cmdRDRefresh_Click(sender As Object, e As EventArgs) Handles cmdRDRefresh.Click
        RefreshGridsRD()
    End Sub

    Private Sub CboRInstrument_Leave(sender As Object, e As EventArgs) Handles CboRInstrument.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub cboRISIN_Leave(sender As Object, e As EventArgs) Handles cboRISIN.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub dtRTransDate_Leave(sender As Object, e As EventArgs) Handles dtRTransDate.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub dtRSettleDate_Leave(sender As Object, e As EventArgs) Handles dtRSettleDate.Leave
        PopulatePreviewSWIFT()
    End Sub


    Private Sub txtRAmount_Leave(sender As Object, e As EventArgs) Handles txtRAmount.Leave
        If IsNumeric(txtRAmount.Text) Then
            txtRAmount.Text = CDbl(txtRAmount.Text).ToString("#,##0.00")
            PopulatePreviewSWIFT()
        End If
    End Sub

    Private Sub RRB1_CheckedChanged(sender As Object, e As EventArgs) Handles RRB1.CheckedChanged
        cboRISIN.Text = ""
        CboRInstrument.Enabled = True
        cboRISIN.Enabled = False
    End Sub

    Private Sub RRB2_CheckedChanged(sender As Object, e As EventArgs) Handles RRB2.CheckedChanged
        CboRInstrument.Text = ""
        CboRInstrument.Enabled = False
        cboRISIN.Enabled = True
    End Sub

    Private Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdSubmit.Click
        Dim ExportFilesList As New Dictionary(Of String, String)

        Try
            Dim FullFilePathXL As String = ClsIMS.GetFilePath("IMSExportXL") & "IMSXLRDF" & Now.ToString("_yyMMdd_HHmmssfff") & ".xlsx"
            ClsRD.CreateTransactionsXLRD(FullFilePathXL)

            If ClsIMS.LiveDB() Then
                ClsIMS.RunSQLJob(cSwiftJobNameRD)
            Else
                ClsIMS.RunSQLJob(cSwiftJobNameRD & " UAT")
            End If

            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "Submit receive/deliver transactions files", 0)
            MsgBox("Swift files Exported" & vbNewLine & vbNewLine & "Exported XL file can be found: " & vbNewLine & FullFilePathXL, vbInformation, "Exported")
            ClsRD.ClearClass()
            RefreshGridsRD()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in Submit receive/deliver")
        End Try
    End Sub

    Private Sub dgvRD_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvRD.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvRD, e.RowIndex)
        End If
    End Sub

    Private Sub FormatRDGrid(ByRef dgv As DataGridView)
        If Not dgv.DataSource Is Nothing Then
            dgv.Columns("ID").Width = 60
            dgv.Columns("Status").Width = 120
            dgv.Columns("SwiftAck").Width = 60
            dgv.Columns("ConfAck").Width = 60
            dgv.Columns("StatusAck").Width = 60
            dgv.Columns("IMSAck").Width = 60
            dgv.Columns("ReceiveDeliver").Width = 100
            dgv.Columns("OrderRef").Width = 100
            dgv.Columns("IMSCode").Width = 100
            dgv.Columns("Portfolio").Width = 150
            dgv.Columns("CCYName").Width = 60
            dgv.Columns("InstName").Width = 100
            dgv.Columns("TransDate").Width = 80
            dgv.Columns("SettleDate").Width = 80
            dgv.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns("Amount").DefaultCellStyle.Format = "N2"
            dgv.Columns("Amount").ValueType = GetType(SqlTypes.SqlMoney)
            dgv.Columns("BrokerName").Width = 100
            dgv.Columns("CashAccountName").Width = 100
            dgv.Columns("ClearerName").Width = 100
            dgv.Columns("Agent").Width = 100
            dgv.Columns("AgentAccountNo").Width = 100
            dgv.Columns("SwiftRet").Width = 60
            dgv.Columns("SwiftFile").Width = 100
            dgv.Columns("SwiftStatus").Width = 100
            dgv.Columns("Username").Width = 100
            dgv.Columns("Created").Width = 100
            dgv.Columns("Modified").Width = 100
            dgv.Columns("SentTime").Width = 100
            dgv.Columns("ReceivedTime").Width = 100
            dgv.Columns("ConfReceivedTime").Width = 100
            dgv.Columns("StatusReceivedTime").Width = 100
            dgv.Columns("IMSSentTime").Width = 100
            dgv.Columns("SendIMS").Width = 60
        End If
    End Sub

    Private Sub RefreshGridsRD()
        Dim SendIMS As Boolean = False, CheckSwiftAck As Boolean = False, CheckSwiftPaid As Boolean = False
        Cursor = Cursors.WaitCursor

        ClsIMS.UpdateMovementStatusRD()
        ClsIMS.RefreshRDGrid(dgvRD, GetFilterWhereClause())
        'ClsIMS.CheckStatusGridRD(dgvRD, SendIMS, CheckSwiftAck, CheckSwiftPaid)

        'If SendIMS Then
        'ClsIMS.CreateTransactionsIMSRD()
        'End If

        'If CheckSwiftAck Or CheckSwiftPaid Then
        'ClsIMS.UpdateSwiftStatus(CheckSwiftAck, CheckSwiftPaid, cboSwiftCheck.SelectedValue)
        'End If

        FormatRDGrid(dgvRD)
        Cursor = Cursors.Default
    End Sub

    Private Sub RefreshGridD()
        If IsNumeric(cboDPortfolio.SelectedValue) Then
            ClsIMS.RefreshDGrid(dgvD, cboDPortfolio.SelectedValue, dtDFrom.Value, dtDTo.Value)
            cboDReceivAgent.Text = ""
            txtDReceivAgentAccountNo.Text = ""
        End If
    End Sub

    Private Sub cboDPortfolio_Leave(sender As Object, e As EventArgs) Handles cboDPortfolio.Leave
        RefreshGridD()
    End Sub

    Private Sub dgvD_Click(sender As Object, e As EventArgs) Handles dgvD.Click
        If dgvD.SelectedRows.Count > 0 Then
            PopulateDCashAccount(cboDPortfolio.SelectedValue, dgvD.Rows(dgvD.SelectedRows.Item(0).Index).Cells(4).Value)
            cboDBroker.Text = dgvD.Rows(dgvD.SelectedRows.Item(0).Index).Cells(10).Value
            cboDCashAcc.Text = dgvD.Rows(dgvD.SelectedRows.Item(0).Index).Cells(11).Value
            txtDAmount.Text = dgvD.Rows(dgvD.SelectedRows.Item(0).Index).Cells(6).Value
            If dgvD.Rows.Count > 0 Then
                PopulatePreviewSWIFT()
            End If
        End If
    End Sub

    Private Sub TabPageReceive_Click(sender As Object, e As EventArgs) Handles TabPageReceive.Click
        dgvD.ClearSelection()
        txtSWIFT.Text = ""
    End Sub

    Private Sub TabPageDeliver_Click(sender As Object, e As EventArgs) Handles TabPageDeliver.Click
        dgvD.ClearSelection()
        txtSWIFT.Text = ""
    End Sub

    Private Sub TabPageReceive_TabIndexChanged(sender As Object, e As EventArgs) Handles TabPageReceive.TabIndexChanged
        dgvD.ClearSelection()
        txtSWIFT.Text = ""
    End Sub

    Private Sub TabPageDeliver_TabIndexChanged(sender As Object, e As EventArgs) Handles TabPageDeliver.TabIndexChanged
        dgvD.ClearSelection()
        txtSWIFT.Text = ""
    End Sub

    Private Sub TabPageDeliver_MouseClick(sender As Object, e As MouseEventArgs) Handles TabPageDeliver.MouseClick
        dgvD.ClearSelection()
        txtSWIFT.Text = ""
    End Sub

    Private Sub TabPageReceive_MouseClick(sender As Object, e As MouseEventArgs) Handles TabPageReceive.MouseClick
        dgvD.ClearSelection()
        txtSWIFT.Text = ""
    End Sub

    Private Sub TimerRefreshRD_Tick(sender As Object, e As EventArgs) Handles TimerRefreshRD.Tick
        RefreshGridsRD()
    End Sub

    Private Sub dgvRD_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvRD.RowHeaderMouseDoubleClick
        If ClsRD.SelectedTransactionID = 0 Then
            ClsIMS.GetReaderItemIntoRDClass(dgvRD.Rows(e.RowIndex).Cells(0).Value)
            FrmActionRD.ShowDialog()
            If ClsRD.SelectedAuthoriseOption = 1 Then
                EditRD()
            Else
                ClsRD.ClearClass()
            End If
            RefreshGridsRD()
        Else
            MsgBox("You are currently in edit mode. Please save your current transaction or create a new one to authorise other transactions", vbInformation, "Complete save transaction")
        End If
    End Sub

    Private Sub cboRClearer_Leave(sender As Object, e As EventArgs) Handles cboRClearer.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub cboDReceivAgent_Leave(sender As Object, e As EventArgs)
        If Not cboDReceivAgent.SelectedValue Is Nothing Then
            If cboDReceivAgent.ValueMember <> "" Then
                txtDReceivAgentAccountNo.Text = ClsIMS.GetSQLItem("select top 1 left(ClearerNo,charindex('/',ClearerNo)) from vwDolfinPaymentBrokerClearer where ClearerSwiftAddress = '" & cboDReceivAgent.SelectedValue & "' and charindex('/',ClearerNo)<>0")
                If txtDReceivAgentAccountNo.Text = "" Then
                    MsgBox("Please enter full participant code into the Delivery Agent's account No i.e. Euroclear = ECLR/12345", vbInformation, "Enter full code")
                End If
            End If
        End If
    End Sub

    Private Sub cboRBroker_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRBroker.SelectedIndexChanged
        If IsNumeric(cboRBroker.SelectedValue) Then
            Call PopulateCbo(cboRClearer, "Select ClearerCode,ClearerName from vwDolfinPaymentBrokerClearer where isnull(ClearerSwiftAddress,'')<> '' and isnull(ClearerNo,'')<> '' and BrokerCode = " & cboRBroker.SelectedValue & " group by ClearerCode,ClearerName")
            PopulatePreviewSWIFT()
        End If
    End Sub

    Private Sub cboRDelivAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRDelivAgent.SelectedIndexChanged
        If Not cboRDelivAgent.SelectedValue Is Nothing Then
            If cboRDelivAgent.ValueMember <> "" Then
                txtRDelivAgentAccountNo.Text = ClsIMS.GetSQLItem("select top 1 left(ClearerNo,charindex('/',ClearerNo)) from vwDolfinPaymentBrokerClearer where ClearerSwiftAddress = '" & cboRDelivAgent.SelectedValue & "' and charindex('/',ClearerNo)<>0 
                                                            union select 'XXXXXXXXXXXX/XXXXXXXXXXXXXXXXX' from vwDolfinPaymentSwiftCodes where SC_CountryCode = 'RU' and SC_SwiftCode = '" & cboRDelivAgent.SelectedValue & "'")
                If InStr(txtRDelivAgentAccountNo.Text, "X", vbTextCompare) <> 0 Then
                    cboRPlaceSettle.SelectedValue = "NADCRUMM"
                End If
            End If
        End If
    End Sub

    Private Sub cboDReceivAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDReceivAgent.SelectedIndexChanged
        If Not cboDReceivAgent.SelectedValue Is Nothing Then
            If cboDReceivAgent.ValueMember <> "" Then
                txtDReceivAgentAccountNo.Text = ClsIMS.GetSQLItem("select top 1 left(ClearerNo,charindex('/',ClearerNo)) from vwDolfinPaymentBrokerClearer where ClearerSwiftAddress = '" & cboDReceivAgent.SelectedValue & "' and charindex('/',ClearerNo)<>0 
                                                            union select 'XXXXXXXXXXXX/XXXXXXXXXXXXXXXXX' from vwDolfinPaymentSwiftCodes where SC_CountryCode = 'RU' and SC_SwiftCode = '" & cboDReceivAgent.SelectedValue & "'")
                If InStr(txtDReceivAgentAccountNo.Text, "X", vbTextCompare) <> 0 Then
                    cboRPlaceSettle.SelectedValue = "NADCRUMM"
                End If
            End If
        End If
    End Sub

    Private Sub cboDBroker_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboDBroker.SelectedIndexChanged
        If IsNumeric(cboDBroker.SelectedValue) Then
            Call PopulateCbo(cboDClearer, "Select ClearerCode,ClearerName from vwDolfinPaymentBrokerClearer where isnull(ClearerSwiftAddress,'')<> '' and isnull(ClearerNo,'')<> '' and BrokerCode = " & cboDBroker.SelectedValue & " group by ClearerCode,ClearerName")
            PopulatePreviewSWIFT()
        End If
    End Sub

    Private Sub PopulateRInstAndCashAccount()
        If IsNumeric(cboRPortfolio.SelectedValue) And IsNumeric(CboRCCY.SelectedValue) Then
            Call PopulateCbo(CboRInstrument, "Select T_Code,T_Name1 from vwDolfinPaymentInstruments where isnull(T_ISIN,'')<>'' and t_currency = " & CboRCCY.SelectedValue & " order by T_Name1")
            Call PopulateCbo(cboRISIN, "Select T_Code,T_ISIN from vwDolfinPaymentInstruments where isnull(T_ISIN,'')<>'' and t_currency = " & CboRCCY.SelectedValue & " order by T_ISIN")
            Call PopulateCbo(cboRCashAcc, "select b_code,b_name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboRPortfolio.SelectedValue & " and cr_code = " & CboRCCY.SelectedValue & " group by b_code,b_name1")
            PopulatePreviewSWIFT()
        End If
    End Sub

    Private Sub PopulateDCashAccount(ByVal varPortfoliocode As Integer, ByVal CCYCode As Integer)
        Call PopulateCbo(cboDCashAcc, "select b_code,b_name1 from vwDolfinPaymentSelectAccount where pf_code = " & varPortfoliocode & " and cr_code = " & CCYCode & " group by b_code,b_name1")
        PopulatePreviewSWIFT()
    End Sub

    Private Sub CboFilterStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboFilterStatus.SelectedIndexChanged
        Cursor = Cursors.WaitCursor
        If CboFilterStatus.Text = "" Then
            ClsIMS.RefreshRDGrid(dgvRD)
        Else
            ClsIMS.RefreshRDGrid(dgvRD, GetFilterWhereClause())
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub dtDFrom_Leave(sender As Object, e As EventArgs) Handles dtDFrom.Leave
        RefreshGridD()
    End Sub

    Private Sub dtDTo_Leave(sender As Object, e As EventArgs) Handles dtDTo.Leave
        RefreshGridD()
    End Sub

    Private Sub cboRPortfolio_Leave(sender As Object, e As EventArgs) Handles cboRPortfolio.Leave
        PopulateRInstAndCashAccount()
    End Sub

    Private Sub CboRCCY_Leave(sender As Object, e As EventArgs) Handles CboRCCY.Leave
        PopulateRInstAndCashAccount()
    End Sub

    Private Sub CboRCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboRCCY.SelectedIndexChanged
        CboRInstrument.Text = ""
        cboRISIN.Text = ""
        cboRCashAcc.Text = ""
    End Sub
End Class