﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmActionRD
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Rb3 = New System.Windows.Forms.RadioButton()
        Me.Rb1 = New System.Windows.Forms.RadioButton()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.Rb2 = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtRDAuthID = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtRDAuthType = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtRDAuthISIN = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtRDAuthSettleDate = New System.Windows.Forms.DateTimePicker()
        Me.lblTransDate = New System.Windows.Forms.Label()
        Me.dtRDAuthTransDate = New System.Windows.Forms.DateTimePicker()
        Me.txtRDAuthDelivAgentAccountNo = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtRDAuthDelivAgent = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtRDAuthClearer = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtRDAuthBroker = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRDAuthInstrument = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtRDAuthPortfolio = New System.Windows.Forms.TextBox()
        Me.lblauthportfolio = New System.Windows.Forms.Label()
        Me.txtRDAuthCCY = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtRDAuthAmount = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.Rb4 = New System.Windows.Forms.RadioButton()
        Me.Rb5 = New System.Windows.Forms.RadioButton()
        Me.Rb6 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Rb3
        '
        Me.Rb3.AutoSize = True
        Me.Rb3.Location = New System.Drawing.Point(49, 422)
        Me.Rb3.Name = "Rb3"
        Me.Rb3.Size = New System.Drawing.Size(138, 17)
        Me.Rb3.TabIndex = 32
        Me.Rb3.Text = "Cancel Free of Payment"
        Me.Rb3.UseVisualStyleBackColor = True
        '
        'Rb1
        '
        Me.Rb1.AutoSize = True
        Me.Rb1.Checked = True
        Me.Rb1.Location = New System.Drawing.Point(49, 376)
        Me.Rb1.Name = "Rb1"
        Me.Rb1.Size = New System.Drawing.Size(123, 17)
        Me.Rb1.TabIndex = 31
        Me.Rb1.TabStop = True
        Me.Rb1.Text = "Edit Free of Payment"
        Me.Rb1.UseVisualStyleBackColor = True
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(10, 532)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(400, 23)
        Me.cmdContinue.TabIndex = 30
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'Rb2
        '
        Me.Rb2.AutoSize = True
        Me.Rb2.Location = New System.Drawing.Point(49, 399)
        Me.Rb2.Name = "Rb2"
        Me.Rb2.Size = New System.Drawing.Size(129, 17)
        Me.Rb2.TabIndex = 29
        Me.Rb2.Text = "Copy Free of Payment"
        Me.Rb2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(73, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(285, 24)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Receive/Deliver Free Options"
        '
        'txtRDAuthID
        '
        Me.txtRDAuthID.AcceptsReturn = True
        Me.txtRDAuthID.AcceptsTab = True
        Me.txtRDAuthID.BackColor = System.Drawing.SystemColors.Control
        Me.txtRDAuthID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtRDAuthID.Enabled = False
        Me.txtRDAuthID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRDAuthID.Location = New System.Drawing.Point(86, 19)
        Me.txtRDAuthID.Multiline = True
        Me.txtRDAuthID.Name = "txtRDAuthID"
        Me.txtRDAuthID.Size = New System.Drawing.Size(103, 25)
        Me.txtRDAuthID.TabIndex = 50
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtRDAuthType)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthISIN)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.dtRDAuthSettleDate)
        Me.GroupBox1.Controls.Add(Me.lblTransDate)
        Me.GroupBox1.Controls.Add(Me.dtRDAuthTransDate)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthDelivAgentAccountNo)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthDelivAgent)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthClearer)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthBroker)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthInstrument)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthPortfolio)
        Me.GroupBox1.Controls.Add(Me.lblauthportfolio)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthCCY)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthAmount)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblportfoliofee)
        Me.GroupBox1.Controls.Add(Me.txtRDAuthID)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 37)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(394, 315)
        Me.GroupBox1.TabIndex = 51
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Summary Details"
        '
        'txtRDAuthType
        '
        Me.txtRDAuthType.Enabled = False
        Me.txtRDAuthType.Location = New System.Drawing.Point(86, 51)
        Me.txtRDAuthType.Name = "txtRDAuthType"
        Me.txtRDAuthType.Size = New System.Drawing.Size(291, 20)
        Me.txtRDAuthType.TabIndex = 104
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(44, 54)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 103
        Me.Label11.Text = "Type:"
        '
        'txtRDAuthISIN
        '
        Me.txtRDAuthISIN.Enabled = False
        Me.txtRDAuthISIN.Location = New System.Drawing.Point(86, 180)
        Me.txtRDAuthISIN.Name = "txtRDAuthISIN"
        Me.txtRDAuthISIN.Size = New System.Drawing.Size(119, 20)
        Me.txtRDAuthISIN.TabIndex = 102
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(47, 183)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(31, 13)
        Me.Label10.TabIndex = 101
        Me.Label10.Text = "ISIN:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Enabled = False
        Me.Label9.Location = New System.Drawing.Point(215, 130)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 13)
        Me.Label9.TabIndex = 100
        Me.Label9.Text = "Settle:"
        '
        'dtRDAuthSettleDate
        '
        Me.dtRDAuthSettleDate.Enabled = False
        Me.dtRDAuthSettleDate.Location = New System.Drawing.Point(258, 130)
        Me.dtRDAuthSettleDate.Name = "dtRDAuthSettleDate"
        Me.dtRDAuthSettleDate.Size = New System.Drawing.Size(119, 20)
        Me.dtRDAuthSettleDate.TabIndex = 99
        '
        'lblTransDate
        '
        Me.lblTransDate.AutoSize = True
        Me.lblTransDate.Location = New System.Drawing.Point(17, 130)
        Me.lblTransDate.Name = "lblTransDate"
        Me.lblTransDate.Size = New System.Drawing.Size(63, 13)
        Me.lblTransDate.TabIndex = 98
        Me.lblTransDate.Text = "Trans Date:"
        '
        'dtRDAuthTransDate
        '
        Me.dtRDAuthTransDate.Enabled = False
        Me.dtRDAuthTransDate.Location = New System.Drawing.Point(86, 129)
        Me.dtRDAuthTransDate.Name = "dtRDAuthTransDate"
        Me.dtRDAuthTransDate.Size = New System.Drawing.Size(119, 20)
        Me.dtRDAuthTransDate.TabIndex = 97
        '
        'txtRDAuthDelivAgentAccountNo
        '
        Me.txtRDAuthDelivAgentAccountNo.Enabled = False
        Me.txtRDAuthDelivAgentAccountNo.Location = New System.Drawing.Point(86, 284)
        Me.txtRDAuthDelivAgentAccountNo.Name = "txtRDAuthDelivAgentAccountNo"
        Me.txtRDAuthDelivAgentAccountNo.Size = New System.Drawing.Size(291, 20)
        Me.txtRDAuthDelivAgentAccountNo.TabIndex = 96
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 287)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(64, 13)
        Me.Label8.TabIndex = 95
        Me.Label8.Text = "DA Acc No:"
        '
        'txtRDAuthDelivAgent
        '
        Me.txtRDAuthDelivAgent.Enabled = False
        Me.txtRDAuthDelivAgent.Location = New System.Drawing.Point(86, 258)
        Me.txtRDAuthDelivAgent.Name = "txtRDAuthDelivAgent"
        Me.txtRDAuthDelivAgent.Size = New System.Drawing.Size(291, 20)
        Me.txtRDAuthDelivAgent.TabIndex = 94
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 261)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 13)
        Me.Label7.TabIndex = 93
        Me.Label7.Text = "Deliv Agent:"
        '
        'txtRDAuthClearer
        '
        Me.txtRDAuthClearer.Enabled = False
        Me.txtRDAuthClearer.Location = New System.Drawing.Point(86, 232)
        Me.txtRDAuthClearer.Name = "txtRDAuthClearer"
        Me.txtRDAuthClearer.Size = New System.Drawing.Size(291, 20)
        Me.txtRDAuthClearer.TabIndex = 92
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(37, 235)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 91
        Me.Label4.Text = "Clearer:"
        '
        'txtRDAuthBroker
        '
        Me.txtRDAuthBroker.Enabled = False
        Me.txtRDAuthBroker.Location = New System.Drawing.Point(86, 206)
        Me.txtRDAuthBroker.Name = "txtRDAuthBroker"
        Me.txtRDAuthBroker.Size = New System.Drawing.Size(291, 20)
        Me.txtRDAuthBroker.TabIndex = 90
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(39, 209)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Broker:"
        '
        'txtRDAuthInstrument
        '
        Me.txtRDAuthInstrument.Enabled = False
        Me.txtRDAuthInstrument.Location = New System.Drawing.Point(86, 155)
        Me.txtRDAuthInstrument.Name = "txtRDAuthInstrument"
        Me.txtRDAuthInstrument.Size = New System.Drawing.Size(291, 20)
        Me.txtRDAuthInstrument.TabIndex = 88
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(21, 155)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 87
        Me.Label6.Text = "Instrument:"
        '
        'txtRDAuthPortfolio
        '
        Me.txtRDAuthPortfolio.Enabled = False
        Me.txtRDAuthPortfolio.Location = New System.Drawing.Point(86, 77)
        Me.txtRDAuthPortfolio.Name = "txtRDAuthPortfolio"
        Me.txtRDAuthPortfolio.Size = New System.Drawing.Size(291, 20)
        Me.txtRDAuthPortfolio.TabIndex = 86
        '
        'lblauthportfolio
        '
        Me.lblauthportfolio.AutoSize = True
        Me.lblauthportfolio.Location = New System.Drawing.Point(30, 80)
        Me.lblauthportfolio.Name = "lblauthportfolio"
        Me.lblauthportfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblauthportfolio.TabIndex = 85
        Me.lblauthportfolio.Text = "Portfolio:"
        '
        'txtRDAuthCCY
        '
        Me.txtRDAuthCCY.Enabled = False
        Me.txtRDAuthCCY.Location = New System.Drawing.Point(86, 103)
        Me.txtRDAuthCCY.Name = "txtRDAuthCCY"
        Me.txtRDAuthCCY.Size = New System.Drawing.Size(72, 20)
        Me.txtRDAuthCCY.TabIndex = 84
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(49, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "CCY:"
        '
        'txtRDAuthAmount
        '
        Me.txtRDAuthAmount.Enabled = False
        Me.txtRDAuthAmount.Location = New System.Drawing.Point(258, 104)
        Me.txtRDAuthAmount.Name = "txtRDAuthAmount"
        Me.txtRDAuthAmount.Size = New System.Drawing.Size(119, 20)
        Me.txtRDAuthAmount.TabIndex = 82
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Enabled = False
        Me.Label3.Location = New System.Drawing.Point(206, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "Amount:"
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Enabled = False
        Me.lblportfoliofee.Location = New System.Drawing.Point(179, 84)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 76
        '
        'Rb4
        '
        Me.Rb4.AutoSize = True
        Me.Rb4.Location = New System.Drawing.Point(49, 445)
        Me.Rb4.Name = "Rb4"
        Me.Rb4.Size = New System.Drawing.Size(267, 17)
        Me.Rb4.TabIndex = 52
        Me.Rb4.Text = "Manually completed Free of Payment (Send to IMS)"
        Me.Rb4.UseVisualStyleBackColor = True
        '
        'Rb5
        '
        Me.Rb5.AutoSize = True
        Me.Rb5.Location = New System.Drawing.Point(49, 491)
        Me.Rb5.Name = "Rb5"
        Me.Rb5.Size = New System.Drawing.Size(289, 17)
        Me.Rb5.TabIndex = 53
        Me.Rb5.Text = "Re-Send Free of Payment to IMS (If status on IMS Error)"
        Me.Rb5.UseVisualStyleBackColor = True
        '
        'Rb6
        '
        Me.Rb6.AutoSize = True
        Me.Rb6.Location = New System.Drawing.Point(49, 468)
        Me.Rb6.Name = "Rb6"
        Me.Rb6.Size = New System.Drawing.Size(297, 17)
        Me.Rb6.TabIndex = 54
        Me.Rb6.Text = "Manually completed Free of Payment (NO SEND TO IMS)"
        Me.Rb6.UseVisualStyleBackColor = True
        '
        'FrmActionRD
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(422, 567)
        Me.Controls.Add(Me.Rb6)
        Me.Controls.Add(Me.Rb5)
        Me.Controls.Add(Me.Rb4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Rb3)
        Me.Controls.Add(Me.Rb1)
        Me.Controls.Add(Me.cmdContinue)
        Me.Controls.Add(Me.Rb2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmActionRD"
        Me.Text = "Action Receive/Deliver"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Rb3 As RadioButton
    Friend WithEvents Rb1 As RadioButton
    Friend WithEvents cmdContinue As Button
    Friend WithEvents Rb2 As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents txtRDAuthID As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents txtRDAuthPortfolio As TextBox
    Friend WithEvents lblauthportfolio As Label
    Friend WithEvents txtRDAuthCCY As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtRDAuthAmount As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtRDAuthDelivAgentAccountNo As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtRDAuthDelivAgent As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtRDAuthClearer As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtRDAuthBroker As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtRDAuthInstrument As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents dtRDAuthSettleDate As DateTimePicker
    Friend WithEvents lblTransDate As Label
    Friend WithEvents dtRDAuthTransDate As DateTimePicker
    Friend WithEvents Label9 As Label
    Friend WithEvents txtRDAuthISIN As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtRDAuthType As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Rb4 As RadioButton
    Friend WithEvents Rb5 As RadioButton
    Friend WithEvents Rb6 As RadioButton
End Class
