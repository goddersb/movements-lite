'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class vwDolfinPaymentSelectAccountTreasury
    Public Property CCYCode As Integer
    Public Property CCY As String
    Public Property BrokerCodeA As Integer
    Public Property BrokerA As String
    Public Property PortfolioCodeA As Integer
    Public Property PortfolioA As String
    Public Property AccountCodeA As Integer
    Public Property AccountA As String
    Public Property BalanceA As String
    Public Property BrokerCodeB As Integer
    Public Property BrokerB As String
    Public Property PortfolioCodeB As Integer
    Public Property PortfolioB As String
    Public Property AccountCodeB As Integer
    Public Property AccountB As String
    Public Property BalanceB As String
    Public Property PortfolioShortcutA As String
    Public Property AccountShortcutA As String
    Public Property BrokerShortcutA As String
    Public Property PortfolioShortcutB As String
    Public Property AccountShortcutB As String
    Public Property BrokerShortcutB As String
    Public Property PT_IMSPayINCode As String
    Public Property PT_IMSPayOutCode As String

End Class
