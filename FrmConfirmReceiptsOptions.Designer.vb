﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfirmReceiptsOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConfirmReceiptsOptions))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCRMFNotes = New System.Windows.Forms.TextBox()
        Me.CRMFFiles = New System.Windows.Forms.ListView()
        Me.txtCRMType = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCRMBankName = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtRDAuthSettleDate = New System.Windows.Forms.DateTimePicker()
        Me.lblTransDate = New System.Windows.Forms.Label()
        Me.dtRDAuthTransDate = New System.Windows.Forms.DateTimePicker()
        Me.txtCRMPortfolio = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCRMSender = New System.Windows.Forms.TextBox()
        Me.lblauthben = New System.Windows.Forms.Label()
        Me.txtCRMCCY = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCRMAmt = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.txtCRMID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Rb1 = New System.Windows.Forms.RadioButton()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.Rb2 = New System.Windows.Forms.RadioButton()
        Me.Rb3 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtCRMFNotes)
        Me.GroupBox1.Controls.Add(Me.CRMFFiles)
        Me.GroupBox1.Controls.Add(Me.txtCRMType)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtCRMBankName)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.dtRDAuthSettleDate)
        Me.GroupBox1.Controls.Add(Me.lblTransDate)
        Me.GroupBox1.Controls.Add(Me.dtRDAuthTransDate)
        Me.GroupBox1.Controls.Add(Me.txtCRMPortfolio)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtCRMSender)
        Me.GroupBox1.Controls.Add(Me.lblauthben)
        Me.GroupBox1.Controls.Add(Me.txtCRMCCY)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtCRMAmt)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblportfoliofee)
        Me.GroupBox1.Controls.Add(Me.txtCRMID)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 37)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(394, 340)
        Me.GroupBox1.TabIndex = 67
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Summary Details"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 279)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 108
        Me.Label4.Text = "Documents:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 207)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 107
        Me.Label1.Text = "Notes:"
        '
        'txtCRMFNotes
        '
        Me.txtCRMFNotes.Enabled = False
        Me.txtCRMFNotes.Location = New System.Drawing.Point(70, 204)
        Me.txtCRMFNotes.Multiline = True
        Me.txtCRMFNotes.Name = "txtCRMFNotes"
        Me.txtCRMFNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCRMFNotes.Size = New System.Drawing.Size(318, 69)
        Me.txtCRMFNotes.TabIndex = 106
        '
        'CRMFFiles
        '
        Me.CRMFFiles.AllowDrop = True
        Me.CRMFFiles.BackColor = System.Drawing.SystemColors.Window
        Me.CRMFFiles.BackgroundImage = CType(resources.GetObject("CRMFFiles.BackgroundImage"), System.Drawing.Image)
        Me.CRMFFiles.FullRowSelect = True
        Me.CRMFFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.CRMFFiles.Location = New System.Drawing.Point(70, 279)
        Me.CRMFFiles.MultiSelect = False
        Me.CRMFFiles.Name = "CRMFFiles"
        Me.CRMFFiles.Size = New System.Drawing.Size(318, 51)
        Me.CRMFFiles.TabIndex = 105
        Me.CRMFFiles.UseCompatibleStateImageBehavior = False
        '
        'txtCRMType
        '
        Me.txtCRMType.Enabled = False
        Me.txtCRMType.Location = New System.Drawing.Point(241, 24)
        Me.txtCRMType.Name = "txtCRMType"
        Me.txtCRMType.Size = New System.Drawing.Size(147, 20)
        Me.txtCRMType.TabIndex = 104
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(199, 27)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 103
        Me.Label11.Text = "Type:"
        '
        'txtCRMBankName
        '
        Me.txtCRMBankName.Enabled = False
        Me.txtCRMBankName.Location = New System.Drawing.Point(70, 178)
        Me.txtCRMBankName.Name = "txtCRMBankName"
        Me.txtCRMBankName.Size = New System.Drawing.Size(318, 20)
        Me.txtCRMBankName.TabIndex = 102
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(1, 181)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 13)
        Me.Label10.TabIndex = 101
        Me.Label10.Text = "Bank Name:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Enabled = False
        Me.Label9.Location = New System.Drawing.Point(189, 130)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(63, 13)
        Me.Label9.TabIndex = 100
        Me.Label9.Text = "Settle Date:"
        '
        'dtRDAuthSettleDate
        '
        Me.dtRDAuthSettleDate.Enabled = False
        Me.dtRDAuthSettleDate.Location = New System.Drawing.Point(258, 130)
        Me.dtRDAuthSettleDate.Name = "dtRDAuthSettleDate"
        Me.dtRDAuthSettleDate.Size = New System.Drawing.Size(130, 20)
        Me.dtRDAuthSettleDate.TabIndex = 99
        '
        'lblTransDate
        '
        Me.lblTransDate.AutoSize = True
        Me.lblTransDate.Location = New System.Drawing.Point(4, 130)
        Me.lblTransDate.Name = "lblTransDate"
        Me.lblTransDate.Size = New System.Drawing.Size(64, 13)
        Me.lblTransDate.TabIndex = 98
        Me.lblTransDate.Text = "Trade Date:"
        '
        'dtRDAuthTransDate
        '
        Me.dtRDAuthTransDate.Enabled = False
        Me.dtRDAuthTransDate.Location = New System.Drawing.Point(70, 130)
        Me.dtRDAuthTransDate.Name = "dtRDAuthTransDate"
        Me.dtRDAuthTransDate.Size = New System.Drawing.Size(119, 20)
        Me.dtRDAuthTransDate.TabIndex = 97
        '
        'txtCRMPortfolio
        '
        Me.txtCRMPortfolio.Enabled = False
        Me.txtCRMPortfolio.Location = New System.Drawing.Point(70, 155)
        Me.txtCRMPortfolio.Name = "txtCRMPortfolio"
        Me.txtCRMPortfolio.Size = New System.Drawing.Size(318, 20)
        Me.txtCRMPortfolio.TabIndex = 88
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 158)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 13)
        Me.Label6.TabIndex = 87
        Me.Label6.Text = "Portfolio:"
        '
        'txtCRMSender
        '
        Me.txtCRMSender.Enabled = False
        Me.txtCRMSender.Location = New System.Drawing.Point(70, 50)
        Me.txtCRMSender.Multiline = True
        Me.txtCRMSender.Name = "txtCRMSender"
        Me.txtCRMSender.Size = New System.Drawing.Size(318, 47)
        Me.txtCRMSender.TabIndex = 86
        '
        'lblauthben
        '
        Me.lblauthben.AutoSize = True
        Me.lblauthben.Location = New System.Drawing.Point(20, 53)
        Me.lblauthben.Name = "lblauthben"
        Me.lblauthben.Size = New System.Drawing.Size(44, 13)
        Me.lblauthben.TabIndex = 85
        Me.lblauthben.Text = "Sender:"
        '
        'txtCRMCCY
        '
        Me.txtCRMCCY.Enabled = False
        Me.txtCRMCCY.Location = New System.Drawing.Point(70, 103)
        Me.txtCRMCCY.Name = "txtCRMCCY"
        Me.txtCRMCCY.Size = New System.Drawing.Size(72, 20)
        Me.txtCRMCCY.TabIndex = 84
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(36, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "CCY:"
        '
        'txtCRMAmt
        '
        Me.txtCRMAmt.Enabled = False
        Me.txtCRMAmt.Location = New System.Drawing.Point(258, 104)
        Me.txtCRMAmt.Name = "txtCRMAmt"
        Me.txtCRMAmt.Size = New System.Drawing.Size(130, 20)
        Me.txtCRMAmt.TabIndex = 82
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Enabled = False
        Me.Label3.Location = New System.Drawing.Point(206, 106)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "Amount:"
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Enabled = False
        Me.lblportfoliofee.Location = New System.Drawing.Point(179, 84)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 76
        '
        'txtCRMID
        '
        Me.txtCRMID.AcceptsReturn = True
        Me.txtCRMID.AcceptsTab = True
        Me.txtCRMID.BackColor = System.Drawing.Color.OldLace
        Me.txtCRMID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCRMID.Enabled = False
        Me.txtCRMID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCRMID.Location = New System.Drawing.Point(86, 19)
        Me.txtCRMID.Multiline = True
        Me.txtCRMID.Name = "txtCRMID"
        Me.txtCRMID.Size = New System.Drawing.Size(103, 25)
        Me.txtCRMID.TabIndex = 50
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(127, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(169, 24)
        Me.Label2.TabIndex = 66
        Me.Label2.Text = "Receipts Options"
        '
        'Rb1
        '
        Me.Rb1.AutoSize = True
        Me.Rb1.Location = New System.Drawing.Point(86, 390)
        Me.Rb1.Name = "Rb1"
        Me.Rb1.Size = New System.Drawing.Size(146, 17)
        Me.Rb1.TabIndex = 64
        Me.Rb1.Text = "Authorise MANUAL funds"
        Me.Rb1.UseVisualStyleBackColor = True
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(11, 477)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(394, 23)
        Me.cmdContinue.TabIndex = 63
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'Rb2
        '
        Me.Rb2.AutoSize = True
        Me.Rb2.Location = New System.Drawing.Point(87, 413)
        Me.Rb2.Name = "Rb2"
        Me.Rb2.Size = New System.Drawing.Size(145, 17)
        Me.Rb2.TabIndex = 62
        Me.Rb2.Text = "Remove MANUAL Funds"
        Me.Rb2.UseVisualStyleBackColor = True
        '
        'Rb3
        '
        Me.Rb3.AutoSize = True
        Me.Rb3.Checked = True
        Me.Rb3.Location = New System.Drawing.Point(87, 436)
        Me.Rb3.Name = "Rb3"
        Me.Rb3.Size = New System.Drawing.Size(98, 17)
        Me.Rb3.TabIndex = 65
        Me.Rb3.TabStop = True
        Me.Rb3.Text = "View Audit Trail"
        Me.Rb3.UseVisualStyleBackColor = True
        '
        'FrmConfirmReceiptsOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(424, 511)
        Me.Controls.Add(Me.Rb3)
        Me.Controls.Add(Me.Rb1)
        Me.Controls.Add(Me.Rb2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmdContinue)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmConfirmReceiptsOptions"
        Me.Text = "Confirm Receipts Options"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtCRMType As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtCRMBankName As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents dtRDAuthSettleDate As DateTimePicker
    Friend WithEvents lblTransDate As Label
    Friend WithEvents dtRDAuthTransDate As DateTimePicker
    Friend WithEvents txtCRMPortfolio As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtCRMSender As TextBox
    Friend WithEvents lblauthben As Label
    Friend WithEvents txtCRMCCY As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtCRMAmt As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents txtCRMID As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Rb1 As RadioButton
    Friend WithEvents cmdContinue As Button
    Friend WithEvents Rb2 As RadioButton
    Friend WithEvents Rb3 As RadioButton
    Friend WithEvents CRMFFiles As ListView
    Friend WithEvents Label4 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCRMFNotes As TextBox
End Class
