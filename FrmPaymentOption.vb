﻿Public Class FrmPaymentOption

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Me.Close()
    End Sub

    Private Sub CmdGenerateEmail_Click(sender As Object, e As EventArgs) Handles CmdGenerateEmail.Click
        ClsPayAuth.SelectedEmailAttachments = False
        FrmEmail.ShowDialog()
    End Sub

    Private Sub CmdGenerateEmailAttached_Click(sender As Object, e As EventArgs) Handles CmdGenerateEmailAttached.Click
        ClsPayAuth.SelectedEmailAttachments = True
        FrmEmail.ShowDialog()
    End Sub

    Private Sub optRb1_CheckedChanged(sender As Object, e As EventArgs) Handles optRb1.CheckedChanged
        ClsPay.SelectedPaymentOption = 1
        ColorRadio(optRb1)
    End Sub

    Private Sub optRb2_CheckedChanged(sender As Object, e As EventArgs) Handles optRb2.CheckedChanged
        ClsPay.SelectedPaymentOption = 2
        ColorRadio(optRb2)
    End Sub

    Private Sub optRb3_CheckedChanged(sender As Object, e As EventArgs) Handles optRb3.CheckedChanged
        ClsPay.SelectedPaymentOption = 3
        ColorRadio(optRb3)
    End Sub

    Private Sub optRb4_CheckedChanged(sender As Object, e As EventArgs) Handles optRb4.CheckedChanged
        ClsPay.SelectedPaymentOption = 4
        ColorRadio(optRb4)
    End Sub

    Private Sub FrmPaymentOption_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        optRb1.Checked = True
        optRb2.Enabled = False
        optRb3.Enabled = False
        If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
            optRb3.Enabled = True
        ElseIf ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExIn) Then
            optRb2.Enabled = True
        End If
        txtMsg.Text = "Please select options for your next movement." & vbNewLine & "Email options include generating a Mail or generating a mail with attachments"
    End Sub

    Private Sub ColorRadio(ByVal Rb As RadioButton)
        If Rb.Checked Then
            Rb.ForeColor = Color.DarkGreen
        Else
            Rb.ForeColor = Color.Black
        End If
    End Sub

End Class