﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReceiveDeliver
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ChkCashRefresh = New System.Windows.Forms.CheckBox()
        Me.CboFilterStatus = New System.Windows.Forms.ComboBox()
        Me.cmdRDRefresh = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.dgvRD = New System.Windows.Forms.DataGridView()
        Me.cmdAddRD = New System.Windows.Forms.Button()
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.RDBox = New System.Windows.Forms.GroupBox()
        Me.TabRD = New System.Windows.Forms.TabControl()
        Me.TabPageReceive = New System.Windows.Forms.TabPage()
        Me.GrpTransType = New System.Windows.Forms.GroupBox()
        Me.ChkRSendIMS = New System.Windows.Forms.CheckBox()
        Me.cboRCashAcc = New System.Windows.Forms.ComboBox()
        Me.cboRClearer = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.cboRPlaceSettle = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtRInstructions = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtRDelivAgentAccountNo = New System.Windows.Forms.TextBox()
        Me.cboRDelivAgent = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CboRCCY = New System.Windows.Forms.ComboBox()
        Me.lblCCY = New System.Windows.Forms.Label()
        Me.lblRMsg = New System.Windows.Forms.Label()
        Me.RRB2 = New System.Windows.Forms.RadioButton()
        Me.RRB1 = New System.Windows.Forms.RadioButton()
        Me.txtRAmount = New System.Windows.Forms.TextBox()
        Me.cboRBroker = New System.Windows.Forms.ComboBox()
        Me.lblTransDate = New System.Windows.Forms.Label()
        Me.dtRTransDate = New System.Windows.Forms.DateTimePicker()
        Me.lblSettleDate = New System.Windows.Forms.Label()
        Me.dtRSettleDate = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboRISIN = New System.Windows.Forms.ComboBox()
        Me.lblCCYAmt = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboRPortfolio = New System.Windows.Forms.ComboBox()
        Me.LblPayType = New System.Windows.Forms.Label()
        Me.CboRInstrument = New System.Windows.Forms.ComboBox()
        Me.TabPageDeliver = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ChkDSendIMS = New System.Windows.Forms.CheckBox()
        Me.cboDCashAcc = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtDAmount = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.dtDTo = New System.Windows.Forms.DateTimePicker()
        Me.dtDFrom = New System.Windows.Forms.DateTimePicker()
        Me.cboDBroker = New System.Windows.Forms.ComboBox()
        Me.cboDClearer = New System.Windows.Forms.ComboBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.dtDTransDate = New System.Windows.Forms.DateTimePicker()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.dtDSettleDate = New System.Windows.Forms.DateTimePicker()
        Me.cboDPlaceSettle = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtDInstructions = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cboDReceivAgent = New System.Windows.Forms.ComboBox()
        Me.txtDReceivAgentAccountNo = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dgvD = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboDPortfolio = New System.Windows.Forms.ComboBox()
        Me.TabPageRDPreview = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtSWIFT = New System.Windows.Forms.TextBox()
        Me.TimerRefreshRD = New System.Windows.Forms.Timer(Me.components)
        Me.cboSwiftCheck = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        CType(Me.dgvRD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RDBox.SuspendLayout()
        Me.TabRD.SuspendLayout()
        Me.TabPageReceive.SuspendLayout()
        Me.GrpTransType.SuspendLayout()
        Me.TabPageDeliver.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageRDPreview.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChkCashRefresh
        '
        Me.ChkCashRefresh.AutoSize = True
        Me.ChkCashRefresh.Checked = True
        Me.ChkCashRefresh.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCashRefresh.ForeColor = System.Drawing.Color.Maroon
        Me.ChkCashRefresh.Location = New System.Drawing.Point(6, 706)
        Me.ChkCashRefresh.Name = "ChkCashRefresh"
        Me.ChkCashRefresh.Size = New System.Drawing.Size(146, 17)
        Me.ChkCashRefresh.TabIndex = 59
        Me.ChkCashRefresh.Text = "Auto Refresh Grid On/Off"
        Me.ChkCashRefresh.UseVisualStyleBackColor = True
        '
        'CboFilterStatus
        '
        Me.CboFilterStatus.FormattingEnabled = True
        Me.CboFilterStatus.Location = New System.Drawing.Point(1013, 422)
        Me.CboFilterStatus.Name = "CboFilterStatus"
        Me.CboFilterStatus.Size = New System.Drawing.Size(138, 21)
        Me.CboFilterStatus.TabIndex = 58
        '
        'cmdRDRefresh
        '
        Me.cmdRDRefresh.Location = New System.Drawing.Point(6, 420)
        Me.cmdRDRefresh.Name = "cmdRDRefresh"
        Me.cmdRDRefresh.Size = New System.Drawing.Size(255, 23)
        Me.cmdRDRefresh.TabIndex = 53
        Me.cmdRDRefresh.Text = "Refresh Transactions"
        Me.cmdRDRefresh.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(945, 425)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 57
        Me.Label10.Text = "Filter Status"
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Location = New System.Drawing.Point(900, 706)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(251, 24)
        Me.cmdSubmit.TabIndex = 56
        Me.cmdSubmit.Text = "Submit Transactions"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'dgvRD
        '
        Me.dgvRD.AllowUserToAddRows = False
        Me.dgvRD.AllowUserToDeleteRows = False
        Me.dgvRD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRD.Location = New System.Drawing.Point(7, 446)
        Me.dgvRD.MultiSelect = False
        Me.dgvRD.Name = "dgvRD"
        Me.dgvRD.ReadOnly = True
        Me.dgvRD.Size = New System.Drawing.Size(1144, 254)
        Me.dgvRD.TabIndex = 55
        '
        'cmdAddRD
        '
        Me.cmdAddRD.Location = New System.Drawing.Point(888, 313)
        Me.cmdAddRD.Name = "cmdAddRD"
        Me.cmdAddRD.Size = New System.Drawing.Size(255, 24)
        Me.cmdAddRD.TabIndex = 14
        Me.cmdAddRD.Text = "Add Transaction"
        Me.cmdAddRD.UseVisualStyleBackColor = True
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(440, 16)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(207, 24)
        Me.lblpaymentTitle.TabIndex = 61
        Me.lblpaymentTitle.Text = "Receive/Deliver Free"
        '
        'RDBox
        '
        Me.RDBox.Controls.Add(Me.TabRD)
        Me.RDBox.Controls.Add(Me.cmdAddRD)
        Me.RDBox.Location = New System.Drawing.Point(12, 43)
        Me.RDBox.Name = "RDBox"
        Me.RDBox.Size = New System.Drawing.Size(1151, 343)
        Me.RDBox.TabIndex = 62
        Me.RDBox.TabStop = False
        Me.RDBox.Text = "Receive/Deliver Details"
        '
        'TabRD
        '
        Me.TabRD.Controls.Add(Me.TabPageReceive)
        Me.TabRD.Controls.Add(Me.TabPageDeliver)
        Me.TabRD.Controls.Add(Me.TabPageRDPreview)
        Me.TabRD.Location = New System.Drawing.Point(6, 19)
        Me.TabRD.Name = "TabRD"
        Me.TabRD.SelectedIndex = 0
        Me.TabRD.Size = New System.Drawing.Size(1137, 286)
        Me.TabRD.TabIndex = 61
        '
        'TabPageReceive
        '
        Me.TabPageReceive.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPageReceive.Controls.Add(Me.GrpTransType)
        Me.TabPageReceive.Location = New System.Drawing.Point(4, 22)
        Me.TabPageReceive.Name = "TabPageReceive"
        Me.TabPageReceive.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageReceive.Size = New System.Drawing.Size(1129, 260)
        Me.TabPageReceive.TabIndex = 0
        Me.TabPageReceive.Text = "Receive Free"
        '
        'GrpTransType
        '
        Me.GrpTransType.Controls.Add(Me.ChkRSendIMS)
        Me.GrpTransType.Controls.Add(Me.cboRCashAcc)
        Me.GrpTransType.Controls.Add(Me.cboRClearer)
        Me.GrpTransType.Controls.Add(Me.Label25)
        Me.GrpTransType.Controls.Add(Me.cboRPlaceSettle)
        Me.GrpTransType.Controls.Add(Me.Label16)
        Me.GrpTransType.Controls.Add(Me.txtRInstructions)
        Me.GrpTransType.Controls.Add(Me.Label15)
        Me.GrpTransType.Controls.Add(Me.txtRDelivAgentAccountNo)
        Me.GrpTransType.Controls.Add(Me.cboRDelivAgent)
        Me.GrpTransType.Controls.Add(Me.Label6)
        Me.GrpTransType.Controls.Add(Me.Label7)
        Me.GrpTransType.Controls.Add(Me.Label8)
        Me.GrpTransType.Controls.Add(Me.CboRCCY)
        Me.GrpTransType.Controls.Add(Me.lblCCY)
        Me.GrpTransType.Controls.Add(Me.lblRMsg)
        Me.GrpTransType.Controls.Add(Me.RRB2)
        Me.GrpTransType.Controls.Add(Me.RRB1)
        Me.GrpTransType.Controls.Add(Me.txtRAmount)
        Me.GrpTransType.Controls.Add(Me.cboRBroker)
        Me.GrpTransType.Controls.Add(Me.lblTransDate)
        Me.GrpTransType.Controls.Add(Me.dtRTransDate)
        Me.GrpTransType.Controls.Add(Me.lblSettleDate)
        Me.GrpTransType.Controls.Add(Me.dtRSettleDate)
        Me.GrpTransType.Controls.Add(Me.Label4)
        Me.GrpTransType.Controls.Add(Me.Label3)
        Me.GrpTransType.Controls.Add(Me.cboRISIN)
        Me.GrpTransType.Controls.Add(Me.lblCCYAmt)
        Me.GrpTransType.Controls.Add(Me.lblportfoliofee)
        Me.GrpTransType.Controls.Add(Me.Label1)
        Me.GrpTransType.Controls.Add(Me.cboRPortfolio)
        Me.GrpTransType.Controls.Add(Me.LblPayType)
        Me.GrpTransType.Controls.Add(Me.CboRInstrument)
        Me.GrpTransType.Location = New System.Drawing.Point(6, 6)
        Me.GrpTransType.Name = "GrpTransType"
        Me.GrpTransType.Size = New System.Drawing.Size(1117, 244)
        Me.GrpTransType.TabIndex = 42
        Me.GrpTransType.TabStop = False
        Me.GrpTransType.Text = "Receive Free Details"
        '
        'ChkRSendIMS
        '
        Me.ChkRSendIMS.AutoSize = True
        Me.ChkRSendIMS.Checked = True
        Me.ChkRSendIMS.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkRSendIMS.Location = New System.Drawing.Point(930, 81)
        Me.ChkRSendIMS.Name = "ChkRSendIMS"
        Me.ChkRSendIMS.Size = New System.Drawing.Size(170, 17)
        Me.ChkRSendIMS.TabIndex = 4
        Me.ChkRSendIMS.Text = "Send To IMS (After Confirmed)"
        Me.ChkRSendIMS.UseVisualStyleBackColor = True
        '
        'cboRCashAcc
        '
        Me.cboRCashAcc.FormattingEnabled = True
        Me.cboRCashAcc.Location = New System.Drawing.Point(357, 188)
        Me.cboRCashAcc.Name = "cboRCashAcc"
        Me.cboRCashAcc.Size = New System.Drawing.Size(195, 21)
        Me.cboRCashAcc.TabIndex = 11
        '
        'cboRClearer
        '
        Me.cboRClearer.FormattingEnabled = True
        Me.cboRClearer.Location = New System.Drawing.Point(89, 215)
        Me.cboRClearer.Name = "cboRClearer"
        Me.cboRClearer.Size = New System.Drawing.Size(463, 21)
        Me.cboRClearer.TabIndex = 12
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(295, 191)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(56, 13)
        Me.Label25.TabIndex = 83
        Me.Label25.Text = "Cash Acc:"
        '
        'cboRPlaceSettle
        '
        Me.cboRPlaceSettle.FormattingEnabled = True
        Me.cboRPlaceSettle.Location = New System.Drawing.Point(652, 211)
        Me.cboRPlaceSettle.Name = "cboRPlaceSettle"
        Me.cboRPlaceSettle.Size = New System.Drawing.Size(458, 21)
        Me.cboRPlaceSettle.TabIndex = 15
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(567, 213)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(79, 13)
        Me.Label16.TabIndex = 82
        Me.Label16.Text = "Place of Settle:"
        '
        'txtRInstructions
        '
        Me.txtRInstructions.Location = New System.Drawing.Point(89, 159)
        Me.txtRInstructions.Name = "txtRInstructions"
        Me.txtRInstructions.Size = New System.Drawing.Size(463, 20)
        Me.txtRInstructions.TabIndex = 9
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(5, 162)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 13)
        Me.Label15.TabIndex = 80
        Me.Label15.Text = "Instructions/ID:"
        '
        'txtRDelivAgentAccountNo
        '
        Me.txtRDelivAgentAccountNo.Location = New System.Drawing.Point(652, 185)
        Me.txtRDelivAgentAccountNo.Name = "txtRDelivAgentAccountNo"
        Me.txtRDelivAgentAccountNo.Size = New System.Drawing.Size(458, 20)
        Me.txtRDelivAgentAccountNo.TabIndex = 14
        '
        'cboRDelivAgent
        '
        Me.cboRDelivAgent.FormattingEnabled = True
        Me.cboRDelivAgent.Location = New System.Drawing.Point(652, 157)
        Me.cboRDelivAgent.Name = "cboRDelivAgent"
        Me.cboRDelivAgent.Size = New System.Drawing.Size(458, 21)
        Me.cboRDelivAgent.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(562, 188)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 76
        Me.Label6.Text = "Del Agt Acc No:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(644, 176)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(0, 15)
        Me.Label7.TabIndex = 75
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(581, 160)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 13)
        Me.Label8.TabIndex = 74
        Me.Label8.Text = "Deliv Agent:"
        '
        'CboRCCY
        '
        Me.CboRCCY.FormattingEnabled = True
        Me.CboRCCY.Location = New System.Drawing.Point(807, 79)
        Me.CboRCCY.Name = "CboRCCY"
        Me.CboRCCY.Size = New System.Drawing.Size(81, 21)
        Me.CboRCCY.TabIndex = 3
        '
        'lblCCY
        '
        Me.lblCCY.AutoSize = True
        Me.lblCCY.Location = New System.Drawing.Point(770, 83)
        Me.lblCCY.Name = "lblCCY"
        Me.lblCCY.Size = New System.Drawing.Size(31, 13)
        Me.lblCCY.TabIndex = 73
        Me.lblCCY.Text = "CCY:"
        '
        'lblRMsg
        '
        Me.lblRMsg.AutoSize = True
        Me.lblRMsg.ForeColor = System.Drawing.Color.DarkRed
        Me.lblRMsg.Location = New System.Drawing.Point(19, 20)
        Me.lblRMsg.Name = "lblRMsg"
        Me.lblRMsg.Size = New System.Drawing.Size(39, 13)
        Me.lblRMsg.TabIndex = 71
        Me.lblRMsg.Text = "Label2"
        '
        'RRB2
        '
        Me.RRB2.AutoSize = True
        Me.RRB2.Location = New System.Drawing.Point(37, 132)
        Me.RRB2.Name = "RRB2"
        Me.RRB2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.RRB2.Size = New System.Drawing.Size(46, 17)
        Me.RRB2.TabIndex = 6
        Me.RRB2.TabStop = True
        Me.RRB2.Text = "ISIN"
        Me.RRB2.UseVisualStyleBackColor = True
        '
        'RRB1
        '
        Me.RRB1.AutoSize = True
        Me.RRB1.Location = New System.Drawing.Point(9, 105)
        Me.RRB1.Name = "RRB1"
        Me.RRB1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.RRB1.Size = New System.Drawing.Size(74, 17)
        Me.RRB1.TabIndex = 4
        Me.RRB1.TabStop = True
        Me.RRB1.Text = "Instrument"
        Me.RRB1.UseVisualStyleBackColor = True
        '
        'txtRAmount
        '
        Me.txtRAmount.Location = New System.Drawing.Point(654, 80)
        Me.txtRAmount.Name = "txtRAmount"
        Me.txtRAmount.Size = New System.Drawing.Size(103, 20)
        Me.txtRAmount.TabIndex = 2
        '
        'cboRBroker
        '
        Me.cboRBroker.FormattingEnabled = True
        Me.cboRBroker.Location = New System.Drawing.Point(89, 188)
        Me.cboRBroker.Name = "cboRBroker"
        Me.cboRBroker.Size = New System.Drawing.Size(195, 21)
        Me.cboRBroker.TabIndex = 10
        '
        'lblTransDate
        '
        Me.lblTransDate.AutoSize = True
        Me.lblTransDate.Location = New System.Drawing.Point(585, 107)
        Me.lblTransDate.Name = "lblTransDate"
        Me.lblTransDate.Size = New System.Drawing.Size(63, 13)
        Me.lblTransDate.TabIndex = 66
        Me.lblTransDate.Text = "Trans Date:"
        '
        'dtRTransDate
        '
        Me.dtRTransDate.Location = New System.Drawing.Point(654, 104)
        Me.dtRTransDate.Name = "dtRTransDate"
        Me.dtRTransDate.Size = New System.Drawing.Size(128, 20)
        Me.dtRTransDate.TabIndex = 7
        '
        'lblSettleDate
        '
        Me.lblSettleDate.AutoSize = True
        Me.lblSettleDate.Location = New System.Drawing.Point(585, 134)
        Me.lblSettleDate.Name = "lblSettleDate"
        Me.lblSettleDate.Size = New System.Drawing.Size(63, 13)
        Me.lblSettleDate.TabIndex = 65
        Me.lblSettleDate.Text = "Settle Date:"
        '
        'dtRSettleDate
        '
        Me.dtRSettleDate.Location = New System.Drawing.Point(654, 130)
        Me.dtRSettleDate.Name = "dtRSettleDate"
        Me.dtRSettleDate.Size = New System.Drawing.Size(128, 20)
        Me.dtRSettleDate.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(602, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 62
        Me.Label4.Text = "Amount:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(40, 218)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = "Clearer:"
        '
        'cboRISIN
        '
        Me.cboRISIN.Enabled = False
        Me.cboRISIN.FormattingEnabled = True
        Me.cboRISIN.Location = New System.Drawing.Point(89, 131)
        Me.cboRISIN.Name = "cboRISIN"
        Me.cboRISIN.Size = New System.Drawing.Size(463, 21)
        Me.cboRISIN.TabIndex = 6
        '
        'lblCCYAmt
        '
        Me.lblCCYAmt.AutoSize = True
        Me.lblCCYAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCCYAmt.ForeColor = System.Drawing.Color.Black
        Me.lblCCYAmt.Location = New System.Drawing.Point(1034, 105)
        Me.lblCCYAmt.Name = "lblCCYAmt"
        Me.lblCCYAmt.Size = New System.Drawing.Size(0, 15)
        Me.lblCCYAmt.TabIndex = 52
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Location = New System.Drawing.Point(306, 80)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 39
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Portfolio:"
        '
        'cboRPortfolio
        '
        Me.cboRPortfolio.FormattingEnabled = True
        Me.cboRPortfolio.Location = New System.Drawing.Point(89, 77)
        Me.cboRPortfolio.Name = "cboRPortfolio"
        Me.cboRPortfolio.Size = New System.Drawing.Size(463, 21)
        Me.cboRPortfolio.TabIndex = 1
        '
        'LblPayType
        '
        Me.LblPayType.AutoSize = True
        Me.LblPayType.Location = New System.Drawing.Point(32, 191)
        Me.LblPayType.Name = "LblPayType"
        Me.LblPayType.Size = New System.Drawing.Size(51, 13)
        Me.LblPayType.TabIndex = 6
        Me.LblPayType.Text = "Brkr Acc:"
        '
        'CboRInstrument
        '
        Me.CboRInstrument.FormattingEnabled = True
        Me.CboRInstrument.Location = New System.Drawing.Point(89, 104)
        Me.CboRInstrument.Name = "CboRInstrument"
        Me.CboRInstrument.Size = New System.Drawing.Size(463, 21)
        Me.CboRInstrument.TabIndex = 5
        '
        'TabPageDeliver
        '
        Me.TabPageDeliver.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPageDeliver.Controls.Add(Me.GroupBox2)
        Me.TabPageDeliver.Location = New System.Drawing.Point(4, 22)
        Me.TabPageDeliver.Name = "TabPageDeliver"
        Me.TabPageDeliver.Size = New System.Drawing.Size(1129, 260)
        Me.TabPageDeliver.TabIndex = 2
        Me.TabPageDeliver.Text = "Deliver Free"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ChkDSendIMS)
        Me.GroupBox2.Controls.Add(Me.cboDCashAcc)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.txtDAmount)
        Me.GroupBox2.Controls.Add(Me.Label24)
        Me.GroupBox2.Controls.Add(Me.dtDTo)
        Me.GroupBox2.Controls.Add(Me.dtDFrom)
        Me.GroupBox2.Controls.Add(Me.cboDBroker)
        Me.GroupBox2.Controls.Add(Me.cboDClearer)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.dtDTransDate)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.dtDSettleDate)
        Me.GroupBox2.Controls.Add(Me.cboDPlaceSettle)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.txtDInstructions)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Controls.Add(Me.cboDReceivAgent)
        Me.GroupBox2.Controls.Add(Me.txtDReceivAgentAccountNo)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.dgvD)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.cboDPortfolio)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1120, 251)
        Me.GroupBox2.TabIndex = 43
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Deliver Free Details"
        '
        'ChkDSendIMS
        '
        Me.ChkDSendIMS.AutoSize = True
        Me.ChkDSendIMS.Checked = True
        Me.ChkDSendIMS.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkDSendIMS.Location = New System.Drawing.Point(84, 151)
        Me.ChkDSendIMS.Name = "ChkDSendIMS"
        Me.ChkDSendIMS.Size = New System.Drawing.Size(170, 17)
        Me.ChkDSendIMS.TabIndex = 5
        Me.ChkDSendIMS.Text = "Send To IMS (After Confirmed)"
        Me.ChkDSendIMS.UseVisualStyleBackColor = True
        '
        'cboDCashAcc
        '
        Me.cboDCashAcc.FormattingEnabled = True
        Me.cboDCashAcc.Location = New System.Drawing.Point(347, 197)
        Me.cboDCashAcc.Name = "cboDCashAcc"
        Me.cboDCashAcc.Size = New System.Drawing.Size(195, 21)
        Me.cboDCashAcc.TabIndex = 9
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(285, 201)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(56, 13)
        Me.Label26.TabIndex = 102
        Me.Label26.Text = "Cash Acc:"
        '
        'txtDAmount
        '
        Me.txtDAmount.Location = New System.Drawing.Point(417, 171)
        Me.txtDAmount.Name = "txtDAmount"
        Me.txtDAmount.Size = New System.Drawing.Size(125, 20)
        Me.txtDAmount.TabIndex = 7
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(365, 174)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(46, 13)
        Me.Label24.TabIndex = 101
        Me.Label24.Text = "Amount:"
        '
        'dtDTo
        '
        Me.dtDTo.Location = New System.Drawing.Point(984, 19)
        Me.dtDTo.Name = "dtDTo"
        Me.dtDTo.Size = New System.Drawing.Size(127, 20)
        Me.dtDTo.TabIndex = 3
        '
        'dtDFrom
        '
        Me.dtDFrom.Location = New System.Drawing.Point(826, 19)
        Me.dtDFrom.Name = "dtDFrom"
        Me.dtDFrom.Size = New System.Drawing.Size(127, 20)
        Me.dtDFrom.TabIndex = 2
        '
        'cboDBroker
        '
        Me.cboDBroker.FormattingEnabled = True
        Me.cboDBroker.Location = New System.Drawing.Point(84, 197)
        Me.cboDBroker.Name = "cboDBroker"
        Me.cboDBroker.Size = New System.Drawing.Size(195, 21)
        Me.cboDBroker.TabIndex = 8
        '
        'cboDClearer
        '
        Me.cboDClearer.FormattingEnabled = True
        Me.cboDClearer.Location = New System.Drawing.Point(84, 225)
        Me.cboDClearer.Name = "cboDClearer"
        Me.cboDClearer.Size = New System.Drawing.Size(458, 21)
        Me.cboDClearer.TabIndex = 10
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(35, 228)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(43, 13)
        Me.Label21.TabIndex = 97
        Me.Label21.Text = "Clearer:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(6, 200)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(72, 13)
        Me.Label22.TabIndex = 94
        Me.Label22.Text = "Brkr Account:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(708, 152)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(63, 13)
        Me.Label19.TabIndex = 93
        Me.Label19.Text = "Trans Date:"
        '
        'dtDTransDate
        '
        Me.dtDTransDate.Location = New System.Drawing.Point(777, 149)
        Me.dtDTransDate.Name = "dtDTransDate"
        Me.dtDTransDate.Size = New System.Drawing.Size(128, 20)
        Me.dtDTransDate.TabIndex = 4
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(916, 152)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(63, 13)
        Me.Label20.TabIndex = 92
        Me.Label20.Text = "Settle Date:"
        '
        'dtDSettleDate
        '
        Me.dtDSettleDate.Location = New System.Drawing.Point(985, 149)
        Me.dtDSettleDate.Name = "dtDSettleDate"
        Me.dtDSettleDate.Size = New System.Drawing.Size(128, 20)
        Me.dtDSettleDate.TabIndex = 5
        '
        'cboDPlaceSettle
        '
        Me.cboDPlaceSettle.FormattingEnabled = True
        Me.cboDPlaceSettle.Location = New System.Drawing.Point(656, 225)
        Me.cboDPlaceSettle.Name = "cboDPlaceSettle"
        Me.cboDPlaceSettle.Size = New System.Drawing.Size(458, 21)
        Me.cboDPlaceSettle.TabIndex = 13
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(571, 229)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(79, 13)
        Me.Label18.TabIndex = 89
        Me.Label18.Text = "Place of Settle:"
        '
        'txtDInstructions
        '
        Me.txtDInstructions.Location = New System.Drawing.Point(84, 171)
        Me.txtDInstructions.Name = "txtDInstructions"
        Me.txtDInstructions.Size = New System.Drawing.Size(275, 20)
        Me.txtDInstructions.TabIndex = 6
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 174)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(80, 13)
        Me.Label17.TabIndex = 87
        Me.Label17.Text = "Instructions/ID:"
        '
        'cboDReceivAgent
        '
        Me.cboDReceivAgent.FormattingEnabled = True
        Me.cboDReceivAgent.Location = New System.Drawing.Point(656, 175)
        Me.cboDReceivAgent.Name = "cboDReceivAgent"
        Me.cboDReceivAgent.Size = New System.Drawing.Size(458, 21)
        Me.cboDReceivAgent.TabIndex = 11
        '
        'txtDReceivAgentAccountNo
        '
        Me.txtDReceivAgentAccountNo.Location = New System.Drawing.Point(656, 201)
        Me.txtDReceivAgentAccountNo.Name = "txtDReceivAgentAccountNo"
        Me.txtDReceivAgentAccountNo.Size = New System.Drawing.Size(458, 20)
        Me.txtDReceivAgentAccountNo.TabIndex = 12
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(586, 204)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 13)
        Me.Label12.TabIndex = 82
        Me.Label12.Text = "RA Acc No:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(766, 207)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(0, 15)
        Me.Label13.TabIndex = 81
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(575, 178)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(75, 13)
        Me.Label14.TabIndex = 80
        Me.Label14.Text = "Receiv Agent:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(786, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 70
        Me.Label2.Text = "From:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(959, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(19, 13)
        Me.Label5.TabIndex = 69
        Me.Label5.Text = "to:"
        '
        'dgvD
        '
        Me.dgvD.AllowUserToAddRows = False
        Me.dgvD.AllowUserToDeleteRows = False
        Me.dgvD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvD.Location = New System.Drawing.Point(6, 45)
        Me.dgvD.MultiSelect = False
        Me.dgvD.Name = "dgvD"
        Me.dgvD.ReadOnly = True
        Me.dgvD.Size = New System.Drawing.Size(1107, 97)
        Me.dgvD.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(770, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(0, 13)
        Me.Label9.TabIndex = 39
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(245, 25)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(85, 13)
        Me.Label11.TabIndex = 38
        Me.Label11.Text = "Search Portfolio:"
        '
        'cboDPortfolio
        '
        Me.cboDPortfolio.FormattingEnabled = True
        Me.cboDPortfolio.Location = New System.Drawing.Point(336, 19)
        Me.cboDPortfolio.Name = "cboDPortfolio"
        Me.cboDPortfolio.Size = New System.Drawing.Size(444, 21)
        Me.cboDPortfolio.TabIndex = 1
        '
        'TabPageRDPreview
        '
        Me.TabPageRDPreview.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPageRDPreview.Controls.Add(Me.GroupBox3)
        Me.TabPageRDPreview.Location = New System.Drawing.Point(4, 22)
        Me.TabPageRDPreview.Name = "TabPageRDPreview"
        Me.TabPageRDPreview.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageRDPreview.Size = New System.Drawing.Size(1129, 260)
        Me.TabPageRDPreview.TabIndex = 1
        Me.TabPageRDPreview.Text = "Preview"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtSWIFT)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1028, 212)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "SWIFT Export File Contents"
        '
        'txtSWIFT
        '
        Me.txtSWIFT.AcceptsReturn = True
        Me.txtSWIFT.AcceptsTab = True
        Me.txtSWIFT.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.txtSWIFT.Location = New System.Drawing.Point(6, 19)
        Me.txtSWIFT.Multiline = True
        Me.txtSWIFT.Name = "txtSWIFT"
        Me.txtSWIFT.ReadOnly = True
        Me.txtSWIFT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSWIFT.Size = New System.Drawing.Size(1016, 184)
        Me.txtSWIFT.TabIndex = 34
        '
        'TimerRefreshRD
        '
        Me.TimerRefreshRD.Interval = 120000
        '
        'cboSwiftCheck
        '
        Me.cboSwiftCheck.FormattingEnabled = True
        Me.cboSwiftCheck.Location = New System.Drawing.Point(801, 422)
        Me.cboSwiftCheck.Name = "cboSwiftCheck"
        Me.cboSwiftCheck.Size = New System.Drawing.Size(138, 21)
        Me.cboSwiftCheck.TabIndex = 64
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(698, 425)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(97, 13)
        Me.Label23.TabIndex = 63
        Me.Label23.Text = "Swift Check Period"
        '
        'FrmReceiveDeliver
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1163, 730)
        Me.Controls.Add(Me.cboSwiftCheck)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.RDBox)
        Me.Controls.Add(Me.lblpaymentTitle)
        Me.Controls.Add(Me.ChkCashRefresh)
        Me.Controls.Add(Me.CboFilterStatus)
        Me.Controls.Add(Me.cmdRDRefresh)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.dgvRD)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmReceiveDeliver"
        Me.Text = "Receive/Deliver"
        CType(Me.dgvRD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RDBox.ResumeLayout(False)
        Me.TabRD.ResumeLayout(False)
        Me.TabPageReceive.ResumeLayout(False)
        Me.GrpTransType.ResumeLayout(False)
        Me.GrpTransType.PerformLayout()
        Me.TabPageDeliver.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageRDPreview.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChkCashRefresh As CheckBox
    Friend WithEvents CboFilterStatus As ComboBox
    Friend WithEvents cmdRDRefresh As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents cmdSubmit As Button
    Friend WithEvents dgvRD As DataGridView
    Friend WithEvents cmdAddRD As Button
    Friend WithEvents lblpaymentTitle As Label
    Friend WithEvents RDBox As GroupBox
    Friend WithEvents TabRD As TabControl
    Friend WithEvents TabPageReceive As TabPage
    Friend WithEvents GrpTransType As GroupBox
    Friend WithEvents CboRCCY As ComboBox
    Friend WithEvents lblCCY As Label
    Friend WithEvents lblRMsg As Label
    Friend WithEvents RRB2 As RadioButton
    Friend WithEvents RRB1 As RadioButton
    Friend WithEvents txtRAmount As TextBox
    Friend WithEvents cboRBroker As ComboBox
    Friend WithEvents lblTransDate As Label
    Friend WithEvents dtRTransDate As DateTimePicker
    Friend WithEvents lblSettleDate As Label
    Friend WithEvents dtRSettleDate As DateTimePicker
    Friend WithEvents Label4 As Label
    Friend WithEvents cboRClearer As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cboRISIN As ComboBox
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cboRPortfolio As ComboBox
    Friend WithEvents LblPayType As Label
    Friend WithEvents CboRInstrument As ComboBox
    Friend WithEvents TabPageDeliver As TabPage
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dgvD As DataGridView
    Friend WithEvents Label9 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents cboDPortfolio As ComboBox
    Friend WithEvents TabPageRDPreview As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtSWIFT As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents TimerRefreshRD As Timer
    Friend WithEvents txtRDelivAgentAccountNo As TextBox
    Friend WithEvents cboRDelivAgent As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtDReceivAgentAccountNo As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtRInstructions As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents cboRPlaceSettle As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cboDPlaceSettle As ComboBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtDInstructions As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents cboDReceivAgent As ComboBox
    Friend WithEvents Label19 As Label
    Friend WithEvents dtDTransDate As DateTimePicker
    Friend WithEvents Label20 As Label
    Friend WithEvents dtDSettleDate As DateTimePicker
    Friend WithEvents lblCCYAmt As Label
    Friend WithEvents cboDBroker As ComboBox
    Friend WithEvents cboDClearer As ComboBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents cboSwiftCheck As ComboBox
    Friend WithEvents Label23 As Label
    Friend WithEvents dtDTo As DateTimePicker
    Friend WithEvents dtDFrom As DateTimePicker
    Friend WithEvents txtDAmount As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents cboRCashAcc As ComboBox
    Friend WithEvents Label25 As Label
    Friend WithEvents cboDCashAcc As ComboBox
    Friend WithEvents Label26 As Label
    Friend WithEvents ChkRSendIMS As CheckBox
    Friend WithEvents ChkDSendIMS As CheckBox
End Class
