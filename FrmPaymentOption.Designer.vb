﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaymentOption
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPaymentOption))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.CmdGenerateEmail = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.optRb4 = New System.Windows.Forms.RadioButton()
        Me.optRb3 = New System.Windows.Forms.RadioButton()
        Me.optRb2 = New System.Windows.Forms.RadioButton()
        Me.optRb1 = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtMsg = New System.Windows.Forms.TextBox()
        Me.CmdGenerateEmailAttached = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(53, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(234, 24)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Next Movement Options"
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(6, 141)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(315, 23)
        Me.cmdContinue.TabIndex = 9
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'CmdGenerateEmail
        '
        Me.CmdGenerateEmail.BackColor = System.Drawing.Color.White
        Me.CmdGenerateEmail.ForeColor = System.Drawing.Color.DarkGreen
        Me.CmdGenerateEmail.Image = CType(resources.GetObject("CmdGenerateEmail.Image"), System.Drawing.Image)
        Me.CmdGenerateEmail.Location = New System.Drawing.Point(175, 19)
        Me.CmdGenerateEmail.Name = "CmdGenerateEmail"
        Me.CmdGenerateEmail.Size = New System.Drawing.Size(71, 71)
        Me.CmdGenerateEmail.TabIndex = 10
        Me.CmdGenerateEmail.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optRb4)
        Me.GroupBox1.Controls.Add(Me.optRb3)
        Me.GroupBox1.Controls.Add(Me.optRb2)
        Me.GroupBox1.Controls.Add(Me.optRb1)
        Me.GroupBox1.Controls.Add(Me.cmdContinue)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 138)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(329, 176)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Options for next trade"
        '
        'optRb4
        '
        Me.optRb4.AutoSize = True
        Me.optRb4.Location = New System.Drawing.Point(26, 101)
        Me.optRb4.Name = "optRb4"
        Me.optRb4.Size = New System.Drawing.Size(224, 17)
        Me.optRb4.TabIndex = 13
        Me.optRb4.TabStop = True
        Me.optRb4.Text = "Close these options (revert to screen as is)"
        Me.optRb4.UseVisualStyleBackColor = True
        '
        'optRb3
        '
        Me.optRb3.AutoSize = True
        Me.optRb3.Location = New System.Drawing.Point(26, 78)
        Me.optRb3.Name = "optRb3"
        Me.optRb3.Size = New System.Drawing.Size(180, 17)
        Me.optRb3.TabIndex = 12
        Me.optRb3.TabStop = True
        Me.optRb3.Text = "Same Beneficiary, different Order"
        Me.optRb3.UseVisualStyleBackColor = True
        '
        'optRb2
        '
        Me.optRb2.AutoSize = True
        Me.optRb2.Location = New System.Drawing.Point(26, 55)
        Me.optRb2.Name = "optRb2"
        Me.optRb2.Size = New System.Drawing.Size(180, 17)
        Me.optRb2.TabIndex = 11
        Me.optRb2.TabStop = True
        Me.optRb2.Text = "Same Order, different Beneficiary"
        Me.optRb2.UseVisualStyleBackColor = True
        '
        'optRb1
        '
        Me.optRb1.AutoSize = True
        Me.optRb1.Location = New System.Drawing.Point(26, 32)
        Me.optRb1.Name = "optRb1"
        Me.optRb1.Size = New System.Drawing.Size(229, 17)
        Me.optRb1.TabIndex = 10
        Me.optRb1.TabStop = True
        Me.optRb1.Text = "New Movement (empty movements screen)"
        Me.optRb1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtMsg)
        Me.GroupBox2.Controls.Add(Me.CmdGenerateEmailAttached)
        Me.GroupBox2.Controls.Add(Me.CmdGenerateEmail)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 36)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(329, 96)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Email Options"
        '
        'txtMsg
        '
        Me.txtMsg.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtMsg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMsg.Location = New System.Drawing.Point(7, 20)
        Me.txtMsg.Multiline = True
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.Size = New System.Drawing.Size(162, 70)
        Me.txtMsg.TabIndex = 14
        '
        'CmdGenerateEmailAttached
        '
        Me.CmdGenerateEmailAttached.BackColor = System.Drawing.Color.White
        Me.CmdGenerateEmailAttached.ForeColor = System.Drawing.Color.DarkGreen
        Me.CmdGenerateEmailAttached.Image = CType(resources.GetObject("CmdGenerateEmailAttached.Image"), System.Drawing.Image)
        Me.CmdGenerateEmailAttached.Location = New System.Drawing.Point(252, 19)
        Me.CmdGenerateEmailAttached.Name = "CmdGenerateEmailAttached"
        Me.CmdGenerateEmailAttached.Size = New System.Drawing.Size(71, 71)
        Me.CmdGenerateEmailAttached.TabIndex = 13
        Me.CmdGenerateEmailAttached.UseVisualStyleBackColor = False
        '
        'FrmPaymentOption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(351, 321)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmPaymentOption"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select next option for movement"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdContinue As System.Windows.Forms.Button
    Friend WithEvents CmdGenerateEmail As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CmdGenerateEmailAttached As Button
    Friend WithEvents optRb1 As RadioButton
    Friend WithEvents optRb2 As RadioButton
    Friend WithEvents optRb3 As RadioButton
    Friend WithEvents optRb4 As RadioButton
    Friend WithEvents txtMsg As TextBox
End Class
