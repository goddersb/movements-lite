﻿Public Class ClsPaymentRequest
    Private varSelectedAuthoriseOption As Integer = 0
    Private varID As Double = 0
    Private varStatus As String = ""
    Private varPortfolioCode As Double = 0
    Private varPortfolioName As String = ""
    Private varCCYCode As Double = 0
    Private varCCYName As String = ""
    Private varOrderRef As String = ""
    Private varAmount As Double = 0
    Private varPayRequestEmail As String = ""
    Private varFilePath As String = ""
    Private varBeneficiaryTypeCode As Integer
    Private varBeneficiaryType As String = ""
    Private varBeneficiaryName As String = ""
    Private varBusinessName As String = ""
    Private varFirstname As String = ""
    Private varMiddlename As String = ""
    Private varSurname As String = ""
    Private varRelationshipCode As Integer
    Private varRelationship As String = ""
    Private varAddress As String = ""
    Private varCity As String = ""
    Private varCounty As String = ""
    Private varPostcode As String = ""
    Private varCountryCode As Integer = 0
    Private varCountry As String = ""
    Private varTemplateName As String = ""
    Private varPayRequestType As Integer = 0
    Private varPayRequestRate As Double = 0
    Private varBenPortfolioCode As Double = 0
    Private varBenPortfolioName As String = ""
    Private varCheckedSanctions As Boolean = False
    Private varCheckedPEP As Boolean = False
    Private varCheckedCDD As Boolean = False
    Private varIssuesDisclosed As String = ""
    Private varCheckedBankName As String = ""
    Private varCheckedBankCountryID As Integer = 0
    Private varCheckedBankCountry As String = ""
    Private varCheckedSwiftSortCode As String = ""
    Private varCheckedAccountNo As String = ""
    Private varReference As String = ""
    Private varBenSubBankName As String = ""
    Private varBenSubBankSwift As String = ""
    Private varBenSubBankIBAN As String = ""
    Private varBenBankName As String = ""
    Private varBenBankSwift As String = ""
    Private varBenBankIBAN As String = ""
    Private varManualPay As Boolean = False
    Private varMMBuySell As Integer = 0
    Private varMMInstrumentCode As Integer = 0
    Private varMMInstrument As String = ""
    Private varMMDurationType As Integer = 0
    Private varMMStartDate As Date = Now
    Private varMMEndDate As Date = Now
    Private varMMFirmMove As Boolean = False

    Property SelectedAuthoriseOption() As Double
        Get
            Return varSelectedAuthoriseOption
        End Get
        Set(value As Double)
            varSelectedAuthoriseOption = value
        End Set
    End Property

    Property ID() As Double
        Get
            Return varID
        End Get
        Set(value As Double)
            varID = value
        End Set
    End Property

    Property Status() As String
        Get
            Return varStatus
        End Get
        Set(value As String)
            varStatus = value
        End Set
    End Property

    Property PortfolioCode() As Double
        Get
            Return varPortfolioCode
        End Get
        Set(value As Double)
            varPortfolioCode = value
        End Set
    End Property

    Property PortfolioName() As String
        Get
            Return varPortfolioName
        End Get
        Set(value As String)
            varPortfolioName = value
        End Set
    End Property

    Property CCYCode() As Double
        Get
            Return varCCYCode
        End Get
        Set(value As Double)
            varCCYCode = value
        End Set
    End Property

    Property CCYName() As String
        Get
            Return varCCYName
        End Get
        Set(value As String)
            varCCYName = value
        End Set
    End Property

    Property OrderRef() As String
        Get
            Return varOrderRef
        End Get
        Set(value As String)
            varOrderRef = value
        End Set
    End Property

    Property Amount() As Double
        Get
            Return varAmount
        End Get
        Set(value As Double)
            varAmount = value
        End Set
    End Property

    Property PayRequestEmail() As String
        Get
            Return varPayRequestEmail
        End Get
        Set(value As String)
            varPayRequestEmail = value
        End Set
    End Property

    Property FilePath() As String
        Get
            Return varFilePath
        End Get
        Set(value As String)
            varFilePath = value
        End Set
    End Property

    Property BeneficiaryTypeCode() As Integer
        Get
            Return varBeneficiaryTypeCode
        End Get
        Set(value As Integer)
            varBeneficiaryTypeCode = value
        End Set
    End Property

    Property BeneficiaryType() As String
        Get
            Return varBeneficiaryType
        End Get
        Set(value As String)
            varBeneficiaryType = value
        End Set
    End Property


    Property BeneficiaryName() As String
        Get
            Return varBeneficiaryName
        End Get
        Set(value As String)
            varBeneficiaryName = value
        End Set
    End Property

    Property BusinessName() As String
        Get
            Return varBusinessName
        End Get
        Set(value As String)
            varBusinessName = value
        End Set
    End Property

    Property Firstname() As String
        Get
            Return varFirstname
        End Get
        Set(value As String)
            varFirstname = value
        End Set
    End Property

    Property Middlename() As String
        Get
            Return varMiddlename
        End Get
        Set(value As String)
            varMiddlename = value
        End Set
    End Property

    Property Surname() As String
        Get
            Return varSurname
        End Get
        Set(value As String)
            varSurname = value
        End Set
    End Property

    Property RelationshipCode() As Integer
        Get
            Return varRelationshipCode
        End Get
        Set(value As Integer)
            varRelationshipCode = value
        End Set
    End Property

    Property Relationship() As String
        Get
            Return varRelationship
        End Get
        Set(value As String)
            varRelationship = value
        End Set
    End Property

    Property Address() As String
        Get
            Return varAddress
        End Get
        Set(value As String)
            varAddress = value
        End Set
    End Property

    Property City() As String
        Get
            Return varCity
        End Get
        Set(value As String)
            varCity = value
        End Set
    End Property

    Property County() As String
        Get
            Return varCounty
        End Get
        Set(value As String)
            varCounty = value
        End Set
    End Property

    Property Postcode() As String
        Get
            Return varPostcode
        End Get
        Set(value As String)
            varPostcode = value
        End Set
    End Property

    Property CountryCode() As Integer
        Get
            Return varCountryCode
        End Get
        Set(value As Integer)
            varCountryCode = value
        End Set
    End Property

    Property Country() As String
        Get
            Return varCountry
        End Get
        Set(value As String)
            varCountry = value
        End Set
    End Property

    Property TemplateName() As String
        Get
            Return varTemplateName
        End Get
        Set(value As String)
            varTemplateName = value
        End Set
    End Property


    Property PayRequestType As Integer
        Get
            Return varPayRequestType
        End Get
        Set(value As Integer)
            varPayRequestType = value
        End Set
    End Property

    Property PayRequestRate As Double
        Get
            Return varPayRequestRate
        End Get
        Set(value As Double)
            varPayRequestRate = value
        End Set
    End Property

    Property BeneficiaryPortfolioCode() As Double
        Get
            Return varBenPortfolioCode
        End Get
        Set(value As Double)
            varBenPortfolioCode = value
        End Set
    End Property

    Property BeneficiaryPortfolioName() As String
        Get
            Return varBenPortfolioName
        End Get
        Set(value As String)
            varBenPortfolioName = value
        End Set
    End Property

    Property CheckedSanctions() As Boolean
        Get
            Return varCheckedSanctions
        End Get
        Set(value As Boolean)
            varCheckedSanctions = value
        End Set
    End Property

    Property CheckedPEP() As Boolean
        Get
            Return varCheckedPEP
        End Get
        Set(value As Boolean)
            varCheckedPEP = value
        End Set
    End Property

    Property CheckedCDD() As Boolean
        Get
            Return varCheckedCDD
        End Get
        Set(value As Boolean)
            varCheckedCDD = value
        End Set
    End Property

    Property IssuesDisclosed() As String
        Get
            Return varIssuesDisclosed
        End Get
        Set(value As String)
            varIssuesDisclosed = value
        End Set
    End Property

    Property CheckedBankName() As String
        Get
            Return varCheckedBankName
        End Get
        Set(value As String)
            varCheckedBankName = value
        End Set
    End Property

    Property CheckedBankCountryID() As Integer
        Get
            Return varCheckedBankCountryID
        End Get
        Set(value As Integer)
            varCheckedBankCountryID = value
        End Set
    End Property

    Property CheckedBankCountry() As String
        Get
            Return varCheckedBankCountry
        End Get
        Set(value As String)
            varCheckedBankCountry = value
        End Set
    End Property

    Property CheckedSwiftSortCode() As String
        Get
            Return varCheckedSwiftSortCode
        End Get
        Set(value As String)
            varCheckedSwiftSortCode = value
        End Set
    End Property

    Property CheckedAccountNo() As String
        Get
            Return varCheckedAccountNo
        End Get
        Set(value As String)
            varCheckedAccountNo = value
        End Set
    End Property

    Property Reference() As String
        Get
            Return varReference
        End Get
        Set(value As String)
            varReference = value
        End Set
    End Property

    Property BeneficiarySubBankName() As String
        Get
            Return varBenSubBankName
        End Get
        Set(value As String)
            varBenSubBankName = value
        End Set
    End Property

    Property BeneficiarySubBankSwift() As String
        Get
            Return varBenSubBankSwift
        End Get
        Set(value As String)
            varBenSubBankSwift = value
        End Set
    End Property

    Property BeneficiarySubBankIBAN() As String
        Get
            Return varBenSubBankIBAN
        End Get
        Set(value As String)
            varBenSubBankIBAN = value
        End Set
    End Property

    Property BeneficiaryBankName() As String
        Get
            Return varBenBankName
        End Get
        Set(value As String)
            varBenBankName = value
        End Set
    End Property

    Property BeneficiaryBankSwift() As String
        Get
            Return varBenBankSwift
        End Get
        Set(value As String)
            varBenBankSwift = value
        End Set
    End Property

    Property BeneficiaryBankIBAN() As String
        Get
            Return varBenBankIBAN
        End Get
        Set(value As String)
            varBenBankIBAN = value
        End Set
    End Property

    Property ManualPay() As Boolean
        Get
            Return varManualPay
        End Get
        Set(value As Boolean)
            varManualPay = value
        End Set
    End Property

    Property MMBuySell() As Integer
        Get
            Return varMMBuySell
        End Get
        Set(value As Integer)
            varMMBuySell = value
        End Set
    End Property

    Property MMInstrumentCode() As Integer
        Get
            Return varMMInstrumentCode
        End Get
        Set(value As Integer)
            varMMInstrumentCode = value
        End Set
    End Property

    Property MMInstrument() As String
        Get
            Return varMMInstrument
        End Get
        Set(value As String)
            varMMInstrument = value
        End Set
    End Property

    Property MMDurationType() As Integer
        Get
            Return varMMDurationType
        End Get
        Set(value As Integer)
            varMMDurationType = value
        End Set
    End Property

    Property MMStartDate() As Date
        Get
            Return varMMStartDate
        End Get
        Set(value As Date)
            varMMStartDate = value
        End Set
    End Property

    Property MMEndDate() As Date
        Get
            Return varMMEndDate
        End Get
        Set(value As Date)
            varMMEndDate = value
        End Set
    End Property

    Property MMFirmMove() As Boolean
        Get
            Return varMMFirmMove
        End Get
        Set(value As Boolean)
            varMMFirmMove = value
        End Set
    End Property

End Class
