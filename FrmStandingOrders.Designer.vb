﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmStandingOrders
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmStandingOrders))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnNewStandingOrder = New System.Windows.Forms.Button()
        Me.gpbStandingOrders = New System.Windows.Forms.GroupBox()
        Me.btnReset = New System.Windows.Forms.Button()
        Me.CmdExcelPR = New System.Windows.Forms.Button()
        Me.ChkRefresh = New System.Windows.Forms.CheckBox()
        Me.cmdPRSearch = New System.Windows.Forms.Button()
        Me.lblCCY = New System.Windows.Forms.Label()
        Me.cboCCY = New System.Windows.Forms.ComboBox()
        Me.lblPortfolio = New System.Windows.Forms.Label()
        Me.cboPRPortfolio = New System.Windows.Forms.ComboBox()
        Me.dgvStandingOrders = New System.Windows.Forms.DataGridView()
        Me.lblStandingOrders = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.gpbStandingOrders.SuspendLayout()
        CType(Me.dgvStandingOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnNewStandingOrder)
        Me.GroupBox1.Controls.Add(Me.gpbStandingOrders)
        Me.GroupBox1.Controls.Add(Me.dgvStandingOrders)
        Me.GroupBox1.Controls.Add(Me.lblStandingOrders)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1728, 598)
        Me.GroupBox1.TabIndex = 46
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Standing Order Details"
        '
        'btnNewStandingOrder
        '
        Me.btnNewStandingOrder.Location = New System.Drawing.Point(297, 29)
        Me.btnNewStandingOrder.Name = "btnNewStandingOrder"
        Me.btnNewStandingOrder.Size = New System.Drawing.Size(174, 31)
        Me.btnNewStandingOrder.TabIndex = 1
        Me.btnNewStandingOrder.Text = "Create New Standing Order"
        Me.btnNewStandingOrder.UseVisualStyleBackColor = True
        '
        'gpbStandingOrders
        '
        Me.gpbStandingOrders.BackColor = System.Drawing.Color.OldLace
        Me.gpbStandingOrders.Controls.Add(Me.btnReset)
        Me.gpbStandingOrders.Controls.Add(Me.CmdExcelPR)
        Me.gpbStandingOrders.Controls.Add(Me.ChkRefresh)
        Me.gpbStandingOrders.Controls.Add(Me.cmdPRSearch)
        Me.gpbStandingOrders.Controls.Add(Me.lblCCY)
        Me.gpbStandingOrders.Controls.Add(Me.cboCCY)
        Me.gpbStandingOrders.Controls.Add(Me.lblPortfolio)
        Me.gpbStandingOrders.Controls.Add(Me.cboPRPortfolio)
        Me.gpbStandingOrders.Location = New System.Drawing.Point(905, 10)
        Me.gpbStandingOrders.Name = "gpbStandingOrders"
        Me.gpbStandingOrders.Size = New System.Drawing.Size(817, 69)
        Me.gpbStandingOrders.TabIndex = 79
        Me.gpbStandingOrders.TabStop = False
        Me.gpbStandingOrders.Text = "Search Standing Orders"
        '
        'btnReset
        '
        Me.btnReset.Enabled = False
        Me.btnReset.Location = New System.Drawing.Point(448, 40)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(142, 21)
        Me.btnReset.TabIndex = 7
        Me.btnReset.Text = "Reset Filter"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'CmdExcelPR
        '
        Me.CmdExcelPR.Image = CType(resources.GetObject("CmdExcelPR.Image"), System.Drawing.Image)
        Me.CmdExcelPR.Location = New System.Drawing.Point(761, 18)
        Me.CmdExcelPR.Name = "CmdExcelPR"
        Me.CmdExcelPR.Size = New System.Drawing.Size(39, 32)
        Me.CmdExcelPR.TabIndex = 8
        Me.CmdExcelPR.UseVisualStyleBackColor = True
        '
        'ChkRefresh
        '
        Me.ChkRefresh.AutoSize = True
        Me.ChkRefresh.Checked = True
        Me.ChkRefresh.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkRefresh.ForeColor = System.Drawing.Color.Maroon
        Me.ChkRefresh.Location = New System.Drawing.Point(618, 43)
        Me.ChkRefresh.Name = "ChkRefresh"
        Me.ChkRefresh.Size = New System.Drawing.Size(146, 17)
        Me.ChkRefresh.TabIndex = 81
        Me.ChkRefresh.Text = "Auto Refresh Grid On/Off"
        Me.ChkRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkRefresh.UseVisualStyleBackColor = True
        Me.ChkRefresh.Visible = False
        '
        'cmdPRSearch
        '
        Me.cmdPRSearch.Enabled = False
        Me.cmdPRSearch.Location = New System.Drawing.Point(448, 15)
        Me.cmdPRSearch.Name = "cmdPRSearch"
        Me.cmdPRSearch.Size = New System.Drawing.Size(142, 21)
        Me.cmdPRSearch.TabIndex = 6
        Me.cmdPRSearch.Text = "Filter Data Grid"
        Me.cmdPRSearch.UseVisualStyleBackColor = True
        '
        'lblCCY
        '
        Me.lblCCY.AutoSize = True
        Me.lblCCY.Location = New System.Drawing.Point(38, 45)
        Me.lblCCY.Name = "lblCCY"
        Me.lblCCY.Size = New System.Drawing.Size(31, 13)
        Me.lblCCY.TabIndex = 80
        Me.lblCCY.Text = "CCY:"
        '
        'cboCCY
        '
        Me.cboCCY.FormattingEnabled = True
        Me.cboCCY.Location = New System.Drawing.Point(75, 41)
        Me.cboCCY.Name = "cboCCY"
        Me.cboCCY.Size = New System.Drawing.Size(85, 21)
        Me.cboCCY.TabIndex = 5
        '
        'lblPortfolio
        '
        Me.lblPortfolio.AutoSize = True
        Me.lblPortfolio.Location = New System.Drawing.Point(18, 19)
        Me.lblPortfolio.Name = "lblPortfolio"
        Me.lblPortfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblPortfolio.TabIndex = 78
        Me.lblPortfolio.Text = "Portfolio:"
        '
        'cboPRPortfolio
        '
        Me.cboPRPortfolio.FormattingEnabled = True
        Me.cboPRPortfolio.Location = New System.Drawing.Point(75, 16)
        Me.cboPRPortfolio.Name = "cboPRPortfolio"
        Me.cboPRPortfolio.Size = New System.Drawing.Size(323, 21)
        Me.cboPRPortfolio.TabIndex = 2
        '
        'dgvStandingOrders
        '
        Me.dgvStandingOrders.AllowUserToAddRows = False
        Me.dgvStandingOrders.AllowUserToOrderColumns = True
        Me.dgvStandingOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStandingOrders.Location = New System.Drawing.Point(6, 85)
        Me.dgvStandingOrders.Name = "dgvStandingOrders"
        Me.dgvStandingOrders.Size = New System.Drawing.Size(1716, 507)
        Me.dgvStandingOrders.TabIndex = 75
        '
        'lblStandingOrders
        '
        Me.lblStandingOrders.AutoSize = True
        Me.lblStandingOrders.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStandingOrders.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblStandingOrders.Location = New System.Drawing.Point(6, 16)
        Me.lblStandingOrders.Name = "lblStandingOrders"
        Me.lblStandingOrders.Size = New System.Drawing.Size(162, 24)
        Me.lblStandingOrders.TabIndex = 43
        Me.lblStandingOrders.Text = "Standing Orders"
        '
        'FrmStandingOrders
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(1752, 622)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmStandingOrders"
        Me.Text = "Add/Edit Standing Orders"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gpbStandingOrders.ResumeLayout(False)
        Me.gpbStandingOrders.PerformLayout()
        CType(Me.dgvStandingOrders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents gpbStandingOrders As GroupBox
    Friend WithEvents CmdExcelPR As Button
    Friend WithEvents ChkRefresh As CheckBox
    Friend WithEvents cmdPRSearch As Button
    Friend WithEvents lblCCY As Label
    Friend WithEvents cboCCY As ComboBox
    Friend WithEvents lblPortfolio As Label
    Friend WithEvents cboPRPortfolio As ComboBox
    Friend WithEvents dgvStandingOrders As DataGridView
    Friend WithEvents lblStandingOrders As Label
    Friend WithEvents btnReset As Button
    Friend WithEvents btnNewStandingOrder As Button
End Class
