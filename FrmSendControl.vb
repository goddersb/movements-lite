﻿Imports System.ComponentModel

Public Class FrmSendControl

    Private Sub ChkSwift_CheckedChanged(sender As Object, e As EventArgs) Handles ChkSwift.CheckedChanged
        ColorCheckbox(ChkSwift)
    End Sub

    Private Sub ChkIMS_CheckedChanged(sender As Object, e As EventArgs) Handles ChkIMS.CheckedChanged
        ColorCheckbox(ChkIMS)
    End Sub

    Private Sub FrmSendControl_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtAuthID.Text = "#" & ClsPayAuth.SelectedPaymentID
        ChkSwift.Checked = ClsPayAuth.SendSwift
        ChkIMS.Checked = ClsPayAuth.SendIMS
        ColorCheckbox(ChkSwift)
        ColorCheckbox(ChkIMS)
    End Sub

    Private Sub FrmSendControl_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        ClsPayAuth.SendSwift = ChkSwift.Checked
        ClsPayAuth.SendIMS = ChkIMS.Checked
        ClsIMS.ExecuteString(ClsIMS.GetCurrentConnection, "update vwdolfinpayment set p_sendswift = " & IIf(ClsPayAuth.SendSwift, 1, 0) & ",p_modifiedtime=getdate() where p_id = " & ClsPayAuth.SelectedPaymentID)
        ClsIMS.ExecuteString(ClsIMS.GetCurrentConnection, "update vwdolfinpayment set p_sendIMS = " & IIf(ClsPayAuth.SendIMS, 1, 0) & ",p_modifiedtime=getdate() where p_id = " & ClsPayAuth.SelectedPaymentID)
        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovements, "SendSwift is " & IIf(ClsPayAuth.SendSwift, "True", "False") & ", SendIMS is " & IIf(ClsPayAuth.SendSwift, "True", "False") & " in FLOW payment authorisation (Send Control form)", ClsPayAuth.SelectedPaymentID)
    End Sub
End Class