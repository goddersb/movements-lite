﻿Imports System.Net
Imports System.IO

Public Class ClsC6Search
    Private Sub CreatePersonColumns(ByRef TblPerson As DataTable)
        AddDataColumn(TblPerson, "score", "System.Int32")
        AddDataColumn(TblPerson, "id", "System.String")
        AddDataColumn(TblPerson, "title", "System.String")
        AddDataColumn(TblPerson, "alternativeTitle", "System.String")
        AddDataColumn(TblPerson, "forename", "System.String")
        AddDataColumn(TblPerson, "middlename", "System.String")
        AddDataColumn(TblPerson, "surname", "System.String")
        AddDataColumn(TblPerson, "dateOfBirth", "System.DateTime")
        AddDataColumn(TblPerson, "yearOfBirth", "System.String")
        AddDataColumn(TblPerson, "dateOfDeath", "System.DateTime")
        AddDataColumn(TblPerson, "yearOfDeath", "System.String")
        AddDataColumn(TblPerson, "isDeceased", "System.String")
        AddDataColumn(TblPerson, "gender", "System.String")
        AddDataColumn(TblPerson, "nationality", "System.String")
        AddDataColumn(TblPerson, "imageURL", "System.String")
        AddDataColumn(TblPerson, "telephoneNumber", "System.String")
        AddDataColumn(TblPerson, "faxNumber", "System.String")
        AddDataColumn(TblPerson, "mobileNumber", "System.String")
        AddDataColumn(TblPerson, "email", "System.String")
        AddDataColumn(TblPerson, "pepLevel", "System.Int32")
        AddDataColumn(TblPerson, "isPEP", "System.String")
        AddDataColumn(TblPerson, "isSanctionsCurrent", "System.String")
        AddDataColumn(TblPerson, "isSanctionsPrevious", "System.String")
        AddDataColumn(TblPerson, "isLawEnforcement", "System.String")
        AddDataColumn(TblPerson, "isFinancialregulator", "System.String")
        AddDataColumn(TblPerson, "isDisqualifiedDirector", "System.String")
        AddDataColumn(TblPerson, "isInsolvent", "System.String")
        AddDataColumn(TblPerson, "isAdverseMedia", "System.String")
    End Sub

    Private Sub PopulatePerson(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblPerson As DataTable)
        On Error Resume Next
        Dim personrow As DataRow
        personrow = TblPerson.NewRow()
        personrow("score") = jsonBody.Item("score").ToString()
        personrow("id") = jsonBody.Item("person").item("id").ToString()
        personrow("title") = Left(jsonBody.Item("person").item("title").item("description").ToString(), 100)
        personrow("alternativeTitle") = Left(jsonBody.Item("person").item("alternativeTitle").ToString(), 100)
        personrow("forename") = Left(jsonBody.Item("person").item("forename").ToString(), 200)
        personrow("middlename") = Left(jsonBody.Item("person").item("middlename").ToString(), 200)
        personrow("surname") = Left(jsonBody.Item("person").item("surname").ToString(), 200)
        personrow("dateOfBirth") = IIf(Not IsDate(jsonBody.Item("person").item("dateOfBirth")), DBNull.Value, jsonBody.Item("person").item("dateOfBirth"))
        personrow("yearOfBirth") = Left(jsonBody.Item("person").item("yearOfBirth").ToString(), 5)
        personrow("dateOfDeath") = IIf(Not IsDate(jsonBody.Item("person").item("dateOfDeath")), DBNull.Value, jsonBody.Item("person").item("dateOfBirth"))
        personrow("yearOfDeath") = Left(jsonBody.Item("person").item("yearOfDeath").ToString(), 5)
        personrow("isDeceased") = jsonBody.Item("person").item("isDeceased").ToString()
        personrow("gender") = Left(jsonBody.Item("person").item("gender").ToString(), 10)
        personrow("nationality") = Left(jsonBody.Item("person").item("nationality").item("nationality").ToString(), 100)
        personrow("imageURL") = Left(jsonBody.Item("person").item("imageURL").ToString(), 200)
        personrow("telephoneNumber") = Left(jsonBody.Item("person").item("telephoneNumber").ToString(), 50)
        personrow("faxNumber") = Left(jsonBody.Item("person").item("faxNumber").ToString(), 50)
        personrow("mobileNumber") = Left(jsonBody.Item("person").item("mobileNumber").ToString(), 50)
        personrow("email") = Left(jsonBody.Item("person").item("email").ToString(), 100)
        personrow("pepLevel") = IIf(Not IsNumeric(jsonBody.Item("person").item("pepLevel")), 0, jsonBody.Item("person").item("pepLevel"))
        personrow("isPEP") = jsonBody.Item("person").item("isPEP").ToString()
        personrow("isSanctionsCurrent") = jsonBody.Item("person").item("isSanctionsCurrent").ToString()
        personrow("isSanctionsPrevious") = jsonBody.Item("person").item("isSanctionsPrevious").ToString()
        personrow("isLawEnforcement") = jsonBody.Item("person").item("isLawEnforcement").ToString()
        personrow("isFinancialregulator") = jsonBody.Item("person").item("isFinancialregulator").ToString()
        personrow("isDisqualifiedDirector") = jsonBody.Item("person").item("isDisqualifiedDirector").ToString()
        personrow("isInsolvent") = jsonBody.Item("person").item("isInsolvent").ToString()
        personrow("isAdverseMedia") = jsonBody.Item("person").item("isAdverseMedia").ToString()
        TblPerson.Rows.Add(personrow)
    End Sub

    Private Sub CreateAddressesColumns(ByRef TblAddresses As DataTable)
        AddDataColumn(TblAddresses, "id", "System.String")
        AddDataColumn(TblAddresses, "address1", "System.String")
        AddDataColumn(TblAddresses, "address2", "System.String")
        AddDataColumn(TblAddresses, "address3", "System.String")
        AddDataColumn(TblAddresses, "address4", "System.String")
        AddDataColumn(TblAddresses, "city", "System.String")
        AddDataColumn(TblAddresses, "county", "System.String")
        AddDataColumn(TblAddresses, "postcode", "System.String")
        AddDataColumn(TblAddresses, "country", "System.String")
    End Sub

    Private Sub PopulateAddresses(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblAddresses As DataTable, ByVal varTypeName As String)
        On Error Resume Next
        For loopAddress As Integer = 0 To jsonBody.Item(varTypeName).Item("addresses").count - 1
            Dim addressrow As DataRow
            addressrow = TblAddresses.NewRow()
            addressrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            addressrow("address1") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("address1").ToString(), 200)
            addressrow("address2") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("address2").ToString(), 200)
            addressrow("address3") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("address3").ToString(), 200)
            addressrow("address4") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("address4").ToString(), 200)
            addressrow("city") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("city").ToString(), 100)
            addressrow("county") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("county").ToString(), 100)
            addressrow("postcode") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("postcode").ToString(), 10)
            addressrow("country") = Left(jsonBody.Item(varTypeName).item("addresses").item(loopAddress).item("country").item("name").ToString(), 255)
            TblAddresses.Rows.Add(addressrow)
        Next
    End Sub

    Private Sub CreateAliasColumns(ByRef TblAlias As DataTable)
        AddDataColumn(TblAlias, "id", "System.String")
        AddDataColumn(TblAlias, "title", "System.String")
        AddDataColumn(TblAlias, "alternativeTitle", "System.String")
        AddDataColumn(TblAlias, "forename", "System.String")
        AddDataColumn(TblAlias, "middlename", "System.String")
        AddDataColumn(TblAlias, "surname", "System.String")
        AddDataColumn(TblAlias, "businessName", "System.String")
    End Sub

    Private Sub PopulateAlias(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblAlias As DataTable, ByVal varTypeName As String)
        On Error Resume Next
        For loopAlias As Integer = 0 To jsonBody.Item(varTypeName).Item("aliases").count - 1
            Dim aliasrow As DataRow
            aliasrow = TblAlias.NewRow()
            aliasrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            If varTypeName = "person" Then
                aliasrow("title") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("title").ToString(), 100)
                aliasrow("alternativeTitle") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("alternativeTitle").ToString(), 100)
                aliasrow("forename") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("forename").ToString(), 200)
                aliasrow("middlename") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("middlename").ToString(), 200)
                aliasrow("surname") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("surname").ToString(), 200)
                aliasrow("businessName") = "n/a"
            ElseIf varTypeName = "business" Then
                aliasrow("businessName") = Left(jsonBody.Item(varTypeName).item("aliases").item(loopAlias).item("businessName").ToString(), 200)
            End If
            TblAlias.Rows.Add(aliasrow)
        Next
    End Sub

    Private Sub CreateArticlesColumns(ByRef TblArticles As DataTable)
        AddDataColumn(TblArticles, "id", "System.String")
        AddDataColumn(TblArticles, "originalURL", "System.String")
        AddDataColumn(TblArticles, "dateCollected", "System.DateTime")
        AddDataColumn(TblArticles, "c6URL", "System.String")
        AddDataColumn(TblArticles, "AtricleCategory1", "System.String")
        AddDataColumn(TblArticles, "AtricleCategory2", "System.String")
        AddDataColumn(TblArticles, "AtricleCategory3", "System.String")
    End Sub

    Private Sub PopulateArticles(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblArticles As DataTable, ByVal varTypeName As String)
        On Error Resume Next
        For loopArticles As Integer = 0 To jsonBody.Item(varTypeName).Item("articles").count - 1
            Dim Articlesrow As DataRow
            Articlesrow = TblArticles.NewRow()
            Articlesrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            Articlesrow("originalURL") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("originalURL").ToString(), 1000)
            Articlesrow("dateCollected") = IIf(Not IsDate(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("dateCollected").ToString()), DBNull.Value, jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("dateCollected").ToString())
            Articlesrow("c6URL") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("c6URL").ToString(), 1000)
            If jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").count > 0 Then
                Articlesrow("AtricleCategory1") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").item(0).item("name").ToString, 50)
            End If

            If jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").count > 1 Then
                Articlesrow("AtricleCategory2") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").item(1).item("name").ToString, 50)
            End If

            If jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").count > 2 Then
                Articlesrow("AtricleCategory3") = Left(jsonBody.Item(varTypeName).item("articles").item(loopArticles).item("categories").item(2).item("name").ToString, 50)
            End If
            TblArticles.Rows.Add(Articlesrow)
        Next
    End Sub

    Private Sub CreateSanctionsColumns(ByRef TblSanctions As DataTable)
        AddDataColumn(TblSanctions, "id", "System.String")
        AddDataColumn(TblSanctions, "sanctionType", "System.String")
        AddDataColumn(TblSanctions, "isCurrent", "System.String")
    End Sub

    Private Sub PopulateSanctions(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblSanctions As DataTable, ByVal varTypeName As String)
        On Error Resume Next
        For loopSanctions As Integer = 0 To jsonBody.Item(varTypeName).Item("sanctions").count - 1
            Dim Sanctionsrow As DataRow
            Sanctionsrow = TblSanctions.NewRow()
            Sanctionsrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            Sanctionsrow("sanctionType") = Left(jsonBody.Item(varTypeName).item("sanctions").item(loopSanctions).item("sanctionType").item("description").ToString(), 50)
            Sanctionsrow("isCurrent") = jsonBody.Item(varTypeName).item("sanctions").item(loopSanctions).item("isCurrent").ToString()
            TblSanctions.Rows.Add(Sanctionsrow)
        Next
    End Sub

    Private Sub CreateNotesColumns(ByRef TblNotes As DataTable)
        AddDataColumn(TblNotes, "id", "System.String")
        AddDataColumn(TblNotes, "dataSource", "System.String")
        AddDataColumn(TblNotes, "text", "System.String")
    End Sub

    Private Sub PopulateNotes(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblNotes As DataTable, ByVal varTypeName As String)
        On Error Resume Next
        For loopNotes As Integer = 0 To jsonBody.Item(varTypeName).Item("notes").count - 1
            Dim Notesrow As DataRow
            Notesrow = TblNotes.NewRow()
            Notesrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            Notesrow("dataSource") = Left(jsonBody.Item(varTypeName).item("notes").item(loopNotes).item("dataSource").item("name").ToString(), 100)
            Notesrow("text") = jsonBody.Item(varTypeName).item("notes").item(loopNotes).item("text").ToString()
            TblNotes.Rows.Add(Notesrow)
        Next
    End Sub

    Private Sub CreateLinkedBusinessesColumns(ByRef TblLinkedBusinesses As DataTable)
        AddDataColumn(TblLinkedBusinesses, "id", "System.String")
        AddDataColumn(TblLinkedBusinesses, "businessName", "System.String")
        AddDataColumn(TblLinkedBusinesses, "position", "System.String")
        AddDataColumn(TblLinkedBusinesses, "linkDescription", "System.String")
    End Sub

    Private Sub PopulateLinkedBusinesses(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblLinkedBusinesses As DataTable, ByVal varTypeName As String)
        On Error Resume Next
        For loopLinkedBusinesses As Integer = 0 To jsonBody.Item(varTypeName).Item("linkedBusinesses").count - 1
            Dim Notesrow As DataRow
            Notesrow = TblLinkedBusinesses.NewRow()
            Notesrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            Notesrow("businessName") = Left(jsonBody.Item(varTypeName).item("linkedBusinesses").item(loopLinkedBusinesses).item("businessName").ToString(), 200)

            If varTypeName = "person" Then
                Notesrow("position") = Left(jsonBody.Item(varTypeName).item("linkedBusinesses").item(loopLinkedBusinesses).item("position").ToString(), 200)
            ElseIf varTypeName = "business" Then
                Notesrow("linkDescription") = Left(jsonBody.Item(varTypeName).item("linkedBusinesses").item(loopLinkedBusinesses).item("linkDescription").ToString(), 200)
            End If
            TblLinkedBusinesses.Rows.Add(Notesrow)
        Next
    End Sub

    Private Sub CreateLinkedPersonsColumns(ByRef TblLinkedPersons As DataTable)
        AddDataColumn(TblLinkedPersons, "id", "System.String")
        AddDataColumn(TblLinkedPersons, "personId", "System.String")
        AddDataColumn(TblLinkedPersons, "name", "System.String")
        AddDataColumn(TblLinkedPersons, "association", "System.String")
        AddDataColumn(TblLinkedPersons, "position", "System.String")
    End Sub

    Private Sub PopulateLinkedPersons(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblLinkedPersons As DataTable, ByVal varTypeName As String)
        On Error Resume Next
        For loopLinkedPersons As Integer = 0 To jsonBody.Item(varTypeName).Item("linkedPersons").count - 1
            Dim Notesrow As DataRow
            Notesrow = TblLinkedPersons.NewRow()
            Notesrow("id") = jsonBody.Item(varTypeName).item("id").ToString()
            Notesrow("personId") = jsonBody.Item(varTypeName).item("linkedPersons").item(loopLinkedPersons).item("personId").ToString()
            Notesrow("name") = Left(jsonBody.Item(varTypeName).item("linkedPersons").item(loopLinkedPersons).item("name").ToString(), 200)

            If varTypeName = "person" Then
                Notesrow("association") = Left(jsonBody.Item(varTypeName).item("linkedPersons").item(loopLinkedPersons).item("association").ToString(), 200)
                Notesrow("position") = ""
            ElseIf varTypeName = "business" Then
                Notesrow("association") = ""
                Notesrow("position") = Left(jsonBody.Item(varTypeName).item("linkedPersons").item(loopLinkedPersons).item("position").ToString(), 200)
            End If
            TblLinkedPersons.Rows.Add(Notesrow)
        Next
    End Sub

    Private Sub CreatePoliticalPositionsColumns(ByRef TblPoliticalPositions As DataTable)
        AddDataColumn(TblPoliticalPositions, "id", "System.String")
        AddDataColumn(TblPoliticalPositions, "description", "System.String")
        AddDataColumn(TblPoliticalPositions, "from", "System.DateTime")
        AddDataColumn(TblPoliticalPositions, "to", "System.DateTime")
        AddDataColumn(TblPoliticalPositions, "country", "System.String")
    End Sub

    Private Sub PopulatePoliticalPositions(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblPoliticalPositions As DataTable, ByVal varTypeName As String)
        On Error Resume Next
        For loopPoliticalPositions As Integer = 0 To jsonBody.Item(varTypeName).Item("politicalPositions").count - 1
            Dim PProw As DataRow
            PProw = TblPoliticalPositions.NewRow()
            PProw("id") = jsonBody.Item(varTypeName).item("id").ToString()
            PProw("description") = Left(jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("description").ToString(), 200)
            PProw("from") = IIf(Not IsDate(jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("from").ToString()), DBNull.Value, jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("from").ToString())
            PProw("to") = IIf(Not IsDate(jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("to").ToString()), DBNull.Value, jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("to").ToString())
            PProw("country") = Left(jsonBody.Item(varTypeName).item("politicalPositions").item(loopPoliticalPositions).item("country").item("name").ToString(), 100)
            TblPoliticalPositions.Rows.Add(PProw)
        Next
    End Sub

    Private Sub CreateBusinessColumns(ByRef TblBusiness As DataTable)
        AddDataColumn(TblBusiness, "score", "System.Int32")
        AddDataColumn(TblBusiness, "id", "System.String")
        AddDataColumn(TblBusiness, "businessname", "System.String")
        AddDataColumn(TblBusiness, "telephoneNumber", "System.String")
        AddDataColumn(TblBusiness, "faxNumber", "System.String")
        AddDataColumn(TblBusiness, "website", "System.String")
        AddDataColumn(TblBusiness, "isPEP", "System.String")
        AddDataColumn(TblBusiness, "isSanctionsCurrent", "System.String")
        AddDataColumn(TblBusiness, "isSanctionsPrevious", "System.String")
        AddDataColumn(TblBusiness, "isLawEnforcement", "System.String")
        AddDataColumn(TblBusiness, "isFinancialregulator", "System.String")
        AddDataColumn(TblBusiness, "isDisqualifiedDirector", "System.String")
        AddDataColumn(TblBusiness, "isInsolvent", "System.String")
        AddDataColumn(TblBusiness, "isAdverseMedia", "System.String")
    End Sub

    Private Sub PopulateBusiness(ByRef jsonBody As Dictionary(Of String, Object), ByRef TblBusiness As DataTable)
        On Error Resume Next
        Dim businessrow As DataRow
        businessrow = TblBusiness.NewRow()
        businessrow("score") = jsonBody.Item("score").ToString()
        businessrow("id") = jsonBody.Item("business").item("id").ToString()
        businessrow("businessname") = Left(jsonBody.Item("business").item("businessName").ToString(), 200)
        businessrow("telephoneNumber") = Left(jsonBody.Item("business").item("telephoneNumber").ToString(), 50)
        businessrow("faxNumber") = Left(jsonBody.Item("business").item("faxNumber").ToString(), 50)
        businessrow("website") = Left(jsonBody.Item("business").item("website").ToString(), 500)
        businessrow("isPEP") = jsonBody.Item("business").item("isPEP").ToString()
        businessrow("isSanctionsCurrent") = jsonBody.Item("business").item("isSanctionsCurrent").ToString()
        businessrow("isSanctionsPrevious") = jsonBody.Item("business").item("isSanctionsPrevious").ToString()
        businessrow("isLawEnforcement") = jsonBody.Item("business").item("isLawEnforcement").ToString()
        businessrow("isFinancialregulator") = jsonBody.Item("business").item("isFinancialregulator").ToString()
        businessrow("isDisqualifiedDirector") = jsonBody.Item("business").item("isDisqualifiedDirector").ToString()
        businessrow("isInsolvent") = jsonBody.Item("business").item("isInsolvent").ToString()
        businessrow("isAdverseMedia") = jsonBody.Item("business").item("isAdverseMedia").ToString()
        TblBusiness.Rows.Add(businessrow)
    End Sub

    Public Sub PersonSearch(ByVal varThreshold As Integer, ByVal varPEP As Boolean, ByVal varPrevSanc As Boolean, ByVal varCurrSanc As Boolean, ByVal varLaw As Boolean, ByVal varFinReg As Boolean, ByVal varIns As Boolean,
                                         ByVal varDisqDir As Boolean, ByVal varAdMedia As Boolean, ByVal varForename As String, ByVal varMiddlename As String, ByVal varSurname As String, ByVal varDateofBirth As String,
                                         ByVal varYearOfBirth As String, ByVal varAddress As String, ByVal varCity As String, ByVal varCounty As String, ByVal varPostCode As String, ByVal varCountry As String)
        Dim TblPerson As New DataTable, TblAddresses As New DataTable, TblAlias As New DataTable
        Dim TblArticles As New DataTable, TblSanctions As New DataTable, TblNotes As New DataTable
        Dim TblLinkedBusinesses As New DataTable, TblLinkedPersons As New DataTable, TblPoliticalPositions As New DataTable

        Dim varRecordsFound As Integer = 0
        Dim LstPerson As Dictionary(Of String, String) = Nothing
        'Dim varJSONValidCountries As String = GetJSONValidCountries()
        Dim varJSONPerson As String = GetJSONPersonSearch(varThreshold, varPEP, varPrevSanc, varCurrSanc, varLaw, varFinReg, varIns, varDisqDir, varAdMedia, varForename, varMiddlename, varSurname, varDateofBirth, varYearOfBirth, varAddress, varCity, varCounty, varPostCode, varCountry)

        If varJSONPerson <> "" Then
            Dim LstjsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(varJSONPerson)

            For j As Integer = 0 To LstjsonResult.Item("matches").count - 1
                If j = 0 Then
                    CreatePersonColumns(TblPerson)
                    CreateAddressesColumns(TblAddresses)
                    CreateAliasColumns(TblAlias)
                    CreateArticlesColumns(TblArticles)
                    CreateSanctionsColumns(TblSanctions)
                    CreateNotesColumns(TblNotes)
                    CreateLinkedBusinessesColumns(TblLinkedBusinesses)
                    CreateLinkedPersonsColumns(TblLinkedPersons)
                    CreatePoliticalPositionsColumns(TblPoliticalPositions)
                End If
                Dim jsonToPerson = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(LstjsonResult.Item("matches").item(j).ToString())

                PopulatePerson(jsonToPerson, TblPerson)
                PopulateAddresses(jsonToPerson, TblAddresses, "person")
                PopulateAlias(jsonToPerson, TblAlias, "person")
                PopulateArticles(jsonToPerson, TblArticles, "person")
                PopulateSanctions(jsonToPerson, TblSanctions, "person")
                PopulateNotes(jsonToPerson, TblNotes, "person")
                PopulateLinkedBusinesses(jsonToPerson, TblLinkedBusinesses, "person")
                PopulateLinkedPersons(jsonToPerson, TblLinkedPersons, "person")
                PopulatePoliticalPositions(jsonToPerson, TblPoliticalPositions, "person")
            Next

            ClsIMS.UpdateRiskAssessmentPersonTable(TblPerson, TblAddresses, TblAlias, TblArticles, TblSanctions, TblNotes, TblLinkedBusinesses, TblLinkedPersons, TblPoliticalPositions)
        End If
    End Sub

    Public Sub BusinessSearch(ByVal varThreshold As Integer, ByVal varBusinessName As String, ByVal varPEP As Boolean, ByVal varPrevSanc As Boolean, ByVal varCurrSanc As Boolean, ByVal varLaw As Boolean, ByVal varFinReg As Boolean, ByVal varIns As Boolean,
                                         ByVal varDisqDir As Boolean, ByVal varAdMedia As Boolean, ByVal varAddress As String, ByVal varCity As String, ByVal varCounty As String, ByVal varPostCode As String, ByVal varCountry As String)
        Dim TblBusiness As New DataTable, TblAddresses As New DataTable, TblAlias As New DataTable
        Dim TblArticles As New DataTable, TblSanctions As New DataTable, TblNotes As New DataTable
        Dim TblLinkedBusinesses As New DataTable, TblLinkedPersons As New DataTable, TblPoliticalPositions As New DataTable

        Dim varRecordsFound As Integer = 0
        Dim LstBusiness As Dictionary(Of String, String) = Nothing
        Dim varJSONBusiness As String = GetJSONBusinessSearch(varThreshold, varBusinessName, varPEP, varPrevSanc, varCurrSanc, varLaw, varFinReg, varIns, varDisqDir, varAdMedia, varAddress, varCity, varCounty, varPostCode, varCountry)

        If varJSONBusiness <> "" Then
            Dim LstjsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(varJSONBusiness)

            For j As Integer = 0 To LstjsonResult.Item("matches").count - 1
                If j = 0 Then
                    CreateBusinessColumns(TblBusiness)
                    CreateAddressesColumns(TblAddresses)
                    CreateAliasColumns(TblAlias)
                    CreateArticlesColumns(TblArticles)
                    CreateSanctionsColumns(TblSanctions)
                    CreateNotesColumns(TblNotes)
                    CreateLinkedBusinessesColumns(TblLinkedBusinesses)
                    CreateLinkedPersonsColumns(TblLinkedPersons)
                End If
                Dim jsonToBusiness = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(LstjsonResult.Item("matches").item(j).ToString())

                PopulateBusiness(jsonToBusiness, TblBusiness)
                PopulateAddresses(jsonToBusiness, TblAddresses, "business")
                PopulateAlias(jsonToBusiness, TblAlias, "business")
                PopulateArticles(jsonToBusiness, TblArticles, "business")
                PopulateSanctions(jsonToBusiness, TblSanctions, "business")
                PopulateNotes(jsonToBusiness, TblNotes, "business")
                PopulateLinkedBusinesses(jsonToBusiness, TblLinkedBusinesses, "business")
                PopulateLinkedPersons(jsonToBusiness, TblLinkedPersons, "business")
            Next

            ClsIMS.UpdateRiskAssessmentBusinessTable(TblBusiness, TblAddresses, TblAlias, TblArticles, TblSanctions, TblNotes, TblLinkedBusinesses, TblLinkedPersons)
        End If
    End Sub



    Private Function GetJSONValidCountries()
        GetJSONValidCountries = ""
        'Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create("https://uat.c6-intelligence.com/api/v2_0/api/countries"), HttpWebRequest)
        Dim request2 As HttpWebRequest = DirectCast(HttpWebRequest.Create("https://api1.c6-intelligence.com/api/v2_0/api/countries"), HttpWebRequest)
        'request.Method = "GET"
        'request.Headers("apiKey") = "1c57a4a1-c1c0-4003-9abf-39efd3b3e5dd"
        request2.Method = "GET"
        'request2.Headers("apiKey") = "2729be39-e243-4fab-bc80-7b12e4258a05"  'expired
        request2.Headers("apiKey") = "0e2e29b6-513c-4b9f-b31d-e25d8bb82ca5"  '2nd june 2019

        'Dim origResponse2 As HttpWebResponse = DirectCast(request2.GetResponse(), HttpWebResponse)
        'Dim response As WebResponse = request.GetResponse()
        Dim response2 As WebResponse = request2.GetResponse()
        Dim dataStream2 As Stream = response2.GetResponseStream()
        Dim reader2 As New StreamReader(dataStream2)
        Dim responseFromServer2 As String = reader2.ReadToEnd()
        If responseFromServer2 <> "" Then
            GetJSONValidCountries = responseFromServer2
        End If
        reader2.Close()
        dataStream2.Close()
        response2.Close()
    End Function

    Private Function GetJSONPersonSearch(ByVal varThreshold As Integer, ByVal varPEP As Boolean, ByVal varPrevSanc As Boolean, ByVal varCurrSanc As Boolean, ByVal varLaw As Boolean, ByVal varFinReg As Boolean, ByVal varIns As Boolean,
                                         ByVal varDisqDir As Boolean, ByVal varAdMedia As Boolean, ByVal varForename As String, ByVal varMiddlename As String, ByVal varSurname As String, ByVal varDateofBirth As String,
                                         ByVal varYearOfBirth As String, ByVal varAddress As String, ByVal varCity As String, ByVal varCounty As String, ByVal varPostCode As String, ByVal varCountry As String) As String

        GetJSONPersonSearch = ""

        If varThreshold > 0 And varThreshold < 100 And varSurname <> "" Then

            'Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create("https://uat.c6-intelligence.com/api/v2_0/api/persons/search"), HttpWebRequest)
            Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create("https://api1.c6-intelligence.com/api/v2_0/api/persons/search"), HttpWebRequest)
            request.Method = "POST"

            Dim postData As String = "{ ""Threshold"":" & varThreshold &
            ",""PEP"":" & varPEP.ToString().ToLower &
            ",""PreviousSanctions"":" & varPrevSanc.ToString().ToLower &
            ",""CurrentSanctions"":" & varCurrSanc.ToString().ToLower &
            ",""LawEnforcement"":" & varLaw.ToString().ToLower &
            ",""FinancialRegulator"":" & varFinReg.ToString().ToLower &
            ",""Insolvency"":" & varIns.ToString().ToLower &
            ",""DisqualifiedDirector"":" & varDisqDir.ToString().ToLower &
            ",""AdverseMedia"":" & varAdMedia.ToString().ToLower &
            ",""Forename"":" & IIf(varForename = "", "null", """" & varForename & """") &
            ",""Middlename"":" & IIf(varMiddlename = "", "null", """" & varMiddlename & """") &
            ",""Surname"":" & IIf(varSurname = "", "null", """" & varSurname & """")
            If IsDate(varDateofBirth) Then
                postData = postData & ",""DateOfBirth"":""" & CDate(varDateofBirth).ToString("yyyy-MM-dd") & """"
            Else
                postData = postData & ",""DateOfBirth"": null"
            End If

            If IsNumeric(varDateofBirth) Then
                postData = postData & ",""YearOfBirth"":""" & varYearOfBirth.ToString() & """"
            Else
                postData = postData & ",""YearOfBirth"": null"
            End If

            postData = postData & ",""Address"":" & IIf(varAddress = "", "null", """" & varAddress & """") &
           ",""City"":" & IIf(varCity = "", "null", """" & varCity & """") &
            ",""County"":" & IIf(varCounty = "", "null", """" & varCounty & """") &
            ",""Postcode"":" & IIf(varPostCode = "", "null", """" & varPostCode & """") &
            ",""Country"":" & IIf(varCountry = "", "null", """" & varCountry & """") &
            "}"
            Diagnostics.Debug.WriteLine(postData)
            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(postData)
            'request.Headers("apiKey") = "2729be39-e243-4fab-bc80-7b12e4258a05" old
            request.Headers("apiKey") = "0e2e29b6-513c-4b9f-b31d-e25d8bb82ca5" 'good
            'request.Headers("apiKey") = "1c57a4a1-c1c0-4003-9abf-39efd3b3e5dd" 'uat
            request.ContentType = "application/json"
            request.ContentLength = byteArray.Length
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()
            Dim origResponse As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)

            Dim response As WebResponse = request.GetResponse()
            Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            If responseFromServer <> "" Then
                GetJSONPersonSearch = responseFromServer
            End If
            reader.Close()
            dataStream.Close()
            response.Close()
        End If
    End Function

    Private Function GetJSONBusinessSearch(ByVal varThreshold As Integer, ByVal varBusinessName As String, ByVal varPEP As Boolean, ByVal varPrevSanc As Boolean, ByVal varCurrSanc As Boolean, ByVal varLaw As Boolean, ByVal varFinReg As Boolean, ByVal varIns As Boolean,
                                         ByVal varDisqDir As Boolean, ByVal varAdMedia As Boolean, ByVal varAddress As String, ByVal varCity As String, ByVal varCounty As String, ByVal varPostCode As String, ByVal varCountry As String) As String
        GetJSONBusinessSearch = ""
        If varThreshold > 0 And varThreshold < 100 And varBusinessName <> "" Then
            'Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create("https://uat.c6-intelligence.com/api/v2_0/api/businesses/search"), HttpWebRequest)
            Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create("https://api1.c6-intelligence.com/api/v2_0/api/businesses/search"), HttpWebRequest)
            request.Method = "POST"

            Dim postData As String = "{ ""Threshold"":" & varThreshold &
            ",""BusinessName"":" & IIf(varBusinessName = "", "null", """" & varBusinessName & """") &
            ",""PEP"":" & varPEP.ToString().ToLower &
            ",""PreviousSanctions"":" & varPrevSanc.ToString().ToLower &
            ",""CurrentSanctions"":" & varCurrSanc.ToString().ToLower &
            ",""LawEnforcement"":" & varLaw.ToString().ToLower &
            ",""FinancialRegulator"":" & varFinReg.ToString().ToLower &
            ",""Insolvency"":" & varIns.ToString().ToLower &
            ",""DisqualifiedDirector"":" & varDisqDir.ToString().ToLower &
            ",""AdverseMedia"":" & varAdMedia.ToString().ToLower &
            ",""Address"":" & IIf(varAddress = "", "null", """" & varAddress & """") &
            ",""City"":" & IIf(varCity = "", "null", """" & varCity & """") &
            ",""County"":" & IIf(varCounty = "", "null", """" & varCounty & """") &
            ",""Postcode"":" & IIf(varPostCode = "", "null", """" & varPostCode & """") &
            ",""Country"":" & IIf(varCountry = "", "null", """" & varCountry & """") &
            "}"
            Diagnostics.Debug.WriteLine(postData)
            'Dim postData As String = "Threshold"
            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(postData)
            'request.Headers("apiKey") = "2729be39-e243-4fab-bc80-7b12e4258a05"
            'request.Headers("apiKey") = "1c57a4a1-c1c0-4003-9abf-39efd3b3e5dd"
            request.Headers("apiKey") = "0e2e29b6-513c-4b9f-b31d-e25d8bb82ca5"
            request.ContentType = "application/json"
            request.ContentLength = byteArray.Length
            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()
            Dim origResponse As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)

            Dim response As WebResponse = request.GetResponse()
            Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
            dataStream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            If responseFromServer <> "" Then
                GetJSONBusinessSearch = responseFromServer
            End If
            reader.Close()
            dataStream.Close()
            response.Close()
        End If
    End Function


End Class
