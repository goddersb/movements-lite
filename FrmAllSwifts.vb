﻿Public Class FrmAllSwifts

    Private Sub FormatSwiftGrid(ByRef dgv As DataGridView)
        If Not dgv.DataSource Is Nothing Then
            dgv.Columns("ID").Width = 60
            dgv.Columns("Status").Width = 120
            dgv.Columns("SwiftAck").Width = 60
            'dgv.Columns("ConfAck").Width = 60
            'dgv.Columns("StatusAck").Width = 60
            dgv.Columns("SwiftRet").Width = 60
            dgv.Columns("PaymentType").Width = 100
            dgv.Columns("OrderRef").Width = 100
            dgv.Columns("Portfolio").Width = 150
            dgv.Columns("CCYName").Width = 60
            'dgv.Columns("InstName").Width = 100
            dgv.Columns("TransDate").Width = 80
            dgv.Columns("SettleDate").Width = 80
            dgv.Columns("Buy").Width = 100
            dgv.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns("Amount").DefaultCellStyle.Format = "N2"
            dgv.Columns("Amount").ValueType = GetType(SqlTypes.SqlMoney)
            dgv.Columns("BrokerName").Width = 100
            dgv.Columns("ISIN").Width = 100
            dgv.Columns("SwiftFile").Width = 100
            dgv.Columns("SwiftStatus").Width = 100
            dgv.Columns("Username").Width = 100
            dgv.Columns("Created").Width = 100
            dgv.Columns("SwiftSentTime").Width = 100
            dgv.Columns("ReceivedTime").Width = 100
            'dgv.Columns("ConfReceivedTime").Width = 100
            'dgv.Columns("StatusReceivedTime").Width = 100
        End If
    End Sub

    Private Sub RefreshGridSwift()
        Dim CheckSwiftAck As Boolean = False, CheckSwiftPaid As Boolean = False
        Cursor = Cursors.WaitCursor

        ClsIMS.RefreshGridSwift(dgvSwift)
        FormatSwiftGrid(dgvSwift)
        Cursor = Cursors.Default
    End Sub

    Private Sub LoadForm()
        ModRMS.DoubleBuffered(dgvSwift, True)

        If CboFilterStatus.Items.Count = 0 Then
            ClsIMS.PopulatecbostatusTypes(CboFilterStatus)
        End If
        If cboPayCheck.Items.Count = 0 Then
            ClsIMS.PopulatecboPayCheck(cboPayCheck)
        End If
        cboPayCheck.SelectedValue = -1
        RefreshGridSwift()
        ClsRD.ClearClass()
    End Sub

    Private Sub FrmAllSwifts_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadForm()
    End Sub

    Private Sub cmdRDRefresh_Click(sender As Object, e As EventArgs) Handles cmdRDRefresh.Click
        RefreshGridSwift()
    End Sub

    Private Sub CmdAutoSendCSV_Click(sender As Object, e As EventArgs) Handles CmdAutoSendCSV.Click
        SendClientReports()
    End Sub
End Class