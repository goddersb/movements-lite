﻿Public Class FrmPaymentAuthorise
    Public Function ValidateConfirm() As Boolean
        ValidateConfirm = True
        Dim IsMyPayment As Boolean = IIf(LCase(ClsIMS.FullUserName) = LCase(ClsPayAuth.UserNameEntered), True, False)

        If ValidateConfirm And (Rb1.Checked Or Rb2.Checked) And ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPayAuth.PortfolioFeeID <> 0 Then
            Dim varId = ClsIMS.GetSQLItem("Select P_ID from vwDolfinPaymentGridview p1 where p1.p_portfolioisfee = 1 and p1.P_ID = " & ClsPayAuth.SelectedPaymentID.ToString & " And (p1.P_Status <>'" & ClsIMS.GetStatusType(cStatusCancelled) & "' or (p1.P_Status='" & ClsIMS.GetStatusType(cStatusCancelled) & "' and p1.P_PortfolioFeeID = (Select P_ID from vwDolfinPaymentGridview p2 where p2.P_Status <>'" & ClsIMS.GetStatusType(cStatusCancelled) & "' and p1.P_PortfolioFeeID = p2.p_id And (p1.P_ID = P2.P_PortfolioFeeID Or isnull(P2.P_PortfolioFeeID, 0)=0))))")

            If varId = "" Then
                ValidateConfirm = False
                MsgBox("You cannot un-cancel this fee because its original payment has been removed or linked to a different fee.", vbCritical, "Cannot un-cancel")
            End If
        End If
        If (Rb1.Checked Or Rb2.Checked) And ChkSwift.Checked And (ClsPayAuth.SwiftAcknowledged Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusSent) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusSwiftAck) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusAwaitingSwift)) Then
            ValidateConfirm = False
            MsgBox("You cannot run any swift actions against this transaction as it has the status " & ClsPayAuth.Status, vbCritical, "Cannot action")
        ElseIf (Rb1.Checked Or Rb2.Checked) And ChkIMS.Checked And (ClsPayAuth.IMSAcknowledged Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusSent) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusSwiftErr) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusAwaitingIMS)) Then
            ValidateConfirm = False
            MsgBox("You cannot run any IMS actions against this transaction as it has the status " & ClsPayAuth.Status, vbCritical, "Cannot action")
        ElseIf (Rb1.Checked Or Rb2.Checked) And ClsIMS.PaymentSendsSwift(ClsPayAuth.PaymentType) And ChkSwift.Checked And ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingReview) Then
            ValidateConfirm = False
            MsgBox("You cannot run any payment actions against this transaction as it has the status " & ClsPayAuth.Status, vbCritical, "Cannot action")
        ElseIf (Rb1.Checked Or Rb2.Checked) And (ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusSwiftErr) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusSwiftRej) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusIMSErr)) Then
            ValidateConfirm = False
            MsgBox("You cannot authorise/unauthorise a transaction on error status. Please copy this transaction to resend", vbCritical, "Cannot resend error transaction")
        ElseIf (Rb1.Checked Or Rb2.Checked) And ClsPayAuth.Status <> ClsIMS.GetStatusType(cStatusReady) And (ClsPayAuth.Status <> ClsIMS.GetStatusType(cStatusAboveThreshold) And (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And (Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise3rdPartyPayments))) Then
            ValidateConfirm = False
            MsgBox("You are not allowed to authorise/unauthorise 3rd party movements", vbCritical, "Cannot authorise/unauthorise 3rd party movements")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCashMovements, "Blocked user from authorising 3rd party movement for ID: " & ClsPayAuth.SelectedPaymentID.ToString, ClsPayAuth.SelectedPaymentID)
        ElseIf (Rb1.Checked Or Rb2.Checked) And ClsPayAuth.Status <> ClsIMS.GetStatusType(cStatusReady) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientOut) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescExOut) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescExVolopaOut) And (Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise)) Then
            ValidateConfirm = False
            MsgBox("You are not allowed to authorise/unauthorise movements", vbCritical, "Cannot authorise/unauthorise movements")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCashMovements, "Blocked user from authorising movement for ID: " & ClsPayAuth.SelectedPaymentID.ToString, ClsPayAuth.SelectedPaymentID)
        ElseIf (Rb1.Checked Or Rb2.Checked) And ClsPayAuth.Status <> ClsIMS.GetStatusType(cStatusReady) And (Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseMyPayments) And IsMyPayment) Then
            ValidateConfirm = False
            MsgBox("You are not allowed to authorise your own movements", vbCritical, "Cannot authorise your own movements")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCashMovements, "Blocked user from authorising their own movement for ID: " & ClsPayAuth.SelectedPaymentID.ToString, ClsPayAuth.SelectedPaymentID)
        ElseIf (Rb4.Checked And Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserEdit)) Then
            ValidateConfirm = False
            MsgBox("You are not authorised to edit movements", vbCritical, "Cannot edit movements")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCashMovements, "Blocked user from editing movement for ID: " & ClsPayAuth.SelectedPaymentID.ToString, ClsPayAuth.SelectedPaymentID)
        ElseIf (Rb5.Checked And Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserDelete)) Then
            ValidateConfirm = False
            MsgBox("You are not authorised to delete movements", vbCritical, "Cannot remove movements")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCashMovements, "Blocked user from removing payment for ID: " & ClsPayAuth.SelectedPaymentID.ToString, ClsPayAuth.SelectedPaymentID)
        ElseIf (Rb1.Checked And ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingAuth) And ClsPayAuth.AboveThreshold And Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAboveThreshold)) Then
            ValidateConfirm = False
            MsgBox("You cannot authorise a transaction that is above the threshold. Please get management to authorise this payment", vbCritical, "Cannot authorise payment above threshold")
        ElseIf RB7.Checked And ((Not (ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusSwiftErr) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusSwiftRej))) Or Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise)) Then
            ValidateConfirm = False
            MsgBox("You are not authorised to re-send a swift rejected/swift error transaction. Please get management to authorise this instruction", vbCritical, "Cannot authorise re-sending swift instruction")
        ElseIf RB8.Checked And ((Not (ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusIMSErr) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusAwaitingIMS))) Or Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise)) Then
            ValidateConfirm = False
            MsgBox("You are not authorised to re-send an IMS error transaction. Please get management to authorise this instruction", vbCritical, "Cannot authorise re-sending IMS instruction")
        End If
    End Function

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Dim varResponse As Integer
        Dim varDesc As String = ""


        ClsPayAuth.SelectedAuthoriseOption = 0
        If ValidateConfirm() Then

            If Rb1.Checked Then
                varResponse = MsgBox("Are you sure you want to authorise this movement?", vbQuestion + vbYesNo, "Authorise Payment")
                If varResponse = vbYes Then
                    ClsPayAuth.SelectedAuthoriseOption = 1
                    varDesc = "Authorised movement from FLOW"
                End If
            ElseIf Rb2.Checked Then
                varResponse = MsgBox("Are you sure you want to unauthorise this movement?", vbQuestion + vbYesNo, "Unauthorise Payment")
                If varResponse = vbYes Then
                    ClsPayAuth.SelectedAuthoriseOption = 2
                    varDesc = "Un-authorised movement from FLOW"
                End If
            ElseIf Rb3.Checked Then
                varResponse = MsgBox("Are you sure you want to edit this movement?", vbQuestion + vbYesNo, "Edit Payment")
                If varResponse = vbYes Then
                    ClsPayAuth.SelectedAuthoriseOption = 7
                    varDesc = "Edited movement from FLOW"
                End If
            ElseIf Rb4.Checked Then
                varResponse = MsgBox("Are you sure you want to copy this movement?", vbQuestion + vbYesNo, "Copy movement")
                If varResponse = vbYes Then
                    ClsPayAuth.SelectedAuthoriseOption = 4
                    varDesc = "copy movement from FLOW"
                End If
            ElseIf Rb5.Checked Then
                If ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Then
                    varResponse = MsgBox("Are you sure you want to delete this payment?" & vbNewLine & "Note: This will also remove any associated payment fees", vbQuestion + vbYesNo, "Remove Payment")
                Else
                    varResponse = MsgBox("Are you sure you want to delete this movement?", vbQuestion + vbYesNo, "delete Movement")
                End If
                If varResponse = vbYes Then
                    ClsPayAuth.SelectedAuthoriseOption = 5
                    ClsIMS.DeleteSwiftFile()
                    varDesc = "Deleted movement from FLOW"
                End If
            ElseIf RB6.Checked Then
                varResponse = MsgBox("Are you sure you want to remove this movement from the grid?", vbQuestion + vbYesNo, "remove movement")
                If varResponse = vbYes Then
                    ClsPayAuth.SelectedAuthoriseOption = 6
                    varDesc = "removed movement from grid in FLOW"
                End If
            ElseIf RB7.Checked Then
                varResponse = MsgBox("Are you sure you want to re-send this authorised Swift message (Send to Auto-Ready)?", vbQuestion + vbYesNo, "Re-Send Swift")
                If varResponse = vbYes Then
                    ClsPayAuth.SelectedAuthoriseOption = 8
                    varDesc = "Re-send swift from grid in FLOW"
                End If
            ElseIf RB8.Checked Then
                varResponse = MsgBox("Are you sure you want to re-send this authorised IMS message (Send to Re-Send IMS)?", vbQuestion + vbYesNo, "Re-Send IMS")
                If varResponse = vbYes Then
                    ClsPayAuth.SelectedAuthoriseOption = 9
                    varDesc = "Re-send IMS from grid in FLOW"
                End If
            End If

            ClsIMS.ActionMovement(ClsPayAuth.SelectedAuthoriseOption, ClsPayAuth.SelectedPaymentID, ChkSwift.Checked, ChkIMS.Checked, ClsIMS.FullUserName, varDesc)

            If varResponse = vbYes Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub FrmPaymentAuthorise_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtAuthID.Text = "#" & ClsPayAuth.SelectedPaymentID
        txtAuthPayType.Text = ClsPayAuth.PaymentType

        If Not ClsIMS.PaymentSendsSwift(ClsPayAuth.PaymentType) Then
            ChkSwift.Enabled = False
            ChkIMS.Checked = ClsPayAuth.SendIMS
        ElseIf ClsPayAuth.OtherID = cPayDescInOtherNoticetoReceive Or ClsPayAuth.OtherID = cPayDescInOtherStatementRequest Then
            ChkSwift.Checked = ClsPayAuth.SendSwift
            ChkIMS.Enabled = False
        Else
            ChkSwift.Checked = ClsPayAuth.SendSwift
            ChkIMS.Checked = ClsPayAuth.SendIMS
        End If

        If ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
            ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
            ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
            If ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Then
                txtAuthPortfolio.Text = ""
                lblauthportfolio.Text = ""
            Else
                lblauthportfolio.Text = "Fee Type: "
                txtAuthPortfolio.Text = ClsIMS.GetPaymentOtherType(ClsPayAuth.OtherID)
            End If
            txtAuthOrderAccount.Text = ClsPayAuth.Portfolio
            txtAuthOrder.Text = ClsPayAuth.OrderAccount
            txtAuthBenAccount.Text = ClsPayAuth.BeneficiaryPortfolio
            txtAuthBen.Text = ClsPayAuth.BeneficiaryAccount


            lblauthorderaccount.Text = "Order Portfolio: "
            lblauthordername.Text = "Order Acct: "
            lblauthbenaccount.Text = "Ben Portfolio: "
            lblauthbenname.Text = "Ben Acct: "
        Else
            txtAuthPortfolio.Text = ClsPayAuth.Portfolio
            If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescIn) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) Then
                txtAuthOrderAccount.Text = ClsPayAuth.OrderAccount
                txtAuthOrder.Text = ClsPayAuth.OrderName
            End If
            txtAuthBenAccount.Text = ClsPayAuth.BeneficiaryAccount
            If ClsPayAuth.BeneficiaryTypeID = 0 Then
                txtAuthBen.Text = ClsPayAuth.BeneficiaryFirstName & " " & ClsPayAuth.BeneficiaryMiddleName & " " & ClsPayAuth.BeneficiarySurname
            Else
                txtAuthBen.Text = ClsPayAuth.BeneficiaryName
            End If
        End If

        If IsNumeric(ClsPayAuth.Amount) Then
            txtAuthAmount.Text = CDbl(ClsPayAuth.Amount).ToString("#,##0.00")
        End If

        txtBatchOtherID.Text = ClsPayAuth.PaymentIDOther
        txtAuthCCY.Text = ClsPayAuth.CCY
        txtAuthComments.Text = ClsPayAuth.Comments
        If ((ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And ClsIMS.PaymentHasFee(ClsPayAuth.SelectedPaymentID)) Or
            (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPayAuth.PortfolioIsFee) Then
            txtPortfolioFee.Text = ClsPayAuth.PortfolioFeeID
            txtPortfolioFee.BackColor = Color.MistyRose
        End If

        ChkIMSReturned.Checked = ClsPayAuth.IMSReturned
        ChkIMSAck.Checked = ClsPayAuth.IMSAcknowledged
        txtIMSStatus.Text = ClsPayAuth.IMSStatus
        ChkSwiftReturned.Checked = ClsPayAuth.SwiftReturned
        ChkSwiftAck.Checked = ClsPayAuth.SwiftAcknowledged
        txtSwiftStatus.Text = ClsPayAuth.SwiftStatus
    End Sub

    Private Sub Rb1_CheckedChanged(sender As Object, e As EventArgs) Handles Rb1.CheckedChanged
        ColorRadio(Rb1)
    End Sub

    Private Sub Rb2_CheckedChanged(sender As Object, e As EventArgs) Handles Rb2.CheckedChanged
        ColorRadio(Rb2)
    End Sub

    Private Sub Rb3_CheckedChanged(sender As Object, e As EventArgs) Handles Rb3.CheckedChanged
        ColorRadio(Rb3)
    End Sub

    Private Sub Rb4_CheckedChanged(sender As Object, e As EventArgs) Handles Rb4.CheckedChanged
        ColorRadio(Rb4)
    End Sub

    Private Sub CmdMailAttach_Click(sender As Object, e As EventArgs) Handles CmdMailAttach.Click
        If ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingAuth) Then
            If ClsPayAuth.CanSendEmail Then
                FrmEmail.ShowDialog()
            Else
                MsgBox("The email to authorise this payment has already been sent for this manual payment.", vbInformation, "Payment email authorisation has already been sent")
            End If
        Else
            ClsPayAuth.OpenOutlookEmail(False)
        End If
    End Sub

    Private Sub CmdMail_Click(sender As Object, e As EventArgs) Handles CmdMail.Click
        If ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingAuth) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusAllocatingFunds) Then
            FrmEmail.ShowDialog()
        Else
            ClsPayAuth.OpenOutlookEmail(True)
        End If
    End Sub

    Private Sub cmdSendControl_Click(sender As Object, e As EventArgs) Handles cmdSendControl.Click
        If (ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingAuth) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingReview) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusReady)) Then
            NewSendControl = New FrmSendControl
            NewSendControl.ShowDialog()
            ChkSwift.Checked = ClsPayAuth.SendSwift
            ChkIMS.Checked = ClsPayAuth.SendIMS
        Else
            MsgBox("Cannot change the status of send IMS checkbox because the status is not pending or ready", vbInformation)
        End If
    End Sub
End Class