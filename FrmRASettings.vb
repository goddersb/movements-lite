﻿Public Class FrmRASettings

    Dim RAConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet
    Dim dsCCY As New DataSet
    Dim dgvdataCCY As New SqlClient.SqlDataAdapter
    Dim cboCCYColumn As New DataGridViewComboBoxColumn
    Dim dsStatus As New DataSet
    Dim cboStatusColumn As New DataGridViewComboBoxColumn

    Private Sub FrmRASettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadRASettings()
    End Sub

    Private Sub LoadRASettings()
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "select Ctry_ID,Ctry_Name,Ctry_Score from DolfinPaymentRARangeCountry"
            dgvCtry.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRARangeCountry")
            dgvCtry.AutoGenerateColumns = True
            dgvCtry.DataSource = ds.Tables("DolfinPaymentRARangeCountry")
            dgvCtry.Columns("Ctry_ID").Width = 60
            dgvCtry.Columns("Ctry_ID").ReadOnly = True
            dgvCtry.Columns("Ctry_ID").HeaderText = "ID"
            dgvCtry.Columns("Ctry_Name").Width = 150
            dgvCtry.Columns("Ctry_Name").HeaderText = "Country"
            dgvCtry.Columns("Ctry_Name").ReadOnly = True
            dgvCtry.Columns("Ctry_Score").Width = 60
            dgvCtry.Columns("Ctry_Score").HeaderText = "Score"

            For Each column In dgvCtry.Columns
                If column.index > 1 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvCtry.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCtry.Refresh()

            varSQL = "select PEP_ID,PEP_Description,PEP_MinScoreRange,PEP_MaxScoreRange,PEP_Score from DolfinPaymentRARangePEP"
            dgvPEP.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRAPEP")
            dgvPEP.AutoGenerateColumns = True
            dgvPEP.DataSource = ds.Tables("DolfinPaymentRAPEP")
            dgvPEP.Columns("PEP_ID").Width = 60
            dgvPEP.Columns("PEP_ID").ReadOnly = True
            dgvPEP.Columns("PEP_ID").HeaderText = "ID"
            dgvPEP.Columns("PEP_Description").Width = 150
            dgvPEP.Columns("PEP_Description").HeaderText = "PEP Description"
            dgvPEP.Columns("PEP_MinScoreRange").Width = 60
            dgvPEP.Columns("PEP_MinScoreRange").HeaderText = "Min Score"
            dgvPEP.Columns("PEP_MaxScoreRange").Width = 60
            dgvPEP.Columns("PEP_MaxScoreRange").HeaderText = "Max Score"
            dgvPEP.Columns("PEP_Score").Width = 60
            dgvPEP.Columns("PEP_Score").HeaderText = "Score"

            For Each column In dgvPEP.Columns
                If column.index > 1 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvPEP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvPEP.Refresh()

            varSQL = "select REC_ID,REC_Description,REC_MinScoreRange,REC_MaxScoreRange,REC_Score from DolfinPaymentRARangeRecipient"
            dgvRecipient.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRARangeRecipient")
            dgvRecipient.AutoGenerateColumns = True
            dgvRecipient.DataSource = ds.Tables("DolfinPaymentRARangeRecipient")
            dgvRecipient.Columns("REC_ID").Width = 60
            dgvRecipient.Columns("REC_ID").ReadOnly = True
            dgvRecipient.Columns("REC_ID").HeaderText = "ID"
            dgvRecipient.Columns("REC_Description").Width = 150
            dgvRecipient.Columns("REC_Description").HeaderText = "Recipient Description"
            dgvRecipient.Columns("REC_MinScoreRange").Width = 60
            dgvRecipient.Columns("REC_MinScoreRange").HeaderText = "Min Score"
            dgvRecipient.Columns("REC_MaxScoreRange").Width = 60
            dgvRecipient.Columns("REC_MaxScoreRange").HeaderText = "Max Score"
            dgvRecipient.Columns("REC_Score").Width = 60
            dgvRecipient.Columns("REC_Score").HeaderText = "Score"

            For Each column In dgvRecipient.Columns
                If column.index > 1 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvRecipient.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvRecipient.Refresh()

            varSQL = "select BNK_ID,BNK_Description,BNK_MinScoreRange,BNK_MaxScoreRange,BNK_Score from DolfinPaymentRARangeBank"
            dgvBank.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRARangeBank")
            dgvBank.AutoGenerateColumns = True
            dgvBank.DataSource = ds.Tables("DolfinPaymentRARangeBank")
            dgvBank.Columns("BNK_ID").Width = 60
            dgvBank.Columns("BNK_ID").ReadOnly = True
            dgvBank.Columns("BNK_ID").HeaderText = "ID"
            dgvBank.Columns("BNK_Description").Width = 150
            dgvBank.Columns("BNK_Description").HeaderText = "Bank Description"
            dgvBank.Columns("BNK_MinScoreRange").Width = 60
            dgvBank.Columns("BNK_MinScoreRange").HeaderText = "Min Score"
            dgvBank.Columns("BNK_MaxScoreRange").Width = 60
            dgvBank.Columns("BNK_MaxScoreRange").HeaderText = "Max Score"
            dgvBank.Columns("BNK_Score").Width = 60
            dgvBank.Columns("BNK_Score").HeaderText = "Score"

            For Each column In dgvBank.Columns
                If column.index > 1 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvBank.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvBank.Refresh()

            varSQL = "select CCY_ID,CCY_Description,CCY_Score from DolfinPaymentRARangeCCY"
            dgvCCY.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRARangeCCY")
            dgvCCY.AutoGenerateColumns = True
            dgvCCY.DataSource = ds.Tables("DolfinPaymentRARangeCCY")
            dgvCCY.Columns("CCY_ID").Width = 60
            dgvCCY.Columns("CCY_ID").ReadOnly = True
            dgvCCY.Columns("CCY_ID").HeaderText = "ID"
            dgvCCY.Columns("CCY_Description").Width = 150
            dgvCCY.Columns("CCY_Description").HeaderText = "CCY"
            dgvCCY.Columns("CCY_Score").Width = 60
            dgvCCY.Columns("CCY_Score").HeaderText = "Score"

            For Each column In dgvCCY.Columns
                If column.index > 1 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvCCY.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCCY.Refresh()

            dgvdataCCY = New SqlClient.SqlDataAdapter("Select CR_Name1 as CCY_Description from vwDolfinPaymentCurrencies", RAConn)
            dsCCY = New DataSet
            dgvdataCCY.Fill(dsCCY, "vwDolfinPaymentCurrencies")
            cboCCYColumn = New DataGridViewComboBoxColumn
            cboCCYColumn.HeaderText = "CCY"
            cboCCYColumn.DataPropertyName = "CCY_Description"
            cboCCYColumn.DataSource = dsCCY.Tables("vwDolfinPaymentCurrencies")
            cboCCYColumn.ValueMember = dsCCY.Tables("vwDolfinPaymentCurrencies").Columns(0).ColumnName
            cboCCYColumn.DisplayMember = dsCCY.Tables("vwDolfinPaymentCurrencies").Columns(0).ColumnName
            cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCCYColumn.FlatStyle = FlatStyle.Flat
            cboCCYColumn.Width = 285
            dgvCCY.Columns.Insert(1, cboCCYColumn)
            dgvCCY.Columns(2).Visible = False

            varSQL = "select Pay_ID,Pay_RangeFrom,Pay_RangeTo,Pay_Score from DolfinPaymentRARangePay"
            dgvPayRanges.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRARangePay")
            dgvPayRanges.AutoGenerateColumns = True
            dgvPayRanges.DataSource = ds.Tables("DolfinPaymentRARangePay")
            dgvPayRanges.Columns("Pay_ID").Width = 60
            dgvPayRanges.Columns("Pay_ID").ReadOnly = True
            dgvPayRanges.Columns("Pay_ID").HeaderText = "ID"
            dgvPayRanges.Columns("Pay_RangeFrom").Width = 150
            dgvPayRanges.Columns("Pay_RangeFrom").HeaderText = "From (Payment Range)"
            dgvPayRanges.Columns("Pay_RangeFrom").DefaultCellStyle.Format = "##,0"
            dgvPayRanges.Columns("Pay_RangeFrom").ReadOnly = True
            dgvPayRanges.Columns("Pay_RangeFrom").DefaultCellStyle.BackColor = Color.LightGray
            dgvPayRanges.Columns("Pay_RangeTo").Width = 100
            dgvPayRanges.Columns("Pay_RangeTo").HeaderText = "To (Payment Range)"
            dgvPayRanges.Columns("Pay_RangeTo").DefaultCellStyle.Format = "##,0"
            dgvPayRanges.Columns("Pay_Score").Width = 100
            dgvPayRanges.Columns("Pay_Score").HeaderText = "Score"

            For Each column In dgvPayRanges.Columns
                If column.index > 0 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvPayRanges.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvPayRanges.Refresh()

            varSQL = "select PTy_ID,PTy_Description,PTy_Score from DolfinPaymentRARangePayType"
            dgvPayTypes.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRARangePayType")
            dgvPayTypes.AutoGenerateColumns = True
            dgvPayTypes.DataSource = ds.Tables("DolfinPaymentRARangePayType")
            dgvPayTypes.Columns("PTy_ID").Width = 60
            dgvPayTypes.Columns("PTy_ID").ReadOnly = True
            dgvPayTypes.Columns("PTy_ID").HeaderText = "ID"
            dgvPayTypes.Columns("PTy_Description").Width = 285
            dgvPayTypes.Columns("PTy_Description").HeaderText = "Pay Type Description"
            dgvPayTypes.Columns("PTy_Score").Width = 60
            dgvPayTypes.Columns("PTy_Score").HeaderText = "Score"

            For Each column In dgvPayTypes.Columns
                If column.index > 1 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvPayTypes.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvPayTypes.Refresh()

            varSQL = "select Pay_ID,Pay_RangeFrom,Pay_RangeTo,Pay_Score from DolfinPaymentRARangePayCumulative"
            dgvPayRangesCumulative.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRARangePayCumulative")
            dgvPayRangesCumulative.AutoGenerateColumns = True
            dgvPayRangesCumulative.DataSource = ds.Tables("DolfinPaymentRARangePayCumulative")
            dgvPayRangesCumulative.Columns("Pay_ID").Width = 60
            dgvPayRangesCumulative.Columns("Pay_ID").ReadOnly = True
            dgvPayRangesCumulative.Columns("Pay_ID").HeaderText = "ID"
            dgvPayRangesCumulative.Columns("Pay_RangeFrom").Width = 150
            dgvPayRangesCumulative.Columns("Pay_RangeFrom").HeaderText = "From (Payment Range)"
            dgvPayRangesCumulative.Columns("Pay_RangeFrom").DefaultCellStyle.Format = "##,0"
            dgvPayRangesCumulative.Columns("Pay_RangeFrom").ReadOnly = True
            dgvPayRangesCumulative.Columns("Pay_RangeFrom").DefaultCellStyle.BackColor = Color.LightGray
            dgvPayRangesCumulative.Columns("Pay_RangeTo").Width = 100
            dgvPayRangesCumulative.Columns("Pay_RangeTo").HeaderText = "To (Payment Range)"
            dgvPayRangesCumulative.Columns("Pay_RangeTo").DefaultCellStyle.Format = "##,0"
            dgvPayRangesCumulative.Columns("Pay_Score").Width = 100
            dgvPayRangesCumulative.Columns("Pay_Score").HeaderText = "Score"

            For Each column In dgvPayRangesCumulative.Columns
                If column.index > 0 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvPayRangesCumulative.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvPayRangesCumulative.Refresh()

            varSQL = "select BeneficiaryTypeID,BeneficiaryType,BeneficiaryTypeThreshold from vwDolfinPaymentBeneficiaryType"
            dgvBenTypes.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentBeneficiaryType")
            dgvBenTypes.AutoGenerateColumns = True
            dgvBenTypes.DataSource = ds.Tables("vwDolfinPaymentBeneficiaryType")
            dgvBenTypes.Columns("BeneficiaryTypeID").Width = 60
            dgvBenTypes.Columns("BeneficiaryTypeID").ReadOnly = True
            dgvBenTypes.Columns("BeneficiaryTypeID").HeaderText = "ID"
            dgvBenTypes.Columns("BeneficiaryType").Width = 150
            dgvBenTypes.Columns("BeneficiaryType").HeaderText = "Type"
            dgvBenTypes.Columns("BeneficiaryTypeThreshold").Width = 60
            dgvBenTypes.Columns("BeneficiaryTypeThreshold").HeaderText = "Threshold"

            For Each column In dgvBenTypes.Columns
                If column.index > 1 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvPayTypes.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvPayTypes.Refresh()

            varSQL = "select RR_ID,S_ID,S_ID_Escalated,RR_Description,RR_MinScoreRange,RR_MaxScoreRange,RR_RiskScore from DolfinPaymentRARangeResult where RR_MinScoreRange >= 0"
            dgvPayResults.DataSource = Nothing
            RAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "DolfinPaymentRARangeResult")
            dgvPayResults.AutoGenerateColumns = True
            dgvPayResults.DataSource = ds.Tables("DolfinPaymentRARangeResult")
            dgvPayResults.Columns("RR_ID").Width = 60
            dgvPayResults.Columns("RR_ID").ReadOnly = True
            dgvPayResults.Columns("RR_ID").HeaderText = "ID"
            dgvPayResults.Columns("RR_Description").Width = 200
            dgvPayResults.Columns("RR_Description").HeaderText = "Result Description"
            dgvPayResults.Columns("RR_MinScoreRange").Width = 60
            dgvPayResults.Columns("RR_MinScoreRange").HeaderText = "Min Score"
            dgvPayResults.Columns("RR_MaxScoreRange").Width = 60
            dgvPayResults.Columns("RR_MaxScoreRange").HeaderText = "Max Score"
            dgvPayResults.Columns("RR_RiskScore").Width = 60
            dgvPayResults.Columns("RR_RiskScore").HeaderText = "Risk Score"
            dgvPayResults.Columns("S_ID").Visible = False
            dgvPayResults.Columns("S_ID_Escalated").Visible = False

            varSQL = "select S_ID, S_Status from vwDolfinPaymentStatus"
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            dsStatus = New DataSet
            dgvdata.Fill(dsStatus, "vwDolfinPaymentStatus")
            cboStatusColumn = New DataGridViewComboBoxColumn
            cboStatusColumn.HeaderText = "Status"
            cboStatusColumn.DataPropertyName = "S_ID"
            cboStatusColumn.DataSource = dsStatus.Tables("vwDolfinPaymentStatus")
            cboStatusColumn.ValueMember = dsStatus.Tables("vwDolfinPaymentStatus").Columns(0).ColumnName
            cboStatusColumn.DisplayMember = dsStatus.Tables("vwDolfinPaymentStatus").Columns(1).ColumnName
            cboStatusColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboStatusColumn.FlatStyle = FlatStyle.Flat

            dgvPayResults.Columns.Insert(7, cboStatusColumn)
            dgvPayResults.Columns(7).Width = 200

            varSQL = "select S_ID as S_ID_Escalated, S_Status from vwDolfinPaymentStatus"
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RAConn)
            dsStatus = New DataSet
            dgvdata.Fill(dsStatus, "vwDolfinPaymentStatus")
            cboStatusColumn = New DataGridViewComboBoxColumn
            cboStatusColumn.HeaderText = "Status Escalated"
            cboStatusColumn.DataPropertyName = "S_ID_Escalated"
            cboStatusColumn.DataSource = dsStatus.Tables("vwDolfinPaymentStatus")
            cboStatusColumn.ValueMember = dsStatus.Tables("vwDolfinPaymentStatus").Columns(0).ColumnName
            cboStatusColumn.DisplayMember = dsStatus.Tables("vwDolfinPaymentStatus").Columns(1).ColumnName
            cboStatusColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboStatusColumn.FlatStyle = FlatStyle.Flat

            dgvPayResults.Columns.Insert(8, cboStatusColumn)
            dgvPayResults.Columns(8).Width = 200

            dgvPayResults.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvPayResults.Refresh()


            For Each column In dgvPayResults.Columns
                If column.index > 1 Then
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvCtry.DataSource = Nothing
            dgvPEP.DataSource = Nothing
            dgvRecipient.DataSource = Nothing
            dgvBank.DataSource = Nothing
            dgvCCY.DataSource = Nothing
            dgvPayRanges.DataSource = Nothing
            dgvPayTypes.DataSource = Nothing
            dgvBenTypes.DataSource = Nothing
            dgvPayRangesCumulative.DataSource = Nothing
            dgvPayResults.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameSystemSettings, Err.Description & " - Problem with viewing system settings")
        End Try
    End Sub

    Private Sub dgvCtry_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCtry.CellFormatting
        If e.ColumnIndex = 2 Then
            FormatGrid(dgvCtry, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Public Sub FormatGrid(ByRef dgv As DataGridView, ByRef row As Integer, ByRef col As Integer)
        If Not IsDBNull(dgv.Rows(row).Cells(col).Value) Then
            If Not dgv.Rows(row).Cells(col).Value Is Nothing Then
                If dgv.Rows(row).Cells(col).Value > 900 Then
                    dgv.Rows(row).DefaultCellStyle.BackColor = Color.LightCoral
                ElseIf dgv.Rows(row).Cells(col).Value >= 150 And dgv.Rows(row).Cells(col).Value <= 900 Then
                    dgv.Rows(row).DefaultCellStyle.BackColor = Color.DarkOrange
                ElseIf dgv.Rows(row).Cells(col).Value >= 31 And dgv.Rows(row).Cells(col).Value <= 149 Then
                    dgv.Rows(row).DefaultCellStyle.BackColor = Color.LightYellow
                ElseIf dgv.Rows(row).Cells(col).Value >= 0 And dgv.Rows(row).Cells(col).Value <= 30 Then
                    dgv.Rows(row).DefaultCellStyle.BackColor = Color.LightGreen
                End If
            End If
        End If
    End Sub


    Private Sub dgvPEP_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvPEP.CellFormatting
        If e.ColumnIndex = 4 Then
            FormatGrid(dgvPEP, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Private Sub dgvBank_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvBank.CellFormatting
        If e.ColumnIndex = 4 Then
            FormatGrid(dgvBank, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Private Sub dgvCCY_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCCY.CellFormatting
        If e.ColumnIndex = 2 Then
            FormatGrid(dgvCCY, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Private Sub dgvPayRanges_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvPayRanges.CellFormatting
        If e.ColumnIndex = 3 Then
            FormatGrid(dgvPayRanges, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Private Sub dgvPayTypes_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvPayTypes.CellFormatting
        If e.ColumnIndex = 2 Then
            FormatGrid(dgvPayTypes, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Private Sub dgvRecipient_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvRecipient.CellFormatting
        If e.ColumnIndex = 4 Then
            FormatGrid(dgvRecipient, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Private Sub dgvPayRangesCumulative_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvPayRangesCumulative.CellFormatting
        If e.ColumnIndex = 3 Then
            FormatGrid(dgvPayRangesCumulative, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Private Sub dgvPayResults_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvPayResults.CellFormatting
        If e.ColumnIndex = 4 Then
            FormatGrid(dgvPayResults, e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Private Sub dgvBenTypes_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvBenTypes.CellFormatting
        If e.ColumnIndex = 2 Then
            FormatGrid(dgvBenTypes, e.RowIndex, e.ColumnIndex)
        End If
    End Sub
End Class