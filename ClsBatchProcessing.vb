﻿Public Class ClsBatchProcessing
    Private varTemplateID As Double = 0
    Private varPortfolioCode As Double = 0
    Private varPortfolioName As String = ""
    Private varCCYCode As Double = 0
    Private varCCYName As String = ""


    Property TemplateID() As Double
        Get
            Return varTemplateID
        End Get
        Set(value As Double)
            varTemplateID = value
        End Set
    End Property

    Property PortfolioCode() As Double
        Get
            Return varPortfolioCode
        End Get
        Set(value As Double)
            varPortfolioCode = value
        End Set
    End Property

    Property PortfolioName() As String
        Get
            Return varPortfolioName
        End Get
        Set(value As String)
            varPortfolioName = value
        End Set
    End Property

    Property CCYCode() As Double
        Get
            Return varCCYCode
        End Get
        Set(value As Double)
            varCCYCode = value
        End Set
    End Property

    Property CCYName() As String
        Get
            Return varCCYName
        End Get
        Set(value As String)
            varCCYName = value
        End Set
    End Property
End Class
