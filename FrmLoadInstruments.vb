﻿Imports System.Data.SqlClient

Public Class FrmLoadInstruments

    Dim _formLoaded As Boolean = False
    Dim _cboSectorLoaded As Boolean = False
    Dim _cboGroupLoaded As Boolean = False
    Dim _cboIndustriesLoaded As Boolean = False
    Dim _cboSubsLoaded As Boolean = False
    Dim _issuerPU_Code As Integer
    Dim _underlyingInstrument As Boolean = False
    Dim _underlyingTicker As String = String.Empty

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub FrmLoadInstruments_Load(sender As Object, e As EventArgs) Handles Me.Load

        PopulateCbo()

    End Sub

    Public Function GetInstrumentShortCut(SecInstrType As String, SecDes As String, SecSymbol As String,
                                          SecISIN As String, SecCUSIP As String, SecSEDOL As String,
                                          SecDes2 As String) As String

        Dim Shortcut As String = ""
        Select Case SecInstrType
            Case "ETF"
                Shortcut = SecSymbol
            Case "Fund"
                Shortcut = SecSymbol
            Case "Equity", "Warrant"
                Shortcut = SecSymbol
            Case "FixedIncome"
                Shortcut = SecISIN
            Case "MutualFund"
                Shortcut = SecSymbol
            Case "Future"
                Shortcut = Trim(Replace(SecDes2, " COMB", " "))
            Case "Option"
                Shortcut = Trim(Replace(SecDes, " COMB", " "))
            Case "Currency"
                Shortcut = SecDes
            Case "Index"
                Shortcut = SecDes
            Case Else
                Shortcut = IIf(Shortcut = "", SecDes, SecCUSIP)
        End Select
        GetInstrumentShortCut = Shortcut

    End Function

    Public Function GetInstrumentType(SecInstrType As String, SecFundType As String) As String

        Dim InstrumentType As String = ""
        Select Case SecInstrType
            Case "ETF", "Fund", "MutualFund"
                InstrumentType = "Mutual Funds"
            Case "Equity", "Warrant"
                InstrumentType = "Equities"
            Case "FixedIncome"
                InstrumentType = "Bonds"
            Case "Future"
                InstrumentType = "Futures"
            Case "Option"
                InstrumentType = "Options"
            Case "Currency"
                InstrumentType = "Currencies"
            Case "Index"
                InstrumentType = SecInstrType
            Case Else
                InstrumentType = IIf(InstrumentType = "", SecFundType, SecInstrType)
        End Select
        GetInstrumentType = InstrumentType

    End Function

    Public Function ReturnSuffix(ByVal varTicker As String) As String

        varTicker = LCase(varTicker)
        If Strings.Right(varTicker, 7) = " curncy" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 6) & "Curncy"
        ElseIf Strings.Right(varTicker, 7) = " comdty" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 6) & "Comdty"
        ElseIf Strings.Right(varTicker, 7) = " equity" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 6) & "Equity"
        ElseIf Strings.Right(varTicker, 6) = " index" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 5) & "Index"
        ElseIf Strings.Right(varTicker, 12) = " msg1 curncy" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 11) & "msg1 Curncy"
        ElseIf Strings.Right(varTicker, 5) = " corp" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 4) & "Corp"
        ElseIf Strings.Right(varTicker, 10) = " msg1 corp" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 9) & "msg1 Corp"
        ElseIf Strings.Right(varTicker, 11) = " @msg1 corp" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 10) & "@msg1 Corp"
        ElseIf Strings.Right(varTicker, 4) = " pfd" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 3) & "pfd"
        ElseIf Strings.Right(varTicker, 5) = " govt" Then
            ReturnSuffix = Strings.Left(UCase(varTicker), Len(varTicker) - 4) & "Govt"
        Else
            ReturnSuffix = ""
        End If

    End Function

    Private Sub PopulateCbo()

        'instrument main
        ClsIMS.PopulateComboboxWithData(CboCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentCurrencies group by CR_Code,CR_Name1 order by CR_Name1")
        'ClsIMS.PopulateComboboxWithData(cboTradedCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentCurrencies group by CR_Code,CR_Name1 order by CR_Name1")
        ClsIMS.PopulateComboboxWithData(cboTradedCCY, "SELECT 1, 'GBp' UNION SELECT 2, 'GBX'")
        ClsIMS.PopulateComboboxWithData(cboCouponCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentCurrencies group by CR_Code,CR_Name1 order by CR_Name1")
        'ClsIMS.PopulateComboboxWithData(cboType, "SELECT TC_Code, TC_Name FROM titlecategory GROUP BY TC_Code,TC_Name UNION SELECT 27, 'Warrant' ORDER BY TC_Name")
        ClsIMS.PopulateComboboxWithData(cboType, "SELECT TC_Code,TC_Name FROM titlecategory GROUP BY TC_Code,TC_Name ORDER BY TC_Name")
        ClsIMS.PopulateComboboxWithData(cboIssuedCtry, "Select Cry_Shortcut,Cry_Name from vwDolfinPaymentCountries group by Cry_Shortcut,Cry_Name order by Cry_Name")
        ClsIMS.PopulateComboboxWithData(cboIssuer, "select pu_code,pu_name1 from publisher group by pu_code,pu_name1 order by pu_name1")
        ClsIMS.PopulateComboboxWithData(cboEquityType, "SELECT 0, 'By Name' UNION SELECT 1, 'Anonymous'")

        'main info
        ClsIMS.PopulateComboboxWithData(cboPeriod, ";WITH ctePeriod AS (SELECT 12 AS [ind], 'Year' AS [Per], 1 AS [idx] UNION SELECT 6, '6 months', 2 UNION SELECT 4, '4 months', 3UNION SELECT 3, 'Quarter', 4 UNION SELECT 3, '2 months' , 5 UNION SELECT 1, 'Month', 6 UNION SELECT 20, 'Months...', 7 UNION SELECT 30, 'Days...', 8) SELECT [ind], [per] FROM ctePeriod ORDER BY [idx] ASC", False)
        ClsIMS.PopulateComboboxWithData(cboTitleType, "SELECT 0,'Dematerialized' UNION SELECT 1,'Physical'")
        ClsIMS.PopulateComboboxWithData(cboSetDays, ";WITH Numbers AS(SELECT [SetDays] = 1 UNION ALL SELECT [SetDays] + 1 FROM Numbers WHERE [SetDays] + 1 <= 31) SELECT [SetDays] AS [Id], CAST([SetDays] AS CHAR (2)) AS [Set Days] FROM Numbers")
        ClsIMS.PopulateComboboxWithData(cboOptionType, "SELECT 1, 'CALL' UNION SELECT 2, 'PUT'")
        ClsIMS.PopulateComboboxWithData(cboStyle, "SELECT 0 AS [Id], 'European' AS [Name] UNION SELECT 1, 'American' UNION SELECT 2, 'Asian' UNION SELECT 3, 'Bermudan' UNION SELECT 4, 'Other'")
        ClsIMS.PopulateComboboxWithData(cboDeliveryType, "SELECT 0 AS [Id], 'Optional' AS [Name] UNION SELECT 1, 'Physically settled' UNION SELECT 2, 'Cash settled' ")
        ClsIMS.PopulateComboboxWithData(cboCommDerivInd, " SELECT 0, 'N/A' UNION SELECT 1, 'False' UNION SELECT 2, 'True'")
        ClsIMS.PopulateComboboxWithData(cboPortfolioLink, ";WITH Numbers AS(SELECT [val] = 0 UNION ALL SELECT [val] + 1 FROM Numbers WHERE [val] + 1 <= 31) SELECT [val] AS [Id], CAST([val] AS CHAR (2)) AS [val] FROM Numbers")

        'links
        ClsIMS.PopulateComboboxWithData(cboBBGType, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'BLOOMBERGTYPES' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr]")
        ClsIMS.PopulateComboboxWithData(cboPriceQuoteType, "SELECT 0, 'Quoted' UNION SELECT 1 , 'Discount Quoted'")
        ClsIMS.PopulateComboboxWithData(cboBBGPriceSource, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'BLOOMBERGPRCSRC' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr] ")
        ClsIMS.PopulateComboboxWithData(cboExternalFeedModel, "SELECT EFM_ID, CASE WHEN LEN(LTRIM(ISNULL(EFM_Description1,'')))>0 THEN EFM_Description1 ELSE IMSplus.dbo.fnGetExternalFeedModelDescription(EFM_ID) END AS Description FROM IMSplus.dbo.ExternalFeedModels LEFT JOIN IMSplus.dbo.PriceProviders PP1 ON PP1.PP_Id=EFM_RTFeedProviderId  LEFT JOIN IMSplus.dbo.PriceProviders PP2 ON PP2.PP_Id=EFM_AutoUpdStaticProviderId LEFT JOIN IMSplus.dbo.PriceProviders PP3 ON PP3.PP_Id=EFM_AutoFeedPriceProviderId  LEFT JOIN IMSplus.dbo.ExtPriceFeedModel ON EPFM_ID=EFM_ExPrFeedModelID UNION  SELECT 0, ''")

        'fundamentals
        ClsIMS.PopulateComboboxWithData(cboGroup, "Select TGN_Code,TGN_name1 from TitleGroups group by TGN_Code,TGN_name1 order by TGN_name1")
        ClsIMS.PopulateComboboxWithData(cboMoodys, "Select [RH_ID],[RH_Value] FROM [IMSplus].[dbo].[RatingHouses] where rh_type = 9 GROUP BY [RH_ID],[RH_Value] ORDER BY [RH_Value]")
        ClsIMS.PopulateComboboxWithData(cboSP, "Select [RH_ID],[RH_Value] FROM [IMSplus].[dbo].[RatingHouses] where rh_type = 10 GROUP BY [RH_ID],[RH_Value] ORDER BY [RH_Value]")
        ClsIMS.PopulateComboboxWithData(cboFitch, "Select [RH_ID],[RH_Value] FROM [IMSplus].[dbo].[RatingHouses] where rh_type = 11 GROUP BY [RH_ID],[RH_Value] ORDER BY [RH_Value]")

        ClsIMS.PopulateComboboxWithData(cboCalcType, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'BONDBASETYPES' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr] ")
        ClsIMS.PopulateComboboxWithData(cboCouponType, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'T_CouponType' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr] ")
        ClsIMS.PopulateComboboxWithData(cboBondType, "SELECT 0,'Bond'  UNION SELECT 1, 'T-Bill' UNION SELECT 2, 'Zero Coupon'")
        ClsIMS.PopulateComboboxWithData(cboRelatedIndex, "select GRT_Code, GRT_Shortcut + ' [' + GRT_Shortcut + ']' from [dbo].[General_Ref_Table] where GRT_Categ = 1 group by GRT_Code, GRT_Shortcut order by GRT_Code, GRT_Shortcut")
        ClsIMS.PopulateComboboxWithData(cboTaxCalc, " SELECT ROW_NUMBER() over (order by [L_Value]),[L_Value] FROM [IMSplus].[dbo].[Strings] s where [L_Section] = 'OBJECTS' and left(L_Entry,14) = 'TI_TAX_CALC_AT' GROUP BY [L_Value] ORDER BY s.[L_Value]")
        ClsIMS.PopulateComboboxWithData(cboCalcTerms, "SELECT 0, '<Default>' UNION SELECT 1, 'Round coupon/round accruals based on coupon (5dc)' UNION SELECT 2, 'Round accruals based on period days (5dc)' UNION SELECT 3, 'Inflation adjustment using index ratio' ")
        ClsIMS.PopulateComboboxWithData(cboLongCoupon, "SELECT 0,'No Coupon' UNION SELECT 1,'Abnormal First Coupon' UNION SELECT 9,'Abnormal Last Coupon'")
        ClsIMS.PopulateComboboxWithData(cboInDayOff, "SELECT 1, 'Next Working Day' UNION SELECT -1, 'Previous Working Day'")
        ClsIMS.PopulateComboboxWithData(cboIntCalc1, "SELECT 1,'Whole' UNION SELECT 0,'Proportional'")
        ClsIMS.PopulateComboboxWithData(cboInNewMonth, "SELECT 1, 'Next Working Day' UNION SELECT -1, 'Previous Working Day'")
        ClsIMS.PopulateComboboxWithData(cboIntCalc2, " SELECT 1,'Whole' UNION SELECT 0,'Proportional'")
        ClsIMS.PopulateComboboxWithData(cboCollateralType, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'BOND_COLLTYPE' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr]")
        ClsIMS.PopulateComboboxWithData(cboIssueType, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'BOND_ISSUETYPE' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr]")
        ClsIMS.PopulateComboboxWithData(cboBondYieldType, "SELECT 0,'Default' UNION SELECT 1,'Romania'")

        ClsIMS.PopulateComboboxWithData(cboTradedBy, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'TRADED_BY' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr] ")
        ClsIMS.PopulateComboboxWithData(cbotier, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'TIER' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr] ")
        ClsIMS.PopulateComboboxWithData(cboRedemptionOptions, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'REDEMPTIONOPTIONS' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr] ")
        ClsIMS.PopulateComboboxWithData(cboInflationLinked, "SELECT [P_Code],[P_Descr] FROM [IMSplus].[dbo].[Parameters] where [P_Type] = 'INFLATIONLINKED' GROUP BY [P_Code],[P_Descr] ORDER BY [P_Descr] ")

        'SSI
        ClsIMS.PopulateComboboxWithData(cboPOS, "select GRT_Code, GRT_Shortcut from [dbo].[General_Ref_Table] group by GRT_Code, GRT_Shortcut order by GRT_Code, GRT_Shortcut")
        ClsIMS.PopulateComboboxWithData(cboCpty, "SELECT [B_Code],[B_Name1] FROM Brokers GROUP BY [B_Code],[B_Name1] ORDER BY [B_Name1] ")
        ClsIMS.PopulateComboboxWithData(cboSecClearingAgt1, "SELECT [B_Code],[B_Name1] FROM Brokers GROUP BY [B_Code],[B_Name1] ORDER BY [B_Name1] ")
        ClsIMS.PopulateComboboxWithData(cboSecClearingAgt2, "SELECT [B_Code],[B_Name1] FROM Brokers GROUP BY [B_Code],[B_Name1] ORDER BY [B_Name1] ")
        ClsIMS.PopulateComboboxWithData(cboCashClearingAgt, "SELECT [B_Code],[B_Name1] FROM Brokers GROUP BY [B_Code],[B_Name1] ORDER BY [B_Name1] ")

    End Sub

    Private Function GetInstrumentValue(ByVal LstjsonResult As Dictionary(Of String, Object), ByVal varTicker As String, ByVal varName As String) As String

        On Error GoTo NotFound
        GetInstrumentValue = ""
        If LstjsonResult.Item(varTicker).ContainsKey(varName) Then
            GetInstrumentValue = LstjsonResult.Item(varTicker).item(varName).ToString()
        End If
        Exit Function

NotFound:

    End Function

    Private Function MapPeriod(varCPNFREQ As Integer, varCountryCode As String) As String

        MapPeriod = ""
        If varCPNFREQ = 1 And varCountryCode <> "RU" Then
            MapPeriod = "Year"
        ElseIf varCPNFREQ = 2 And varCountryCode <> "RU" Then
            MapPeriod = "6 months"
        ElseIf varCPNFREQ = 4 And varCountryCode <> "RU" Then
            MapPeriod = "Quarter"
        ElseIf varCPNFREQ = 12 And varCountryCode <> "RU" Then
            MapPeriod = "Month"
        ElseIf varCountryCode = "RU" Then
            MapPeriod = "Months..."
        End If

    End Function

    ''' <summary>
    ''' Map the Bloomberg field DERIVATIVE_DELIVERY_TYPE
    ''' </summary>
    ''' <param name="_deliveryType"></param>
    ''' <returns></returns>
    Private Function MapDerivativeDeliveryType(ByVal _deliveryType As String)

        If _deliveryType = "OPTL" Then
            Return 0
        ElseIf _deliveryType = "PHYS" Then
            Return 1
        Else
            Return 2 'Cash Settled
        End If

    End Function

    ''' <summary>
    ''' Map the Bloomberg field OPT_EXER_TYP
    ''' </summary>
    ''' <param name="_opt_Exer_Typ"></param>
    ''' <returns></returns>
    Private Function MapStyle(ByVal _opt_Exer_Typ As String)

        If _opt_Exer_Typ = "European" Then
            Return 0
        ElseIf _opt_Exer_Typ = "American" Then
            Return 1
        ElseIf _opt_Exer_Typ = "Asian" Then
            Return 2
        ElseIf _opt_Exer_Typ = "Bermudan" Then
            Return 3
        Else 'Other
            Return 4
        End If

    End Function

    Private Function MapSubType(ByVal varType As String, ByVal varIsPutable As String, ByVal varIsCallable As String, ByVal varIsConvertible As String, ByVal varIsSinkable As String) As Integer

        MapSubType = 0
        If varType = "Bonds" Then
            If varIsPutable = "N" And varIsCallable = "N" And varIsConvertible = "N" And varIsSinkable = "Y" Then
                MapSubType = 203 ' BondSinking
            ElseIf varIsPutable = "Y" And varIsCallable = "N" And varIsConvertible = "N" And varIsSinkable = "N" Then
                MapSubType = 204 ' BondPuttable
            ElseIf varIsPutable = "N" And varIsCallable = "Y" And varIsConvertible = "N" And varIsSinkable = "N" Then
                MapSubType = 201 ' BondCallable
            ElseIf varIsPutable = "N" And varIsCallable = "N" And varIsConvertible = "Y" And varIsSinkable = "N" Then
                MapSubType = 202 ' BondConvertible
            ElseIf varIsPutable = "Y" And varIsCallable = "N" And varIsConvertible = "N" And varIsSinkable = "Y" Then
                MapSubType = 213 ' BondPuttableSinkable
            ElseIf varIsPutable = "N" And varIsCallable = "Y" And varIsConvertible = "N" And varIsSinkable = "Y" Then
                MapSubType = 211 ' BondCallableSinkable
            ElseIf varIsPutable = "Y" And varIsCallable = "N" And varIsConvertible = "Y" And varIsSinkable = "N" Then
                MapSubType = 212 ' BondPuttableConvertible
            ElseIf varIsPutable = "N" And varIsCallable = "Y" And varIsConvertible = "Y" And varIsSinkable = "N" Then
                MapSubType = 205 ' BondCallableConvertible
            ElseIf varIsPutable = "N" And varIsCallable = "N" And varIsConvertible = "Y" And varIsSinkable = "Y" Then
                MapSubType = 216 ' BondConvertibleSinkable
            ElseIf varIsPutable = "N" And varIsCallable = "Y" And varIsConvertible = "Y" And varIsSinkable = "Y" Then
                MapSubType = 214 ' BondCallableConvertibleSinkable
            ElseIf varIsPutable = "Y" And varIsCallable = "N" And varIsConvertible = "Y" And varIsSinkable = "Y" Then
                MapSubType = 215 ' BondPuttableConvertibleSinkable
            End If
        ElseIf varType = "Mutual Funds" Then
            MapSubType = 402
        ElseIf varType = "Options" Then
            MapSubType = 0
        End If

    End Function

    Private Function MapCpnType(ByVal varCpnType As String) As Integer
        'MapCpnType = 0
        If varCpnType = "FIXED" Then
            MapCpnType = 0
        ElseIf varCpnType = "FLOATING" Or varCpnType = "STEP CPN" Then
            MapCpnType = 1
        Else
            MapCpnType = 2
        End If
    End Function

    Private Function MapBondType(ByVal varSecName As String, ByVal varIsZeroCpn As String) As String
        MapBondType = "Bond"
        If InStr(varSecName, "Treasury Bill") <> 0 Then
            MapBondType = "T-Bill"
        ElseIf varIsZeroCpn = "Y" Then
            MapBondType = "Zero Coupon"
        End If
    End Function

    Private Function MapCalcType(ByVal varDayCount As Integer) As Integer
        MapCalcType = 0
        MapCalcType = ClsIMS.GetSQLItem("Select MAX(IMSplus_Day_Cnt) FROM IMSPlus.dbo.Title_DCC_Map WHERE Source_Day_Cnt = " & varDayCount)
    End Function

    Private Function MapPOS(ByVal varCountryCode As String) As Integer
        MapPOS = 0
        MapPOS = ClsIMS.GetSQLItem("Select ISNULL(IMSPlus.dbo._Dolfin_GetSettlePlaceID((select CRY_Code from countries where cry_shortcut = '" & varCountryCode & "'), 2) + 1000, -2)")
    End Function

    Private Function MapGroup(ByVal varType As String, ByVal varMarketDesc As String, ByVal varCountryCode As String, ByVal _fundType As String) As Integer

        MapGroup = 0
        If varType = "Equity" Or varType = "Equities" Then
            Dim MapGroupStr As String = ClsIMS.GetSQLItem($"SELECT IMSPlus.dbo._Dolfin_GetTitleGroupEquity(IMSPlus.dbo._Dolfin_GetCountryID('{varCountryCode}'), IMSPlus.dbo._Dolfin_GetCountryRegionID('{varCountryCode}'))")
            If IsNumeric(MapGroupStr) Then
                MapGroup = MapGroupStr
            End If
        ElseIf varType = "FixedIncome" Then
            If InStr(varMarketDesc, "Govt") Then
                MapGroup = 207
            ElseIf InStr(varMarketDesc, "Corp") Then
                MapGroup = 206
            Else
                MapGroup = 205
            End If
        ElseIf varType = "Fund" Then
            If _fundType = "ETF" Then
                MapGroup = 404
            ElseIf varType = "Fund" Or varType = "MutualFund" Then
                MapGroup = 213
            End If
        ElseIf varType = "Future" Then
            MapGroup = 74
        ElseIf varType = "Option" Then
            MapGroup = 13
        ElseIf varType = "Warrant" Then
            MapGroup = 210
        ElseIf varType = "Index" Then
            MapGroup = 220
        End If

    End Function

    Private Function MapMarket(ByVal varType As String, ByVal CCY As String) As String

        MapMarket = ""
        If varType = "Equity" Then
            MapMarket = "OTC-Equities-" & CCY
        ElseIf varType = "FixedIncome" Then
            MapMarket = "OTC-Bond-" & CCY
        ElseIf varType = "ETF" Or varType = "Fund" Or varType = "MutualFund" Then
            MapMarket = "Funds-" + CCY
        ElseIf varType = "Future" Or varType = "Option" Then
            MapMarket = "OTC-Derivative-" + CCY
        ElseIf varType = "Index" Then
            MapMarket = "INDEX AND OPTIONS MARKET" + CCY
        End If

    End Function

    Private Sub PopulateInstrumentClass(ByVal varTicker As String, ByVal LstjsonResult As Dictionary(Of String, Object))

        ClsInstrument.ReferenceSecurityClass = GetInstrumentValue(LstjsonResult, varTicker, "BPIPE_REFERENCE_SECURITY_CLASS")
        ClsInstrument.CCY = GetInstrumentValue(LstjsonResult, varTicker, "CRNCY")
        ClsInstrument.CouponCCY = GetInstrumentValue(LstjsonResult, varTicker, "CPN_CRNCY")
        ClsInstrument.TradeCCY = GetInstrumentValue(LstjsonResult, varTicker, "TRADE_CRNCY")
        ClsInstrument.Description = GetInstrumentValue(LstjsonResult, varTicker, "CIE_DES")
        ClsInstrument.Name = GetInstrumentValue(LstjsonResult, varTicker, "NAME")
        ClsInstrument.FundType = GetInstrumentValue(LstjsonResult, varTicker, "FUND_TYP")
        ClsInstrument.MarketSectorDescription = GetInstrumentValue(LstjsonResult, varTicker, "MARKET_SECTOR_DES")
        ClsInstrument.CountryISO = GetInstrumentValue(LstjsonResult, varTicker, "CNTRY_ISSUE_ISO")
        ClsInstrument.SecurityDescription = GetInstrumentValue(LstjsonResult, varTicker, "SECURITY_DES")
        ClsInstrument.ISIN = GetInstrumentValue(LstjsonResult, varTicker, "ID_ISIN")
        ClsInstrument.MIFIDIndicator = GetInstrumentValue(LstjsonResult, varTicker, "MIFID_II_FIRDS_INDICATOR")
        ClsInstrument.Issuer = GetInstrumentValue(LstjsonResult, varTicker, "ISSUER")
        ClsInstrument.Putable = GetInstrumentValue(LstjsonResult, varTicker, "PUTABLE")
        ClsInstrument.Callable = GetInstrumentValue(LstjsonResult, varTicker, "CALLABLE")
        ClsInstrument.Convertible = GetInstrumentValue(LstjsonResult, varTicker, "CONVERTIBLE")
        ClsInstrument.Sinkable = GetInstrumentValue(LstjsonResult, varTicker, "SINKABLE")
        ClsInstrument.CFICode = GetInstrumentValue(LstjsonResult, varTicker, "CFI_CODE")
        ClsInstrument.IssueDate = GetInstrumentValue(LstjsonResult, varTicker, "ISSUE_DT")
        ClsInstrument.InitDate = GetInstrumentValue(LstjsonResult, varTicker, "OPEN_INT_DATE")
        ClsInstrument.MaturityDate = GetInstrumentValue(LstjsonResult, varTicker, "MATURITY")
        ClsInstrument.FutLastDate = GetInstrumentValue(LstjsonResult, varTicker, "FUT_DLV_DT_LAST")
        ClsInstrument.CouponFrequency = GetInstrumentValue(LstjsonResult, varTicker, "CPN_FREQ")
        ClsInstrument.Interest = GetInstrumentValue(LstjsonResult, varTicker, "INT_ACC")
        ClsInstrument.Coupon = GetInstrumentValue(LstjsonResult, varTicker, "CPN")
        ClsInstrument.FaceValue = GetInstrumentValue(LstjsonResult, varTicker, "PAR_AMT")
        ClsInstrument.Cusip = GetInstrumentValue(LstjsonResult, varTicker, "ID_CUSIP")
        ClsInstrument.Sedol = GetInstrumentValue(LstjsonResult, varTicker, "ID_SEDOL1")
        ClsInstrument.TickerAndExchangeCode = GetInstrumentValue(LstjsonResult, varTicker, "TICKER_AND_EXCH_CODE")
        ClsInstrument.DayCount = GetInstrumentValue(LstjsonResult, varTicker, "DAY_CNT")
        ClsInstrument.CouponType = GetInstrumentValue(LstjsonResult, varTicker, "CPN_TYP")
        ClsInstrument.ZeroCoupon = GetInstrumentValue(LstjsonResult, varTicker, "ZERO_CPN")
        ClsInstrument.MinimumDenomination = GetInstrumentValue(LstjsonResult, varTicker, "MIN_INCREMENT")
        ClsInstrument.MinimumTradingQuantity = GetInstrumentValue(LstjsonResult, varTicker, "MIN_PIECE")
        ClsInstrument.Exchange_Symbol = GetInstrumentValue(LstjsonResult, varTicker, "ID_EXCH_SYMBOL")
        If Not String.IsNullOrEmpty(GetInstrumentValue(LstjsonResult, varTicker, "OPT_MULTIPLIER")) Then
            ClsInstrument.T_Factor = GetInstrumentValue(LstjsonResult, varTicker, "OPT_MULTIPLIER")
        End If
        If Not String.IsNullOrEmpty(GetInstrumentValue(LstjsonResult, varTicker, "PX_POS_MULT_FACTOR")) Then
            ClsInstrument.POS_Multiplier_Factor = (GetInstrumentValue(LstjsonResult, varTicker, "PX_POS_MULT_FACTOR"))
        End If
        If Not String.IsNullOrEmpty(GetInstrumentValue(LstjsonResult, varTicker, "FUT_NOTICE_FIRST")) Then
            ClsInstrument.Future_Notice_First_Date = (GetInstrumentValue(LstjsonResult, varTicker, "FUT_NOTICE_FIRST"))
        End If
        If Not String.IsNullOrEmpty(GetInstrumentValue(LstjsonResult, varTicker, "TICKER")) Then
            ClsInstrument.Ticker = (GetInstrumentValue(LstjsonResult, varTicker, "TICKER"))
        End If
        ClsInstrument.Future_First_Trade_Date = GetInstrumentValue(LstjsonResult, varTicker, "FUT_FIRST_TRADE_DT")
        If Not String.IsNullOrEmpty(GetInstrumentValue(LstjsonResult, varTicker, "DAYS_TO_SETTLE")) Then
            ClsInstrument.SETTLEMENT_DAYS = GetInstrumentValue(LstjsonResult, varTicker, "DAYS_TO_SETTLE")
        Else
            ClsInstrument.SETTLEMENT_DAYS = "0"
        End If
        'ClsInstrument.FIRST_TRADING_DATE = GetInstrumentValue(LstjsonResult, varTicker, "FIRST_TRADING_DATE")

        ClsInstrument.RatingsMoodys = GetInstrumentValue(LstjsonResult, varTicker, "RTG_MOODY_NO_WATCH")
        ClsInstrument.RatingsSP = GetInstrumentValue(LstjsonResult, varTicker, "RTG_SP_NO_WATCH")
        ClsInstrument.RatingsFitch = GetInstrumentValue(LstjsonResult, varTicker, "RTG_FITCH_NO_WATCH")

        ClsInstrument.FirstCouponDate = GetInstrumentValue(LstjsonResult, varTicker, "FIRST_CPN_DT")
        ClsInstrument.Industry = GetInstrumentValue(LstjsonResult, varTicker, "GICS_SUB_INDUSTRY")
        ClsInstrument.AskPrice = GetInstrumentValue(LstjsonResult, varTicker, "PX_ASK")
        ClsInstrument.BidPrice = GetInstrumentValue(LstjsonResult, varTicker, "PX_BID")
        ClsInstrument.OpenPrice = GetInstrumentValue(LstjsonResult, varTicker, "PX_OPEN")
        ClsInstrument.ClosePrice = GetInstrumentValue(LstjsonResult, varTicker, "PX_YEST_CLOSE")

        ClsInstrument.PutCall = GetInstrumentValue(LstjsonResult, varTicker, "OPT_PUT_CALL")
        ClsInstrument.Strike = GetInstrumentValue(LstjsonResult, varTicker, "OPT_STRIKE_PX")
        ClsInstrument.TickSize = GetInstrumentValue(LstjsonResult, varTicker, "OPT_TICK_SIZE")
        ClsInstrument.Derivative_Delivery_Type = GetInstrumentValue(LstjsonResult, varTicker, "DERIVATIVE_DELIVERY_TYPE")
        ClsInstrument.Style = GetInstrumentValue(LstjsonResult, varTicker, "OPT_EXER_TYP")

        'Extra Info.
        ClsInstrument.BBG_ID = GetInstrumentValue(LstjsonResult, varTicker, "ID_BB_UNIQUE")
        ClsInstrument.Company_To_Parent_Relationship = IIf(String.IsNullOrEmpty(GetInstrumentValue(LstjsonResult, varTicker, "PARENT_COMP_NAME")), GetInstrumentValue(LstjsonResult, varTicker, "COMPANY_TO_PARENT_RELATIONSHIP"), (GetInstrumentValue(LstjsonResult, varTicker, "PARENT_COMP_NAME")))
        ClsInstrument.MFID_Complex_Instr_Indicator = GetInstrumentValue(LstjsonResult, varTicker, "MIFID_II_COMPLEX_INSTR_INDICATOR")
        ClsInstrument.Delivery_Type = GetInstrumentValue(LstjsonResult, varTicker, "DELIVERY_TYP")
        ClsInstrument.Close_Price = GetInstrumentValue(LstjsonResult, varTicker, "PX_YEST_CLOSE")
        ClsInstrument.High_Price = GetInstrumentValue(LstjsonResult, varTicker, "PX_HIGH")
        ClsInstrument.Low_Price = GetInstrumentValue(LstjsonResult, varTicker, "PX_LOW")

        If ClsInstrument.CCY = "GBp" Then
            ClsInstrument.InPence = True
        Else
            ClsInstrument.InPence = False
        End If

    End Sub

    Private Sub PopulateForm()

        If _formLoaded Then
            PopulateCbo()
        End If

        txtDesc.Text = ClsInstrument.Description

        cboType.Text = GetInstrumentType(ClsInstrument.ReferenceSecurityClass, ClsInstrument.FundType)
        txtName.Text = IIf(cboType.SelectedValue = 7, ClsInstrument.SecurityDescription, IIf(cboType.SelectedValue = 8, $"{ClsInstrument.Ticker} {ClsInstrument.MarketSectorDescription}", ClsInstrument.Name))
        CboCCY.Text = Strings.UCase(ClsInstrument.CCY)
        cboCouponCCY.Text = Strings.UCase(ClsInstrument.CouponCCY)
        cboTradedCCY.Text = IIf(Strings.UCase(ClsInstrument.CCY) = "GB" Or ClsInstrument.CCY = "GBp", ClsInstrument.CCY, String.Empty)

        If IsNumeric(cboType.SelectedValue) Then
            cboGroup.SelectedValue = MapGroup(ClsInstrument.ReferenceSecurityClass, ClsInstrument.MarketSectorDescription, ClsInstrument.CountryISO, ClsInstrument.FundType)
            cboPortfolioLink.SelectedValue = ClsInstrument.SETTLEMENT_DAYS
            If cboType.SelectedValue = 1 Or cboType.SelectedValue = 8 Then
                ClsIMS.PopulateComboboxWithData(cboSubType, "SELECT	0 AS [tsubtype], 'Normal' AS [Name1]")
                cboSubType.SelectedIndex = 0
                If cboType.SelectedValue = 8 Then
                    cboDeliveryType.SelectedValue = MapDerivativeDeliveryType(ClsInstrument.Derivative_Delivery_Type)
                End If
            ElseIf cboType.SelectedValue = 7 Then
                cboStyle.SelectedValue = MapStyle(ClsInstrument.Style)
                cboDeliveryType.SelectedValue = MapDerivativeDeliveryType(ClsInstrument.Derivative_Delivery_Type)
                ClsIMS.PopulateComboboxWithData(cboSubType, "SELECT	0 AS [tsubtype], 'Normal' AS [Name1]")
                cboSubType.SelectedIndex = 0
            Else
                ClsIMS.PopulateComboboxWithData(cboSubType, "SELECT tsubtype, Name1 FROM [dbo].[titlesubtype] WHERE ttype = " & cboType.SelectedValue & " GROUP BY tsubtype, Name1 ORDER BY tsubtype")
            End If
            ClsIMS.PopulateComboboxWithData(cboMarket, "SELECT [M_Code], [M_Name] FROM [IMSplus].[dbo].[Markets] WHERE M_CRCode = " & CboCCY.SelectedValue & " GROUP BY [M_Code], [M_Name] ORDER BY M_Name")
            cboMarket.Text = MapMarket(ClsInstrument.ReferenceSecurityClass, CboCCY.Text)
        End If

        txtISIN.Text = ClsInstrument.ISIN
        If ClsInstrument.MIFIDIndicator = "Y" Then
            ChkESMA.Checked = True
        Else
            ChkESMA.Checked = False
        End If

        cboIssuer.Text = ClsInstrument.Issuer
        cboIssuedCtry.SelectedValue = ClsInstrument.CountryISO
        cboSubType.SelectedValue = MapSubType(cboType.Text, ClsInstrument.Putable, ClsInstrument.Callable, ClsInstrument.Convertible, ClsInstrument.Sinkable)
        txttax.Text = "0"
        txtCFICode.Text = ClsInstrument.CFICode
        ChkApproved.Checked = ClsInstrument.Approved
        ChkActive.Checked = ClsInstrument.Active

        'Main info
        txtIssueDate.Text = ClsInstrument.IssueDate
        txtInitDate.Text = ClsInstrument.InitDate
        cboEquityType.SelectedValue = 0
        chkMidCost.Checked = True
        chkMidCost.Enabled = False
        lblDeliveryType.Visible = False
        lblDeliveryType.Location = New Point(365, 275)
        cboDeliveryType.Visible = False
        cboDeliveryType.Location = New Point(446, 325)
        lblSedol.Visible = True
        txtSedol.Visible = True
        lblCusip.Visible = True
        txtCusip.Visible = True
        lblMainTitle.Location = New Point(361, 175)
        lblMainTitle.Text = "Main Title:"
        txtMainTitle.Size = New Size(148, 20)
        txtMainTitle.Text = String.Empty
        lblTaxPolicy.Location = New Point(40, 275)
        chkMidCost.Location = New Point(446, 249)

        cboCommDerivInd.Visible = False
        cboCommDerivInd.Location = New Point(446, 352)
        lblCommDerivInd.Visible = False
        lblCommDerivInd.Location = New Point(356, 355)

        txtInterest.Location = New Point(105, 219)
        lblInterest.Location = New Point(54, 223)

        'txtEntranceValue.Text = 1 -- Moved to Type specific below.
        If ClsInstrument.IssueDate = "" Then
            txtIssueDate.Text = ClsInstrument.InitDate
        End If
        If ClsInstrument.InitDate = "" Then
            txtInitDate.Text = ClsInstrument.IssueDate
        End If

        txtMaturityDate.Text = ClsInstrument.MaturityDate
        If txtMaturityDate.Text = "" Then
            txtMaturityDate.Text = ClsInstrument.FutLastDate
        End If

        Dim varCpnFreq As String = ClsInstrument.CouponFrequency
        If IsNumeric(varCpnFreq) Then
            If cboIssuedCtry.Text <> "" Then
                cboPeriod.Text = MapPeriod(CInt(varCpnFreq), cboIssuedCtry.SelectedValue)
                If varCpnFreq = 2 And cboIssuedCtry.SelectedValue = "RU" Then
                    txtPeriod.Text = 182
                ElseIf varCpnFreq = 4 And cboIssuedCtry.SelectedValue = "RU" Then
                    txtPeriod.Text = 91
                End If
            Else
                cboPeriod.Text = MapPeriod(CInt(varCpnFreq), 0)
            End If
        End If

        'txtInterest.Text = ClsInstrument.Coupon -- Moved to Type specific below.
        'txtCurrentCoupon.Text = CalcCouponPeriod().ToString  -- Moved to Type specific below.
        'txtPremium.Text = "0" -- Moved to Type specific below.

        txtBeta.Text = 0
        cboTitleType.SelectedIndex = 0
        txtOutstandingFV.Text = 0
        txtIssuedFV.Text = 0  'ClsInstrument.FaceValue -- Reset to 0 as discussed with DH on 08/09/21
        txtFreefield1.Text = ClsInstrument.FreeField1
        txtFreefield2.Text = ClsInstrument.FreeField2
        txtFreefield3.Text = ClsInstrument.FreeField3
        txtFreefield4.Text = ClsInstrument.Description

        'ChkListed.Checked = ClsInstrument.Listed -- Moved to Type specific below.
        ChkReCapitalised.Checked = ClsInstrument.Recapitalised

        'Links
        cboBBGType.Text = ClsInstrument.MarketSectorDescription
        txtCusip.Text = ClsInstrument.Cusip
        txtSedol.Text = ClsInstrument.Sedol

        'Assign the ticker value based on the Type.
        Select Case cboType.SelectedValue
            Case 1
                txtTicker.Text = GetInstrumentShortCut(ClsInstrument.ReferenceSecurityClass, txtTicker.Text, ClsInstrument.TickerAndExchangeCode, txtISIN.Text, txtCusip.Text, txtSedol.Text, ClsInstrument.SecurityDescription)
            Case 2, 7
                txtTicker.Text = ClsInstrument.SecurityDescription
            Case 4
                txtTicker.Text = ClsInstrument.TickerAndExchangeCode
            Case 8
                txtTicker.Text = ClsInstrument.Ticker
        End Select
        txtProfileLink.Text = GetInstrumentShortCut(ClsInstrument.ReferenceSecurityClass, txtTicker.Text, ClsInstrument.TickerAndExchangeCode, txtISIN.Text, txtCusip.Text, txtSedol.Text, ClsInstrument.SecurityDescription)

        If cboType.SelectedValue <> 8 Then
            txtStockExchangeCode.Text = txtProfileLink.Text
        End If

        gbxOption.Visible = False
        chkBogException.Location = New Point(423, 258)
        txtTickSize.Visible = False
        lblTickSize.Visible = False
        cboPriceQuoteType.SelectedIndex = 0

        'Fundamentals
        txtMultiplier.Enabled = True
        If IsNumeric(cboType.SelectedValue) Then
            If cboType.SelectedValue = 1 Then
                txtMultiplier.Text = 0
            ElseIf cboType.SelectedValue = 2 Then

                txtCurrentCoupon.Text = CalcCouponPeriod().ToString
                txtMultiplier.Text = 1
                txtLimitAdjFactor.Text = 1
                cboCalcType.SelectedValue = MapCalcType(ClsInstrument.DayCount)
                cboCouponType.SelectedValue = MapCpnType(ClsInstrument.CouponType)
                cboBondType.Text = MapBondType(txtName.Text, ClsInstrument.ZeroCoupon)
                txtMinDenomination.Text = ClsInstrument.MinimumDenomination
                txtMinTradingQty.Text = ClsInstrument.MinimumTradingQuantity
                txtDailyQty.Text = 0
                txtMaxAllowedQty.Text = 0
                cboPortfolioLink.SelectedValue = 0

                cboMoodys.Text = ClsInstrument.RatingsMoodys
                cboSP.Text = ClsInstrument.RatingsSP
                cboFitch.Text = ClsInstrument.RatingsFitch
                cboTaxCalc.SelectedValue = 1

                txtCpnDate.Text = ClsInstrument.FirstCouponDate
                cboLongCoupon.Text = "No Coupon"
                cboInDayOff.SelectedIndex = 0
                cboIntCalc1.SelectedIndex = 0
                cboInNewMonth.SelectedIndex = 0
                cboIntCalc2.SelectedIndex = 0
                cboCollateralType.SelectedIndex = 5
                cboBondYieldType.SelectedIndex = 0

                ChkConvertible.Checked = IIf(ClsInstrument.Convertible = "Y", 1, 0)
                ChkSinkable.Checked = IIf(ClsInstrument.Sinkable = "Y", 1, 0)
                ChkCpnGuaranteed.Checked = True
                ChkCapGuaranteed.Checked = True

                cbotier.SelectedValue = 0
                cboRedemptionOptions.SelectedValue = 0
                txtMinCapGauranteed.Text = 100
                txtDiscountFactor.Text = 100
            ElseIf cboType.SelectedValue = 4 Then
                txtMultiplier.Enabled = False
                txtQtyDecimals.Text = 4
                txtPriceDecimals.Text = 2
                cboPortfolioLink.SelectedValue = -1
                txtLimitAdjFactor.Text = 1
            ElseIf cboType.SelectedValue = 7 Then
                cboPeriod.Text = "Days..."
                txtPeriod.Text = 0
                txtMultiplier.Text = ClsInstrument.T_Factor
                cboCommDerivInd.SelectedValue = 0
            ElseIf cboType.SelectedValue = 8 Then
                txtMultiplier.Text = ClsInstrument.POS_Multiplier_Factor
                txtFreefield3.Text = ClsInstrument.Future_Notice_First_Date
                cboCommDerivInd.SelectedValue = 0
                cboIssuer.SelectedValue = 510
            End If
        End If

        'SSI
        cboPOS.SelectedValue = MapPOS(ClsInstrument.CountryISO)

        'Hide the Fundamentals Tab Page for Options and Futures.
        If cboType.SelectedValue = 7 Or cboType.SelectedValue = 8 Then
            HideFundamentalsTabPage(True)
        Else
            HideFundamentalsTabPage(False)
        End If

        'Hide the Bond Tab Page for Instruments other than Bonds.
        If cboType.SelectedValue = 2 Then
            HideTabPageState(False)
        Else
            HideTabPageState(True)
        End If

        'Type Specific actions
        FormatForm(cboType.SelectedValue)

        'Extra Info.
        txtBBG_ID.Text = ClsInstrument.BBG_ID
        txtDayCount.Text = ClsInstrument.DayCount
        txtCompToParentRel.Text = ClsInstrument.Company_To_Parent_Relationship
        txtMFIDIndicator.Text = IIf(String.IsNullOrEmpty(ClsInstrument.MIFIDIndicator), "N", ClsInstrument.MIFIDIndicator)
        txtMFIDComplexInstrInd.Text = IIf(String.IsNullOrEmpty(ClsInstrument.MFID_Complex_Instr_Indicator), "N", ClsInstrument.MFID_Complex_Instr_Indicator)
        txtPriceClose.Text = ClsInstrument.Close_Price
        txtPriceHigh.Text = ClsInstrument.High_Price
        txtLowPrice.Text = ClsInstrument.Low_Price

        If cboType.SelectedValue = 4 Then
            lblDerivitiveDelType.Visible = True
            txtDerivitiveDelType.Visible = True
            txtDerivitiveDelType.Text = ClsInstrument.Delivery_Type
        Else
            lblDerivitiveDelType.Visible = False
            txtDerivitiveDelType.Visible = False
        End If

        'Populate the Industry combo boxes where the value exists.
        If Not String.IsNullOrEmpty(ClsInstrument.Industry) Then
            LoadSectorCombo()
            cboIndustrySector.SelectedIndex = 0
            cboIndustrySector.SelectedValue = CInt(Strings.Left(ClsInstrument.Industry, 2))
            cboIndustryGroups.SelectedValue = CInt(Strings.Left(ClsInstrument.Industry, 4))
            cboIndustries.SelectedValue = CInt(Strings.Left(ClsInstrument.Industry, 6))
            cboIndustrySubs.SelectedValue = CInt(Strings.Left(ClsInstrument.Industry, 8))
        Else
            LoadSectorCombo()
        End If

        If ClsInstrument.ClosePrice > "200" And Not ClsInstrument.InPence Then
            MsgBox("This instrument is priced as a structured product. If this is a structured product, please enter a Bloomberg source into the Freefield3/Bloomberg Source field to price correctly from Bloomberg e.g. LEOZ", vbInformation, "Structured Product?")
        End If

        _formLoaded = True

    End Sub

    ''' <summary>
    ''' Handle the change of Sub Type.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboType.SelectedIndexChanged

        If _formLoaded Then
            cboGroup.SelectedValue = MapGroup(cboType.Text, ClsInstrument.MarketSectorDescription, ClsInstrument.CountryISO, ClsInstrument.FundType)

            If cboType.SelectedValue = 1 Or cboType.SelectedValue = 8 Then
                ClsIMS.PopulateComboboxWithData(cboSubType, "SELECT	0 AS [tsubtype], 'Normal' AS [Name1]")
                cboSubType.SelectedIndex = 0
            ElseIf cboType.SelectedValue = 7 Then
                cboStyle.SelectedValue = MapStyle(ClsInstrument.Style)
                cboDeliveryType.SelectedValue = MapDerivativeDeliveryType(ClsInstrument.Derivative_Delivery_Type)
                ClsIMS.PopulateComboboxWithData(cboSubType, "SELECT	0 AS [tsubtype], 'Normal' AS [Name1]")
                cboSubType.SelectedIndex = 0
            Else
                ClsIMS.PopulateComboboxWithData(cboSubType, "SELECT tsubtype, Name1 FROM [dbo].[titlesubtype] WHERE ttype = " & cboType.SelectedValue & " GROUP BY tsubtype, Name1 ORDER BY tsubtype")
            End If
        End If

    End Sub

    ''' <summary>
    ''' Set the form fields based on the instrument Type.
    ''' </summary>
    ''' <param name="_selectedType"></param>
    Private Sub FormatForm(ByVal _selectedType As Integer)

        'Type Specific actions
        If _selectedType = 1 Then
            txtInterest.Text = 1
            txtCurrentCoupon.Text = 0
            txtEntranceValue.Text = 0
            txtPremium.Visible = True
            lblPremium.Visible = True
            txtPremium.Text = 0
            ChkListed.Visible = True
            ChkListed.Checked = ClsInstrument.Listed
            chkMidCost.Visible = True
            chkMidCost.Location = New Point(764, 174)
            'txtTicker.Text = GetInstrumentShortCut(ClsInstrument.ReferenceSecurityClass, txtTicker.Text, ClsInstrument.TickerAndExchangeCode, txtISIN.Text, txtCusip.Text, txtSedol.Text, ClsInstrument.SecurityDescription)
            cboPeriod.Visible = False
            txtPeriod.Visible = False
            lblPeriod.Visible = False
            txtIssueDate.Visible = False
            lblIssueDate.Visible = False
            txtCurrentCoupon.Visible = True
            lblCurrentCoupon.Visible = True
            txtEntranceValue.Visible = True
            lblEntranceValue.Visible = True
            lblEntranceValue.Text = "Entrance Value:"
            txtInterest.Visible = True
            lblInterest.Visible = True
            cboCouponCCY.Visible = False
            lblCouponCcy.Visible = True
            lblCouponCcy.Text = "Type:"  '
            lblCouponCcy.Location = New Point(65, 181)
            ChkReCapitalised.Visible = False
            cboPricingScenarios.Visible = False
            lblPricingScenarios.Visible = False
            txtMaturityDate.Visible = False
            lblMaturityDate.Visible = False
            cboEquityType.Visible = True
            cboEquityType.Location = New Point(105, 178)
            cboEquityType.SelectedIndex = 0
            cboPortfolioLink.Visible = False
            lblPortfolioLink.Visible = False
            lblMultiplier.Text = "Base:"
            lblMultiplier.Location = New Point(406, 72)
            cboTaxPolicy.Visible = False
            lblTaxPolicy.Visible = False
            cboTradedCCY.Visible = True
            lblTradedCcy.Visible = True
            txtStartofInvPeriod.Visible = False
            txtStartofInvPeriod.Location = New Point(105, 326)
            txtEndOfInvPeriod.Visible = False
            txtEndOfInvPeriod.Location = New Point(105, 352)
            cboSetDays.Visible = False
            cboSetDays.Location = New Point(193, 21)
            txtMultiplier.Visible = True
            lblMultiplier.Visible = True
            cboOptionType.Visible = False
            cboOptionType.Location = New Point(401, 299)
            lblTitleType.Visible = True
            lblTitleType.Text = "Title Type:"
            lblTitleType.Location = New Point(383, 163)
            cboTitleType.Visible = True
            lblOutstandingFV.Visible = True
            txtOutstandingFV.Visible = True
            lblIssuedFV.Visible = True
            txtIssuedFV.Visible = True
            cboStyle.Visible = False
            cboStyle.Location = New Point(446, 325)
        ElseIf _selectedType = 2 Then
            txtEntranceValue.Text = 1
            txtInterest.Text = ClsInstrument.Coupon
            txtCurrentCoupon.Text = CalcCouponPeriod().ToString
            txtPremium.Text = 0
            txtPremium.Visible = True
            lblPremium.Visible = True
            ChkListed.Visible = True
            ChkListed.Checked = ClsInstrument.Listed
            chkMidCost.Visible = False
            chkMidCost.Location = New Point(446, 249)
            'txtTicker.Text = ClsInstrument.SecurityDescription
            cboPeriod.Visible = True
            lblPeriod.Visible = True
            txtIssueDate.Visible = True
            lblIssueDate.Visible = True
            txtCurrentCoupon.Visible = True
            lblCurrentCoupon.Visible = True
            txtEntranceValue.Visible = True
            lblEntranceValue.Visible = True
            lblEntranceValue.Text = "Entrance Value:"
            txtInterest.Visible = True
            lblInterest.Visible = True
            cboCouponCCY.Visible = True
            lblCouponCcy.Visible = True
            lblCouponCcy.Text = "Coupon CCY:"
            lblCouponCcy.Location = New Point(28, 181)
            ChkReCapitalised.Visible = True
            cboPricingScenarios.Visible = True
            lblPricingScenarios.Visible = True
            txtMaturityDate.Visible = True
            lblMaturityDate.Visible = True
            cboEquityType.Visible = False
            cboEquityType.Location = New Point(105, 299)
            cboPortfolioLink.Visible = True
            lblPortfolioLink.Visible = True
            lblMultiplier.Text = "Multiplier:"
            lblMultiplier.Location = New Point(389, 72)
            cboTaxPolicy.Visible = True
            lblTaxPolicy.Visible = True
            lblTaxPolicy.Text = "Tax Policy:"
            lblTaxPolicy.Location = New Point(40, 275)
            cboTradedCCY.Visible = False
            lblTradedCcy.Visible = False
            txtStartofInvPeriod.Visible = False
            txtStartofInvPeriod.Location = New Point(105, 326)
            txtEndOfInvPeriod.Visible = False
            txtEndOfInvPeriod.Location = New Point(105, 352)
            cboSetDays.Visible = False
            cboSetDays.Location = New Point(193, 21)
            txtMultiplier.Visible = True
            lblMultiplier.Visible = True
            cboOptionType.Visible = False
            cboOptionType.Location = New Point(401, 299)
            lblTitleType.Visible = True
            lblTitleType.Text = "Title Type:"
            lblTitleType.Location = New Point(383, 163)
            cboTitleType.Visible = True
            lblOutstandingFV.Visible = True
            txtOutstandingFV.Visible = True
            lblIssuedFV.Visible = True
            txtIssuedFV.Visible = True
            cboStyle.Visible = False
            cboStyle.Location = New Point(446, 325)
        ElseIf _selectedType = 4 Then 'Mutual Funds/ETF.
            txtPremium.Text = 0
            txtPremium.Visible = True
            lblPremium.Visible = True
            'txtTicker.Text = ClsInstrument.TickerAndExchangeCode
            chkMidCost.Visible = True
            chkMidCost.Location = New Point(446, 249)
            cboPeriod.Visible = True
            lblPeriod.Visible = True
            cboPricingScenarios.Visible = False
            lblPricingScenarios.Visible = False
            cboTradedCCY.Visible = True
            lblTradedCcy.Visible = True
            lblEntranceValue.Visible = True
            lblEntranceValue.Text = "Inv Period Start:"
            lblCouponCcy.Visible = True
            lblCouponCcy.Text = "Inv Period End:"
            lblCouponCcy.Location = New Point(21, 181)
            ChkReCapitalised.Visible = False
            txtEntranceValue.Visible = False
            txtStartofInvPeriod.Visible = True
            txtStartofInvPeriod.Location = New Point(105, 149)
            txtEndOfInvPeriod.Visible = True
            txtEndOfInvPeriod.Location = New Point(105, 178)
            cboEquityType.Visible = False
            cboPortfolioLink.Visible = True
            lblPortfolioLink.Visible = True
            cboCouponCCY.Visible = False
            txtCurrentCoupon.Visible = False
            lblCurrentCoupon.Visible = False
            txtIssueDate.Visible = False
            lblIssueDate.Visible = False
            txtMaturityDate.Visible = False
            lblMaturityDate.Visible = False
            cboEquityType.Location = New Point(105, 299)
            cboTaxPolicy.Visible = False
            cboSetDays.Visible = True
            cboSetDays.Location = New Point(105, 272)
            lblTaxPolicy.Text = "Sub Set Days:"
            lblTaxPolicy.Visible = True
            lblTaxPolicy.Location = New Point(24, 275)
            ChkListed.Visible = False
            txtMultiplier.Visible = False
            lblMultiplier.Visible = False
            cboOptionType.Visible = False
            cboOptionType.Location = New Point(401, 299)
            txtInterest.Visible = False
            lblInterest.Visible = False
            lblTitleType.Visible = True
            lblTitleType.Text = "Title Type:"
            lblTitleType.Location = New Point(383, 163)
            cboTitleType.Visible = True
            lblOutstandingFV.Visible = True
            txtOutstandingFV.Visible = True
            lblIssuedFV.Visible = True
            txtIssuedFV.Visible = True
            cboStyle.Visible = False
            cboStyle.Location = New Point(446, 325)
        ElseIf _selectedType = 7 Then 'Options.
            'txtTicker.Text = ClsInstrument.SecurityDescription
            txtIssueDate.Visible = False
            lblIssueDate.Visible = False
            txtMaturityDate.Visible = True
            lblMaturityDate.Visible = True
            txtCurrentCoupon.Visible = False
            lblCurrentCoupon.Visible = False
            cboPeriod.Visible = True
            lblPeriod.Visible = True
            cboEquityType.Visible = False
            cboTaxPolicy.Visible = False
            txtEntranceValue.Visible = True
            lblEntranceValue.Visible = True
            txtEntranceValue.Text = ClsInstrument.Strike
            lblEntranceValue.Text = "Entrance Value:"
            cboCouponCCY.Visible = False
            lblCouponCcy.Visible = True
            lblCouponCcy.Text = "Option Type:"
            lblCouponCcy.Location = New Point(33, 181)
            cboOptionType.Visible = True
            cboOptionType.Location = New Point(105, 178)
            cboOptionType.SelectedText = ClsInstrument.PutCall.ToUpper
            txtInterest.Visible = True
            lblInterest.Visible = True
            txtInterest.Text = 12
            cboPortfolioLink.Visible = False
            lblPortfolioLink.Visible = False
            txtStartofInvPeriod.Visible = False
            txtEndOfInvPeriod.Visible = False
            cboSetDays.Visible = False
            txtMultiplier.Visible = True
            lblMultiplier.Visible = True
            txtPremium.Visible = False
            lblPremium.Visible = False
            lblTaxPolicy.Visible = False
            chkMidCost.Visible = True
            chkMidCost.Location = New Point(446, 187)
            cboTitleType.Visible = False
            lblOutstandingFV.Visible = False
            txtOutstandingFV.Visible = False
            lblIssuedFV.Visible = False
            txtIssuedFV.Visible = False
            cboStyle.Visible = True
            cboStyle.Location = New Point(446, 160)
            lblTitleType.Text = "Style:"
            lblTitleType.Location = New Point(401, 163)
            lblDeliveryType.Visible = True
            cboDeliveryType.Visible = True
            cboDeliveryType.Location = New Point(446, 216)
            lblDeliveryType.Location = New Point(365, 219)
            lblCommDerivInd.Visible = True
            lblCommDerivInd.Location = New Point(355, 249)
            cboCommDerivInd.Visible = True
            cboCommDerivInd.Location = New Point(446, 246)
            lblSedol.Visible = False
            txtSedol.Visible = False
            lblCusip.Visible = False
            txtCusip.Visible = False
            lblMainTitle.Location = New Point(318, 176)
            lblMainTitle.Text = "Underlying product:"
            txtMainTitle.Text = ClsInstrument.Underlying_Product
            txtMainTitle.Size = New Size(347, 20)
            chkBogException.Location = New Point(423, 284)
            txtTickSize.Visible = True
            lblTickSize.Visible = True
            txtTickSize.Text = 1
            txtFreefield1.Text = ClsInstrument.SecurityDescription
        ElseIf _selectedType = 8 Then 'Futures.
            'txtTicker.Text = ClsInstrument.Ticker
            ChkReCapitalised.Visible = False
            chkMidCost.Location = New Point(764, 174)
            cboSetDays.Visible = False
            txtMaturityDate.Visible = True
            lblMaturityDate.Visible = True
            lblIssueDate.Visible = False
            txtIssueDate.Visible = False
            lblEntranceValue.Visible = False
            txtEntranceValue.Visible = False
            lblSedol.Visible = False
            txtSedol.Visible = False
            lblCusip.Visible = False
            txtCusip.Visible = False
            txtPremium.Visible = False
            lblPremium.Visible = False
            txtMultiplier.Visible = True
            lblMultiplier.Visible = True
            txtCurrentCoupon.Visible = False
            lblCurrentCoupon.Visible = False
            cboPortfolioLink.Visible = False
            lblPortfolioLink.Visible = False
            txtStartofInvPeriod.Visible = False
            txtEndOfInvPeriod.Visible = False
            lblOutstandingFV.Visible = False
            txtOutstandingFV.Visible = False
            lblIssuedFV.Visible = False
            txtIssuedFV.Visible = False
            cboCouponCCY.Visible = False
            lblCouponCcy.Visible = False
            lblTaxPolicy.Visible = False
            cboTaxPolicy.Visible = False
            chkMidCost.Visible = True
            txtInterest.Location = New Point(105, 149)
            txtInterest.Visible = True
            txtInterest.Text = 12
            lblInterest.Location = New Point(54, 152)
            lblInterest.Visible = True
            cboTitleType.Visible = False
            lblTitleType.Visible = False
            cboDeliveryType.Visible = True
            cboDeliveryType.Location = New Point(446, 160)
            lblDeliveryType.Visible = True
            lblDeliveryType.Location = New Point(366, 163)
            cboCommDerivInd.Visible = True
            cboCommDerivInd.Location = New Point(446, 187)
            lblCommDerivInd.Visible = True
            lblCommDerivInd.Location = New Point(355, 190)
            txtFreefield1.Text = ClsInstrument.Ticker
            txtTickSize.Visible = True
            lblTickSize.Visible = True
            txtTickSize.Text = 1
        End If

    End Sub

    Private Sub CmdLoadInstrument_Click(sender As Object, e As EventArgs) Handles CmdLoadInstrument.Click

        Cursor = Cursors.WaitCursor
        _formLoaded = False
        Try

            Dim varTicker As String, varUnderlyingTicker As String
            Dim varFields As String = "ISSUER,BB_TO_EXCH_PX_SCALING_FACTOR,BPIPE_REFERENCE_SECURITY_CLASS,CALLABLE,CALC_TYP,CFI_CODE,DAYS_TO_SETTLE," &
                                            "CIE_DES,CNTRY_ISSUE_ISO,CONVERTIBLE,CPN,CPN_FREQ,CPN_TYP,CRNCY,CURRENCY,DAY_CNT,DERIVATIVE_DELIVERY_TYPE,EXCH_CODE,FIRST_CPN_DT,FUND_TYP,FUT_DLV_DT_LAST," &
                                            "FUT_NOTICE_FIRST,GICS_SUB_INDUSTRY,COMPANY_TO_PARENT_RELATIONSHIP,ID_CUSIP,ID_EXCH_SYMBOL,ID_ISIN,ID_SEDOL1,IDX_RATIO,INT_ACC,IS_INDEX_LINKED,MARKET_SECTOR_DES," &
                                            "MATURITY,MIFID_II_FIRDS_INDICATOR,MIN_INCREMENT,MIN_PIECE,NAME,ISSUE_DT,OPEN_INT_DATE,OPT_CONT_SIZE,OPT_EXER_TYP,OPT_EXPIRE_DT,OPT_FIRST_TRADE_DT,OPT_MULTIPLIER," &
                                            "OPT_PUT_CALL,OPT_STRIKE_PX,OPT_TICK_SIZE,PAR_AMT,PRINCIPAL_FACTOR,PUTABLE,PX_ASK,PX_BID,PX_OPEN,PX_YEST_CLOSE,PX_POS_MULT_FACTOR,RTG_MOODY_NO_WATCH,RTG_SP_NO_WATCH," &
                                            "RTG_FITCH_NO_WATCH,SECURITY_DES,SECURITY_TYP,SINKABLE,TICKER,TICKER_AND_EXCH_CODE,UNDERLYING_CUSIP,UNDERLYING_ISIN,UNDERLYING_SECURITY_DES,ZERO_CPN,CPN_CRNCY,TRADE_CRNCY," &
                                            "MIFID_II_COMPLEX_INSTR_INDICATOR,PRIMARY_EXCHANGE_MIC_RT,ID_BB_UNIQUE,PARENT_COMP_NAME,DELIVERY_TYP,PX_YEST_CLOSE,PX_HIGH,PX_LOW,FIRST_TRADING_DATE,FUT_FIRST_TRADE_DT"


            varTicker = txtTickerSearch.Text
            varTicker = ReturnSuffix(varTicker)
            If InStr(1, varTicker, " Corp Corp", vbTextCompare) Then ' Avoid REPLACE as sheet is case sensitive
                varTicker = Strings.Left(varTicker, InStr(1, varTicker, " ")) ' Can't do replace as case sensitive
                varTicker = varTicker & "Corp"
            End If

            Dim GetMasterInstrument As String = ClsIMS.GetInstrument(Replace(varTicker, " ", "%20"), varFields)
            Dim LstjsonResultMaster As Dictionary(Of String, Object) = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(GetMasterInstrument)

            If LstjsonResultMaster Is Nothing Then
                MsgBox("There is a problem with the Bloomberg feed. Please contact systems support to restart the service", vbCritical, "BBG Service Issue")
            Else
                If LstjsonResultMaster.Item(varTicker).Count < 1 Then
                    MsgBox("Invalid instrument ticker: """ & varTicker & """. Please check and try again!", vbCritical, "Check Instrument")

                ElseIf LstjsonResultMaster.Item(varTicker).item("BPIPE_REFERENCE_SECURITY_CLASS").ToString() = "" Then
                    MsgBox("Invalid instrument ticker: """ & varTicker & """. Please check and try again!", vbCritical, "Check Instrument")
                Else
                    If LstjsonResultMaster.Item(varTicker).item("BPIPE_REFERENCE_SECURITY_CLASS") = "Option" Then

                        _underlyingInstrument = True

                        If LstjsonResultMaster.Item(varTicker).item("UNDERLYING_SECURITY_DES") <> "" Then
                            varUnderlyingTicker = LstjsonResultMaster.Item(varTicker).item("UNDERLYING_SECURITY_DES")
                        ElseIf Len(LstjsonResultMaster.Item(varTicker).item("UNDERLYING_ISIN")) = 12 Then
                            varUnderlyingTicker = LstjsonResultMaster.Item(varTicker).item("UNDERLYING_ISIN") & " " & LstjsonResultMaster.Item(varTicker).item("MARKET_SECTOR_DES")
                        ElseIf Len(LstjsonResultMaster.Item(varTicker).item("UNDERLYING_CUSIP")) > 0 Then
                            varUnderlyingTicker = LstjsonResultMaster.Item(varTicker).item("UNDERLYING_CUSIP") & " " & LstjsonResultMaster.Item(varTicker).item("MARKET_SECTOR_DES")
                        Else
                            varUnderlyingTicker = LstjsonResultMaster.Item(varTicker).item("TICKER") & " " & LstjsonResultMaster.Item(varTicker).item("EXCH_CODE") & " " & LstjsonResultMaster.Item(varTicker).item("MARKET_SECTOR_DES")
                        End If
                        Dim GetUnderlyingInstrument As String = ClsIMS.GetInstrument(Replace(varUnderlyingTicker, " ", "%20"), varFields)
                        Dim LstjsonResultUnderlying As Dictionary(Of String, Object) = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(GetUnderlyingInstrument)

                        'Does the Underlying Instrument exist?
                        Dim _ticker As String = LstjsonResultUnderlying.Item(varUnderlyingTicker).item("TICKER_AND_EXCH_CODE")
                        Dim _tCode As String = IIf(String.IsNullOrEmpty(ClsIMS.CheckForUnderlyingInstrument(_ticker)), String.Empty, ClsIMS.CheckForUnderlyingInstrument(_ticker))
                        Dim _name As String = LstjsonResultUnderlying.Item(varUnderlyingTicker).item("NAME")
                        If String.IsNullOrEmpty(_tCode) Then

                            MsgBox($"The underlying Instrument {vbNewLine}({LstjsonResultUnderlying.Item(varUnderlyingTicker).item("TICKER_AND_EXCH_CODE")} - {LstjsonResultUnderlying.Item(varUnderlyingTicker).item("NAME")}) {vbNewLine}will be required to loaded first.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
                            _underlyingTicker = txtTicker.Text
                            PopulateInstrumentClass(varUnderlyingTicker, LstjsonResultUnderlying)
                            PopulateForm()
                            btnInsertInstrument.Text = "Load Underlying Instrument to IMS"
                        Else
                            _underlyingInstrument = False
                            PopulateInstrumentClass(varTicker, LstjsonResultMaster)
                            ClsInstrument.Underlying_Product = LstjsonResultUnderlying.Item(varUnderlyingTicker).item("NAME").ToString()
                            PopulateForm()
                            btnInsertInstrument.Text = "Load Instrument to IMS"
                            gbxOption.Visible = True
                            txtDerivTCode.Text = _tCode
                            txtUnderlyingName.Text = _name
                            txtUnderlyingShortCut.Text = _ticker
                        End If
                    Else
                        _underlyingInstrument = False

                        PopulateInstrumentClass(varTicker, LstjsonResultMaster)
                        PopulateForm()
                    End If

                End If
            End If
        Catch ex As Exception
            MsgBox($"Error fetching details for the Ticker {txtTickerSearch.Text} - {ex.Message}", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Load instrument details error")
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTitles, $"Error fetching details for the Ticker {txtTickerSearch.Text} - {ex.Message}" & " - Error on CmdLoadInstrument_Click")
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Handle the change of the period combo box valie.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboPeriod_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPeriod.SelectedIndexChanged

        If cboPeriod.Text = "Months..." Or cboPeriod.Text = "Days..." Then
            txtPeriod.Visible = True
            txtPeriod.Text = ""
        Else
            txtPeriod.Visible = False
            If IsNumeric(cboPeriod.SelectedValue) Then
                txtPeriod.Text = cboPeriod.SelectedValue
            End If
        End If

        If _formLoaded Then
            txtCurrentCoupon.Text = CalcCouponPeriod().ToString
        End If

    End Sub

    Private Sub txtPeriod_TextChanged(sender As Object, e As EventArgs) Handles txtPeriod.TextChanged

        If _formLoaded And Not String.IsNullOrEmpty(txtPeriod.Text) Then
            txtCurrentCoupon.Text = CalcCouponPeriod().ToString
        End If

    End Sub

    Private Sub txtName_TextChanged(sender As Object, e As EventArgs) Handles txtName.TextChanged

        If txtName.Text <> "" Then
            btnInsertInstrument.Enabled = True
            btnClearForm.Enabled = True
        Else
            btnInsertInstrument.Enabled = False
            btnClearForm.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Clear out the vlayes from the text and combo boxes, and set the check boxes to unchecked.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnClearForm_Click(sender As Object, e As EventArgs) Handles btnClearForm.Click

        Try
            With Me
                For Each ctl In Me.Controls
                    If TypeOf ctl Is GroupBox Then
                        For Each gctl In ctl.Controls
                            If TypeOf gctl Is TextBox Then
                                gctl.Text = ""
                            ElseIf TypeOf gctl Is ComboBox Then
                                gctl.Text = ""
                            ElseIf TypeOf gctl Is CheckBox Then
                                gctl.Checked = False
                            End If
                        Next
                    ElseIf TypeOf ctl Is TabControl Then
                        For Each TabPage1 In ctl.TabPages
                            For Each tctl In TabPage1.Controls
                                If TypeOf tctl Is GroupBox Then
                                    For Each gbctl In tctl.Controls
                                        If TypeOf gbctl Is TextBox Then
                                            gbctl.Text = ""
                                        ElseIf TypeOf gbctl Is ComboBox Then
                                            gbctl.Text = ""
                                        End If
                                    Next
                                End If
                                If TypeOf tctl Is TextBox Then
                                    tctl.Text = ""
                                ElseIf TypeOf tctl Is ComboBox Then
                                    tctl.Text = ""
                                ElseIf TypeOf tctl Is CheckBox Then
                                    tctl.Checked = False
                                ElseIf TypeOf tctl Is TabControl Then
                                    For Each InnerTabPage In tctl.TabPages
                                        For Each tctl2 In InnerTabPage.Controls
                                            If TypeOf tctl2 Is TextBox Then
                                                tctl2.Text = ""
                                            ElseIf TypeOf tctl2 Is ComboBox Then
                                                tctl2.Text = ""
                                            ElseIf TypeOf tctl2 Is CheckBox Then
                                                tctl2.Checked = False
                                            End If
                                        Next
                                    Next
                                End If
                            Next
                        Next
                    End If
                Next
            End With

            'Supplementary changes on clearing the form.
            _underlyingInstrument = False
            gbxOption.Visible = False
            txtDerivTCode.Text = String.Empty
            btnInsertInstrument.Text = "Load Instrument to IMS"

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Clear instrument details error")
        End Try

    End Sub

    ''' <summary>
    ''' 'Load the combo box cboIndustrySector
    ''' </summary>
    Private Sub LoadSectorCombo()

        Try
            ClsIMS.PopulateComboboxWithData(cboIndustrySector, "SELECT INS_SectCode, INS_SectDescr FROM [IMSplus].[dbo].[IndustrySectors] ORDER BY INS_SectCode ASC")
            'cboIndustrySector.SelectedIndex = 0
            _cboSectorLoaded = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Handle the change of the Industry Sectors combo box.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboIndustrySector_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboIndustrySector.SelectedIndexChanged

        If _cboSectorLoaded Then
            LoadIndustryGroupCombo(cboIndustrySector.SelectedValue)
        End If

    End Sub

    ''' <summary>
    ''' 'Load the combo box cboIndustryGroups
    ''' </summary>
    Private Sub LoadIndustryGroupCombo(ByVal id As Integer)

        Try
            ClsIMS.PopulateComboboxWithData(cboIndustryGroups, $"SELECT ING_GroupCode, ING_GroupDescr FROM [IMSplus].[dbo].[IndustryGroups] WHERE ING_INSSectorCode =  {id}")
            _cboGroupLoaded = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Handle the change of the Industry Groups combo box.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboIndustryGroups_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboIndustryGroups.SelectedIndexChanged

        If _cboGroupLoaded And Not String.IsNullOrEmpty(cboIndustryGroups.Text) Then
            LoadIndustriesCombo(cboIndustryGroups.SelectedValue)
        End If

    End Sub

    ''' <summary>
    ''' 'Load the combo box cboIndustryGroups
    ''' </summary>
    Private Sub LoadIndustriesCombo(ByVal id As Integer)

        Try
            ClsIMS.PopulateComboboxWithData(cboIndustries, $"SELECT IND_IndustryCode, IND_IndustryDescr FROM [IMSplus].[dbo].[Industries] WHERE IND_INGGroupCode = {id}")
            _cboIndustriesLoaded = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Handle the change of the Industries combo box.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cboIndustries_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboIndustries.SelectedIndexChanged

        If _cboIndustriesLoaded And Not String.IsNullOrEmpty(cboIndustries.Text) Then
            LoadSubsCombo(cboIndustries.SelectedValue)
        End If

    End Sub

    ''' <summary>
    ''' 'Load the combo box cboIndustrySubs
    ''' </summary>
    Private Sub LoadSubsCombo(ByVal id As Integer)

        Try
            ClsIMS.PopulateComboboxWithData(cboIndustrySubs, $"SELECT INDG_SubgroupCode, INDG_SubgroupDescr FROM [IMSplus].[dbo].[IndustrySubs] WHERE INDG_INDIndustryCode = {id}")
            _cboSubsLoaded = True
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Enable or disable buttons.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtTickerSearch_TextChanged(sender As Object, e As EventArgs) Handles txtTickerSearch.TextChanged

        If txtTickerSearch.Text = "" Then
            CmdLoadInstrument.Enabled = False
            btnClearForm.Enabled = False
        Else
            CmdLoadInstrument.Enabled = True
            btnClearForm.Enabled = True
        End If

    End Sub

    ''' <summary>
    ''' Handle the enter key event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtTickerSearch_KeyDown(sender As Object, e As KeyEventArgs) Handles txtTickerSearch.KeyDown

        If e.KeyValue = Keys.Enter Then
            CmdLoadInstrument_Click(sender, e)
        End If

    End Sub

    ''' <summary>
    ''' Perform the instrument insert.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnInsertInstrument_Click(sender As Object, e As EventArgs) Handles btnInsertInstrument.Click

        Dim _titlesDictionary As New Dictionary(Of String, String)

        'Structured product
        If Not String.IsNullOrEmpty(txtFreefield3.Text) Then
            Dim _ticker As String = Replace(txtTickerSearch.Text, " ", "%20") + "%40" + txtFreefield3.Text
            Dim GetStructuredInstrumentPrices As String = ClsIMS.GetInstrument(_ticker, "PX_YEST_CLOSE,PX_HIGH,PX_LOW")
            Dim LstjsonStructuredProducts As Dictionary(Of String, Object) = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(GetStructuredInstrumentPrices)
            ClsInstrument.Close_Price = GetInstrumentValue(LstjsonStructuredProducts, $"{txtTickerSearch.Text}@{txtFreefield3.Text}", "PX_YEST_CLOSE")
            ClsInstrument.High_Price = GetInstrumentValue(LstjsonStructuredProducts, $"{txtTickerSearch.Text}@{txtFreefield3.Text}", "PX_HIGH")
            ClsInstrument.Low_Price = GetInstrumentValue(LstjsonStructuredProducts, $"{txtTickerSearch.Text}@{txtFreefield3.Text}", "PX_LOW")
        End If

        Try
            _titlesDictionary.Add("T_Comment", IIf(String.IsNullOrEmpty(txtComments.Text), $"Entered by FLOW {Environment.UserName}", $"{txtComments.Text}. Entered by FLOW {Environment.UserName}"))
            Select Case cboType.SelectedValue
                Case 1 'Equities
                    Dim _shortcut As String
                    If ClsInstrument.ReferenceSecurityClass = "FixedIncome" Then
                        _shortcut = IIf(String.IsNullOrEmpty(ClsInstrument.ISIN), IIf(String.IsNullOrEmpty(ClsInstrument.Cusip), ClsInstrument.Sedol, ClsInstrument.Cusip), ClsInstrument.ISIN)
                    Else
                        _shortcut = txtTicker.Text.ToString
                    End If
                    _titlesDictionary.Add("T_Group", cboGroup.SelectedValue)
                    _titlesDictionary.Add("T_Type", cboType.SelectedValue)
                    _titlesDictionary.Add("T_Master", 0)
                    _titlesDictionary.Add("T_Name", txtName.Text)
                    _titlesDictionary.Add("T_ShortCut", _shortcut)
                    _titlesDictionary.Add("T_SubType", cboSubType.SelectedValue)
                    _titlesDictionary.Add("T_TSymbol", ClsInstrument.Exchange_Symbol)
                    _titlesDictionary.Add("T_AccType", 0)
                    _titlesDictionary.Add("T_Link1", txtLink1.Text.ToString)
                    _titlesDictionary.Add("T_Link2", txtLink2.Text.ToString)
                    _titlesDictionary.Add("T_Link3", txtLink3.Text.ToString)
                    _titlesDictionary.Add("T_Link4", txtLink4.Text.ToString)
                    _titlesDictionary.Add("T_FreeField1", txtFreefield1.Text.ToString)
                    _titlesDictionary.Add("T_FreeField2", txtFreefield2.Text.ToString)
                    _titlesDictionary.Add("T_FreeField3", txtFreefield3.Text.ToString)
                    _titlesDictionary.Add("T_FreeField4", txtFreefield4.Text.ToString)
                    _titlesDictionary.Add("T_Currency", CboCCY.SelectedValue)
                    _titlesDictionary.Add("T_BLOOMBERGTYPE", cboBBGType.SelectedValue)
                    If Not String.IsNullOrEmpty(cboTradedCCY.Text) Then
                        _titlesDictionary.Add("T_TradingCRDCode", cboTradedCCY.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_OutstdFV", txtOutstandingFV.Text)
                    _titlesDictionary.Add("T_IssuedFV", txtIssuedFV.Text)
                    _titlesDictionary.Add("T_CollType", -1)
                    _titlesDictionary.Add("T_Tax", IIf(String.IsNullOrEmpty(txttax.Text), 0, txttax.Text))
                    _titlesDictionary.Add("T_Beta", txtBeta.Text.ToString)
                    _titlesDictionary.Add("Issuer_Name", cboIssuer.Text.ToString)
                    _titlesDictionary.Add("Issuer_Cty", cboIssuedCtry.SelectedValue)
                    _titlesDictionary.Add("T_BetaDate", 0)
                    _titlesDictionary.Add("T_ViewInLists", IIf(CInt(ChkActive.Checked), 1, 0))
                    _titlesDictionary.Add("T_ApproveIn", IIf(CInt(ChkApproved.Checked), 1, 0))
                    _titlesDictionary.Add("T_Tokens", txtCurrentCoupon.Text)
                    _titlesDictionary.Add("T_LinkExport1", txtExportLink1.Text.ToString)
                    _titlesDictionary.Add("T_LinkExport2", txtExportLink2.Text.ToString)
                    _titlesDictionary.Add("T_ISIN", txtISIN.Text)
                    _titlesDictionary.Add("T_Market", cboMarket.SelectedValue)
                    _titlesDictionary.Add("T_IssuerCountry", cboIssuedCtry.SelectedValue)
                    _titlesDictionary.Add("T_AccKind", 0)
                    _titlesDictionary.Add("T_InASE", 1)
                    _titlesDictionary.Add("T_PFolio", 0)
                    _titlesDictionary.Add("T_AverageCost", 1)
                    _titlesDictionary.Add("T_Premium", txtPremium.Text.ToString)
                    _titlesDictionary.Add("T_Base", txtMultiplier.Text)
                    _titlesDictionary.Add("T_FaceValue", txtEntranceValue.Text)
                    _titlesDictionary.Add("T_TaxOnFV", 0)
                    _titlesDictionary.Add("T_BondType", 0)
                    _titlesDictionary.Add("T_CalcType", 0)
                    _titlesDictionary.Add("T_CalcTax", 0)
                    _titlesDictionary.Add("T_NumOfWarningDates", 0)
                    _titlesDictionary.Add("T_MaxQuantity", 0)
                    _titlesDictionary.Add("T_MinDenomination", 1)
                    _titlesDictionary.Add("T_DOAdd", 1)
                    _titlesDictionary.Add("T_DOAcc", 1)
                    _titlesDictionary.Add("T_DOAddEOM", 1)
                    _titlesDictionary.Add("T_DOAccEOM", 1)
                    _titlesDictionary.Add("T_BCurrency", -1)
                    _titlesDictionary.Add("T_ReCapitalize", 0)
                    '_titlesDictionary.Add("T_QntDecimals", txtQtyDecimals.Text)
                    '_titlesDictionary.Add("T_PrcDecimals", txtPriceDecimals.Text)
                    If Not String.IsNullOrEmpty(txtInitDate.Text) Then
                        _titlesDictionary.Add("T_InitDate", txtInitDate.Text.ToString)
                    End If
                    If Not String.IsNullOrEmpty(cboExternalFeedModel.Text) Then
                        _titlesDictionary.Add("T_ExtFeedModelId", cboExternalFeedModel.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_Period", 0)
                    _titlesDictionary.Add("T_Flag", cboEquityType.SelectedValue)
                    _titlesDictionary.Add("T_Amount", txtInterest.Text)
                    _titlesDictionary.Add("T_BOGExcemption", CInt(chkBogException.Checked))
                    If Not String.IsNullOrEmpty(cboIndustrySector.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSECTORS", cboIndustrySector.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustryGroups.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYGROUPS", cboIndustryGroups.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustries.Text) Then
                        _titlesDictionary.Add("T_INDUSTRIES", cboIndustries.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustrySubs.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSUBS", cboIndustrySubs.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_Scenarios", -1)
                    _titlesDictionary.Add("T_YieldType", 0)
                    _titlesDictionary.Add("T_IsTemplate", 0)
                    _titlesDictionary.Add("T_MinTradeVolume", 0)
                    _titlesDictionary.Add("T_MarginAcc", 0)
                    _titlesDictionary.Add("T_ValMethod", 0)
                    _titlesDictionary.Add("T_ValType", 0)
                    _titlesDictionary.Add("T_EvalMethod", 0)
                    _titlesDictionary.Add("T_PmntOption", 0)
                    _titlesDictionary.Add("T_CapAdjOpt", 0)
                    _titlesDictionary.Add("T_CouponGuaranteed", 1)
                    _titlesDictionary.Add("T_Sinkable", 0)
                    _titlesDictionary.Add("T_InflationLinked", 0)
                    _titlesDictionary.Add("T_PaybackFactorPrc", 100)
                    _titlesDictionary.Add("T_CapitalGuaranteed", 1)
                    _titlesDictionary.Add("T_MinCapitalGuaranteedPrc", 100)
                    _titlesDictionary.Add("T_Convertible", 0)
                    _titlesDictionary.Add("T_PerpetualFlag", 0)
                    _titlesDictionary.Add("T_Tier", 0)
                    _titlesDictionary.Add("T_MarketSector", 0)
                    _titlesDictionary.Add("T_SettlePlace", IIf(cboPOS.SelectedValue Is Nothing, 0, cboPOS.SelectedValue))
                    _titlesDictionary.Add("T_DefBroker", IIf(String.IsNullOrEmpty(cboCpty.Text), 0, cboCpty.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBroker", IIf(String.IsNullOrEmpty(cboSecClearingAgt1.Text), 0, cboSecClearingAgt1.SelectedValue))
                    _titlesDictionary.Add("T_SafeAccountOnAgent", IIf(String.IsNullOrEmpty(txtAgentCodePOS.Text), String.Empty, txtAgentCodePOS.Text))
                    _titlesDictionary.Add("T_TSDEFBrokerAgent", IIf(String.IsNullOrEmpty(cboSecClearingAgt2.Text), 0, cboSecClearingAgt2.SelectedValue))
                    _titlesDictionary.Add("T_AgentCode", IIf(String.IsNullOrEmpty(txtClearingAgentCodePOS.Text), String.Empty, txtClearingAgentCodePOS.Text))
                    _titlesDictionary.Add("T_CSDefBroker", IIf(String.IsNullOrEmpty(cboCashClearingAgt.Text), 0, cboCashClearingAgt.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBrokerCode", IIf(String.IsNullOrEmpty(txtCashSettlementBroker.Text), String.Empty, txtCashSettlementBroker.Text))
                    _titlesDictionary.Add("T_PUCODE", cboIssuer.SelectedValue)
                    _titlesDictionary.Add("T_Factor", 1)
                    _titlesDictionary.Add("T_MONTH30", 0)
                    _titlesDictionary.Add("T_PriceTickSize", 1)
                    _titlesDictionary.Add("T_CFICode", IIf(String.IsNullOrEmpty(txtCFICode.Text), vbNullString, txtCFICode.Text))
                    _titlesDictionary.Add("T_OnESMA", CInt(ChkESMA.Checked))
                    _titlesDictionary.Add("T_XAAType", 0)
                    _titlesDictionary.Add("T_AbNormalCoupon", 0)
                    _titlesDictionary.Add("T_IssueType", -1)
                    If Not String.IsNullOrEmpty(txtCusip.Text) Then
                        _titlesDictionary.Add("T_CUSIP", txtCusip.Text.ToString)
                    End If
                    If Not String.IsNullOrEmpty(txtSedol.Text) Then
                        _titlesDictionary.Add("T_SEDOL", txtSedol.Text)
                    End If
                    _titlesDictionary.Add("T_DeliverType", 0)
                    _titlesDictionary.Add("PriceClose", ClsInstrument.Close_Price)
                    _titlesDictionary.Add("PriceLow", ClsInstrument.Low_Price)
                    _titlesDictionary.Add("PriceHigh", ClsInstrument.High_Price)
                    _titlesDictionary.Add("PriceQuoteType", cboPriceQuoteType.SelectedIndex)
                Case 2 'Bonds
                    Dim _shortcut As String = IIf(String.IsNullOrEmpty(ClsInstrument.ISIN), IIf(String.IsNullOrEmpty(ClsInstrument.Cusip), ClsInstrument.Sedol, ClsInstrument.Cusip), ClsInstrument.ISIN)
                    _titlesDictionary.Add("T_Group", cboGroup.SelectedValue)
                    _titlesDictionary.Add("T_Type", cboType.SelectedValue)
                    _titlesDictionary.Add("T_Master", 0)
                    _titlesDictionary.Add("T_Name", txtTicker.Text.ToString)
                    _titlesDictionary.Add("T_ShortCut", _shortcut)
                    _titlesDictionary.Add("T_Link1", txtLink1.Text.ToString)
                    _titlesDictionary.Add("T_Link2", txtLink2.Text.ToString)
                    _titlesDictionary.Add("T_Link3", txtLink3.Text.ToString)
                    _titlesDictionary.Add("T_Link4", txtLink4.Text.ToString)
                    _titlesDictionary.Add("T_FreeField1", txtFreefield1.Text.ToString)
                    _titlesDictionary.Add("T_FreeField2", txtFreefield2.Text.ToString)
                    _titlesDictionary.Add("T_FreeField3", txtFreefield3.Text.ToString)
                    _titlesDictionary.Add("T_FreeField4", txtFreefield4.Text.ToString)
                    _titlesDictionary.Add("T_Currency", CboCCY.SelectedValue)
                    _titlesDictionary.Add("T_Tax", IIf(String.IsNullOrEmpty(txttax.Text), 0, txttax.Text))
                    _titlesDictionary.Add("T_Beta", txtBeta.Text.ToString)
                    _titlesDictionary.Add("Issuer_Name", cboIssuer.Text.ToString)
                    _titlesDictionary.Add("Issuer_Cty", cboIssuedCtry.SelectedValue)
                    _titlesDictionary.Add("T_BetaDate", 0)
                    _titlesDictionary.Add("T_ViewInLists", IIf(CInt(ChkActive.Checked), 1, 0))
                    _titlesDictionary.Add("T_ApproveIn", IIf(CInt(ChkApproved.Checked), 1, 0))
                    _titlesDictionary.Add("T_InitDate", txtInitDate.Text.ToString)
                    _titlesDictionary.Add("T_ExpDate", txtMaturityDate.Text.ToString)
                    _titlesDictionary.Add("T_PeriodType", 1)
                    _titlesDictionary.Add("T_Period", IIf(String.IsNullOrEmpty(txtPeriod.Text), cboPeriod.SelectedValue, txtPeriod.Text))
                    _titlesDictionary.Add("T_FaceValue", txtEntranceValue.Text)
                    _titlesDictionary.Add("T_Tokens", txtCurrentCoupon.Text)
                    _titlesDictionary.Add("T_BCurrency", cboCouponCCY.SelectedValue)
                    _titlesDictionary.Add("T_Base", 365)
                    _titlesDictionary.Add("T_AccKind", 0)
                    _titlesDictionary.Add("T_PFolio", 0)
                    _titlesDictionary.Add("T_InASE", IIf(ChkListed.Checked, 1, 0))
                    _titlesDictionary.Add("T_ReCapitalize", IIf(ChkReCapitalised.Checked, 1, 0))
                    _titlesDictionary.Add("T_Amount", txtInterest.Text)
                    _titlesDictionary.Add("T_TSymbol", _shortcut)
                    _titlesDictionary.Add("T_Premium", txtPremium.Text.ToString)
                    _titlesDictionary.Add("T_Market", cboMarket.SelectedValue)
                    _titlesDictionary.Add("T_LinkExport1", txtExportLink1.Text.ToString)
                    _titlesDictionary.Add("T_LinkExport2", txtExportLink2.Text.ToString)
                    _titlesDictionary.Add("T_ISIN", txtISIN.Text)
                    _titlesDictionary.Add("T_BondType", cboBondType.SelectedValue)
                    _titlesDictionary.Add("T_Flag", 0)
                    If Not String.IsNullOrEmpty(txtGuarantor.Text) Then
                        _titlesDictionary.Add("T_Guarantor", txtGuarantor.Text)
                    End If
                    _titlesDictionary.Add("T_CalcType", cboCalcType.SelectedValue)
                    _titlesDictionary.Add("T_CalcTax", cboTaxCalc.SelectedValue)
                    _titlesDictionary.Add("T_CouponDate", txtCpnDate.Text.ToString)
                    _titlesDictionary.Add("T_NumOfWarningDates", 0)
                    _titlesDictionary.Add("T_MaxQuantity", 0)
                    _titlesDictionary.Add("T_PUCODE", cboIssuer.SelectedValue)
                    _titlesDictionary.Add("T_MinDenomination", txtMinDenomination.Text.ToString)
                    _titlesDictionary.Add("T_Factor", txtLimitAdjFactor.Text.ToString)
                    _titlesDictionary.Add("T_AbNormalCoupon", cboLongCoupon.SelectedValue)
                    _titlesDictionary.Add("T_DOAdd", cboInDayOff.SelectedValue)
                    _titlesDictionary.Add("T_DOAcc", cboIntCalc1.SelectedValue)
                    _titlesDictionary.Add("T_DOAddEOM", cboInNewMonth.SelectedValue)
                    _titlesDictionary.Add("T_DOAccEOM", cboIntCalc2.SelectedValue)
                    _titlesDictionary.Add("T_SubType", cboSubType.SelectedValue)
                    _titlesDictionary.Add("T_AccType", 0)
                    _titlesDictionary.Add("T_BOGExcemption", CInt(chkBogException.Checked))
                    _titlesDictionary.Add("T_SettleDays", cboPortfolioLink.SelectedValue)
                    _titlesDictionary.Add("T_Tier", 0)
                    _titlesDictionary.Add("T_MONTH30", 0)
                    _titlesDictionary.Add("T_AverageCost", 1)
                    _titlesDictionary.Add("T_SPRating", cboSP.SelectedValue)
                    _titlesDictionary.Add("T_IssueDate", txtIssueDate.Text)
                    _titlesDictionary.Add("T_Moodys", cboMoodys.SelectedValue)
                    If Not String.IsNullOrEmpty(txtCusip.Text) Then
                        _titlesDictionary.Add("T_CUSIP", txtCusip.Text.ToString)
                    End If
                    If Not String.IsNullOrEmpty(txtSedol.Text) Then
                        _titlesDictionary.Add("T_SEDOL", txtSedol.Text)
                    End If
                    _titlesDictionary.Add("T_CollType", cboCollateralType.SelectedValue)
                    _titlesDictionary.Add("T_IssueType", IIf(IsNothing(cboIssueType.SelectedValue), -1, cboIssueType.SelectedValue))
                    _titlesDictionary.Add("T_XAAType", cboTitleType.SelectedValue)
                    _titlesDictionary.Add("T_OutstdFV", txtOutstandingFV.Text)
                    _titlesDictionary.Add("T_IssuedFV", txtIssuedFV.Text)
                    _titlesDictionary.Add("T_CouponType", cboCouponType.SelectedValue)
                    _titlesDictionary.Add("T_SettlePlace", IIf(cboPOS.SelectedValue Is Nothing, 0, cboPOS.SelectedValue))
                    _titlesDictionary.Add("T_DefBroker", IIf(String.IsNullOrEmpty(cboCpty.Text), 0, cboCpty.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBroker", IIf(String.IsNullOrEmpty(cboSecClearingAgt1.Text), 0, cboSecClearingAgt1.SelectedValue))
                    _titlesDictionary.Add("T_SafeAccountOnAgent", IIf(String.IsNullOrEmpty(txtAgentCodePOS.Text), String.Empty, txtAgentCodePOS.Text))
                    _titlesDictionary.Add("T_TSDEFBrokerAgent", IIf(String.IsNullOrEmpty(cboSecClearingAgt2.Text), 0, cboSecClearingAgt2.SelectedValue))
                    _titlesDictionary.Add("T_AgentCode", IIf(String.IsNullOrEmpty(txtClearingAgentCodePOS.Text), String.Empty, txtClearingAgentCodePOS.Text))
                    _titlesDictionary.Add("T_CSDefBroker", IIf(String.IsNullOrEmpty(cboCashClearingAgt.Text), 0, cboCashClearingAgt.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBrokerCode", IIf(String.IsNullOrEmpty(txtCashSettlementBroker.Text), String.Empty, txtCashSettlementBroker.Text))
                    _titlesDictionary.Add("T_YieldType", cboBondYieldType.SelectedValue)
                    _titlesDictionary.Add("T_Scenarios", -1)
                    _titlesDictionary.Add("T_TaxOnFV", IIf(ChkTaxCalc.Checked, 1, 0))
                    '_titlesDictionary.Add("T_PeriodType", 1)
                    _titlesDictionary.Add("T_IsTemplate", 0)
                    _titlesDictionary.Add("T_MinTradeVolume", txtMinTradingQty.Text.ToString)
                    _titlesDictionary.Add("T_IssuerCountry", cboIssuedCtry.SelectedValue)
                    _titlesDictionary.Add("T_RelateRateDaysOffset", 0)
                    _titlesDictionary.Add("T_MarginAcc", 0)
                    _titlesDictionary.Add("T_ValMethod", 0)
                    _titlesDictionary.Add("T_ValType", 0)
                    _titlesDictionary.Add("T_EvalMethod", 0)
                    _titlesDictionary.Add("T_PmntOption", 0)
                    If Not String.IsNullOrEmpty(cboIndustrySector.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSECTORS", cboIndustrySector.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustryGroups.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYGROUPS", cboIndustryGroups.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustries.Text) Then
                        _titlesDictionary.Add("T_INDUSTRIES", cboIndustries.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustrySubs.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSUBS", cboIndustrySubs.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_CapAdjOpt", 0)
                    _titlesDictionary.Add("T_Sinkable", CInt(ChkSinkable.Checked))
                    _titlesDictionary.Add("T_InflationLinked", cboInflationLinked.SelectedValue)
                    _titlesDictionary.Add("T_CapitalGuaranteed", CInt(ChkCapGuaranteed.Checked))
                    _titlesDictionary.Add("T_MinCapitalGuaranteedPrc", txtMinCapGauranteed.Text)
                    If Not String.IsNullOrEmpty(txtDiscountFactor.Text) Then
                        _titlesDictionary.Add("T_PaybackFactorPrc", txtDiscountFactor.Text)
                    End If
                    _titlesDictionary.Add("T_CouponGuaranteed", CInt(ChkCpnGuaranteed.Checked))
                    _titlesDictionary.Add("T_FitchRating", cboFitch.SelectedValue)
                    _titlesDictionary.Add("T_Convertible", CInt(ChkConvertible.Checked))
                    _titlesDictionary.Add("T_PerpetualFlag", CInt(ChkPerpetual.Checked))
                    _titlesDictionary.Add("T_BLOOMBERGTYPE", cboBBGType.SelectedValue)
                    If Not String.IsNullOrEmpty(cboExternalFeedModel.Text) Then
                        _titlesDictionary.Add("T_ExtFeedModelId", cboExternalFeedModel.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboCalcTerms.Text) Then
                        _titlesDictionary.Add("T_BondCalcTerms", cboCalcTerms.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_PriceTickSize", 1)
                    _titlesDictionary.Add("T_CFICode", IIf(String.IsNullOrEmpty(txtCFICode.Text), vbNullString, txtCFICode.Text))
                    _titlesDictionary.Add("T_OnESMA", CInt(ChkESMA.Checked))
                    _titlesDictionary.Add("T_DeliverType", 0)
                    _titlesDictionary.Add("PriceClose", ClsInstrument.Close_Price)
                    _titlesDictionary.Add("PriceLow", ClsInstrument.Low_Price)
                    _titlesDictionary.Add("PriceHigh", ClsInstrument.High_Price)
                    _titlesDictionary.Add("PriceQuoteType", cboPriceQuoteType.SelectedValue)
                Case 4 'Mutual Funds/ETF
                    _titlesDictionary.Add("T_Group", cboGroup.SelectedValue)
                    _titlesDictionary.Add("T_Type", cboType.SelectedValue)
                    _titlesDictionary.Add("T_Master", 0)
                    _titlesDictionary.Add("T_Name", txtName.Text)
                    _titlesDictionary.Add("T_ISIN", txtISIN.Text)
                    _titlesDictionary.Add("T_ShortCut", txtTicker.Text.ToString)
                    _titlesDictionary.Add("T_SubType", cboSubType.SelectedValue)
                    _titlesDictionary.Add("T_TSymbol", txtStockExchangeCode.Text)
                    _titlesDictionary.Add("T_Premium", txtPremium.Text.ToString)
                    _titlesDictionary.Add("T_Market", cboMarket.SelectedValue)
                    _titlesDictionary.Add("T_IssuerCountry", cboIssuedCtry.SelectedValue)
                    _titlesDictionary.Add("T_AccType", 0)
                    _titlesDictionary.Add("T_InASE", 1)
                    _titlesDictionary.Add("T_Flag", 0)
                    _titlesDictionary.Add("T_BCurrency", -1)
                    _titlesDictionary.Add("T_Link1", txtLink1.Text.ToString)
                    _titlesDictionary.Add("T_Link2", txtLink2.Text.ToString)
                    _titlesDictionary.Add("T_Link3", txtLink3.Text.ToString)
                    _titlesDictionary.Add("T_Link4", txtLink4.Text.ToString)
                    _titlesDictionary.Add("T_FreeField1", txtFreefield1.Text.ToString)
                    _titlesDictionary.Add("T_FreeField2", txtFreefield2.Text.ToString)
                    _titlesDictionary.Add("T_FreeField3", txtFreefield3.Text.ToString)
                    _titlesDictionary.Add("T_FreeField4", txtFreefield4.Text.ToString)
                    _titlesDictionary.Add("T_Currency", CboCCY.SelectedValue)
                    _titlesDictionary.Add("T_InitDate", txtInitDate.Text.ToString)
                    _titlesDictionary.Add("T_OutstdFV", txtOutstandingFV.Text)
                    _titlesDictionary.Add("T_IssuedFV", txtIssuedFV.Text)
                    If Not String.IsNullOrEmpty(cboPeriod.Text) Then
                        _titlesDictionary.Add("T_Period", IIf(String.IsNullOrEmpty(txtPeriod.Text), cboPeriod.SelectedValue, txtPeriod.Text))
                    End If
                    _titlesDictionary.Add("T_QntDecimals", txtQtyDecimals.Text)
                    _titlesDictionary.Add("T_PrcDecimals", txtPriceDecimals.Text)
                    If Not String.IsNullOrEmpty(cboTradedCCY.Text) Then
                        _titlesDictionary.Add("T_TradingCRDCode", cboTradedCCY.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_TaxOnFV", 0)
                    _titlesDictionary.Add("T_Tax", IIf(String.IsNullOrEmpty(txttax.Text), 0, txttax.Text))
                    _titlesDictionary.Add("T_Beta", txtBeta.Text.ToString)
                    _titlesDictionary.Add("Issuer_Name", cboIssuer.Text.ToString)
                    _titlesDictionary.Add("Issuer_Cty", cboIssuedCtry.SelectedValue)
                    _titlesDictionary.Add("T_BetaDate", 0)
                    _titlesDictionary.Add("T_Base", 0)
                    _titlesDictionary.Add("T_Tokens", 0)
                    _titlesDictionary.Add("T_AverageCost", 1)
                    _titlesDictionary.Add("T_ViewInLists", IIf(CInt(ChkActive.Checked), 1, 0))
                    _titlesDictionary.Add("T_ApproveIn", IIf(CInt(ChkApproved.Checked), 1, 0))
                    _titlesDictionary.Add("T_LinkExport1", txtExportLink1.Text.ToString)
                    _titlesDictionary.Add("T_LinkExport2", txtExportLink2.Text.ToString)
                    _titlesDictionary.Add("T_BLOOMBERGTYPE", cboBBGType.SelectedValue)
                    If Not String.IsNullOrEmpty(cboExternalFeedModel.Text) Then
                        _titlesDictionary.Add("T_ExtFeedModelId", cboExternalFeedModel.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_BOGExcemption", CInt(chkBogException.Checked))
                    _titlesDictionary.Add("T_ReCapitalize", 0)
                    If Not String.IsNullOrEmpty(cboIndustrySector.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSECTORS", cboIndustrySector.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustryGroups.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYGROUPS", cboIndustryGroups.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustries.Text) Then
                        _titlesDictionary.Add("T_INDUSTRIES", cboIndustries.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustrySubs.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSUBS", cboIndustrySubs.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_AccKind", 0)
                    _titlesDictionary.Add("T_YieldType", 0)
                    _titlesDictionary.Add("T_IsTemplate", 0)
                    _titlesDictionary.Add("T_MarginAcc", 0)
                    _titlesDictionary.Add("T_ValMethod", 0)
                    _titlesDictionary.Add("T_ValType", 0)
                    _titlesDictionary.Add("T_EvalMethod", 0)
                    _titlesDictionary.Add("T_PmntOption", 0)
                    _titlesDictionary.Add("T_CapAdjOpt", 0)
                    _titlesDictionary.Add("T_CouponGuaranteed", 1)
                    _titlesDictionary.Add("T_Sinkable", 0)
                    _titlesDictionary.Add("T_InflationLinked", 0)
                    _titlesDictionary.Add("T_PaybackFactorPrc", 100)
                    _titlesDictionary.Add("T_CapitalGuaranteed", 1)
                    _titlesDictionary.Add("T_MinCapitalGuaranteedPrc", 100)
                    _titlesDictionary.Add("T_Convertible", 0)
                    _titlesDictionary.Add("T_PerpetualFlag", 0)
                    _titlesDictionary.Add("T_Scenarios", -1)
                    _titlesDictionary.Add("T_MinTradeVolume", 0)
                    _titlesDictionary.Add("T_Tier", 0)
                    _titlesDictionary.Add("T_MONTH30", 0)
                    _titlesDictionary.Add("T_PFolio", 0)
                    _titlesDictionary.Add("T_FaceValue", 0)
                    _titlesDictionary.Add("T_XAAType", 0)
                    _titlesDictionary.Add("T_BondType", 0)
                    _titlesDictionary.Add("T_CalcTax", 0)
                    _titlesDictionary.Add("T_CalcType", 0)
                    _titlesDictionary.Add("T_NumOfWarningDates", 0)
                    _titlesDictionary.Add("T_MaxQuantity", 0)
                    _titlesDictionary.Add("T_DOAdd", 1)
                    _titlesDictionary.Add("T_DOAcc", 1)
                    _titlesDictionary.Add("T_DOAddEOM", 1)
                    _titlesDictionary.Add("T_DOAccEOM", 1)
                    _titlesDictionary.Add("T_CollType", -1)
                    _titlesDictionary.Add("T_IssueType", -1)
                    _titlesDictionary.Add("T_Amount", 1)
                    _titlesDictionary.Add("T_MinDenomination", 1)
                    If Not String.IsNullOrEmpty(cboPortfolioLink.Text) Then
                        _titlesDictionary.Add("T_SettleDays", cboPortfolioLink.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_SettlePlace", IIf(cboPOS.SelectedValue Is Nothing, 0, cboPOS.SelectedValue))
                    _titlesDictionary.Add("T_DefBroker", IIf(String.IsNullOrEmpty(cboCpty.Text), 0, cboCpty.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBroker", IIf(String.IsNullOrEmpty(cboSecClearingAgt1.Text), 0, cboSecClearingAgt1.SelectedValue))
                    _titlesDictionary.Add("T_SafeAccountOnAgent", IIf(String.IsNullOrEmpty(txtAgentCodePOS.Text), String.Empty, txtAgentCodePOS.Text))
                    _titlesDictionary.Add("T_TSDEFBrokerAgent", IIf(String.IsNullOrEmpty(cboSecClearingAgt2.Text), 0, cboSecClearingAgt2.SelectedValue))
                    _titlesDictionary.Add("T_AgentCode", IIf(String.IsNullOrEmpty(txtClearingAgentCodePOS.Text), String.Empty, txtClearingAgentCodePOS.Text))
                    _titlesDictionary.Add("T_CSDefBroker", IIf(String.IsNullOrEmpty(cboCashClearingAgt.Text), 0, cboCashClearingAgt.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBrokerCode", IIf(String.IsNullOrEmpty(txtCashSettlementBroker.Text), String.Empty, txtCashSettlementBroker.Text))
                    _titlesDictionary.Add("T_PUCODE", cboIssuer.SelectedValue)
                    _titlesDictionary.Add("T_Factor", txtLimitAdjFactor.Text)
                    _titlesDictionary.Add("T_PriceTickSize", 1)
                    _titlesDictionary.Add("T_CFICode", IIf(String.IsNullOrEmpty(txtCFICode.Text), vbNullString, txtCFICode.Text))
                    _titlesDictionary.Add("T_OnESMA", CInt(ChkESMA.Checked))
                    If Not String.IsNullOrEmpty(txtCusip.Text) Then
                        _titlesDictionary.Add("T_CUSIP", txtCusip.Text.ToString)
                    End If
                    If Not String.IsNullOrEmpty(txtSedol.Text) Then
                        _titlesDictionary.Add("T_SEDOL", txtSedol.Text)
                    End If
                    _titlesDictionary.Add("T_AbNormalCoupon", 0)
                    _titlesDictionary.Add("T_DeliverType", 0)
                    _titlesDictionary.Add("PriceClose", ClsInstrument.Close_Price)
                    _titlesDictionary.Add("PriceLow", ClsInstrument.Low_Price)
                    _titlesDictionary.Add("PriceHigh", ClsInstrument.High_Price)
                    _titlesDictionary.Add("PriceQuoteType", cboPriceQuoteType.SelectedValue)
                Case 7 'Options
                    _titlesDictionary.Add("T_Group", cboGroup.SelectedValue)
                    _titlesDictionary.Add("T_Type", cboType.SelectedValue)
                    _titlesDictionary.Add("T_Master", txtDerivTCode.Text)
                    _titlesDictionary.Add("T_Name", $"{ClsInstrument.SecurityDescription} {ClsInstrument.MarketSectorDescription}")
                    _titlesDictionary.Add("T_ISIN", txtISIN.Text)
                    _titlesDictionary.Add("T_ShortCut", txtTicker.Text.ToString)
                    _titlesDictionary.Add("T_SubType", cboSubType.SelectedValue)
                    _titlesDictionary.Add("T_PUCODE", cboIssuer.SelectedValue)
                    _titlesDictionary.Add("T_TSymbol", String.Empty)
                    _titlesDictionary.Add("T_FaceValue", txtEntranceValue.Text.ToString)
                    _titlesDictionary.Add("T_Tokens", 0)
                    _titlesDictionary.Add("T_BCurrency", -1)
                    _titlesDictionary.Add("T_Premium", 0)
                    _titlesDictionary.Add("T_Market", cboMarket.SelectedValue)
                    _titlesDictionary.Add("T_IssuerCountry", cboIssuedCtry.SelectedValue)
                    _titlesDictionary.Add("T_AccType", 0)
                    _titlesDictionary.Add("T_Flag", 0)
                    _titlesDictionary.Add("T_ExpDate", txtMaturityDate.Text.ToString)
                    _titlesDictionary.Add("T_Period", IIf(String.IsNullOrEmpty(txtPeriod.Text), cboPeriod.SelectedValue, txtPeriod.Text))
                    _titlesDictionary.Add("T_Link1", txtLink1.Text.ToString)
                    _titlesDictionary.Add("T_Link2", txtLink2.Text.ToString)
                    _titlesDictionary.Add("T_Link3", txtLink3.Text.ToString)
                    _titlesDictionary.Add("T_Link4", txtLink4.Text.ToString)
                    _titlesDictionary.Add("T_FreeField1", txtFreefield1.Text.ToString)
                    _titlesDictionary.Add("T_FreeField2", txtFreefield2.Text.ToString)
                    _titlesDictionary.Add("T_FreeField3", txtFreefield3.Text.ToString)
                    _titlesDictionary.Add("T_FreeField4", txtFreefield4.Text.ToString)
                    _titlesDictionary.Add("T_PriceTickSize", txtTickSize.Text)
                    _titlesDictionary.Add("T_Currency", CboCCY.SelectedValue)
                    _titlesDictionary.Add("T_InitDate", txtInitDate.Text.ToString)
                    _titlesDictionary.Add("T_OutstdFV", txtOutstandingFV.Text)
                    _titlesDictionary.Add("T_IssuedFV", txtIssuedFV.Text)
                    _titlesDictionary.Add("T_Factor", ClsInstrument.T_Factor)
                    _titlesDictionary.Add("T_Amount", txtInterest.Text)
                    _titlesDictionary.Add("T_ReCapitalize", cboStyle.SelectedValue)
                    _titlesDictionary.Add("T_DeliverType", cboDeliveryType.SelectedValue)
                    _titlesDictionary.Add("T_SettlePlace", IIf(cboPOS.SelectedValue Is Nothing, 0, cboPOS.SelectedValue))
                    _titlesDictionary.Add("T_DefBroker", IIf(String.IsNullOrEmpty(cboCpty.Text), 0, cboCpty.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBroker", IIf(String.IsNullOrEmpty(cboSecClearingAgt1.Text), 0, cboSecClearingAgt1.SelectedValue))
                    _titlesDictionary.Add("T_SafeAccountOnAgent", IIf(String.IsNullOrEmpty(txtAgentCodePOS.Text), String.Empty, txtAgentCodePOS.Text))
                    _titlesDictionary.Add("T_TSDEFBrokerAgent", IIf(String.IsNullOrEmpty(cboSecClearingAgt2.Text), 0, cboSecClearingAgt2.SelectedValue))
                    _titlesDictionary.Add("T_AgentCode", IIf(String.IsNullOrEmpty(txtClearingAgentCodePOS.Text), String.Empty, txtClearingAgentCodePOS.Text))
                    _titlesDictionary.Add("T_CSDefBroker", IIf(String.IsNullOrEmpty(cboCashClearingAgt.Text), 0, cboCashClearingAgt.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBrokerCode", IIf(String.IsNullOrEmpty(txtCashSettlementBroker.Text), String.Empty, txtCashSettlementBroker.Text))
                    If Not String.IsNullOrEmpty(cboTradedCCY.Text) Then
                        _titlesDictionary.Add("T_TradingCRDCode", cboTradedCCY.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_AverageCost", 1)
                    _titlesDictionary.Add("T_AccKind", 0)
                    _titlesDictionary.Add("T_YieldType", 0)
                    _titlesDictionary.Add("T_IsTemplate", 0)
                    _titlesDictionary.Add("T_MarginAcc", 0)
                    _titlesDictionary.Add("T_ValMethod", 0)
                    _titlesDictionary.Add("T_ValType", 0)
                    _titlesDictionary.Add("T_EvalMethod", 0)
                    _titlesDictionary.Add("T_PmntOption", 0)
                    _titlesDictionary.Add("T_CapAdjOpt", 0)
                    _titlesDictionary.Add("T_CouponGuaranteed", 1)
                    _titlesDictionary.Add("T_Sinkable", 0)
                    _titlesDictionary.Add("T_InflationLinked", 0)
                    _titlesDictionary.Add("T_PaybackFactorPrc", 100)
                    _titlesDictionary.Add("T_CapitalGuaranteed", 1)
                    _titlesDictionary.Add("T_MinCapitalGuaranteedPrc", 100)
                    _titlesDictionary.Add("T_MinDenomination", 1)
                    _titlesDictionary.Add("T_Convertible", 0)
                    _titlesDictionary.Add("T_PerpetualFlag", 0)
                    _titlesDictionary.Add("T_Scenarios", -1)
                    _titlesDictionary.Add("T_MinTradeVolume", 0)
                    _titlesDictionary.Add("T_Tier", 0)
                    _titlesDictionary.Add("T_MONTH30", 0)
                    _titlesDictionary.Add("T_PFolio", 0)
                    _titlesDictionary.Add("T_IssueType", -1)
                    _titlesDictionary.Add("T_DOAdd", 0)
                    _titlesDictionary.Add("T_DOAddEOM", 0)
                    _titlesDictionary.Add("T_Tax", IIf(String.IsNullOrEmpty(txttax.Text), 0, txttax.Text))
                    _titlesDictionary.Add("T_Beta", txtBeta.Text.ToString)
                    _titlesDictionary.Add("Issuer_Name", cboIssuer.Text.ToString)
                    _titlesDictionary.Add("Issuer_Cty", cboIssuedCtry.SelectedValue)
                    _titlesDictionary.Add("T_BetaDate", 0)
                    _titlesDictionary.Add("T_ViewInLists", IIf(CInt(ChkActive.Checked), 1, 0))
                    _titlesDictionary.Add("T_ApproveIn", IIf(CInt(ChkApproved.Checked), 1, 0))
                    _titlesDictionary.Add("T_LinkExport1", txtExportLink1.Text.ToString)
                    _titlesDictionary.Add("T_LinkExport2", txtExportLink2.Text.ToString)
                    _titlesDictionary.Add("T_BLOOMBERGTYPE", cboBBGType.SelectedValue)
                    _titlesDictionary.Add("T_BOGExcemption", IIf(chkBogException.Checked, 1, 0))
                    If Not String.IsNullOrEmpty(cboIndustrySector.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSECTORS", cboIndustrySector.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustryGroups.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYGROUPS", cboIndustryGroups.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustries.Text) Then
                        _titlesDictionary.Add("T_INDUSTRIES", cboIndustries.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustrySubs.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSUBS", cboIndustrySubs.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_CFICode", IIf(String.IsNullOrEmpty(txtCFICode.Text), vbNullString, txtCFICode.Text))
                    _titlesDictionary.Add("T_OnESMA", IIf(ChkESMA.Checked, 1, 0))
                    _titlesDictionary.Add("T_CommDerivInd", cboCommDerivInd.SelectedValue)
                    _titlesDictionary.Add("PriceClose", ClsInstrument.Close_Price)
                    _titlesDictionary.Add("PriceLow", ClsInstrument.Low_Price)
                    _titlesDictionary.Add("PriceHigh", ClsInstrument.High_Price)
                    _titlesDictionary.Add("PriceQuoteType", cboPriceQuoteType.SelectedValue)
                Case 8 'Futures
                    _titlesDictionary.Add("T_Group", cboGroup.SelectedValue)
                    _titlesDictionary.Add("T_Type", cboType.SelectedValue)
                    _titlesDictionary.Add("T_Master", IIf(String.IsNullOrEmpty(txtDerivTCode.Text), "9294", txtDerivTCode.Text))
                    _titlesDictionary.Add("T_Name", $"{ClsInstrument.Ticker} {ClsInstrument.MarketSectorDescription}")
                    _titlesDictionary.Add("T_ISIN", IIf(String.IsNullOrEmpty(txtISIN.Text), String.Empty, txtISIN.Text))
                    _titlesDictionary.Add("T_ShortCut", txtTicker.Text.ToString)
                    _titlesDictionary.Add("T_SubType", cboSubType.SelectedValue)
                    _titlesDictionary.Add("T_PUCODE", cboIssuer.SelectedValue)
                    _titlesDictionary.Add("Issuer_Name", cboIssuer.Text.ToString)
                    _titlesDictionary.Add("T_Currency", CboCCY.SelectedValue)
                    '_titlesDictionary.Add("T_TSymbol", ClsInstrument.Exchange_Symbol)
                    _titlesDictionary.Add("T_FaceValue", 0)
                    _titlesDictionary.Add("T_ViewInLists", IIf(CInt(ChkActive.Checked), 1, 0))
                    _titlesDictionary.Add("T_ApproveIn", IIf(CInt(ChkApproved.Checked), 1, 0))
                    _titlesDictionary.Add("T_Tokens", 0)
                    _titlesDictionary.Add("T_BCurrency", -1)
                    '_titlesDictionary.Add("T_Premium", txtPremium.Text.ToString)
                    _titlesDictionary.Add("T_Market", cboMarket.SelectedValue)
                    _titlesDictionary.Add("T_IssuerCountry", IIf(cboIssuedCtry.SelectedValue Is Nothing, 0, cboIssuedCtry.SelectedValue))
                    _titlesDictionary.Add("T_AccType", 0)
                    '_titlesDictionary.Add("T_Flag", 0)
                    _titlesDictionary.Add("T_InitDate", txtInitDate.Text.ToString)
                    _titlesDictionary.Add("T_ExpDate", txtMaturityDate.Text.ToString)
                    _titlesDictionary.Add("Issuer_Cty", IIf(String.IsNullOrEmpty(cboIssuedCtry.Text), 0, cboIssuedCtry.SelectedValue))
                    '_titlesDictionary.Add("T_Period", 0)
                    _titlesDictionary.Add("T_Amount", txtInterest.Text)
                    '_titlesDictionary.Add("T_Base", txtMultiplier.Text)
                    _titlesDictionary.Add("T_InASE", IIf(ChkListed.Checked, 1, 0))
                    _titlesDictionary.Add("T_AverageCost", 0)
                    _titlesDictionary.Add("T_DOAdd", 0)
                    _titlesDictionary.Add("T_DOAddEOM", 0)
                    _titlesDictionary.Add("T_DOAcc", 1)
                    _titlesDictionary.Add("T_DOAccEOM", 1)
                    '_titlesDictionary.Add("T_OptionType", 0)
                    _titlesDictionary.Add("T_SettlePlace", IIf(cboPOS.SelectedValue Is Nothing, -2, cboPOS.SelectedValue))
                    _titlesDictionary.Add("T_DefBroker", IIf(String.IsNullOrEmpty(cboCpty.Text), -2, cboCpty.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBroker", IIf(String.IsNullOrEmpty(cboSecClearingAgt1.Text), -2, cboSecClearingAgt1.SelectedValue))
                    _titlesDictionary.Add("T_SafeAccountOnAgent", IIf(String.IsNullOrEmpty(txtAgentCodePOS.Text), String.Empty, txtAgentCodePOS.Text))
                    _titlesDictionary.Add("T_TSDEFBrokerAgent", IIf(String.IsNullOrEmpty(cboSecClearingAgt2.Text), -2, cboSecClearingAgt2.SelectedValue))
                    _titlesDictionary.Add("T_AgentCode", IIf(String.IsNullOrEmpty(txtClearingAgentCodePOS.Text), String.Empty, txtClearingAgentCodePOS.Text))
                    _titlesDictionary.Add("T_CSDefBroker", IIf(String.IsNullOrEmpty(cboCashClearingAgt.Text), -2, cboCashClearingAgt.SelectedValue))
                    _titlesDictionary.Add("T_TSDefBrokerCode", IIf(String.IsNullOrEmpty(txtCashSettlementBroker.Text), String.Empty, txtCashSettlementBroker.Text))
                    _titlesDictionary.Add("T_Factor", txtMultiplier.Text)
                    _titlesDictionary.Add("T_DeliverType", cboDeliveryType.SelectedValue)
                    _titlesDictionary.Add("T_CommDerivInd", cboCommDerivInd.SelectedValue)
                    _titlesDictionary.Add("T_Link1", txtLink1.Text.ToString)
                    _titlesDictionary.Add("T_Link2", txtLink2.Text.ToString)
                    _titlesDictionary.Add("T_Link3", txtLink3.Text.ToString)
                    _titlesDictionary.Add("T_Link4", txtLink4.Text.ToString)
                    _titlesDictionary.Add("T_PriceTickSize", txtTickSize.Text)
                    _titlesDictionary.Add("T_FreeField1", txtFreefield1.Text.ToString)
                    _titlesDictionary.Add("T_FreeField2", txtFreefield2.Text.ToString)
                    _titlesDictionary.Add("T_FreeField3", txtFreefield3.Text.ToString)
                    _titlesDictionary.Add("T_FreeField4", txtFreefield4.Text.ToString)
                    _titlesDictionary.Add("T_ReCapitalize", IIf(ChkReCapitalised.Checked, 1, 0))
                    _titlesDictionary.Add("T_Beta", txtBeta.Text.ToString)
                    _titlesDictionary.Add("T_LinkExport1", txtExportLink1.Text.ToString)
                    _titlesDictionary.Add("T_LinkExport2", txtExportLink2.Text.ToString)
                    _titlesDictionary.Add("T_BLOOMBERGTYPE", cboBBGType.SelectedValue)
                    _titlesDictionary.Add("T_BOGExcemption", IIf(chkBogException.Checked, 1, 0))
                    If Not String.IsNullOrEmpty(cboIndustrySector.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSECTORS", cboIndustrySector.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustryGroups.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYGROUPS", cboIndustryGroups.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustries.Text) Then
                        _titlesDictionary.Add("T_INDUSTRIES", cboIndustries.SelectedValue)
                    End If
                    If Not String.IsNullOrEmpty(cboIndustrySubs.Text) Then
                        _titlesDictionary.Add("T_INDUSTRYSUBS", cboIndustrySubs.SelectedValue)
                    End If
                    _titlesDictionary.Add("T_CFICode", IIf(String.IsNullOrEmpty(txtCFICode.Text), vbNullString, txtCFICode.Text))
                    _titlesDictionary.Add("T_OnESMA", IIf(ChkESMA.Checked, 1, 0))
                    _titlesDictionary.Add("T_CUSIP", IIf(String.IsNullOrEmpty(ClsInstrument.Cusip), String.Empty, ClsInstrument.Cusip))
                    _titlesDictionary.Add("T_SEDOL", IIf(String.IsNullOrEmpty(ClsInstrument.Sedol), String.Empty, ClsInstrument.Sedol))
                    _titlesDictionary.Add("T_AccKind", 0)
                    _titlesDictionary.Add("T_YieldType", 0)
                    _titlesDictionary.Add("T_IsTemplate", 0)
                    _titlesDictionary.Add("T_MarginAcc", 0)
                    _titlesDictionary.Add("T_ValMethod", 0)
                    _titlesDictionary.Add("T_ValType", 0)
                    _titlesDictionary.Add("T_EvalMethod", 0)
                    _titlesDictionary.Add("T_PmntOption", 0)
                    _titlesDictionary.Add("T_CapAdjOpt", 0)
                    _titlesDictionary.Add("T_CouponGuaranteed", 1)
                    _titlesDictionary.Add("T_Sinkable", 0)
                    _titlesDictionary.Add("T_InflationLinked", 0)
                    _titlesDictionary.Add("T_PaybackFactorPrc", 100)
                    _titlesDictionary.Add("T_CapitalGuaranteed", 1)
                    _titlesDictionary.Add("T_MinCapitalGuaranteedPrc", 100)
                    _titlesDictionary.Add("T_AbNormalCoupon", 0)
                    '_titlesDictionary.Add("T_MinDenomination", 1)
                    _titlesDictionary.Add("T_Convertible", 0)
                    _titlesDictionary.Add("T_PerpetualFlag", 0)
                    _titlesDictionary.Add("T_Scenarios", -1)
                    _titlesDictionary.Add("T_MinTradeVolume", 0)
                    _titlesDictionary.Add("T_Tier", 0)
                    '_titlesDictionary.Add("T_MONTH30", 0)
                    '_titlesDictionary.Add("T_PFolio", 0)
                    _titlesDictionary.Add("T_IssueType", -1)
                    _titlesDictionary.Add("PriceClose", ClsInstrument.Close_Price)
                    _titlesDictionary.Add("PriceLow", ClsInstrument.Low_Price)
                    _titlesDictionary.Add("PriceHigh", ClsInstrument.High_Price)
                    _titlesDictionary.Add("PriceQuoteType", cboPriceQuoteType.SelectedIndex)
            End Select

            If ClsIMS.InsertNewTitles(_titlesDictionary, _underlyingInstrument) Then
                If _underlyingInstrument Then
                    _underlyingTicker = txtTickerSearch.Text
                    btnClearForm_Click(sender, e)
                    txtTickerSearch.Text = _underlyingTicker
                    CmdLoadInstrument_Click(sender, e)

                    'Ensure that where underlying instrument details are displayed the current instrument is not set as underlying.
                    If Not String.IsNullOrEmpty(txtDerivTCode.Text) Then
                        _underlyingInstrument = False
                    End If
                Else
                    btnClearForm_Click(sender, e)
                End If
            Else
                MsgBox($"An error occured whilst attempting to insert the new Instrument ({ClsInstrument.SecurityDescription} - {ClsInstrument.ISIN} )", MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation, "Error during Insert")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Insert instrument details error")
        End Try

    End Sub

    ''' <summary>
    ''' Hide the bonds Tab Page.
    ''' </summary>
    Private Sub HideTabPageState(ByVal _hide As Boolean)

        Try
            If _hide Then
                If TabFundamentals.TabPages.Count = 2 Then
                    TabFundamentals.TabPages("tbpBond").Hide()
                    TabFundamentals.TabPages.Remove(tbpBond)
                End If
            Else
                If TabFundamentals.TabPages.Count = 1 Then
                    TabFundamentals.TabPages.Insert(0, tbpBond)
                    TabFundamentals.TabPages(0).Show()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Set Bond tab page error")
        End Try

    End Sub

    ''' <summary>
    ''' Hide the Fundamentals tab page.
    ''' </summary>
    ''' <param name="_hide"></param>
    Private Sub HideFundamentalsTabPage(ByVal _hide As Boolean)

        Try
            If _hide Then
                If TabInstrument.TabPages.Count = 4 Then
                    TabInstrument.TabPages("TabFund").Hide()
                    TabInstrument.TabPages.Remove(TabFund)
                End If
            Else
                If TabInstrument.TabPages.Count = 3 Then
                    TabInstrument.TabPages.Insert(2, TabFund)
                    TabInstrument.TabPages(2).Show()
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Set Fundamentals tab page error")
        End Try

    End Sub

    ''' <summary>
    ''' Calculate the Coupon period value for the T_Token value.
    ''' </summary>
    ''' <returns></returns>
    Private Function CalcCouponPeriod() As Integer

        Dim _periodDiff As Integer

        If cboType.SelectedValue = 2 Then
            Select Case cboPeriod.Text
                Case "Year"
                    _periodDiff = DateDiff(DateInterval.Year, CDate(ClsInstrument.IssueDate), CDate(ClsInstrument.MaturityDate))
                    Return _periodDiff
                Case "6 months"
                    _periodDiff = (DateDiff(DateInterval.Month, CDate(ClsInstrument.IssueDate), CDate(ClsInstrument.MaturityDate)) / 6)
                    Return _periodDiff
                Case "4 months"
                    _periodDiff = (DateDiff(DateInterval.Month, CDate(ClsInstrument.IssueDate), CDate(ClsInstrument.MaturityDate)) / 3)
                    Return _periodDiff
                Case "2 months"
                    _periodDiff = (DateDiff(DateInterval.Month, CDate(ClsInstrument.IssueDate), CDate(ClsInstrument.MaturityDate)) / 2)
                    Return _periodDiff
                Case "Quarter"
                    _periodDiff = (DateDiff(DateInterval.Month, CDate(ClsInstrument.IssueDate), CDate(ClsInstrument.MaturityDate)) / 4)
                    Return _periodDiff
                Case "Month"
                    _periodDiff = (DateDiff(DateInterval.Month, CDate(ClsInstrument.IssueDate), CDate(ClsInstrument.MaturityDate)))
                    Return _periodDiff
                Case "Months..."
                    If Not String.IsNullOrEmpty(txtPeriod.Text) Then
                        _periodDiff = (DateDiff(DateInterval.Month, CDate(ClsInstrument.IssueDate), CDate(ClsInstrument.MaturityDate)) / CInt(txtPeriod.Text))
                        If _periodDiff = 0 Then
                            _periodDiff = 1
                        End If
                        Return _periodDiff
                    End If
                Case "Days..."
                    If Not String.IsNullOrEmpty(txtPeriod.Text) Then
                        _periodDiff = (DateDiff(DateInterval.Day, CDate(ClsInstrument.IssueDate), CDate(ClsInstrument.MaturityDate)) / CInt(txtPeriod.Text))
                        Return _periodDiff
                    End If
            End Select
        End If

    End Function

    Private Function ValidateInstrument(_titles As Dictionary(Of String, String)) As String

        'If String.IsNullOrEmpty(_titles("T_MinCapitalGuaranteedPrc")) And _titles("T_Type") = 2 Then
        '    Return "A Min Cap Guaranteed % value must be entered"
        'Else
        Return String.Empty
        'End If

    End Function

    Private Sub txtFreefield4_MouseHover(sender As Object, e As EventArgs) Handles txtFreefield4.MouseHover

        ttpMain.ToolTipTitle = "FreeField4/BBG Description:"
        ttpMain.Show(txtFreefield4.Text, txtFreefield4)

    End Sub

End Class