﻿Public Class FrmStandingOrderRequest

    Dim _dicStandingOrder As Dictionary(Of String, String)
    Dim _blnFormLoaded As Boolean = False

    Protected Friend _edit As Boolean = False
    Protected Friend _standingOrderId As Integer

    Private Const cVolopaTemplateName As String = "Volopa Financial Services - GBP"

    Private Sub FrmStandingOrderRequest_Load(sender As Object, e As EventArgs) Handles Me.Load

        If _edit = False Then
            ClsIMS.PopulateComboboxWithData(cboPayType, "SELECT prt_Code, prt_description FROM vwDolfinPaymentRequestType WHERE prt_Code IN (2, 1, 3, 4, 7) ORDER BY prt_description")
            cboInterval.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboInterval.AutoCompleteSource = AutoCompleteSource.ListItems

            ClsIMS.PopulateComboboxWithData(cboInterval, "SELECT SI_ID, SI_Description AS [Interval] FROM DolfinPaymentStandingOrdersInterval ORDER BY SI_ID ASC")
            cboInterval.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboInterval.AutoCompleteSource = AutoCompleteSource.ListItems

            ClsIMS.PopulateComboboxWithData(cboStatus, "SELECT S_ID, S_Status FROM DolfinPaymentStatus WHERE S_ID IN (21, 32) ORDER BY S_Status ASC")
            cboStatus.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboStatus.AutoCompleteSource = AutoCompleteSource.ListItems
        Else
            lblStandingOrders.Text = "Edit Standing Order Request"
            cboCCY.Enabled = False
            cboPayType.Enabled = False
            cboPortfolio.Enabled = False
            cboTemplate.Enabled = False
            txtSwiftRef.Enabled = False
        End If

        _blnFormLoaded = True

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim NextRunDate As Date
        Dim _clsIMSData As New ClsIMSData
        Dim strRef As String = ""

        Try
            strRef = txtSwiftRef.Text
            txtSwiftRef.Text = System.Text.RegularExpressions.Regex.Replace(txtSwiftRef.Text, "[^A-Za-z0-9\-/]", "")

            'Populate the dictionary object with the standing order details.
            _dicStandingOrder = New Dictionary(Of String, String)
            _dicStandingOrder.Add("SO_ID", IIf(_edit, _standingOrderId, 0))
            _dicStandingOrder.Add("PortfolioCode", cboPortfolio.SelectedValue)
            _dicStandingOrder.Add("TemplateId", cboTemplate.SelectedValue)
            _dicStandingOrder.Add("PaymentTypeId", cboPayType.SelectedValue)
            _dicStandingOrder.Add("CurrId", cboCCY.SelectedValue)
            _dicStandingOrder.Add("Amount", txtAmount.Text)
            _dicStandingOrder.Add("StartDate", dtpStartDate.Value)
            _dicStandingOrder.Add("EndDate", dtpEndDate.Value)
            _dicStandingOrder.Add("Interval", cboInterval.SelectedValue)
            _dicStandingOrder.Add("Status", cboStatus.SelectedValue)
            _dicStandingOrder.Add("Username", Environment.UserName)
            _dicStandingOrder.Add("SwiftRef", txtSwiftRef.Text)

            _clsIMSData.UpdateStandingOrder(_dicStandingOrder)

            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameStandingOrders, $"{IIf(_edit = True, $"Edit : SO_ID:{_standingOrderId}", "Insert")}  PortfolioCode: {cboPortfolio.SelectedValue}, TemplateId: {cboTemplate.SelectedValue}, " &
                            $"PaymentTypeId: {cboPayType.SelectedValue}, CurrId: {cboCCY.SelectedValue}, Amount: {txtAmount.Text}, StartDate: {dtpStartDate.Value}, " &
                            $"NextRunDate: {NextRunDate}, EndDate: {dtpEndDate.Value}, Interval: {cboInterval.Text}, Status: {cboStatus.Text}", _standingOrderId)
            Me.Close()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameStandingOrders, $"{Err.Description} - error loading the standing orders form.")
        Finally
            _clsIMSData = Nothing
        End Try

    End Sub

    Private Sub cboPayType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPayType.SelectedIndexChanged

        If IsNumeric(cboPayType.SelectedValue) Then

            'Determine which options a re available for each payment type.
            If cboPayType.SelectedValue = 1 Or cboPayType.SelectedValue = 3 Then
                lblTemplate.Text = "Ben Portfolio"
                lblSwiftRef.Text = "Loan No:"
                txtSwiftRef.Text = IIf(cboPayType.SelectedValue = 1, Now.Date.ToString("yyyymmdd") & " dd <dd/mm/yyyy>", "")
                lblTemplate.Visible = True
                cboTemplate.Visible = True
            ElseIf cboPayType.SelectedValue = 2 Or cboPayType.SelectedValue = 4 Or cboPayType.SelectedValue = 7 Then
                lblTemplate.Text = "Template"
                lblSwiftRef.Text = "Swift Ref:"
                txtSwiftRef.Text = If(cboPayType.SelectedValue = 4, "OwnFundsTransfer", "")
                lblTemplate.Visible = True
                cboTemplate.Visible = True
            End If

            If cboPayType.SelectedValue = 1 Then 'client loan
                ClsIMS.PopulateComboboxWithData(cboPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class in (1,25,37,39) order by Portfolio")
            ElseIf cboPayType.SelectedValue = 2 Then '3rd party payment
                ClsIMS.PopulateComboboxWithData(cboPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_shortcut1 not in ('BBPaySvc') and b_class In (1,25,37,39) group by pf_Code,Portfolio order by Portfolio")
            ElseIf cboPayType.SelectedValue = 3 Then 'client to client payment
                ClsIMS.PopulateComboboxWithData(cboPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class in (1,25,37,39) order by Portfolio")
            ElseIf cboPayType.SelectedValue = 4 Then 'own funds transfer
                ClsIMS.PopulateComboboxWithData(cboPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class in (1,25,37,39) order by Portfolio")
            ElseIf cboPayType.SelectedValue = 7 Then 'dolfin pay
                ClsIMS.PopulateComboboxWithData(cboPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class in (1,25,37,39) order by Portfolio")
            End If
        End If
        OkayToSave()

    End Sub

    Private Sub OkayToSave()

        'Check to see if the Save/Update button is enabled or not
        btnSave.Enabled = cboPortfolio.Text IsNot "" And cboPayType.Text IsNot "" And cboCCY.Text IsNot "" And txtAmount.Text IsNot "" And cboInterval.Text IsNot "" And cboStatus.Text IsNot "" And txtSwiftRef.Text IsNot ""

    End Sub

    Private Sub cboPortfolio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPortfolio.SelectedIndexChanged

        'Populate the combo box with the currencies available for the selected Portfolio.
        If _blnFormLoaded And IsNumeric(cboPortfolio.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(cboCCY, $"select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccountAll where pf_code = {cboPortfolio.SelectedValue} group by CR_Code,CR_Name1 order by CR_Name1")
        End If
        OkayToSave()

    End Sub

    Private Sub cboCCY_Leave(sender As Object, e As EventArgs) Handles cboCCY.Leave

        If IsNumeric(cboCCY.SelectedValue) Then
            If cboPayType.SelectedValue = 10 Or cboPayType.SelectedValue = 11 Then
                ClsIMS.PopulateComboboxWithData(cboTemplate, "Select 0 As pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & IIf(cboPayType.SelectedValue = 10, "42", "47") & " and cr_code = " & cboCCY.SelectedValue & " order by Portfolio")
            Else
                ClsIMS.PopulateComboboxWithData(cboTemplate, "Select 0 As pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and cr_code = " & cboCCY.SelectedValue & " order by Portfolio")
            End If
            If cboPayType.SelectedValue <> 1 Then
                If cboPayType.SelectedValue = 8 Then
                    ClsIMS.PopulateComboboxWithData(cboTemplate, $"SELECT 1 as ID,Pa_TemplateName FROM vwDolfinPaymentAddress WHERE Pa_TemplateName = '{cVolopaTemplateName}' AND pa_templateSelected = 2")
                    cboPortfolio.SelectedValue = 1
                Else
                    ClsIMS.PopulateComboboxWithData(cboTemplate, $"SELECT 0 As ID, '' as Pa_TemplateName UNION SELECT Pa_Id as ID,Pa_TemplateName FROM vwDolfinPaymentAddress WHERE Pa_PortfolioCode = {cboPortfolio.SelectedValue} AND Pa_CCYCode = {cboCCY.SelectedValue } AND Pa_TemplateName <> '' GROUP BY Pa_TemplateName, Pa_Id ORDER BY Pa_TemplateName")
                End If

            End If
        End If

    End Sub

    Private Sub dtpEndDate_CloseUp(sender As Object, e As EventArgs) Handles dtpEndDate.CloseUp
        Dim datStartDate As Date = dtpStartDate.Value
        If dtpStartDate.Value >= dtpEndDate.Value And _blnFormLoaded Then
            MsgBox("The end date cannot be before or the same as the start date, please select a valid date!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Invalid end date entered")
            dtpEndDate.Value = datStartDate.AddDays(7)
        End If
    End Sub

    Private Sub dtpStartDate_CloseUp(sender As Object, e As EventArgs) Handles dtpStartDate.CloseUp
        Dim datEndDate As Date = dtpEndDate.Value
        If dtpEndDate.Value <= dtpStartDate.Value And _blnFormLoaded Then
            MsgBox("The start date cannot be after or the same as the end date, please select a valid date!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Invalid start date entered")
            dtpStartDate.Value = datEndDate.AddDays(-7)
        End If
    End Sub

    Private Sub txtAmount_TextChanged(sender As Object, e As EventArgs) Handles txtAmount.TextChanged
        OkayToSave()
    End Sub

    Private Sub txtAmount_Leave(sender As Object, e As EventArgs) Handles txtAmount.Leave

        If Not IsNumeric(txtAmount.Text) And txtAmount.Text <> "" Then
            MsgBox("Only numeric values permitted in this field, please enter a correct value.", MessageBoxButtons.OK + MessageBoxIcon.Exclamation, "Invalid value entered")
        End If

    End Sub

    Private Sub cboCCY_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboCCY.SelectedValueChanged
        OkayToSave()
    End Sub

    Private Sub cboTemplate_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboTemplate.SelectedValueChanged
        OkayToSave()
    End Sub

    Private Sub cboInterval_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboInterval.SelectedValueChanged
        OkayToSave()
    End Sub

    Private Sub cboStatus_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboStatus.SelectedValueChanged
        OkayToSave()
    End Sub

    Private Sub txtSwiftRef_TextChanged(sender As Object, e As EventArgs) Handles txtSwiftRef.TextChanged
        OkayToSave()
    End Sub
End Class