﻿Public Class FrmAbout
    Private Sub FrmAbout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Dim varIMSDb As String = ClsIMS.GetSQLItem("SELECT COALESCE(PARSENAME(base_object_name,3),DB_NAME(DB_ID())) AS dbName FROM sys.synonyms where name = 'portfolios'")
        'Dim varIFTDb As String = ClsIMS.GetSQLItem("SELECT COALESCE(PARSENAME(base_object_name,3),DB_NAME(DB_ID())) AS dbName FROM sys.synonyms where name = 'IFT_FileLog'")

        Dim varConnAbout As System.Data.SqlClient.SqlConnection = ClsIMS.GetCurrentConnection

        lblpaymentTitle.Text = lblpaymentTitle.Text & " " & ClsIMS.FLOWVersion
        varConnAbout.StatisticsEnabled = True
        txtAbout.Text = "Username: " & ClsIMS.FullUserName & vbNewLine &
        "Workstation ID: " & varConnAbout.WorkstationId.ToString & vbNewLine & vbNewLine &
        "Server: " & varConnAbout.DataSource.ToString & vbNewLine &
        "Payments Database: " & varConnAbout.Database.ToString & vbNewLine &
        "IMS Database: " & ClsIMS.IMSDB & vbNewLine &
        "IFT Database: " & ClsIMS.IMSIFTDB & vbNewLine &
        "SQL Server version: " & varConnAbout.ServerVersion.ToString & vbNewLine &
        "SQL Server Connection State: " & varConnAbout.State.ToString & vbNewLine

        If (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed) Then
            Dim ver As Version
            ver = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion
            txtAbout.Text = txtAbout.Text & String.Format("{0}.{1}.{2}.{3}", ver.Major, ver.Minor, ver.Build, ver.Revision)
        End If
    End Sub
End Class