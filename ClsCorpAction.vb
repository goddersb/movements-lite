﻿Public Class ClsCorpAction
    Private varSelectedAuthoriseOption As Integer = 0
    Private varID As Double = 0
    Private varInstrumentName As String = ""
    Private varISIN As String = ""
    Private varCAEvent As String = ""
    Private varMV As String = ""
    Private varCustodian As String = ""
    Private varCustodianDeadline As Date
    Private varDolfinDeadline As Date
    Private varExDate As Date
    Private varRecDate As Date
    Private varSettledPosition As Double = 0

    Property SelectedAuthoriseOption() As Integer
        Get
            Return varSelectedAuthoriseOption
        End Get
        Set(value As Integer)
            varSelectedAuthoriseOption = value
        End Set
    End Property

    Property ID() As Double
        Get
            Return varID
        End Get
        Set(value As Double)
            varID = value
        End Set
    End Property

    Property InstrumentName() As String
        Get
            Return varInstrumentName
        End Get
        Set(value As String)
            varInstrumentName = value
        End Set
    End Property

    Property ISIN() As String
        Get
            Return varISIN
        End Get
        Set(value As String)
            varISIN = value
        End Set
    End Property

    Property CAEvent() As String
        Get
            Return varCAEvent
        End Get
        Set(value As String)
            varCAEvent = value
        End Set
    End Property

    Property MV() As String
        Get
            Return varMV
        End Get
        Set(value As String)
            varMV = value
        End Set
    End Property

    Property Custodian() As String
        Get
            Return varCustodian
        End Get
        Set(value As String)
            varCustodian = value
        End Set
    End Property

    Property CustodianDeadline() As Date
        Get
            Return varCustodianDeadline
        End Get
        Set(value As Date)
            varCustodianDeadline = value
        End Set
    End Property

    Property DolfinDeadline() As Date
        Get
            Return varDolfinDeadline
        End Get
        Set(value As Date)
            varDolfinDeadline = value
        End Set
    End Property

    Property ExDate() As Date
        Get
            Return varExDate
        End Get
        Set(value As Date)
            varExDate = value
        End Set
    End Property

    Property RecDate() As Date
        Get
            Return varRecDate
        End Get
        Set(value As Date)
            varRecDate = value
        End Set
    End Property

    Property SettledPosition() As Double
        Get
            Return varSettledPosition
        End Get
        Set(value As Double)
            varSettledPosition = value
        End Set
    End Property
End Class