﻿Public Class ClientDetails

    Public Property ClientSwiftCode As String
    Public Property ClientName As String
    Public Property ClientAddress As String
    Public Property ClientCity As String
    Public Property ClientPostCode As String

End Class