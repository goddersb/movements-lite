﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfirmReceiptsAddFunds
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConfirmReceiptsAddFunds))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtCRMFSettleDate = New System.Windows.Forms.DateTimePicker()
        Me.cboCRMFCCY = New System.Windows.Forms.ComboBox()
        Me.lblTransDate = New System.Windows.Forms.Label()
        Me.dtCRMFTradeDate = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboCRMFBankName = New System.Windows.Forms.ComboBox()
        Me.txtCRMFSender = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCRMFAmt = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CmdAddFunds = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GrpCRMFnotes = New System.Windows.Forms.GroupBox()
        Me.txtCRMFNotes = New System.Windows.Forms.TextBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.CRMFFiles = New System.Windows.Forms.ListView()
        Me.ContextMenuStripFiles = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cboCRMFSwiftCode = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Rb1 = New System.Windows.Forms.RadioButton()
        Me.Rb2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.GrpCRMFnotes.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.ContextMenuStripFiles.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox1.Controls.Add(Me.Rb2)
        Me.GroupBox1.Controls.Add(Me.Rb1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cboCRMFSwiftCode)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.dtCRMFSettleDate)
        Me.GroupBox1.Controls.Add(Me.cboCRMFCCY)
        Me.GroupBox1.Controls.Add(Me.lblTransDate)
        Me.GroupBox1.Controls.Add(Me.dtCRMFTradeDate)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cboCRMFBankName)
        Me.GroupBox1.Controls.Add(Me.txtCRMFSender)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtCRMFAmt)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 36)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(655, 152)
        Me.GroupBox1.TabIndex = 52
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Fund Details"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(266, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 122
        Me.Label4.Text = "Settle:"
        '
        'dtCRMFSettleDate
        '
        Me.dtCRMFSettleDate.Location = New System.Drawing.Point(308, 32)
        Me.dtCRMFSettleDate.Name = "dtCRMFSettleDate"
        Me.dtCRMFSettleDate.Size = New System.Drawing.Size(138, 20)
        Me.dtCRMFSettleDate.TabIndex = 1
        '
        'cboCRMFCCY
        '
        Me.cboCRMFCCY.FormattingEnabled = True
        Me.cboCRMFCCY.Location = New System.Drawing.Point(570, 113)
        Me.cboCRMFCCY.Name = "cboCRMFCCY"
        Me.cboCRMFCCY.Size = New System.Drawing.Size(78, 21)
        Me.cboCRMFCCY.TabIndex = 8
        '
        'lblTransDate
        '
        Me.lblTransDate.AutoSize = True
        Me.lblTransDate.Location = New System.Drawing.Point(59, 33)
        Me.lblTransDate.Name = "lblTransDate"
        Me.lblTransDate.Size = New System.Drawing.Size(38, 13)
        Me.lblTransDate.TabIndex = 117
        Me.lblTransDate.Text = "Trade:"
        '
        'dtCRMFTradeDate
        '
        Me.dtCRMFTradeDate.Location = New System.Drawing.Point(107, 32)
        Me.dtCRMFTradeDate.Name = "dtCRMFTradeDate"
        Me.dtCRMFTradeDate.Size = New System.Drawing.Size(138, 20)
        Me.dtCRMFTradeDate.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 13)
        Me.Label2.TabIndex = 115
        Me.Label2.Text = "Bank Name:"
        '
        'cboCRMFBankName
        '
        Me.cboCRMFBankName.FormattingEnabled = True
        Me.cboCRMFBankName.Location = New System.Drawing.Point(107, 87)
        Me.cboCRMFBankName.Name = "cboCRMFBankName"
        Me.cboCRMFBankName.Size = New System.Drawing.Size(541, 21)
        Me.cboCRMFBankName.TabIndex = 4
        '
        'txtCRMFSender
        '
        Me.txtCRMFSender.Location = New System.Drawing.Point(107, 58)
        Me.txtCRMFSender.Multiline = True
        Me.txtCRMFSender.Name = "txtCRMFSender"
        Me.txtCRMFSender.Size = New System.Drawing.Size(541, 23)
        Me.txtCRMFSender.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(35, 61)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 13)
        Me.Label11.TabIndex = 112
        Me.Label11.Text = "Beneficiary:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(533, 116)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 108
        Me.Label5.Text = "CCY:"
        '
        'txtCRMFAmt
        '
        Me.txtCRMFAmt.Location = New System.Drawing.Point(364, 114)
        Me.txtCRMFAmt.Name = "txtCRMFAmt"
        Me.txtCRMFAmt.Size = New System.Drawing.Size(143, 20)
        Me.txtCRMFAmt.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Enabled = False
        Me.Label3.Location = New System.Drawing.Point(312, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 106
        Me.Label3.Text = "Amount:"
        '
        'CmdAddFunds
        '
        Me.CmdAddFunds.Location = New System.Drawing.Point(398, 407)
        Me.CmdAddFunds.Name = "CmdAddFunds"
        Me.CmdAddFunds.Size = New System.Drawing.Size(276, 23)
        Me.CmdAddFunds.TabIndex = 11
        Me.CmdAddFunds.Text = "Add Funds"
        Me.CmdAddFunds.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label1.Location = New System.Drawing.Point(283, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(113, 24)
        Me.Label1.TabIndex = 54
        Me.Label1.Text = "Add Funds"
        '
        'GrpCRMFnotes
        '
        Me.GrpCRMFnotes.Controls.Add(Me.txtCRMFNotes)
        Me.GrpCRMFnotes.Location = New System.Drawing.Point(18, 290)
        Me.GrpCRMFnotes.Name = "GrpCRMFnotes"
        Me.GrpCRMFnotes.Size = New System.Drawing.Size(655, 98)
        Me.GrpCRMFnotes.TabIndex = 56
        Me.GrpCRMFnotes.TabStop = False
        Me.GrpCRMFnotes.Text = "Notes"
        '
        'txtCRMFNotes
        '
        Me.txtCRMFNotes.Location = New System.Drawing.Point(6, 19)
        Me.txtCRMFNotes.Multiline = True
        Me.txtCRMFNotes.Name = "txtCRMFNotes"
        Me.txtCRMFNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCRMFNotes.Size = New System.Drawing.Size(642, 69)
        Me.txtCRMFNotes.TabIndex = 10
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.CRMFFiles)
        Me.GroupBox7.Location = New System.Drawing.Point(18, 203)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(655, 71)
        Me.GroupBox7.TabIndex = 55
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "File Attachments"
        '
        'CRMFFiles
        '
        Me.CRMFFiles.AllowDrop = True
        Me.CRMFFiles.BackColor = System.Drawing.SystemColors.Window
        Me.CRMFFiles.BackgroundImage = CType(resources.GetObject("CRMFFiles.BackgroundImage"), System.Drawing.Image)
        Me.CRMFFiles.FullRowSelect = True
        Me.CRMFFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.CRMFFiles.Location = New System.Drawing.Point(6, 15)
        Me.CRMFFiles.MultiSelect = False
        Me.CRMFFiles.Name = "CRMFFiles"
        Me.CRMFFiles.Size = New System.Drawing.Size(642, 45)
        Me.CRMFFiles.TabIndex = 9
        Me.CRMFFiles.UseCompatibleStateImageBehavior = False
        '
        'ContextMenuStripFiles
        '
        Me.ContextMenuStripFiles.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenFileToolStripMenuItem, Me.RemoveFileToolStripMenuItem, Me.RefreshToolStripMenuItem})
        Me.ContextMenuStripFiles.Name = "ContextMenuStripFiles"
        Me.ContextMenuStripFiles.Size = New System.Drawing.Size(139, 70)
        '
        'OpenFileToolStripMenuItem
        '
        Me.OpenFileToolStripMenuItem.Name = "OpenFileToolStripMenuItem"
        Me.OpenFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.OpenFileToolStripMenuItem.Text = "&Open File"
        '
        'RemoveFileToolStripMenuItem
        '
        Me.RemoveFileToolStripMenuItem.Name = "RemoveFileToolStripMenuItem"
        Me.RemoveFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RemoveFileToolStripMenuItem.Text = "&Remove File"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'cboCRMFSwiftCode
        '
        Me.cboCRMFSwiftCode.Enabled = False
        Me.cboCRMFSwiftCode.FormattingEnabled = True
        Me.cboCRMFSwiftCode.Location = New System.Drawing.Point(107, 114)
        Me.cboCRMFSwiftCode.Name = "cboCRMFSwiftCode"
        Me.cboCRMFSwiftCode.Size = New System.Drawing.Size(166, 21)
        Me.cboCRMFSwiftCode.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(20, 117)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 124
        Me.Label6.Text = "Swift Code:"
        '
        'Rb1
        '
        Me.Rb1.AutoSize = True
        Me.Rb1.Checked = True
        Me.Rb1.Location = New System.Drawing.Point(87, 90)
        Me.Rb1.Name = "Rb1"
        Me.Rb1.Size = New System.Drawing.Size(14, 13)
        Me.Rb1.TabIndex = 3
        Me.Rb1.TabStop = True
        Me.Rb1.UseVisualStyleBackColor = True
        '
        'Rb2
        '
        Me.Rb2.AutoSize = True
        Me.Rb2.Location = New System.Drawing.Point(87, 117)
        Me.Rb2.Name = "Rb2"
        Me.Rb2.Size = New System.Drawing.Size(14, 13)
        Me.Rb2.TabIndex = 5
        Me.Rb2.UseVisualStyleBackColor = True
        '
        'FrmConfirmReceiptsAddFunds
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(686, 444)
        Me.Controls.Add(Me.GrpCRMFnotes)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CmdAddFunds)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmConfirmReceiptsAddFunds"
        Me.Text = "Confirm Receipts Add Funds"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GrpCRMFnotes.ResumeLayout(False)
        Me.GrpCRMFnotes.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.ContextMenuStripFiles.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents CmdAddFunds As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCRMFSender As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtCRMFAmt As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cboCRMFBankName As ComboBox
    Friend WithEvents cboCRMFCCY As ComboBox
    Friend WithEvents lblTransDate As Label
    Friend WithEvents dtCRMFTradeDate As DateTimePicker
    Friend WithEvents GrpCRMFnotes As GroupBox
    Friend WithEvents txtCRMFNotes As TextBox
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents CRMFFiles As ListView
    Friend WithEvents ContextMenuStripFiles As ContextMenuStrip
    Friend WithEvents OpenFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoveFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label4 As Label
    Friend WithEvents dtCRMFSettleDate As DateTimePicker
    Friend WithEvents Rb2 As RadioButton
    Friend WithEvents Rb1 As RadioButton
    Friend WithEvents Label6 As Label
    Friend WithEvents cboCRMFSwiftCode As ComboBox
End Class
