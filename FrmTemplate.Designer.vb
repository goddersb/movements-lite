﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTemplate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTemplate))
        Me.BeneficiaryGroupBox = New System.Windows.Forms.GroupBox()
        Me.TabTemBeneficiary = New System.Windows.Forms.TabControl()
        Me.TabTemMain = New System.Windows.Forms.TabPage()
        Me.txtTemBenName = New System.Windows.Forms.TextBox()
        Me.cboTemBenType = New System.Windows.Forms.ComboBox()
        Me.lbltembenType = New System.Windows.Forms.Label()
        Me.txttembensurname = New System.Windows.Forms.TextBox()
        Me.txtTembenmiddlename = New System.Windows.Forms.TextBox()
        Me.txttembenfirstname = New System.Windows.Forms.TextBox()
        Me.cbotembenrelationship = New System.Windows.Forms.ComboBox()
        Me.lblTemBenRelationship = New System.Windows.Forms.Label()
        Me.lblTemBenSurname = New System.Windows.Forms.Label()
        Me.lblTemBenMiddlename = New System.Windows.Forms.Label()
        Me.lbltemBenFirstname = New System.Windows.Forms.Label()
        Me.cboTemBenCountry = New System.Windows.Forms.ComboBox()
        Me.lblTemBenCountry = New System.Windows.Forms.Label()
        Me.lblTemBenCounty = New System.Windows.Forms.Label()
        Me.txttembencounty = New System.Windows.Forms.TextBox()
        Me.txtTemBenZip = New System.Windows.Forms.TextBox()
        Me.txtTemBenAddress2 = New System.Windows.Forms.TextBox()
        Me.txtTemBenAddress1 = New System.Windows.Forms.TextBox()
        Me.lblTemBenZip = New System.Windows.Forms.Label()
        Me.lblTemBenAddress2 = New System.Windows.Forms.Label()
        Me.lblTemBenAddress1 = New System.Windows.Forms.Label()
        Me.lblBenName = New System.Windows.Forms.Label()
        Me.TabTemBankDetails = New System.Windows.Forms.TabPage()
        Me.lblbenBIK = New System.Windows.Forms.Label()
        Me.txtTemBenBIK = New System.Windows.Forms.TextBox()
        Me.txtTemBenINN = New System.Windows.Forms.TextBox()
        Me.lblbenINN = New System.Windows.Forms.Label()
        Me.PicTemBenIBAN = New System.Windows.Forms.PictureBox()
        Me.PicTemBenSwift = New System.Windows.Forms.PictureBox()
        Me.txtTemBenOther1 = New System.Windows.Forms.TextBox()
        Me.txtTemBenIBAN = New System.Windows.Forms.TextBox()
        Me.txtTemBenSwift = New System.Windows.Forms.TextBox()
        Me.lblBenOther1 = New System.Windows.Forms.Label()
        Me.lblBenIBAN = New System.Windows.Forms.Label()
        Me.lblBenSwift = New System.Windows.Forms.Label()
        Me.BeneficiaryGroupBankGroupBox = New System.Windows.Forms.GroupBox()
        Me.BeneficiarySubBankGroupBox = New System.Windows.Forms.GroupBox()
        Me.cboTemBenSubBankCountry = New System.Windows.Forms.ComboBox()
        Me.txtTemBenSubBankINN = New System.Windows.Forms.TextBox()
        Me.txtTemBenSubBankBIK = New System.Windows.Forms.TextBox()
        Me.lblBenSubBankBIK = New System.Windows.Forms.Label()
        Me.lblBenSubBankINN = New System.Windows.Forms.Label()
        Me.ChkTemBenSubBank = New System.Windows.Forms.CheckBox()
        Me.txtTemBenSubBankOther1 = New System.Windows.Forms.TextBox()
        Me.PicTemBenSubBankIBAN = New System.Windows.Forms.PictureBox()
        Me.PicTemBenSubBankSwift = New System.Windows.Forms.PictureBox()
        Me.CboTemBenSubBankName = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTemBenSubBankIBAN = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtTemBenSubBankSwift = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTemBenSubBankZip = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtTemBenSubBankAddress2 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtTemBenSubBankAddress1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.BeneficiaryBankGroupBox = New System.Windows.Forms.GroupBox()
        Me.cboTemBenBankCountry = New System.Windows.Forms.ComboBox()
        Me.txtTemBenBankINN = New System.Windows.Forms.TextBox()
        Me.txtTemBenBankBIK = New System.Windows.Forms.TextBox()
        Me.lblBenBankBIK = New System.Windows.Forms.Label()
        Me.lblBenBankINN = New System.Windows.Forms.Label()
        Me.txtTemBenBankOther1 = New System.Windows.Forms.TextBox()
        Me.PicTemBenBankIBAN = New System.Windows.Forms.PictureBox()
        Me.lblBenBankName = New System.Windows.Forms.Label()
        Me.PicTemBenBankSwift = New System.Windows.Forms.PictureBox()
        Me.CboTemBenBankName = New System.Windows.Forms.ComboBox()
        Me.lblBenBankSwift = New System.Windows.Forms.Label()
        Me.txtTemBenBankIBAN = New System.Windows.Forms.TextBox()
        Me.lblBenBankIBAN = New System.Windows.Forms.Label()
        Me.txtTemBenBankSwift = New System.Windows.Forms.TextBox()
        Me.lblBenBankOther1 = New System.Windows.Forms.Label()
        Me.txtTemBenBankZip = New System.Windows.Forms.TextBox()
        Me.lblBenBankAddress1 = New System.Windows.Forms.Label()
        Me.txtTemBenBankAddress2 = New System.Windows.Forms.TextBox()
        Me.lblBenBankAddress2 = New System.Windows.Forms.Label()
        Me.txtTemBenBankAddress1 = New System.Windows.Forms.TextBox()
        Me.lblBenBankZip = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.cboTemCCY = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboTemPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtTemTemplateName = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cmdDelete = New System.Windows.Forms.Button()
        Me.cmdTemAddCash = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboTemFindCCY = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboTemFindPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboTemFindName = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cboTemRetrieveCCY = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.CboTemRetrieveBen = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CboTemRetrievePortfolio = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BeneficiaryGroupBox.SuspendLayout()
        Me.TabTemBeneficiary.SuspendLayout()
        Me.TabTemMain.SuspendLayout()
        Me.TabTemBankDetails.SuspendLayout()
        CType(Me.PicTemBenIBAN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicTemBenSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BeneficiaryGroupBankGroupBox.SuspendLayout()
        Me.BeneficiarySubBankGroupBox.SuspendLayout()
        CType(Me.PicTemBenSubBankIBAN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicTemBenSubBankSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BeneficiaryBankGroupBox.SuspendLayout()
        CType(Me.PicTemBenBankIBAN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicTemBenBankSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'BeneficiaryGroupBox
        '
        Me.BeneficiaryGroupBox.Controls.Add(Me.TabTemBeneficiary)
        Me.BeneficiaryGroupBox.Location = New System.Drawing.Point(9, 121)
        Me.BeneficiaryGroupBox.Name = "BeneficiaryGroupBox"
        Me.BeneficiaryGroupBox.Size = New System.Drawing.Size(526, 240)
        Me.BeneficiaryGroupBox.TabIndex = 11
        Me.BeneficiaryGroupBox.TabStop = False
        Me.BeneficiaryGroupBox.Text = "Beneficiary Details"
        '
        'TabTemBeneficiary
        '
        Me.TabTemBeneficiary.Controls.Add(Me.TabTemMain)
        Me.TabTemBeneficiary.Controls.Add(Me.TabTemBankDetails)
        Me.TabTemBeneficiary.Location = New System.Drawing.Point(11, 19)
        Me.TabTemBeneficiary.Name = "TabTemBeneficiary"
        Me.TabTemBeneficiary.SelectedIndex = 0
        Me.TabTemBeneficiary.Size = New System.Drawing.Size(509, 211)
        Me.TabTemBeneficiary.TabIndex = 0
        '
        'TabTemMain
        '
        Me.TabTemMain.BackColor = System.Drawing.Color.Linen
        Me.TabTemMain.Controls.Add(Me.txtTemBenName)
        Me.TabTemMain.Controls.Add(Me.cboTemBenType)
        Me.TabTemMain.Controls.Add(Me.lbltembenType)
        Me.TabTemMain.Controls.Add(Me.txttembensurname)
        Me.TabTemMain.Controls.Add(Me.txtTembenmiddlename)
        Me.TabTemMain.Controls.Add(Me.txttembenfirstname)
        Me.TabTemMain.Controls.Add(Me.cbotembenrelationship)
        Me.TabTemMain.Controls.Add(Me.lblTemBenRelationship)
        Me.TabTemMain.Controls.Add(Me.lblTemBenSurname)
        Me.TabTemMain.Controls.Add(Me.lblTemBenMiddlename)
        Me.TabTemMain.Controls.Add(Me.lbltemBenFirstname)
        Me.TabTemMain.Controls.Add(Me.cboTemBenCountry)
        Me.TabTemMain.Controls.Add(Me.lblTemBenCountry)
        Me.TabTemMain.Controls.Add(Me.lblTemBenCounty)
        Me.TabTemMain.Controls.Add(Me.txttembencounty)
        Me.TabTemMain.Controls.Add(Me.txtTemBenZip)
        Me.TabTemMain.Controls.Add(Me.txtTemBenAddress2)
        Me.TabTemMain.Controls.Add(Me.txtTemBenAddress1)
        Me.TabTemMain.Controls.Add(Me.lblTemBenZip)
        Me.TabTemMain.Controls.Add(Me.lblTemBenAddress2)
        Me.TabTemMain.Controls.Add(Me.lblTemBenAddress1)
        Me.TabTemMain.Controls.Add(Me.lblBenName)
        Me.TabTemMain.Location = New System.Drawing.Point(4, 22)
        Me.TabTemMain.Name = "TabTemMain"
        Me.TabTemMain.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTemMain.Size = New System.Drawing.Size(501, 185)
        Me.TabTemMain.TabIndex = 0
        Me.TabTemMain.Text = "Main"
        '
        'txtTemBenName
        '
        Me.txtTemBenName.Location = New System.Drawing.Point(61, 7)
        Me.txtTemBenName.Name = "txtTemBenName"
        Me.txtTemBenName.Size = New System.Drawing.Size(433, 20)
        Me.txtTemBenName.TabIndex = 164
        Me.txtTemBenName.Tag = "ControlAddressBen"
        '
        'cboTemBenType
        '
        Me.cboTemBenType.BackColor = System.Drawing.Color.White
        Me.cboTemBenType.FormattingEnabled = True
        Me.cboTemBenType.Location = New System.Drawing.Point(62, 33)
        Me.cboTemBenType.Name = "cboTemBenType"
        Me.cboTemBenType.Size = New System.Drawing.Size(184, 21)
        Me.cboTemBenType.TabIndex = 162
        Me.cboTemBenType.Tag = "ControlAddressBen"
        '
        'lbltembenType
        '
        Me.lbltembenType.AutoSize = True
        Me.lbltembenType.Location = New System.Drawing.Point(22, 36)
        Me.lbltembenType.Name = "lbltembenType"
        Me.lbltembenType.Size = New System.Drawing.Size(34, 13)
        Me.lbltembenType.TabIndex = 163
        Me.lbltembenType.Text = "Type:"
        '
        'txttembensurname
        '
        Me.txttembensurname.Location = New System.Drawing.Point(62, 82)
        Me.txttembensurname.Name = "txttembensurname"
        Me.txttembensurname.Size = New System.Drawing.Size(434, 20)
        Me.txttembensurname.TabIndex = 159
        Me.txttembensurname.Tag = "ControlAddressBen"
        '
        'txtTembenmiddlename
        '
        Me.txtTembenmiddlename.Location = New System.Drawing.Point(321, 60)
        Me.txtTembenmiddlename.Name = "txtTembenmiddlename"
        Me.txtTembenmiddlename.Size = New System.Drawing.Size(174, 20)
        Me.txtTembenmiddlename.TabIndex = 157
        Me.txtTembenmiddlename.Tag = "ControlAddressBen"
        '
        'txttembenfirstname
        '
        Me.txttembenfirstname.Location = New System.Drawing.Point(62, 58)
        Me.txttembenfirstname.Name = "txttembenfirstname"
        Me.txttembenfirstname.Size = New System.Drawing.Size(184, 20)
        Me.txttembenfirstname.TabIndex = 155
        Me.txttembenfirstname.Tag = "ControlAddressBen"
        '
        'cbotembenrelationship
        '
        Me.cbotembenrelationship.BackColor = System.Drawing.Color.White
        Me.cbotembenrelationship.FormattingEnabled = True
        Me.cbotembenrelationship.Location = New System.Drawing.Point(321, 33)
        Me.cbotembenrelationship.Name = "cbotembenrelationship"
        Me.cbotembenrelationship.Size = New System.Drawing.Size(173, 21)
        Me.cbotembenrelationship.TabIndex = 160
        Me.cbotembenrelationship.Tag = "ControlAddressBen"
        '
        'lblTemBenRelationship
        '
        Me.lblTemBenRelationship.AutoSize = True
        Me.lblTemBenRelationship.Location = New System.Drawing.Point(252, 36)
        Me.lblTemBenRelationship.Name = "lblTemBenRelationship"
        Me.lblTemBenRelationship.Size = New System.Drawing.Size(68, 13)
        Me.lblTemBenRelationship.TabIndex = 161
        Me.lblTemBenRelationship.Text = "Relationship:"
        '
        'lblTemBenSurname
        '
        Me.lblTemBenSurname.AutoSize = True
        Me.lblTemBenSurname.Location = New System.Drawing.Point(4, 87)
        Me.lblTemBenSurname.Name = "lblTemBenSurname"
        Me.lblTemBenSurname.Size = New System.Drawing.Size(52, 13)
        Me.lblTemBenSurname.TabIndex = 158
        Me.lblTemBenSurname.Text = "Surname:"
        '
        'lblTemBenMiddlename
        '
        Me.lblTemBenMiddlename.AutoSize = True
        Me.lblTemBenMiddlename.Location = New System.Drawing.Point(249, 61)
        Me.lblTemBenMiddlename.Name = "lblTemBenMiddlename"
        Me.lblTemBenMiddlename.Size = New System.Drawing.Size(72, 13)
        Me.lblTemBenMiddlename.TabIndex = 156
        Me.lblTemBenMiddlename.Text = "Middle Name:"
        '
        'lbltemBenFirstname
        '
        Me.lbltemBenFirstname.AutoSize = True
        Me.lbltemBenFirstname.Location = New System.Drawing.Point(4, 61)
        Me.lbltemBenFirstname.Name = "lbltemBenFirstname"
        Me.lbltemBenFirstname.Size = New System.Drawing.Size(57, 13)
        Me.lbltemBenFirstname.TabIndex = 154
        Me.lbltemBenFirstname.Text = "First Name"
        '
        'cboTemBenCountry
        '
        Me.cboTemBenCountry.BackColor = System.Drawing.Color.White
        Me.cboTemBenCountry.FormattingEnabled = True
        Me.cboTemBenCountry.Location = New System.Drawing.Point(61, 155)
        Me.cboTemBenCountry.Name = "cboTemBenCountry"
        Me.cboTemBenCountry.Size = New System.Drawing.Size(174, 21)
        Me.cboTemBenCountry.TabIndex = 152
        Me.cboTemBenCountry.Tag = "ControlAddressBen"
        '
        'lblTemBenCountry
        '
        Me.lblTemBenCountry.AutoSize = True
        Me.lblTemBenCountry.Location = New System.Drawing.Point(9, 158)
        Me.lblTemBenCountry.Name = "lblTemBenCountry"
        Me.lblTemBenCountry.Size = New System.Drawing.Size(46, 13)
        Me.lblTemBenCountry.TabIndex = 153
        Me.lblTemBenCountry.Text = "Country:"
        '
        'lblTemBenCounty
        '
        Me.lblTemBenCounty.AutoSize = True
        Me.lblTemBenCounty.Location = New System.Drawing.Point(12, 134)
        Me.lblTemBenCounty.Name = "lblTemBenCounty"
        Me.lblTemBenCounty.Size = New System.Drawing.Size(43, 13)
        Me.lblTemBenCounty.TabIndex = 151
        Me.lblTemBenCounty.Text = "County:"
        '
        'txttembencounty
        '
        Me.txttembencounty.Location = New System.Drawing.Point(61, 131)
        Me.txttembencounty.Name = "txttembencounty"
        Me.txttembencounty.Size = New System.Drawing.Size(173, 20)
        Me.txttembencounty.TabIndex = 150
        Me.txttembencounty.Tag = "ControlAddressBen"
        '
        'txtTemBenZip
        '
        Me.txtTemBenZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenZip.Location = New System.Drawing.Point(321, 131)
        Me.txtTemBenZip.Name = "txtTemBenZip"
        Me.txtTemBenZip.Size = New System.Drawing.Size(78, 20)
        Me.txtTemBenZip.TabIndex = 95
        Me.txtTemBenZip.Tag = ""
        '
        'txtTemBenAddress2
        '
        Me.txtTemBenAddress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenAddress2.Location = New System.Drawing.Point(321, 107)
        Me.txtTemBenAddress2.Name = "txtTemBenAddress2"
        Me.txtTemBenAddress2.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenAddress2.TabIndex = 94
        Me.txtTemBenAddress2.Tag = ""
        '
        'txtTemBenAddress1
        '
        Me.txtTemBenAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenAddress1.Location = New System.Drawing.Point(62, 107)
        Me.txtTemBenAddress1.Name = "txtTemBenAddress1"
        Me.txtTemBenAddress1.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenAddress1.TabIndex = 93
        Me.txtTemBenAddress1.Tag = ""
        '
        'lblTemBenZip
        '
        Me.lblTemBenZip.AutoSize = True
        Me.lblTemBenZip.Location = New System.Drawing.Point(264, 134)
        Me.lblTemBenZip.Name = "lblTemBenZip"
        Me.lblTemBenZip.Size = New System.Drawing.Size(51, 13)
        Me.lblTemBenZip.TabIndex = 105
        Me.lblTemBenZip.Text = "Post/Zip:"
        '
        'lblTemBenAddress2
        '
        Me.lblTemBenAddress2.AutoSize = True
        Me.lblTemBenAddress2.Location = New System.Drawing.Point(258, 110)
        Me.lblTemBenAddress2.Name = "lblTemBenAddress2"
        Me.lblTemBenAddress2.Size = New System.Drawing.Size(27, 13)
        Me.lblTemBenAddress2.TabIndex = 104
        Me.lblTemBenAddress2.Text = "City:"
        '
        'lblTemBenAddress1
        '
        Me.lblTemBenAddress1.AutoSize = True
        Me.lblTemBenAddress1.Location = New System.Drawing.Point(3, 111)
        Me.lblTemBenAddress1.Name = "lblTemBenAddress1"
        Me.lblTemBenAddress1.Size = New System.Drawing.Size(57, 13)
        Me.lblTemBenAddress1.TabIndex = 103
        Me.lblTemBenAddress1.Text = "Address 1:"
        '
        'lblBenName
        '
        Me.lblBenName.AutoSize = True
        Me.lblBenName.Location = New System.Drawing.Point(1, 9)
        Me.lblBenName.Name = "lblBenName"
        Me.lblBenName.Size = New System.Drawing.Size(59, 13)
        Me.lblBenName.TabIndex = 99
        Me.lblBenName.Text = "Bus Name:"
        '
        'TabTemBankDetails
        '
        Me.TabTemBankDetails.BackColor = System.Drawing.Color.Linen
        Me.TabTemBankDetails.Controls.Add(Me.lblbenBIK)
        Me.TabTemBankDetails.Controls.Add(Me.txtTemBenBIK)
        Me.TabTemBankDetails.Controls.Add(Me.txtTemBenINN)
        Me.TabTemBankDetails.Controls.Add(Me.lblbenINN)
        Me.TabTemBankDetails.Controls.Add(Me.PicTemBenIBAN)
        Me.TabTemBankDetails.Controls.Add(Me.PicTemBenSwift)
        Me.TabTemBankDetails.Controls.Add(Me.txtTemBenOther1)
        Me.TabTemBankDetails.Controls.Add(Me.txtTemBenIBAN)
        Me.TabTemBankDetails.Controls.Add(Me.txtTemBenSwift)
        Me.TabTemBankDetails.Controls.Add(Me.lblBenOther1)
        Me.TabTemBankDetails.Controls.Add(Me.lblBenIBAN)
        Me.TabTemBankDetails.Controls.Add(Me.lblBenSwift)
        Me.TabTemBankDetails.Location = New System.Drawing.Point(4, 22)
        Me.TabTemBankDetails.Name = "TabTemBankDetails"
        Me.TabTemBankDetails.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTemBankDetails.Size = New System.Drawing.Size(501, 185)
        Me.TabTemBankDetails.TabIndex = 1
        Me.TabTemBankDetails.Text = "Bank Details"
        '
        'lblbenBIK
        '
        Me.lblbenBIK.AutoSize = True
        Me.lblbenBIK.Location = New System.Drawing.Point(6, 99)
        Me.lblbenBIK.Name = "lblbenBIK"
        Me.lblbenBIK.Size = New System.Drawing.Size(51, 13)
        Me.lblbenBIK.TabIndex = 122
        Me.lblbenBIK.Text = "BIK.ACC:"
        '
        'txtTemBenBIK
        '
        Me.txtTemBenBIK.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBIK.Location = New System.Drawing.Point(59, 96)
        Me.txtTemBenBIK.Name = "txtTemBenBIK"
        Me.txtTemBenBIK.Size = New System.Drawing.Size(187, 20)
        Me.txtTemBenBIK.TabIndex = 121
        Me.txtTemBenBIK.Tag = ""
        '
        'txtTemBenINN
        '
        Me.txtTemBenINN.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenINN.Location = New System.Drawing.Point(286, 96)
        Me.txtTemBenINN.Name = "txtTemBenINN"
        Me.txtTemBenINN.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenINN.TabIndex = 120
        Me.txtTemBenINN.Tag = ""
        '
        'lblbenINN
        '
        Me.lblbenINN.AutoSize = True
        Me.lblbenINN.Location = New System.Drawing.Point(252, 99)
        Me.lblbenINN.Name = "lblbenINN"
        Me.lblbenINN.Size = New System.Drawing.Size(29, 13)
        Me.lblbenINN.TabIndex = 119
        Me.lblbenINN.Text = "INN:"
        '
        'PicTemBenIBAN
        '
        Me.PicTemBenIBAN.Location = New System.Drawing.Point(465, 46)
        Me.PicTemBenIBAN.Name = "PicTemBenIBAN"
        Me.PicTemBenIBAN.Size = New System.Drawing.Size(23, 17)
        Me.PicTemBenIBAN.TabIndex = 118
        Me.PicTemBenIBAN.TabStop = False
        '
        'PicTemBenSwift
        '
        Me.PicTemBenSwift.Location = New System.Drawing.Point(465, 23)
        Me.PicTemBenSwift.Name = "PicTemBenSwift"
        Me.PicTemBenSwift.Size = New System.Drawing.Size(23, 17)
        Me.PicTemBenSwift.TabIndex = 117
        Me.PicTemBenSwift.TabStop = False
        '
        'txtTemBenOther1
        '
        Me.txtTemBenOther1.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenOther1.Location = New System.Drawing.Point(286, 70)
        Me.txtTemBenOther1.Name = "txtTemBenOther1"
        Me.txtTemBenOther1.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenOther1.TabIndex = 113
        '
        'txtTemBenIBAN
        '
        Me.txtTemBenIBAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenIBAN.Location = New System.Drawing.Point(286, 45)
        Me.txtTemBenIBAN.Name = "txtTemBenIBAN"
        Me.txtTemBenIBAN.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenIBAN.TabIndex = 112
        Me.txtTemBenIBAN.Tag = ""
        '
        'txtTemBenSwift
        '
        Me.txtTemBenSwift.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSwift.Location = New System.Drawing.Point(286, 20)
        Me.txtTemBenSwift.Name = "txtTemBenSwift"
        Me.txtTemBenSwift.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenSwift.TabIndex = 111
        Me.txtTemBenSwift.Tag = ""
        '
        'lblBenOther1
        '
        Me.lblBenOther1.AutoSize = True
        Me.lblBenOther1.Location = New System.Drawing.Point(244, 73)
        Me.lblBenOther1.Name = "lblBenOther1"
        Me.lblBenOther1.Size = New System.Drawing.Size(36, 13)
        Me.lblBenOther1.TabIndex = 116
        Me.lblBenOther1.Text = "Other:"
        '
        'lblBenIBAN
        '
        Me.lblBenIBAN.AutoSize = True
        Me.lblBenIBAN.Location = New System.Drawing.Point(245, 48)
        Me.lblBenIBAN.Name = "lblBenIBAN"
        Me.lblBenIBAN.Size = New System.Drawing.Size(35, 13)
        Me.lblBenIBAN.TabIndex = 115
        Me.lblBenIBAN.Text = "IBAN:"
        '
        'lblBenSwift
        '
        Me.lblBenSwift.AutoSize = True
        Me.lblBenSwift.Location = New System.Drawing.Point(245, 23)
        Me.lblBenSwift.Name = "lblBenSwift"
        Me.lblBenSwift.Size = New System.Drawing.Size(33, 13)
        Me.lblBenSwift.TabIndex = 114
        Me.lblBenSwift.Text = "Swift:"
        '
        'BeneficiaryGroupBankGroupBox
        '
        Me.BeneficiaryGroupBankGroupBox.Controls.Add(Me.BeneficiarySubBankGroupBox)
        Me.BeneficiaryGroupBankGroupBox.Controls.Add(Me.BeneficiaryBankGroupBox)
        Me.BeneficiaryGroupBankGroupBox.Location = New System.Drawing.Point(541, 18)
        Me.BeneficiaryGroupBankGroupBox.Name = "BeneficiaryGroupBankGroupBox"
        Me.BeneficiaryGroupBankGroupBox.Size = New System.Drawing.Size(510, 343)
        Me.BeneficiaryGroupBankGroupBox.TabIndex = 12
        Me.BeneficiaryGroupBankGroupBox.TabStop = False
        Me.BeneficiaryGroupBankGroupBox.Text = "Beneficiary Bank Details"
        '
        'BeneficiarySubBankGroupBox
        '
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.cboTemBenSubBankCountry)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtTemBenSubBankINN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtTemBenSubBankBIK)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.lblBenSubBankBIK)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.lblBenSubBankINN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.ChkTemBenSubBank)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtTemBenSubBankOther1)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.PicTemBenSubBankIBAN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.PicTemBenSubBankSwift)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.CboTemBenSubBankName)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label9)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtTemBenSubBankIBAN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label11)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtTemBenSubBankSwift)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label12)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtTemBenSubBankZip)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label13)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtTemBenSubBankAddress2)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label14)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtTemBenSubBankAddress1)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label15)
        Me.BeneficiarySubBankGroupBox.Location = New System.Drawing.Point(6, 19)
        Me.BeneficiarySubBankGroupBox.Name = "BeneficiarySubBankGroupBox"
        Me.BeneficiarySubBankGroupBox.Size = New System.Drawing.Size(498, 150)
        Me.BeneficiarySubBankGroupBox.TabIndex = 67
        Me.BeneficiarySubBankGroupBox.TabStop = False
        Me.BeneficiarySubBankGroupBox.Text = "Intermediary Institution (56A)"
        '
        'cboTemBenSubBankCountry
        '
        Me.cboTemBenSubBankCountry.BackColor = System.Drawing.Color.White
        Me.cboTemBenSubBankCountry.Enabled = False
        Me.cboTemBenSubBankCountry.FormattingEnabled = True
        Me.cboTemBenSubBankCountry.Location = New System.Drawing.Point(103, 94)
        Me.cboTemBenSubBankCountry.Name = "cboTemBenSubBankCountry"
        Me.cboTemBenSubBankCountry.Size = New System.Drawing.Size(144, 21)
        Me.cboTemBenSubBankCountry.TabIndex = 150
        Me.cboTemBenSubBankCountry.Tag = "ControlAddressIntermediary"
        '
        'txtTemBenSubBankINN
        '
        Me.txtTemBenSubBankINN.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSubBankINN.Enabled = False
        Me.txtTemBenSubBankINN.Location = New System.Drawing.Point(290, 120)
        Me.txtTemBenSubBankINN.Name = "txtTemBenSubBankINN"
        Me.txtTemBenSubBankINN.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenSubBankINN.TabIndex = 75
        Me.txtTemBenSubBankINN.Tag = ""
        '
        'txtTemBenSubBankBIK
        '
        Me.txtTemBenSubBankBIK.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSubBankBIK.Enabled = False
        Me.txtTemBenSubBankBIK.Location = New System.Drawing.Point(65, 120)
        Me.txtTemBenSubBankBIK.Name = "txtTemBenSubBankBIK"
        Me.txtTemBenSubBankBIK.Size = New System.Drawing.Size(183, 20)
        Me.txtTemBenSubBankBIK.TabIndex = 73
        Me.txtTemBenSubBankBIK.Tag = ""
        '
        'lblBenSubBankBIK
        '
        Me.lblBenSubBankBIK.AutoSize = True
        Me.lblBenSubBankBIK.Location = New System.Drawing.Point(8, 123)
        Me.lblBenSubBankBIK.Name = "lblBenSubBankBIK"
        Me.lblBenSubBankBIK.Size = New System.Drawing.Size(51, 13)
        Me.lblBenSubBankBIK.TabIndex = 74
        Me.lblBenSubBankBIK.Text = "BIK.ACC:"
        '
        'lblBenSubBankINN
        '
        Me.lblBenSubBankINN.AutoSize = True
        Me.lblBenSubBankINN.Location = New System.Drawing.Point(256, 123)
        Me.lblBenSubBankINN.Name = "lblBenSubBankINN"
        Me.lblBenSubBankINN.Size = New System.Drawing.Size(29, 13)
        Me.lblBenSubBankINN.TabIndex = 72
        Me.lblBenSubBankINN.Text = "INN:"
        '
        'ChkTemBenSubBank
        '
        Me.ChkTemBenSubBank.AutoSize = True
        Me.ChkTemBenSubBank.Location = New System.Drawing.Point(24, 19)
        Me.ChkTemBenSubBank.Name = "ChkTemBenSubBank"
        Me.ChkTemBenSubBank.Size = New System.Drawing.Size(166, 17)
        Me.ChkTemBenSubBank.TabIndex = 66
        Me.ChkTemBenSubBank.Text = "Tick to add intermediary bank"
        Me.ChkTemBenSubBank.UseVisualStyleBackColor = True
        '
        'txtTemBenSubBankOther1
        '
        Me.txtTemBenSubBankOther1.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSubBankOther1.Enabled = False
        Me.txtTemBenSubBankOther1.Location = New System.Drawing.Point(290, 94)
        Me.txtTemBenSubBankOther1.Name = "txtTemBenSubBankOther1"
        Me.txtTemBenSubBankOther1.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenSubBankOther1.TabIndex = 31
        Me.txtTemBenSubBankOther1.Tag = ""
        '
        'PicTemBenSubBankIBAN
        '
        Me.PicTemBenSubBankIBAN.Location = New System.Drawing.Point(465, 70)
        Me.PicTemBenSubBankIBAN.Name = "PicTemBenSubBankIBAN"
        Me.PicTemBenSubBankIBAN.Size = New System.Drawing.Size(23, 17)
        Me.PicTemBenSubBankIBAN.TabIndex = 65
        Me.PicTemBenSubBankIBAN.TabStop = False
        '
        'PicTemBenSubBankSwift
        '
        Me.PicTemBenSubBankSwift.Location = New System.Drawing.Point(465, 47)
        Me.PicTemBenSubBankSwift.Name = "PicTemBenSubBankSwift"
        Me.PicTemBenSubBankSwift.Size = New System.Drawing.Size(23, 17)
        Me.PicTemBenSubBankSwift.TabIndex = 64
        Me.PicTemBenSubBankSwift.TabStop = False
        '
        'CboTemBenSubBankName
        '
        Me.CboTemBenSubBankName.Enabled = False
        Me.CboTemBenSubBankName.FormattingEnabled = True
        Me.CboTemBenSubBankName.Location = New System.Drawing.Point(193, 17)
        Me.CboTemBenSubBankName.Name = "CboTemBenSubBankName"
        Me.CboTemBenSubBankName.Size = New System.Drawing.Size(270, 21)
        Me.CboTemBenSubBankName.TabIndex = 25
        Me.CboTemBenSubBankName.Tag = ""
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(251, 47)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(33, 13)
        Me.Label9.TabIndex = 58
        Me.Label9.Text = "Swift:"
        '
        'txtTemBenSubBankIBAN
        '
        Me.txtTemBenSubBankIBAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSubBankIBAN.Enabled = False
        Me.txtTemBenSubBankIBAN.Location = New System.Drawing.Point(290, 68)
        Me.txtTemBenSubBankIBAN.Name = "txtTemBenSubBankIBAN"
        Me.txtTemBenSubBankIBAN.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenSubBankIBAN.TabIndex = 30
        Me.txtTemBenSubBankIBAN.Tag = ""
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(249, 71)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 13)
        Me.Label11.TabIndex = 59
        Me.Label11.Text = "IBAN:"
        '
        'txtTemBenSubBankSwift
        '
        Me.txtTemBenSubBankSwift.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSubBankSwift.Enabled = False
        Me.txtTemBenSubBankSwift.Location = New System.Drawing.Point(290, 44)
        Me.txtTemBenSubBankSwift.Name = "txtTemBenSubBankSwift"
        Me.txtTemBenSubBankSwift.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenSubBankSwift.TabIndex = 29
        Me.txtTemBenSubBankSwift.Tag = ""
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(249, 97)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(36, 13)
        Me.Label12.TabIndex = 60
        Me.Label12.Text = "Other:"
        '
        'txtTemBenSubBankZip
        '
        Me.txtTemBenSubBankZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSubBankZip.Enabled = False
        Me.txtTemBenSubBankZip.Location = New System.Drawing.Point(38, 94)
        Me.txtTemBenSubBankZip.Name = "txtTemBenSubBankZip"
        Me.txtTemBenSubBankZip.Size = New System.Drawing.Size(59, 20)
        Me.txtTemBenSubBankZip.TabIndex = 28
        Me.txtTemBenSubBankZip.Tag = ""
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(8, 47)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 13)
        Me.Label13.TabIndex = 61
        Me.Label13.Text = "Add:"
        '
        'txtTemBenSubBankAddress2
        '
        Me.txtTemBenSubBankAddress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSubBankAddress2.Enabled = False
        Me.txtTemBenSubBankAddress2.Location = New System.Drawing.Point(38, 68)
        Me.txtTemBenSubBankAddress2.Name = "txtTemBenSubBankAddress2"
        Me.txtTemBenSubBankAddress2.Size = New System.Drawing.Size(209, 20)
        Me.txtTemBenSubBankAddress2.TabIndex = 27
        Me.txtTemBenSubBankAddress2.Tag = ""
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 71)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(27, 13)
        Me.Label14.TabIndex = 62
        Me.Label14.Text = "City:"
        '
        'txtTemBenSubBankAddress1
        '
        Me.txtTemBenSubBankAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenSubBankAddress1.Enabled = False
        Me.txtTemBenSubBankAddress1.Location = New System.Drawing.Point(38, 44)
        Me.txtTemBenSubBankAddress1.Name = "txtTemBenSubBankAddress1"
        Me.txtTemBenSubBankAddress1.Size = New System.Drawing.Size(209, 20)
        Me.txtTemBenSubBankAddress1.TabIndex = 26
        Me.txtTemBenSubBankAddress1.Tag = ""
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(7, 97)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(25, 13)
        Me.Label15.TabIndex = 63
        Me.Label15.Text = "Zip:"
        '
        'BeneficiaryBankGroupBox
        '
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.cboTemBenBankCountry)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtTemBenBankINN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtTemBenBankBIK)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankBIK)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankINN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtTemBenBankOther1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.PicTemBenBankIBAN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankName)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.PicTemBenBankSwift)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.CboTemBenBankName)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankSwift)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtTemBenBankIBAN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankIBAN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtTemBenBankSwift)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankOther1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtTemBenBankZip)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankAddress1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtTemBenBankAddress2)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankAddress2)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtTemBenBankAddress1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankZip)
        Me.BeneficiaryBankGroupBox.Location = New System.Drawing.Point(6, 179)
        Me.BeneficiaryBankGroupBox.Name = "BeneficiaryBankGroupBox"
        Me.BeneficiaryBankGroupBox.Size = New System.Drawing.Size(498, 154)
        Me.BeneficiaryBankGroupBox.TabIndex = 66
        Me.BeneficiaryBankGroupBox.TabStop = False
        Me.BeneficiaryBankGroupBox.Text = "Account With Institution (57A)"
        '
        'cboTemBenBankCountry
        '
        Me.cboTemBenBankCountry.BackColor = System.Drawing.Color.White
        Me.cboTemBenBankCountry.FormattingEnabled = True
        Me.cboTemBenBankCountry.Location = New System.Drawing.Point(99, 95)
        Me.cboTemBenBankCountry.Name = "cboTemBenBankCountry"
        Me.cboTemBenBankCountry.Size = New System.Drawing.Size(146, 21)
        Me.cboTemBenBankCountry.TabIndex = 151
        Me.cboTemBenBankCountry.Tag = "ControlAddressBenBank"
        '
        'txtTemBenBankINN
        '
        Me.txtTemBenBankINN.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBankINN.Location = New System.Drawing.Point(290, 123)
        Me.txtTemBenBankINN.Name = "txtTemBenBankINN"
        Me.txtTemBenBankINN.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenBankINN.TabIndex = 79
        Me.txtTemBenBankINN.Tag = ""
        '
        'txtTemBenBankBIK
        '
        Me.txtTemBenBankBIK.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBankBIK.Location = New System.Drawing.Point(65, 122)
        Me.txtTemBenBankBIK.Name = "txtTemBenBankBIK"
        Me.txtTemBenBankBIK.Size = New System.Drawing.Size(180, 20)
        Me.txtTemBenBankBIK.TabIndex = 77
        Me.txtTemBenBankBIK.Tag = ""
        '
        'lblBenBankBIK
        '
        Me.lblBenBankBIK.AutoSize = True
        Me.lblBenBankBIK.Location = New System.Drawing.Point(8, 126)
        Me.lblBenBankBIK.Name = "lblBenBankBIK"
        Me.lblBenBankBIK.Size = New System.Drawing.Size(51, 13)
        Me.lblBenBankBIK.TabIndex = 78
        Me.lblBenBankBIK.Text = "BIK.ACC:"
        '
        'lblBenBankINN
        '
        Me.lblBenBankINN.AutoSize = True
        Me.lblBenBankINN.Location = New System.Drawing.Point(256, 126)
        Me.lblBenBankINN.Name = "lblBenBankINN"
        Me.lblBenBankINN.Size = New System.Drawing.Size(29, 13)
        Me.lblBenBankINN.TabIndex = 76
        Me.lblBenBankINN.Text = "INN:"
        '
        'txtTemBenBankOther1
        '
        Me.txtTemBenBankOther1.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBankOther1.Location = New System.Drawing.Point(290, 96)
        Me.txtTemBenBankOther1.Name = "txtTemBenBankOther1"
        Me.txtTemBenBankOther1.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenBankOther1.TabIndex = 31
        '
        'PicTemBenBankIBAN
        '
        Me.PicTemBenBankIBAN.Location = New System.Drawing.Point(465, 72)
        Me.PicTemBenBankIBAN.Name = "PicTemBenBankIBAN"
        Me.PicTemBenBankIBAN.Size = New System.Drawing.Size(23, 17)
        Me.PicTemBenBankIBAN.TabIndex = 65
        Me.PicTemBenBankIBAN.TabStop = False
        '
        'lblBenBankName
        '
        Me.lblBenBankName.AutoSize = True
        Me.lblBenBankName.Location = New System.Drawing.Point(76, 22)
        Me.lblBenBankName.Name = "lblBenBankName"
        Me.lblBenBankName.Size = New System.Drawing.Size(109, 13)
        Me.lblBenBankName.TabIndex = 34
        Me.lblBenBankName.Text = "Account Bank Name:"
        '
        'PicTemBenBankSwift
        '
        Me.PicTemBenBankSwift.Location = New System.Drawing.Point(465, 49)
        Me.PicTemBenBankSwift.Name = "PicTemBenBankSwift"
        Me.PicTemBenBankSwift.Size = New System.Drawing.Size(23, 17)
        Me.PicTemBenBankSwift.TabIndex = 64
        Me.PicTemBenBankSwift.TabStop = False
        '
        'CboTemBenBankName
        '
        Me.CboTemBenBankName.FormattingEnabled = True
        Me.CboTemBenBankName.Location = New System.Drawing.Point(200, 19)
        Me.CboTemBenBankName.Name = "CboTemBenBankName"
        Me.CboTemBenBankName.Size = New System.Drawing.Size(263, 21)
        Me.CboTemBenBankName.TabIndex = 25
        '
        'lblBenBankSwift
        '
        Me.lblBenBankSwift.AutoSize = True
        Me.lblBenBankSwift.Location = New System.Drawing.Point(251, 49)
        Me.lblBenBankSwift.Name = "lblBenBankSwift"
        Me.lblBenBankSwift.Size = New System.Drawing.Size(33, 13)
        Me.lblBenBankSwift.TabIndex = 58
        Me.lblBenBankSwift.Text = "Swift:"
        '
        'txtTemBenBankIBAN
        '
        Me.txtTemBenBankIBAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBankIBAN.Location = New System.Drawing.Point(290, 70)
        Me.txtTemBenBankIBAN.Name = "txtTemBenBankIBAN"
        Me.txtTemBenBankIBAN.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenBankIBAN.TabIndex = 30
        '
        'lblBenBankIBAN
        '
        Me.lblBenBankIBAN.AutoSize = True
        Me.lblBenBankIBAN.Location = New System.Drawing.Point(249, 73)
        Me.lblBenBankIBAN.Name = "lblBenBankIBAN"
        Me.lblBenBankIBAN.Size = New System.Drawing.Size(35, 13)
        Me.lblBenBankIBAN.TabIndex = 59
        Me.lblBenBankIBAN.Text = "IBAN:"
        '
        'txtTemBenBankSwift
        '
        Me.txtTemBenBankSwift.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBankSwift.Location = New System.Drawing.Point(290, 46)
        Me.txtTemBenBankSwift.Name = "txtTemBenBankSwift"
        Me.txtTemBenBankSwift.Size = New System.Drawing.Size(173, 20)
        Me.txtTemBenBankSwift.TabIndex = 29
        '
        'lblBenBankOther1
        '
        Me.lblBenBankOther1.AutoSize = True
        Me.lblBenBankOther1.Location = New System.Drawing.Point(248, 100)
        Me.lblBenBankOther1.Name = "lblBenBankOther1"
        Me.lblBenBankOther1.Size = New System.Drawing.Size(36, 13)
        Me.lblBenBankOther1.TabIndex = 60
        Me.lblBenBankOther1.Text = "Other:"
        '
        'txtTemBenBankZip
        '
        Me.txtTemBenBankZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBankZip.Location = New System.Drawing.Point(38, 95)
        Me.txtTemBenBankZip.Name = "txtTemBenBankZip"
        Me.txtTemBenBankZip.Size = New System.Drawing.Size(59, 20)
        Me.txtTemBenBankZip.TabIndex = 28
        '
        'lblBenBankAddress1
        '
        Me.lblBenBankAddress1.AutoSize = True
        Me.lblBenBankAddress1.Location = New System.Drawing.Point(7, 49)
        Me.lblBenBankAddress1.Name = "lblBenBankAddress1"
        Me.lblBenBankAddress1.Size = New System.Drawing.Size(29, 13)
        Me.lblBenBankAddress1.TabIndex = 61
        Me.lblBenBankAddress1.Text = "Add:"
        '
        'txtTemBenBankAddress2
        '
        Me.txtTemBenBankAddress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBankAddress2.Location = New System.Drawing.Point(38, 70)
        Me.txtTemBenBankAddress2.Name = "txtTemBenBankAddress2"
        Me.txtTemBenBankAddress2.Size = New System.Drawing.Size(207, 20)
        Me.txtTemBenBankAddress2.TabIndex = 27
        '
        'lblBenBankAddress2
        '
        Me.lblBenBankAddress2.AutoSize = True
        Me.lblBenBankAddress2.Location = New System.Drawing.Point(7, 73)
        Me.lblBenBankAddress2.Name = "lblBenBankAddress2"
        Me.lblBenBankAddress2.Size = New System.Drawing.Size(27, 13)
        Me.lblBenBankAddress2.TabIndex = 62
        Me.lblBenBankAddress2.Text = "City:"
        '
        'txtTemBenBankAddress1
        '
        Me.txtTemBenBankAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtTemBenBankAddress1.Location = New System.Drawing.Point(38, 45)
        Me.txtTemBenBankAddress1.Name = "txtTemBenBankAddress1"
        Me.txtTemBenBankAddress1.Size = New System.Drawing.Size(207, 20)
        Me.txtTemBenBankAddress1.TabIndex = 26
        '
        'lblBenBankZip
        '
        Me.lblBenBankZip.AutoSize = True
        Me.lblBenBankZip.Location = New System.Drawing.Point(7, 98)
        Me.lblBenBankZip.Name = "lblBenBankZip"
        Me.lblBenBankZip.Size = New System.Drawing.Size(25, 13)
        Me.lblBenBankZip.TabIndex = 63
        Me.lblBenBankZip.Text = "Zip:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox5)
        Me.GroupBox1.Controls.Add(Me.cmdDelete)
        Me.GroupBox1.Controls.Add(Me.cmdTemAddCash)
        Me.GroupBox1.Controls.Add(Me.BeneficiaryGroupBox)
        Me.GroupBox1.Controls.Add(Me.BeneficiaryGroupBankGroupBox)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(16, 150)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1057, 403)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Template Details"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.cboTemCCY)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Controls.Add(Me.cboTemPortfolio)
        Me.GroupBox5.Controls.Add(Me.Label5)
        Me.GroupBox5.Controls.Add(Me.txtTemTemplateName)
        Me.GroupBox5.Controls.Add(Me.Label17)
        Me.GroupBox5.Location = New System.Drawing.Point(9, 19)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(526, 90)
        Me.GroupBox5.TabIndex = 43
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Template"
        '
        'cboTemCCY
        '
        Me.cboTemCCY.BackColor = System.Drawing.Color.White
        Me.cboTemCCY.FormattingEnabled = True
        Me.cboTemCCY.Location = New System.Drawing.Point(441, 54)
        Me.cboTemCCY.Name = "cboTemCCY"
        Me.cboTemCCY.Size = New System.Drawing.Size(68, 21)
        Me.cboTemCCY.TabIndex = 47
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(404, 57)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 13)
        Me.Label6.TabIndex = 48
        Me.Label6.Text = "CCY:"
        '
        'cboTemPortfolio
        '
        Me.cboTemPortfolio.BackColor = System.Drawing.Color.White
        Me.cboTemPortfolio.FormattingEnabled = True
        Me.cboTemPortfolio.Location = New System.Drawing.Point(73, 54)
        Me.cboTemPortfolio.Name = "cboTemPortfolio"
        Me.cboTemPortfolio.Size = New System.Drawing.Size(316, 21)
        Me.cboTemPortfolio.TabIndex = 45
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(19, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 46
        Me.Label5.Text = "Portfolio:"
        '
        'txtTemTemplateName
        '
        Me.txtTemTemplateName.Location = New System.Drawing.Point(106, 24)
        Me.txtTemTemplateName.Name = "txtTemTemplateName"
        Me.txtTemTemplateName.Size = New System.Drawing.Size(406, 20)
        Me.txtTemTemplateName.TabIndex = 44
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(15, 27)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(85, 13)
        Me.Label17.TabIndex = 43
        Me.Label17.Text = "Template Name:"
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.Location = New System.Drawing.Point(9, 367)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(49, 24)
        Me.cmdDelete.TabIndex = 39
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'cmdTemAddCash
        '
        Me.cmdTemAddCash.Location = New System.Drawing.Point(837, 367)
        Me.cmdTemAddCash.Name = "cmdTemAddCash"
        Me.cmdTemAddCash.Size = New System.Drawing.Size(210, 24)
        Me.cmdTemAddCash.TabIndex = 35
        Me.cmdTemAddCash.Text = "Save Template"
        Me.cmdTemAddCash.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboTemFindCCY)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.cboTemFindPortfolio)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.cboTemFindName)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(16, 56)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(518, 88)
        Me.GroupBox2.TabIndex = 36
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Template Criteria Selection"
        '
        'cboTemFindCCY
        '
        Me.cboTemFindCCY.BackColor = System.Drawing.Color.LightYellow
        Me.cboTemFindCCY.FormattingEnabled = True
        Me.cboTemFindCCY.Location = New System.Drawing.Point(435, 22)
        Me.cboTemFindCCY.Name = "cboTemFindCCY"
        Me.cboTemFindCCY.Size = New System.Drawing.Size(68, 21)
        Me.cboTemFindCCY.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(398, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 13)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "CCY:"
        '
        'cboTemFindPortfolio
        '
        Me.cboTemFindPortfolio.BackColor = System.Drawing.Color.LightYellow
        Me.cboTemFindPortfolio.FormattingEnabled = True
        Me.cboTemFindPortfolio.Location = New System.Drawing.Point(67, 22)
        Me.cboTemFindPortfolio.Name = "cboTemFindPortfolio"
        Me.cboTemFindPortfolio.Size = New System.Drawing.Size(316, 21)
        Me.cboTemFindPortfolio.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 25)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 13)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "Portfolio:"
        '
        'cboTemFindName
        '
        Me.cboTemFindName.BackColor = System.Drawing.Color.LightYellow
        Me.cboTemFindName.FormattingEnabled = True
        Me.cboTemFindName.Location = New System.Drawing.Point(99, 50)
        Me.cboTemFindName.Name = "cboTemFindName"
        Me.cboTemFindName.Size = New System.Drawing.Size(404, 21)
        Me.cboTemFindName.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Template Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(12, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(224, 24)
        Me.Label2.TabIndex = 37
        Me.Label2.Text = "Template Management"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "button_cancel.png")
        Me.ImageList1.Images.SetKeyName(1, "tick (1).png")
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboTemRetrieveCCY)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.CboTemRetrieveBen)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.CboTemRetrievePortfolio)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Location = New System.Drawing.Point(569, 56)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(494, 88)
        Me.GroupBox3.TabIndex = 38
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Template Retrieval"
        '
        'cboTemRetrieveCCY
        '
        Me.cboTemRetrieveCCY.BackColor = System.Drawing.Color.LightBlue
        Me.cboTemRetrieveCCY.FormattingEnabled = True
        Me.cboTemRetrieveCCY.Location = New System.Drawing.Point(413, 23)
        Me.cboTemRetrieveCCY.Name = "cboTemRetrieveCCY"
        Me.cboTemRetrieveCCY.Size = New System.Drawing.Size(68, 21)
        Me.cboTemRetrieveCCY.TabIndex = 53
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(376, 26)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(31, 13)
        Me.Label16.TabIndex = 54
        Me.Label16.Text = "CCY:"
        '
        'CboTemRetrieveBen
        '
        Me.CboTemRetrieveBen.BackColor = System.Drawing.Color.LightBlue
        Me.CboTemRetrieveBen.FormattingEnabled = True
        Me.CboTemRetrieveBen.Location = New System.Drawing.Point(74, 50)
        Me.CboTemRetrieveBen.Name = "CboTemRetrieveBen"
        Me.CboTemRetrieveBen.Size = New System.Drawing.Size(407, 21)
        Me.CboTemRetrieveBen.TabIndex = 33
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Beneficiary:"
        '
        'CboTemRetrievePortfolio
        '
        Me.CboTemRetrievePortfolio.BackColor = System.Drawing.Color.LightBlue
        Me.CboTemRetrievePortfolio.FormattingEnabled = True
        Me.CboTemRetrievePortfolio.Location = New System.Drawing.Point(74, 23)
        Me.CboTemRetrievePortfolio.Name = "CboTemRetrievePortfolio"
        Me.CboTemRetrievePortfolio.Size = New System.Drawing.Size(296, 21)
        Me.CboTemRetrievePortfolio.TabIndex = 31
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(21, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "Portfolio:"
        '
        'FrmTemplate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(1079, 559)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmTemplate"
        Me.Text = "Templates"
        Me.BeneficiaryGroupBox.ResumeLayout(False)
        Me.TabTemBeneficiary.ResumeLayout(False)
        Me.TabTemMain.ResumeLayout(False)
        Me.TabTemMain.PerformLayout()
        Me.TabTemBankDetails.ResumeLayout(False)
        Me.TabTemBankDetails.PerformLayout()
        CType(Me.PicTemBenIBAN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicTemBenSwift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BeneficiaryGroupBankGroupBox.ResumeLayout(False)
        Me.BeneficiarySubBankGroupBox.ResumeLayout(False)
        Me.BeneficiarySubBankGroupBox.PerformLayout()
        CType(Me.PicTemBenSubBankIBAN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicTemBenSubBankSwift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BeneficiaryBankGroupBox.ResumeLayout(False)
        Me.BeneficiaryBankGroupBox.PerformLayout()
        CType(Me.PicTemBenBankIBAN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicTemBenBankSwift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BeneficiaryGroupBox As GroupBox
    Friend WithEvents BeneficiaryGroupBankGroupBox As GroupBox
    Friend WithEvents BeneficiarySubBankGroupBox As GroupBox
    Friend WithEvents ChkTemBenSubBank As CheckBox
    Friend WithEvents txtTemBenSubBankOther1 As TextBox
    Friend WithEvents PicTemBenSubBankIBAN As PictureBox
    Friend WithEvents PicTemBenSubBankSwift As PictureBox
    Friend WithEvents CboTemBenSubBankName As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtTemBenSubBankIBAN As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtTemBenSubBankSwift As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents txtTemBenSubBankZip As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtTemBenSubBankAddress2 As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtTemBenSubBankAddress1 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents BeneficiaryBankGroupBox As GroupBox
    Friend WithEvents txtTemBenBankOther1 As TextBox
    Friend WithEvents PicTemBenBankIBAN As PictureBox
    Friend WithEvents lblBenBankName As Label
    Friend WithEvents PicTemBenBankSwift As PictureBox
    Friend WithEvents CboTemBenBankName As ComboBox
    Friend WithEvents lblBenBankSwift As Label
    Friend WithEvents txtTemBenBankIBAN As TextBox
    Friend WithEvents lblBenBankIBAN As Label
    Friend WithEvents txtTemBenBankSwift As TextBox
    Friend WithEvents lblBenBankOther1 As Label
    Friend WithEvents txtTemBenBankZip As TextBox
    Friend WithEvents lblBenBankAddress1 As Label
    Friend WithEvents txtTemBenBankAddress2 As TextBox
    Friend WithEvents lblBenBankAddress2 As Label
    Friend WithEvents txtTemBenBankAddress1 As TextBox
    Friend WithEvents lblBenBankZip As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cboTemFindName As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents cmdTemAddCash As Button
    Friend WithEvents cmdDelete As Button
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents CboTemRetrieveBen As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents CboTemRetrievePortfolio As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents txtTemTemplateName As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents cboTemCCY As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cboTemPortfolio As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cboTemFindCCY As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cboTemFindPortfolio As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cboTemRetrieveCCY As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtTemBenSubBankINN As TextBox
    Friend WithEvents txtTemBenSubBankBIK As TextBox
    Friend WithEvents lblBenSubBankBIK As Label
    Friend WithEvents lblBenSubBankINN As Label
    Friend WithEvents txtTemBenBankINN As TextBox
    Friend WithEvents txtTemBenBankBIK As TextBox
    Friend WithEvents lblBenBankBIK As Label
    Friend WithEvents lblBenBankINN As Label
    Friend WithEvents TabTemBeneficiary As TabControl
    Friend WithEvents TabTemMain As TabPage
    Friend WithEvents txtTemBenZip As TextBox
    Friend WithEvents txtTemBenAddress2 As TextBox
    Friend WithEvents txtTemBenAddress1 As TextBox
    Friend WithEvents lblTemBenZip As Label
    Friend WithEvents lblTemBenAddress2 As Label
    Friend WithEvents lblTemBenAddress1 As Label
    Friend WithEvents lblBenName As Label
    Friend WithEvents TabTemBankDetails As TabPage
    Friend WithEvents lblbenBIK As Label
    Friend WithEvents txtTemBenBIK As TextBox
    Friend WithEvents txtTemBenINN As TextBox
    Friend WithEvents lblbenINN As Label
    Friend WithEvents PicTemBenIBAN As PictureBox
    Friend WithEvents PicTemBenSwift As PictureBox
    Friend WithEvents txtTemBenOther1 As TextBox
    Friend WithEvents txtTemBenIBAN As TextBox
    Friend WithEvents txtTemBenSwift As TextBox
    Friend WithEvents lblBenOther1 As Label
    Friend WithEvents lblBenIBAN As Label
    Friend WithEvents lblBenSwift As Label
    Friend WithEvents cboTemBenCountry As ComboBox
    Friend WithEvents lblTemBenCountry As Label
    Friend WithEvents lblTemBenCounty As Label
    Friend WithEvents txttembencounty As TextBox
    Friend WithEvents cboTemBenType As ComboBox
    Friend WithEvents lbltembenType As Label
    Friend WithEvents txttembensurname As TextBox
    Friend WithEvents txtTembenmiddlename As TextBox
    Friend WithEvents txttembenfirstname As TextBox
    Friend WithEvents cbotembenrelationship As ComboBox
    Friend WithEvents lblTemBenRelationship As Label
    Friend WithEvents lblTemBenSurname As Label
    Friend WithEvents lblTemBenMiddlename As Label
    Friend WithEvents lbltemBenFirstname As Label
    Friend WithEvents cboTemBenSubBankCountry As ComboBox
    Friend WithEvents cboTemBenBankCountry As ComboBox
    Friend WithEvents txtTemBenName As TextBox
End Class
