'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class vwDolfinPaymentExchangeRate
    Public Property ER_Date As Date
    Public Property ER_Pair As String
    Public Property ER_Rate As String
    Public Property ER_DownloadDate As String
    Public Property ER_DownloadTime As String
    Public Property ER_Ask As String
    Public Property ER_Bid As String
    Public Property ER_LastModifiedDate As Nullable(Of Date)
    Public Property XMLData As String

End Class
