'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class vwDolfinPaymentTransferFee
    Public Property Portfolio As String
    Public Property TF_ID As Nullable(Of Integer)
    Public Property PF_Code As Integer
    Public Property TF_PortfolioCode As Nullable(Of Integer)
    Public Property TF_CCY As String
    Public Property TF_Fee As Nullable(Of Integer)
    Public Property TF_Int_CCY As String
    Public Property TF_Int_Fee As Nullable(Of Integer)
    Public Property TF_UserModifiedBy As String
    Public Property TF_UserModifiedTime As Nullable(Of Date)

End Class
