﻿Public Class FrmtransferFeesConfirm
    Dim TFCConn As New SqlClient.SqlConnection
    Dim dgvdata As SqlClient.SqlDataReader

    Private Sub ColorCheckbox(ByVal Chk As CheckBox)
        If Chk.Checked Then
            Chk.ForeColor = Color.DarkGreen
        Else
            Chk.ForeColor = Color.DarkRed
        End If
    End Sub

    Private Sub FrmtransferFeesConfirm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim varFoundOrderAccountCCY As String = "", varFoundOrderAccount As String = "", varFoundOrderAccountBalance As Double = 0, varFoundOrderAccountFee As Double = 0, varFoundOrderAccountConvertedFee As Double = 0, varExchangeRate As Double = 0
        txtTFCOrderPortfolio.Text = ClsPay.Portfolio
        ClsIMS.PopulateComboboxWithData(cboTFCBenPortfolio, "select distinct pF_Code,Portfolio from vwDolfinPaymentFirmPortfolios" & IIf(ClsIMS.UserLocationCode = 47, "Malta", ""))

        ClsPay.SelectedTransferFeeOption = 3
        'ClsIMS.GetTransferFeeAccount(varFoundOrderAccountCCY, varFoundOrderAccount, varFoundOrderAccountBalance, varFoundOrderAccountFee, varFoundOrderAccountConvertedFee, varExchangeRate)
        ClsIMS.GetTransferFeeAccount(varFoundOrderAccountCCY, CStr(ClsPay.OrderAccountCode), varFoundOrderAccountBalance, varFoundOrderAccountFee, varFoundOrderAccountConvertedFee, varExchangeRate)
        ClsIMS.PopulateComboboxWithData(cboTFCOrderAccount, "Select t_code,t_name1 from vwDolfinPaymentSelectAccount Where b_class in (1,25,37,39) and pf_code = " & ClsPay.PortfolioCode)
        cboTFCOrderAccount.Text = varFoundOrderAccount

        CalcOrdertotals()
        CalcBentotals()
        ApplyExchangeRate(ClsIMS.GetCCYRate(DateAdd(DateInterval.Day, -1, Now), "GBP", ClsPay.PortfolioFeeCCYInt))
        ChkTFSwift.Checked = ClsPay.SendSwift
        ChkTFIMS.Checked = ClsPay.SendIMS
        ColorCheckbox(ChkTFSwift)
        ColorCheckbox(ChkTFIMS)
    End Sub

    Private Sub CalcOrdertotals()
        Dim varFoundOrderAccountCCY As String = "", varFoundOrderAccount As String = ""
        Dim varFoundOrderAccountBalance As Double = 0, varFoundOrderAccountFee As Double = 0, varFoundOrderAccountConvertedFee As Double = 0, OrderRemainingBalance As Double = 0, varExchangeRate As Double = 0

        ClsIMS.GetTransferFeeAccount(varFoundOrderAccountCCY, IIf(IsNumeric(cboTFCOrderAccount.SelectedValue), cboTFCOrderAccount.SelectedValue, 0), varFoundOrderAccountBalance, varFoundOrderAccountFee, varFoundOrderAccountConvertedFee, varExchangeRate)
        If varFoundOrderAccountCCY <> "" Then
            If IsNumeric(cboTFCOrderAccount.SelectedValue) Then
                txtTFCOrderCCY.Text = varFoundOrderAccountCCY 'ClsIMS.GetSQLItem("Select CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & ClsPay.PortfolioCode & " And t_code = " & cboTFCOrderAccount.SelectedValue)
                Dim varFoundOrderAccountBalanceStr As String = varFoundOrderAccountBalance 'ClsIMS.GetSQLItem("Select BalanceIncPending from vwDolfinPaymentSelectAccount where pf_code = " & ClsPay.PortfolioCode & " And t_code = " & cboTFCOrderAccount.SelectedValue)
                If IsNumeric(varFoundOrderAccountBalanceStr) Then
                    varFoundOrderAccountBalance = CDbl(varFoundOrderAccountBalanceStr).ToString("#,##0.00")
                Else
                    varFoundOrderAccountBalance = 0
                End If
            End If

            If txtTFCOrderCCY.Text = "GBP" Then

                ChkTFSwift.Checked = False
                lblrate.Visible = False
                txtrate.Visible = False
                lblrateccy.Visible = False
                ClsPay.PortfolioFee = varFoundOrderAccountFee
                ClsPay.PortfolioFeeCCY = txtTFCOrderCCY.Text
                ClsPay.PortfolioFeeInt = 0
                ClsPay.PortfolioFeeCCYInt = ""
                txtTFCOrderFee.Text = "-" & varFoundOrderAccountFee.ToString("#,##0.00")
                txtTFCOrderFeeEx.Text = "-" & varFoundOrderAccountFee.ToString("#,##0.00")
                txtTFCOrderBalance.Text = varFoundOrderAccountBalance.ToString("#,##0.00")

                If cboTFCOrderAccount.Text = ClsPay.OrderAccount Then
                    OrderRemainingBalance = varFoundOrderAccountBalance - varFoundOrderAccountFee
                    txtTFCOrderRemainingBalance.Text = OrderRemainingBalance.ToString("#,##0.00")
                    'If OrderRemainingBalance > 0 Then
                    'cboTFCOrderAccount.Enabled = False
                    'End If
                Else
                    OrderRemainingBalance = varFoundOrderAccountBalance - varFoundOrderAccountFee
                    txtTFCOrderRemainingBalance.Text = OrderRemainingBalance.ToString("#,##0.00")
                End If
                Me.Rb1.Text = "Use the selected account above to extract " & ClsPay.PortfolioFee.ToString("#,##0.00") & " " & ClsPay.PortfolioFeeCCY & " and continue"
            Else
                ChkTFSwift.Checked = True
                If varExchangeRate <> 0 Then
                    lblrate.Visible = True
                    txtrate.Visible = True
                    lblrateccy.Visible = True
                    lblrateccy.Text = "GBP" & txtTFCOrderCCY.Text
                    txtrate.Text = varExchangeRate
                    ApplyExchangeRate(ClsIMS.GetCCYRate(DateAdd(DateInterval.Day, -1, Now), "GBP", ClsPay.PortfolioFeeCCYInt))
                End If
                ClsPay.PortfolioFee = 0
                ClsPay.PortfolioFeeCCY = ""
                ClsPay.PortfolioFeeInt = varFoundOrderAccountConvertedFee
                ClsPay.PortfolioFeeCCYInt = txtTFCOrderCCY.Text
                txtTFCOrderFee.Text = "-" & varFoundOrderAccountFee.ToString("#,##0.00")
                txtTFCOrderFeeEx.Text = "-" & varFoundOrderAccountConvertedFee.ToString("#,##0.00")
                txtTFCOrderBalance.Text = varFoundOrderAccountBalance.ToString("#,##0.00")

                If cboTFCOrderAccount.Text = ClsPay.OrderAccount Then
                    OrderRemainingBalance = varFoundOrderAccountBalance - varFoundOrderAccountConvertedFee
                    txtTFCOrderRemainingBalance.Text = OrderRemainingBalance.ToString("#,##0.00")
                Else
                    txtTFCOrderBalance.Text = varFoundOrderAccountBalance
                    OrderRemainingBalance = varFoundOrderAccountBalance - varFoundOrderAccountConvertedFee
                    txtTFCOrderRemainingBalance.Text = OrderRemainingBalance.ToString("#,##0.00")
                End If
                Me.Rb1.Text = "Use the selected account above to extract " & ClsPay.PortfolioFeeInt.ToString("#,##0.00") & " " & ClsPay.PortfolioFeeCCYInt & " and continue"
            End If
            LblMsg.Text = "Transfer fee account has been found for this portfolio to extract the transfer fee"

            Me.Rb2.Text = "Waiver this fee and continue with movement only"
            Me.Rb3.Text = "Back to movements screen (fee can still be instructed) "
            Me.Rb1.Select()
        Else
            cboTFCOrderAccount.Text = ""
            txtTFCOrderCCY.Text = ""
            txtTFCOrderBalance.Text = "0.00"
            cboTFCOrderAccount.Enabled = False
            cboTFCBenPortfolio.Enabled = False
            cboTFCBenAccount.Enabled = False

            ClsIMS.PopulateComboboxWithData(cboTFCBenAccount, "select t_code,t_name1 from vwDolfinPaymentSelectAccount where b_class in (1,25,37,39) and pf_Code in (select distinct PF_Code from vwDolfinPaymentFirmPortfolios" & IIf(ClsIMS.UserLocationCode = 47, "Malta", "") & ")")
            LblMsg.Text = "No account has been found with enough funds to extract the fee"
            Me.Rb1.Visible = False
            Me.Rb2.Text = "Waiver this fee and continue with movement only"
            Me.Rb3.Text = "Back to movements screen" & vbNewLine & "(fee can be re-instructed for this transaction if you reduce outgoing payment)"
            Me.Rb3.Select()
        End If
        ColorCheckbox(ChkTFSwift)
        ColorCheckbox(ChkTFIMS)
        ApplyExchangeRate(ClsIMS.GetCCYRate(DateAdd(DateInterval.Day, -1, Now), "GBP", ClsPay.PortfolioFeeCCYInt))
    End Sub

    Public Sub CalcBentotals()
        Dim varFoundBenAccountCCY As String = "", varFoundBenAccount As String = "", varFoundBenAccountBalance As Double = 0

        If IsNumeric(cboTFCBenPortfolio.SelectedValue) Then
            ClsIMS.GetRMSTransferFeeAccount(cboTFCBenPortfolio.SelectedValue, varFoundBenAccountCCY, varFoundBenAccount, varFoundBenAccountBalance)
            If varFoundBenAccountCCY <> "" Then
                If cboTFCBenAccount.Items.Count > 0 Then
                    cboTFCBenAccount.Text = varFoundBenAccount
                    txtTFCBenCCY.Text = varFoundBenAccountCCY
                    txtTFCBenBalance.Text = varFoundBenAccountBalance
                End If
            End If
        Else
            txtTFCBenCCY.Text = ""
            txtTFCBenBalance.Text = "0.00"
        End If
    End Sub

    Private Sub FrmtransferFeesConfirm_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If TFCConn.State = ConnectionState.Open Then
            TFCConn.Close()
        End If
    End Sub

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        If Me.Rb1.Checked Then
            ClsPay.SelectedTransferFeeOption = 1
            If Not IsNumeric(cboTFCOrderAccount.SelectedValue) Or Not IsNumeric(cboTFCBenAccount.SelectedValue) Then
                MsgBox("Please select accounts to continue", vbInformation, "Select accounts")
            ElseIf txtTFCOrderRemainingBalance.Text < 0 Then
                MsgBox("Please select an account to extract transfer fee", vbInformation, "Select correct account for extraction")
            ElseIf Not ChkTFSwift.Checked And Not ChkTFIMS.Checked Then
                MsgBox("Payment currently has no destination. Please tick send swift, send IMS or both", vbInformation, "Select payment destination")
            Else
                If IsNumeric(cboTFCOrderAccount.SelectedValue) Then
                    ClsPay.SendSwift2 = ChkTFSwift.Checked
                    ClsPay.SendIMS2 = ChkTFIMS.Checked
                    'ClsPay.BeneficiaryPortfolioCode = cboTFCBenPortfolio.SelectedValue
                    'ClsPay.BeneficiaryPortfolio = cboTFCBenPortfolio.Text
                    ClsPay.PortfolioFeeAccountCode = cboTFCOrderAccount.SelectedValue
                    Me.Close()
                Else
                    MsgBox("Please select an order account to extract transfer fee", vbInformation, "Select correct account for extraction")
                End If

            End If
        ElseIf Me.Rb2.Checked Then
            ClsPay.SelectedTransferFeeOption = 2
            Me.Close()
        ElseIf Me.Rb3.Checked Then
            ClsPay.SelectedTransferFeeOption = 3
            Me.Close()
        End If
    End Sub

    Private Sub Rb1_CheckedChanged(sender As Object, e As EventArgs) Handles Rb1.CheckedChanged
        If IsNumeric(cboTFCOrderAccount.SelectedValue) Then
            ClsPay.BeneficiaryPortfolioCode = cboTFCBenPortfolio.SelectedValue
            ClsPay.BeneficiaryPortfolio = cboTFCBenPortfolio.Text
            ClsPay.PortfolioFeeAccountCode = cboTFCOrderAccount.SelectedValue
        End If
    End Sub

    Private Sub cboTFCAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTFCOrderAccount.SelectedIndexChanged
        If IsNumeric(cboTFCOrderAccount.SelectedValue) Then
            CalcOrdertotals()
            If IsNumeric(txtTFCOrderBalance.Text) Then
                txtTFCOrderBalance.Text = CDbl(txtTFCOrderBalance.Text).ToString("#,##0.00")
            End If
            ClsPay.PortfolioFeeAccountCode = cboTFCOrderAccount.SelectedValue
        End If
    End Sub

    Private Sub cboTFCBenAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTFCBenAccount.SelectedIndexChanged
        If IsNumeric(cboTFCBenAccount.SelectedValue) Then
            txtTFCBenCCY.Text = ClsIMS.GetSQLItem("select CR_Name1 from vwDolfinPaymentSelectAccount where pf_Code in (select distinct PF_Code from vwDolfinPaymentFirmPortfolios" & IIf(ClsIMS.UserLocationCode = 47, "Malta", "") & ") and t_code = " & cboTFCBenAccount.SelectedValue)
            txtTFCBenBalance.Text = ClsIMS.GetSQLItem("select BalanceIncPending from vwDolfinPaymentSelectAccount where pf_Code in (select distinct PF_Code from vwDolfinPaymentFirmPortfolios" & IIf(ClsIMS.UserLocationCode = 47, "Malta", "") & ") and t_code = " & cboTFCBenAccount.SelectedValue)
            If txtTFCBenBalance.Text = "" Then
                txtTFCBenBalance.Text = 0
            End If
            If IsNumeric(txtTFCBenBalance.Text) Then
                txtTFCBenBalance.Text = CDbl(txtTFCBenBalance.Text).ToString("#,##0.00")
            End If
            ClsPay.PortfolioFeeRMSAccountCode = cboTFCBenAccount.SelectedValue

            'if abn or bony client to firm, swift = true
        End If
    End Sub

    Private Sub cboTFCBenPortfolio_Leave(sender As Object, e As EventArgs) Handles cboTFCBenPortfolio.Leave
        If IsNumeric(cboTFCBenPortfolio.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(cboTFCBenAccount, "Select t_code,t_name1 from vwDolfinPaymentSelectAccount where b_class in (1,25,37,39) and pf_Code = " & cboTFCBenPortfolio.SelectedValue & " And cr_Name1 = '" & txtTFCOrderCCY.Text & "'")
            CalcBentotals()
        End If
    End Sub

    Private Sub txtrate_Leave(sender As Object, e As EventArgs) Handles txtrate.Leave
        If IsNumeric(txtrate.Text) Then
            ApplyExchangeRate(txtrate.Text)
        End If
    End Sub

    Private Sub ChkTFSwift_CheckedChanged(sender As Object, e As EventArgs) Handles ChkTFSwift.CheckedChanged
        ColorCheckbox(ChkTFSwift)
    End Sub

    Private Sub ChkTFIMS_CheckedChanged(sender As Object, e As EventArgs) Handles ChkTFIMS.CheckedChanged
        ColorCheckbox(ChkTFIMS)
    End Sub

    Private Sub ApplyExchangeRate(ByVal varRate As Double)
        If IsNumeric(txtrate.Text) And IsNumeric(txtTFCOrderFee.Text) Then
            ClsPay.ExchangeRate = varRate
            txtrate.Text = ClsPay.ExchangeRate
            txtTFCOrderFeeEx.Text = (CDbl(txtTFCOrderFee.Text) * CDbl(txtrate.Text)).ToString("#,##0.00")

            Dim OrderRemainingBalance As Double = txtTFCOrderBalance.Text + (CDbl(txtTFCOrderFee.Text) * CDbl(txtrate.Text))
            txtTFCOrderRemainingBalance.Text = CDbl(txtTFCOrderBalance.Text) + CDbl(txtTFCOrderFeeEx.Text)
            txtTFCOrderRemainingBalance.Text = OrderRemainingBalance.ToString("#,##0.00")
            If ClsPay.PortfolioFeeCCYInt = "" Then
                ClsPay.PortfolioFee = Math.Abs(CDbl(txtTFCOrderFee.Text) * CDbl(txtrate.Text))
                Me.Rb1.Text = "Use the selected account above to extract " & ClsPay.PortfolioFee & " " & ClsPay.PortfolioFeeCCY & " and continue"
            Else
                ClsPay.PortfolioFeeInt = Math.Abs(CDbl(txtTFCOrderFee.Text) * CDbl(txtrate.Text))
                Me.Rb1.Text = "Use the selected account above to extract " & ClsPay.PortfolioFeeInt & " " & ClsPay.PortfolioFeeCCYInt & " and continue"
            End If
        End If
    End Sub
End Class