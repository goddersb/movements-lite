﻿
Public Class FrmTrsFileManagement

    Dim _dicFileRec As Dictionary(Of String, String)
    Dim _dstFileRec As New DataSet
    Dim _blnFormLoaded As Boolean = False
    Dim _sICConn As SqlClient.SqlConnection
    Dim _tableFileRec As String = String.Empty
    Dim _tableFileName As String = String.Empty

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        _sICConn = ClsIMS.GetCurrentConnection

    End Sub

    Private Sub FrmTrsFileManagement_Load(sender As Object, e As EventArgs) Handles Me.Load

        Try
            Cursor = Cursors.WaitCursor

            Select Case ClsIMS.UserLocationCode
                Case 42
                    _tableFileRec = "IMSPlus.dbo._Dolfin_TrsFileRec"
                    _tableFileName = "IMSPlus.dbo._Dolfin_TrsFile"
                Case 47
                    _tableFileRec = "IMSPlus.dbo._Dolfin_TrsFileRecMaltaNewFile"
                    _tableFileName = "IMSPlus.dbo._Dolfin_TrsFileMaltaNewFile"
            End Select

            'Populate the combo box with a list of DocNo's.
            ClsIMS.PopulateComboboxWithData(Me.cboTrsFiles, $"SELECT DISTINCT '', DocNo FROM {_tableFileRec} WHERE PfBranch = {ClsIMS.UserLocationCode} ORDER BY DocNo ASC")
            cboTrsFiles.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboTrsFiles.AutoCompleteSource = AutoCompleteSource.ListItems

            ClsIMS.PopulateComboboxWithData(Me.cboPortfolio, $"SELECT DISTINCT '', por.PF_FName1 FROM {_tableFileRec} tfr INNER JOIN IMSPlus.dbo.Portfolios por ON por.PF_Code = tfr.PfCode WHERE PfBranch = {ClsIMS.UserLocationCode} ORDER BY por.PF_FName1 ASC")
            cboPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems

            ClsIMS.PopulateComboboxWithData(Me.cboQunatityCcy, $"SELECT DISTINCT '', QuantityCcy FROM {_tableFileRec} WHERE PfBranch = {ClsIMS.UserLocationCode} ORDER BY QuantityCcy ASC")
            cboQunatityCcy.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboQunatityCcy.AutoCompleteSource = AutoCompleteSource.ListItems

            ClsIMS.PopulateComboboxWithData(Me.cboPriceCcy, $"SELECT DISTINCT '', PriceCcy FROM {_tableFileRec} WHERE PfBranch = {ClsIMS.UserLocationCode} ORDER BY PriceCcy ASC")
            cboPriceCcy.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboPriceCcy.AutoCompleteSource = AutoCompleteSource.ListItems

            ClsIMS.PopulateComboboxWithData(Me.cboSubType, $"SELECT DISTINCT '', SubmissionType FROM {_tableFileRec} WHERE PfBranch = {ClsIMS.UserLocationCode} ORDER BY SubmissionType ASC")
            cboSubType.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboSubType.AutoCompleteSource = AutoCompleteSource.ListItems

            LoadFileNameCombo()

            _dicFileRec = New Dictionary(Of String, String)
            _dicFileRec.Add("Docno", cboTrsFiles.Text)
            _dicFileRec.Add("BranchCode", ClsIMS.UserLocationCode)
            _dicFileRec.Add("Portfolio", cboPortfolio.Text)
            _dicFileRec.Add("QuantityCcy", cboQunatityCcy.Text)
            _dicFileRec.Add("PriceCcy", cboPriceCcy.Text)
            _dicFileRec.Add("TrsFileName", cboTrsFileName.Text)
            _dicFileRec.Add("SubmissionType", cboSubType.Text)

            LoadGrid(_dicFileRec)

        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description}- Problem With loading the TRS File Rec form.")
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub LoadFileNameCombo()

        ClsIMS.PopulateComboboxWithData(Me.cboTrsFileName, $"SELECT DISTINCT tfl.FileID, tfl.[FileName] FROM {_tableFileRec} tfr INNER JOIN {_tableFileName} tfl ON tfl.FileID = tfr.FileID 
                                                            WHERE tfr.PfBranch = {ClsIMS.UserLocationCode} AND tfl.FileSequenceNumber <> -1 GROUP BY tfl.FileID, tfl.[FileName] 
                                                            HAVING COUNT(tfr.FileID) >=1 ORDER BY tfl.[FileName] ASC")
        cboTrsFileName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTrsFileName.AutoCompleteSource = AutoCompleteSource.ListItems

    End Sub


    Private Sub LoadGrid(ByVal dictionary As Dictionary(Of String, String))

        Dim clsIMSData As New ClsIMSData
        Dim strColSql As String = ""

        Try

            Cursor = Cursors.WaitCursor
            ModRMS.DoubleBuffered(dgvFileRec, True)
            dgvFileRec.DataSource = Nothing

            _dstFileRec = clsIMSData.GetFileRec(dictionary)
            dgvFileRec.AutoGenerateColumns = True
            dgvFileRec.DataSource = _dstFileRec.Tables("FileRec")

            dgvFileRec.Columns("DocNo").Width = 70
            dgvFileRec.Columns("Submission Type").Width = 80
            dgvFileRec.Columns("Trader").Width = 90
            dgvFileRec.Columns("Trade Date").Width = 90
            dgvFileRec.Columns("Trade Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvFileRec.Columns("Instrument Type").Width = 110
            dgvFileRec.Columns("Instrument Name").Width = 180
            dgvFileRec.Columns("Instrument ISIN").Width = 100
            dgvFileRec.Columns("Portfolio").Width = 215
            dgvFileRec.Columns("Quantity").Width = 80
            dgvFileRec.Columns("Quantity").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvFileRec.Columns("Quantity Ccy").Width = 60
            dgvFileRec.Columns("Price Money").Width = 80
            dgvFileRec.Columns("Price Money").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvFileRec.Columns("Price %").Width = 80
            dgvFileRec.Columns("Price %").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvFileRec.Columns("Price Ccy").Width = 60
            dgvFileRec.Columns("Net Amount").Width = 90
            dgvFileRec.Columns("Net Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvFileRec.Columns("Buy/Sell").Width = 60
            dgvFileRec.Columns("File Name").Width = 200

            _blnFormLoaded = True

            dgvFileRec.Refresh()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description}- Problem With loading the TRS File Rec grid.")
        Finally
            Cursor = Cursors.Default
            clsIMSData = Nothing
        End Try

    End Sub

    Private Sub btnFilterGrid_Click(sender As Object, e As EventArgs) Handles btnFilterGrid.Click

        Try
            _dicFileRec = New Dictionary(Of String, String)
            _dicFileRec.Add("Docno", cboTrsFiles.Text)
            _dicFileRec.Add("BranchCode", ClsIMS.UserLocationCode)
            _dicFileRec.Add("Portfolio", cboPortfolio.Text)
            _dicFileRec.Add("QuantityCcy", cboQunatityCcy.Text)
            _dicFileRec.Add("PriceCcy", cboPriceCcy.Text)
            _dicFileRec.Add("TrsFileName", cboTrsFileName.Text)
            _dicFileRec.Add("SubmissionType", cboSubType.Text)

            LoadGrid(_dicFileRec)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description}- Problem With btnFilterGrid_Click.")
        End Try

    End Sub

    Private Sub btnResetFilter_Click(sender As Object, e As EventArgs) Handles btnResetFilter.Click

        Try
            cboTrsFiles.Text = ""
            cboPortfolio.Text = ""
            cboQunatityCcy.Text = ""
            cboPriceCcy.Text = ""
            cboTrsFileName.Text = ""
            cboSubType.Text = ""
            cboTrsFileName.Text = ""

            _dicFileRec = New Dictionary(Of String, String)
            _dicFileRec.Add("Docno", cboTrsFiles.Text)
            _dicFileRec.Add("BranchCode", ClsIMS.UserLocationCode)
            _dicFileRec.Add("Portfolio", cboPortfolio.Text)
            _dicFileRec.Add("QuantityCcy", cboQunatityCcy.Text)
            _dicFileRec.Add("PriceCcy", cboPriceCcy.Text)
            _dicFileRec.Add("TrsFileName", cboTrsFileName.Text)
            _dicFileRec.Add("SubmissionType", cboSubType.Text)

            LoadGrid(_dicFileRec)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description}- Problem With btnResetFilter_Click.")
        End Try

    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click

        Cursor = Cursors.WaitCursor
        ExprtGridToExcelFrmForm(dgvFileRec, 0)
        Cursor = Cursors.Default

    End Sub

    Private Sub dgvFileRec_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvFileRec.UserDeletingRow

        Dim strDocNo As String = String.Empty
        Dim strSubmissionType As String
        Dim fileName As String = String.Empty
        Try
            strDocNo = IIf(IsDBNull(dgvFileRec.SelectedRows(0).Cells("DocNo").Value), 0, dgvFileRec.SelectedRows(0).Cells("DocNo").Value)
            strSubmissionType = IIf(IsDBNull(dgvFileRec.SelectedRows(0).Cells("Submission Type").Value), String.Empty, dgvFileRec.SelectedRows(0).Cells("Submission Type").Value)
            fileName = cboTrsFileName.Text

            If Not String.IsNullOrEmpty(strSubmissionType) And String.IsNullOrEmpty(cboTrsFileName.Text) Then
                If strSubmissionType = "EXCL" Then
                    Dim varResponse As Integer = MsgBox($"Are you sure you want to delete the File record details for DocNo {strDocNo}?", vbYesNo + vbQuestion, "Are you sure")
                    If varResponse = vbYes Then
                        ClsIMS.ExecuteString(_sICConn, $"DELETE {_tableFileRec} WHERE DocNo = '{strDocNo}'")
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTrsReport, $"The TRS DocNo {strDocNo} has been delted successfully.")

                        'Needed to prevent the grid clearing.
                        e.Cancel = True

                        btnResetFilter_Click(sender, e)
                    Else
                        e.Cancel = True
                    End If
                Else
                    MsgBox($"Unable to remove record for DocNo {strDocNo} as it's submission type is not set as EXCL", vbOKOnly + vbInformation, "TRS Delete Error")
                    e.Cancel = True
                End If
            ElseIf Not String.IsNullOrEmpty(cboTrsFileName.Text) Then
                Dim varResponse As Integer = MsgBox($"Are you sure you want to delete File {cboTrsFileName.Text}?", vbYesNo + vbQuestion, "Are you sure")
                If varResponse = vbYes Then

                    Dim result As Boolean = ClsIMS.DeleteTrsFileRecords(cboTrsFileName.SelectedValue)
                    If result Then
                        MsgBox($"The TRS File {fileName} has been delted successfully", vbOKOnly + vbInformation, "TRS Delete File")
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTrsReport, $"The TRS File {fileName} has been delted successfully.")
                    Else
                        MsgBox($"The TRS File {fileName} has not been deleted, please contact It for support.", vbOKOnly + vbInformation, "TRS Delete File Error")

                    End If

                    ClsIMS.SetTrsFileAsCurrent()
                    LoadFileNameCombo()

                    'Needed to prevent the grid clearing.
                    e.Cancel = True

                    btnResetFilter_Click(sender, e)
                Else
                        e.Cancel = True
                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{ex.Message}- Problem With deleting the {IIf(String.IsNullOrEmpty(strDocNo), "File", "DocNo")} for record {IIf(String.IsNullOrEmpty(strDocNo), fileName, strDocNo)}.")
        End Try

    End Sub

    Private Sub FrmTrsFileManagement_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed

        If _sICConn.State = ConnectionState.Open Then
            _sICConn.Close()
        End If

    End Sub

End Class