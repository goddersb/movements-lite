﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPasswordGenerator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboClient = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtNo = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtDOB = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtCode = New System.Windows.Forms.TextBox()
        Me.GrpVolopa = New System.Windows.Forms.GroupBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtMaxpY = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtMaxpM = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.cboTier = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtMaxpD = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtVolopaToken = New System.Windows.Forms.TextBox()
        Me.GrpQuestions = New System.Windows.Forms.GroupBox()
        Me.txtAns3 = New System.Windows.Forms.TextBox()
        Me.cboQ3 = New System.Windows.Forms.ComboBox()
        Me.txtAns2 = New System.Windows.Forms.TextBox()
        Me.cboQ2 = New System.Windows.Forms.ComboBox()
        Me.txtAns1 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboQ1 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GrpPassword = New System.Windows.Forms.GroupBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtPassword10 = New System.Windows.Forms.TextBox()
        Me.txtPassword9 = New System.Windows.Forms.TextBox()
        Me.txtPassword8 = New System.Windows.Forms.TextBox()
        Me.txtPassword7 = New System.Windows.Forms.TextBox()
        Me.txtPassword6 = New System.Windows.Forms.TextBox()
        Me.txtPassword5 = New System.Windows.Forms.TextBox()
        Me.txtPassword4 = New System.Windows.Forms.TextBox()
        Me.txtPassword3 = New System.Windows.Forms.TextBox()
        Me.txtPassword2 = New System.Windows.Forms.TextBox()
        Me.txtPassword1 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNewPassword = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CmdGen = New System.Windows.Forms.Button()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.CmdSave = New System.Windows.Forms.Button()
        Me.dtStartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.ChkAnnualFee = New System.Windows.Forms.CheckBox()
        Me.txtAnnualFee = New System.Windows.Forms.TextBox()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GrpVolopa.SuspendLayout()
        Me.GrpQuestions.SuspendLayout()
        Me.GrpPassword.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboClient
        '
        Me.cboClient.FormattingEnabled = True
        Me.cboClient.Location = New System.Drawing.Point(52, 19)
        Me.cboClient.Name = "cboClient"
        Me.cboClient.Size = New System.Drawing.Size(622, 21)
        Me.cboClient.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Client:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(12, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 24)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Client Security"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GroupBox5)
        Me.GroupBox2.Controls.Add(Me.GrpVolopa)
        Me.GroupBox2.Controls.Add(Me.GrpQuestions)
        Me.GroupBox2.Controls.Add(Me.GrpPassword)
        Me.GroupBox2.Location = New System.Drawing.Point(16, 52)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(706, 566)
        Me.GroupBox2.TabIndex = 32
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Client Details"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label30)
        Me.GroupBox5.Controls.Add(Me.txtNo)
        Me.GroupBox5.Controls.Add(Me.Label29)
        Me.GroupBox5.Controls.Add(Me.txtDOB)
        Me.GroupBox5.Controls.Add(Me.Label28)
        Me.GroupBox5.Controls.Add(Me.txtEmail)
        Me.GroupBox5.Controls.Add(Me.Label27)
        Me.GroupBox5.Controls.Add(Me.txtCode)
        Me.GroupBox5.Controls.Add(Me.cboClient)
        Me.GroupBox5.Controls.Add(Me.Label1)
        Me.GroupBox5.Location = New System.Drawing.Point(9, 19)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(685, 82)
        Me.GroupBox5.TabIndex = 34
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Client Details"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(265, 50)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(21, 13)
        Me.Label30.TabIndex = 12
        Me.Label30.Text = "No"
        '
        'txtNo
        '
        Me.txtNo.Enabled = False
        Me.txtNo.Location = New System.Drawing.Point(292, 46)
        Me.txtNo.Name = "txtNo"
        Me.txtNo.Size = New System.Drawing.Size(99, 20)
        Me.txtNo.TabIndex = 11
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(156, 50)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(30, 13)
        Me.Label29.TabIndex = 10
        Me.Label29.Text = "DOB"
        '
        'txtDOB
        '
        Me.txtDOB.Enabled = False
        Me.txtDOB.Location = New System.Drawing.Point(186, 47)
        Me.txtDOB.Name = "txtDOB"
        Me.txtDOB.Size = New System.Drawing.Size(73, 20)
        Me.txtDOB.TabIndex = 9
        Me.txtDOB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(397, 50)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(32, 13)
        Me.Label28.TabIndex = 8
        Me.Label28.Text = "Email"
        '
        'txtEmail
        '
        Me.txtEmail.Enabled = False
        Me.txtEmail.Location = New System.Drawing.Point(435, 47)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(239, 20)
        Me.txtEmail.TabIndex = 7
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(10, 50)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(35, 13)
        Me.Label27.TabIndex = 6
        Me.Label27.Text = "Code:"
        '
        'txtCode
        '
        Me.txtCode.Enabled = False
        Me.txtCode.Location = New System.Drawing.Point(52, 47)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(96, 20)
        Me.txtCode.TabIndex = 5
        Me.txtCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GrpVolopa
        '
        Me.GrpVolopa.Controls.Add(Me.txtAnnualFee)
        Me.GrpVolopa.Controls.Add(Me.ChkAnnualFee)
        Me.GrpVolopa.Controls.Add(Me.dtStartDate)
        Me.GrpVolopa.Controls.Add(Me.Label31)
        Me.GrpVolopa.Controls.Add(Me.Label26)
        Me.GrpVolopa.Controls.Add(Me.txtVolopaToken)
        Me.GrpVolopa.Controls.Add(Me.Label22)
        Me.GrpVolopa.Controls.Add(Me.txtMaxpY)
        Me.GrpVolopa.Controls.Add(Me.Label25)
        Me.GrpVolopa.Controls.Add(Me.txtMaxpM)
        Me.GrpVolopa.Controls.Add(Me.Label24)
        Me.GrpVolopa.Controls.Add(Me.cboTier)
        Me.GrpVolopa.Controls.Add(Me.Label23)
        Me.GrpVolopa.Controls.Add(Me.txtMaxpD)
        Me.GrpVolopa.Enabled = False
        Me.GrpVolopa.Location = New System.Drawing.Point(9, 458)
        Me.GrpVolopa.Name = "GrpVolopa"
        Me.GrpVolopa.Size = New System.Drawing.Size(683, 98)
        Me.GrpVolopa.TabIndex = 29
        Me.GrpVolopa.TabStop = False
        Me.GrpVolopa.Text = "Volopa Token ID"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(573, 18)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(94, 13)
        Me.Label26.TabIndex = 20
        Me.Label26.Text = "Max TopUp - Year"
        '
        'txtMaxpY
        '
        Me.txtMaxpY.Enabled = False
        Me.txtMaxpY.Location = New System.Drawing.Point(573, 34)
        Me.txtMaxpY.Name = "txtMaxpY"
        Me.txtMaxpY.Size = New System.Drawing.Size(100, 20)
        Me.txtMaxpY.TabIndex = 19
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(471, 18)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(90, 13)
        Me.Label25.TabIndex = 18
        Me.Label25.Text = "Max TopUp - Mth"
        '
        'txtMaxpM
        '
        Me.txtMaxpM.Enabled = False
        Me.txtMaxpM.Location = New System.Drawing.Point(467, 34)
        Me.txtMaxpM.Name = "txtMaxpM"
        Me.txtMaxpM.Size = New System.Drawing.Size(100, 20)
        Me.txtMaxpM.TabIndex = 17
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(44, 48)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(28, 13)
        Me.Label24.TabIndex = 16
        Me.Label24.Text = "Tier:"
        '
        'cboTier
        '
        Me.cboTier.FormattingEnabled = True
        Me.cboTier.Location = New System.Drawing.Point(78, 45)
        Me.cboTier.Name = "cboTier"
        Me.cboTier.Size = New System.Drawing.Size(125, 21)
        Me.cboTier.TabIndex = 15
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(363, 19)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(91, 13)
        Me.Label23.TabIndex = 14
        Me.Label23.Text = "Max TopUp - Day"
        '
        'txtMaxpD
        '
        Me.txtMaxpD.Enabled = False
        Me.txtMaxpD.Location = New System.Drawing.Point(360, 35)
        Me.txtMaxpD.Name = "txtMaxpD"
        Me.txtMaxpD.Size = New System.Drawing.Size(100, 20)
        Me.txtMaxpD.TabIndex = 13
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(17, 75)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(55, 13)
        Me.Label22.TabIndex = 12
        Me.Label22.Text = "Token ID:"
        '
        'txtVolopaToken
        '
        Me.txtVolopaToken.Location = New System.Drawing.Point(78, 72)
        Me.txtVolopaToken.Name = "txtVolopaToken"
        Me.txtVolopaToken.Size = New System.Drawing.Size(125, 20)
        Me.txtVolopaToken.TabIndex = 7
        '
        'GrpQuestions
        '
        Me.GrpQuestions.BackColor = System.Drawing.Color.OldLace
        Me.GrpQuestions.Controls.Add(Me.txtAns3)
        Me.GrpQuestions.Controls.Add(Me.cboQ3)
        Me.GrpQuestions.Controls.Add(Me.txtAns2)
        Me.GrpQuestions.Controls.Add(Me.cboQ2)
        Me.GrpQuestions.Controls.Add(Me.txtAns1)
        Me.GrpQuestions.Controls.Add(Me.Label10)
        Me.GrpQuestions.Controls.Add(Me.Label9)
        Me.GrpQuestions.Controls.Add(Me.Label8)
        Me.GrpQuestions.Controls.Add(Me.Label7)
        Me.GrpQuestions.Controls.Add(Me.Label6)
        Me.GrpQuestions.Controls.Add(Me.cboQ1)
        Me.GrpQuestions.Controls.Add(Me.Label5)
        Me.GrpQuestions.Enabled = False
        Me.GrpQuestions.Location = New System.Drawing.Point(9, 262)
        Me.GrpQuestions.Name = "GrpQuestions"
        Me.GrpQuestions.Size = New System.Drawing.Size(686, 190)
        Me.GrpQuestions.TabIndex = 33
        Me.GrpQuestions.TabStop = False
        Me.GrpQuestions.Text = "Security Questions"
        '
        'txtAns3
        '
        Me.txtAns3.Location = New System.Drawing.Point(81, 160)
        Me.txtAns3.Name = "txtAns3"
        Me.txtAns3.Size = New System.Drawing.Size(594, 20)
        Me.txtAns3.TabIndex = 16
        '
        'cboQ3
        '
        Me.cboQ3.FormattingEnabled = True
        Me.cboQ3.Location = New System.Drawing.Point(81, 132)
        Me.cboQ3.Name = "cboQ3"
        Me.cboQ3.Size = New System.Drawing.Size(594, 21)
        Me.cboQ3.TabIndex = 15
        '
        'txtAns2
        '
        Me.txtAns2.Location = New System.Drawing.Point(81, 106)
        Me.txtAns2.Name = "txtAns2"
        Me.txtAns2.Size = New System.Drawing.Size(594, 20)
        Me.txtAns2.TabIndex = 14
        '
        'cboQ2
        '
        Me.cboQ2.FormattingEnabled = True
        Me.cboQ2.Location = New System.Drawing.Point(81, 78)
        Me.cboQ2.Name = "cboQ2"
        Me.cboQ2.Size = New System.Drawing.Size(594, 21)
        Me.cboQ2.TabIndex = 13
        '
        'txtAns1
        '
        Me.txtAns1.Location = New System.Drawing.Point(81, 47)
        Me.txtAns1.Name = "txtAns1"
        Me.txtAns1.Size = New System.Drawing.Size(594, 20)
        Me.txtAns1.TabIndex = 12
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(15, 160)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(54, 13)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Answer 3:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(15, 132)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(61, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Question 3:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 106)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Answer 2:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 78)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Question 2:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 50)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(54, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Answer 1:"
        '
        'cboQ1
        '
        Me.cboQ1.FormattingEnabled = True
        Me.cboQ1.Location = New System.Drawing.Point(81, 19)
        Me.cboQ1.Name = "cboQ1"
        Me.cboQ1.Size = New System.Drawing.Size(594, 21)
        Me.cboQ1.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Question 1:"
        '
        'GrpPassword
        '
        Me.GrpPassword.BackColor = System.Drawing.Color.OldLace
        Me.GrpPassword.Controls.Add(Me.Label21)
        Me.GrpPassword.Controls.Add(Me.Label20)
        Me.GrpPassword.Controls.Add(Me.Label19)
        Me.GrpPassword.Controls.Add(Me.Label18)
        Me.GrpPassword.Controls.Add(Me.Label17)
        Me.GrpPassword.Controls.Add(Me.Label16)
        Me.GrpPassword.Controls.Add(Me.Label15)
        Me.GrpPassword.Controls.Add(Me.Label14)
        Me.GrpPassword.Controls.Add(Me.Label13)
        Me.GrpPassword.Controls.Add(Me.Label12)
        Me.GrpPassword.Controls.Add(Me.Label11)
        Me.GrpPassword.Controls.Add(Me.txtPassword10)
        Me.GrpPassword.Controls.Add(Me.txtPassword9)
        Me.GrpPassword.Controls.Add(Me.txtPassword8)
        Me.GrpPassword.Controls.Add(Me.txtPassword7)
        Me.GrpPassword.Controls.Add(Me.txtPassword6)
        Me.GrpPassword.Controls.Add(Me.txtPassword5)
        Me.GrpPassword.Controls.Add(Me.txtPassword4)
        Me.GrpPassword.Controls.Add(Me.txtPassword3)
        Me.GrpPassword.Controls.Add(Me.txtPassword2)
        Me.GrpPassword.Controls.Add(Me.txtPassword1)
        Me.GrpPassword.Controls.Add(Me.Label4)
        Me.GrpPassword.Controls.Add(Me.txtNewPassword)
        Me.GrpPassword.Controls.Add(Me.Label3)
        Me.GrpPassword.Controls.Add(Me.CmdGen)
        Me.GrpPassword.Controls.Add(Me.txtPassword)
        Me.GrpPassword.Enabled = False
        Me.GrpPassword.Location = New System.Drawing.Point(9, 107)
        Me.GrpPassword.Name = "GrpPassword"
        Me.GrpPassword.Size = New System.Drawing.Size(686, 149)
        Me.GrpPassword.TabIndex = 32
        Me.GrpPassword.TabStop = False
        Me.GrpPassword.Text = "Generate Password"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(435, 12)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(107, 13)
        Me.Label21.TabIndex = 28
        Me.Label21.Text = "Password Characters"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(648, 30)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(19, 13)
        Me.Label20.TabIndex = 27
        Me.Label20.Text = "10"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(614, 30)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(13, 13)
        Me.Label19.TabIndex = 26
        Me.Label19.Text = "9"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(579, 30)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(13, 13)
        Me.Label18.TabIndex = 25
        Me.Label18.Text = "8"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(543, 30)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(13, 13)
        Me.Label17.TabIndex = 24
        Me.Label17.Text = "7"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(508, 30)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(13, 13)
        Me.Label16.TabIndex = 23
        Me.Label16.Text = "6"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(471, 30)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(13, 13)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "5"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(435, 30)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(13, 13)
        Me.Label14.TabIndex = 21
        Me.Label14.Text = "4"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(398, 30)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(13, 13)
        Me.Label13.TabIndex = 20
        Me.Label13.Text = "3"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(363, 30)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(13, 13)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "2"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(327, 30)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(13, 13)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "1"
        '
        'txtPassword10
        '
        Me.txtPassword10.Enabled = False
        Me.txtPassword10.Location = New System.Drawing.Point(644, 46)
        Me.txtPassword10.Name = "txtPassword10"
        Me.txtPassword10.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword10.TabIndex = 17
        Me.txtPassword10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword9
        '
        Me.txtPassword9.Enabled = False
        Me.txtPassword9.Location = New System.Drawing.Point(608, 46)
        Me.txtPassword9.Name = "txtPassword9"
        Me.txtPassword9.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword9.TabIndex = 16
        Me.txtPassword9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword8
        '
        Me.txtPassword8.Enabled = False
        Me.txtPassword8.Location = New System.Drawing.Point(572, 46)
        Me.txtPassword8.Name = "txtPassword8"
        Me.txtPassword8.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword8.TabIndex = 15
        Me.txtPassword8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword7
        '
        Me.txtPassword7.Enabled = False
        Me.txtPassword7.Location = New System.Drawing.Point(536, 46)
        Me.txtPassword7.Name = "txtPassword7"
        Me.txtPassword7.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword7.TabIndex = 14
        Me.txtPassword7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword6
        '
        Me.txtPassword6.Enabled = False
        Me.txtPassword6.Location = New System.Drawing.Point(500, 46)
        Me.txtPassword6.Name = "txtPassword6"
        Me.txtPassword6.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword6.TabIndex = 13
        Me.txtPassword6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword5
        '
        Me.txtPassword5.Enabled = False
        Me.txtPassword5.Location = New System.Drawing.Point(464, 46)
        Me.txtPassword5.Name = "txtPassword5"
        Me.txtPassword5.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword5.TabIndex = 12
        Me.txtPassword5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword4
        '
        Me.txtPassword4.Enabled = False
        Me.txtPassword4.Location = New System.Drawing.Point(428, 46)
        Me.txtPassword4.Name = "txtPassword4"
        Me.txtPassword4.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword4.TabIndex = 11
        Me.txtPassword4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword3
        '
        Me.txtPassword3.Enabled = False
        Me.txtPassword3.Location = New System.Drawing.Point(392, 46)
        Me.txtPassword3.Name = "txtPassword3"
        Me.txtPassword3.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword3.TabIndex = 10
        Me.txtPassword3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword2
        '
        Me.txtPassword2.Enabled = False
        Me.txtPassword2.Location = New System.Drawing.Point(356, 46)
        Me.txtPassword2.Name = "txtPassword2"
        Me.txtPassword2.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword2.TabIndex = 9
        Me.txtPassword2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPassword1
        '
        Me.txtPassword1.Enabled = False
        Me.txtPassword1.Location = New System.Drawing.Point(320, 46)
        Me.txtPassword1.Name = "txtPassword1"
        Me.txtPassword1.Size = New System.Drawing.Size(30, 20)
        Me.txtPassword1.TabIndex = 8
        Me.txtPassword1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 114)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "New Password:"
        '
        'txtNewPassword
        '
        Me.txtNewPassword.Location = New System.Drawing.Point(101, 111)
        Me.txtNewPassword.Name = "txtNewPassword"
        Me.txtNewPassword.Size = New System.Drawing.Size(197, 20)
        Me.txtNewPassword.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(5, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Current Password:"
        '
        'CmdGen
        '
        Me.CmdGen.Location = New System.Drawing.Point(101, 72)
        Me.CmdGen.Name = "CmdGen"
        Me.CmdGen.Size = New System.Drawing.Size(158, 23)
        Me.CmdGen.TabIndex = 1
        Me.CmdGen.Text = "Generate New Password"
        Me.CmdGen.UseVisualStyleBackColor = True
        '
        'txtPassword
        '
        Me.txtPassword.Enabled = False
        Me.txtPassword.Location = New System.Drawing.Point(101, 46)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(197, 20)
        Me.txtPassword.TabIndex = 0
        '
        'CmdSave
        '
        Me.CmdSave.Location = New System.Drawing.Point(561, 624)
        Me.CmdSave.Name = "CmdSave"
        Me.CmdSave.Size = New System.Drawing.Size(158, 23)
        Me.CmdSave.TabIndex = 34
        Me.CmdSave.Text = "Save Security Details"
        Me.CmdSave.UseVisualStyleBackColor = True
        '
        'dtStartDate
        '
        Me.dtStartDate.Location = New System.Drawing.Point(78, 19)
        Me.dtStartDate.Name = "dtStartDate"
        Me.dtStartDate.Size = New System.Drawing.Size(125, 20)
        Me.dtStartDate.TabIndex = 21
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(14, 22)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(58, 13)
        Me.Label31.TabIndex = 35
        Me.Label31.Text = "Start Date:"
        '
        'ChkAnnualFee
        '
        Me.ChkAnnualFee.AutoSize = True
        Me.ChkAnnualFee.Location = New System.Drawing.Point(219, 21)
        Me.ChkAnnualFee.Name = "ChkAnnualFee"
        Me.ChkAnnualFee.Size = New System.Drawing.Size(123, 17)
        Me.ChkAnnualFee.TabIndex = 36
        Me.ChkAnnualFee.Text = "Annual Fee Charge?"
        Me.ChkAnnualFee.UseVisualStyleBackColor = True
        '
        'txtAnnualFee
        '
        Me.txtAnnualFee.Location = New System.Drawing.Point(215, 44)
        Me.txtAnnualFee.Name = "txtAnnualFee"
        Me.txtAnnualFee.Size = New System.Drawing.Size(125, 20)
        Me.txtAnnualFee.TabIndex = 37
        '
        'FrmPasswordGenerator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(731, 660)
        Me.Controls.Add(Me.CmdSave)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmPasswordGenerator"
        Me.Text = "Password Generator"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GrpVolopa.ResumeLayout(False)
        Me.GrpVolopa.PerformLayout()
        Me.GrpQuestions.ResumeLayout(False)
        Me.GrpQuestions.PerformLayout()
        Me.GrpPassword.ResumeLayout(False)
        Me.GrpPassword.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboClient As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GrpQuestions As GroupBox
    Friend WithEvents txtAns3 As TextBox
    Friend WithEvents cboQ3 As ComboBox
    Friend WithEvents txtAns2 As TextBox
    Friend WithEvents cboQ2 As ComboBox
    Friend WithEvents txtAns1 As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cboQ1 As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GrpPassword As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtNewPassword As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents CmdGen As Button
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents CmdSave As Button
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtPassword10 As TextBox
    Friend WithEvents txtPassword9 As TextBox
    Friend WithEvents txtPassword8 As TextBox
    Friend WithEvents txtPassword7 As TextBox
    Friend WithEvents txtPassword6 As TextBox
    Friend WithEvents txtPassword5 As TextBox
    Friend WithEvents txtPassword4 As TextBox
    Friend WithEvents txtPassword3 As TextBox
    Friend WithEvents txtPassword2 As TextBox
    Friend WithEvents txtPassword1 As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents GrpVolopa As GroupBox
    Friend WithEvents txtVolopaToken As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents txtMaxpM As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents cboTier As ComboBox
    Friend WithEvents Label23 As Label
    Friend WithEvents txtMaxpD As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents txtMaxpY As TextBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents Label30 As Label
    Friend WithEvents txtNo As TextBox
    Friend WithEvents Label29 As Label
    Friend WithEvents txtDOB As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents txtCode As TextBox
    Friend WithEvents dtStartDate As DateTimePicker
    Friend WithEvents Label31 As Label
    Friend WithEvents txtAnnualFee As TextBox
    Friend WithEvents ChkAnnualFee As CheckBox
End Class
