﻿Public Class ClsStandingOrder

    Private _intStandingOrderId As Integer
    Private _intPortFolioCode As Integer
    Private _intTemplateId As Integer
    Private _intPaymentTypeId As Integer
    Private _intIntervalId As Integer
    Private _intStatusId As Integer
    Private _intCurrencyId As Integer

    Private _datStartDate As Date
    Private _datEndDate As Date

    Private _strPortfolioName As String
    Private _strTemplateName As String
    Private _strPaymentType As String
    Private _strCurrency As String
    Private _strInterval As String
    Private _strStatus As String
    Private _strSwiftRef As String

    Private _dblAmount As Double

    Property StandingOrderId() As Integer
        Get
            Return _intStandingOrderId
        End Get
        Set(value As Integer)
            _intStandingOrderId = value
        End Set
    End Property

    Property PortfolioCode() As Integer
        Get
            Return _intPortFolioCode
        End Get
        Set(value As Integer)
            _intPortFolioCode = value
        End Set
    End Property

    Property TemplateId() As Integer
        Get
            Return _intTemplateId
        End Get
        Set(value As Integer)
            _intTemplateId = value
        End Set
    End Property

    Property PaymentTypeId() As Integer
        Get
            Return _intPaymentTypeId
        End Get
        Set(value As Integer)
            _intPaymentTypeId = value
        End Set
    End Property

    Property IntervalId() As Integer
        Get
            Return _intIntervalId
        End Get
        Set(value As Integer)
            _intIntervalId = value
        End Set
    End Property

    Property StatusId() As Integer
        Get
            Return _intStatusId
        End Get
        Set(value As Integer)
            _intStatusId = value
        End Set
    End Property

    Property CurrencyId() As Integer
        Get
            Return _intCurrencyId
        End Get
        Set(value As Integer)
            _intCurrencyId = value
        End Set
    End Property

    Property StartDate() As Date
        Get
            Return _datStartDate
        End Get
        Set(value As Date)
            _datStartDate = value
        End Set
    End Property

    Property EndDate() As Date
        Get
            Return _datEndDate
        End Get
        Set(value As Date)
            _datEndDate = value
        End Set
    End Property

    Property Amount() As Double
        Get
            Return _dblAmount
        End Get
        Set(value As Double)
            _dblAmount = value
        End Set
    End Property

    Property PortfolioName() As String
        Get
            Return _strPortfolioName
        End Get
        Set(value As String)
            _strPortfolioName = value
        End Set
    End Property

    Property TemplateName() As String
        Get
            Return _strTemplateName
        End Get
        Set(value As String)
            _strTemplateName = value
        End Set
    End Property

    Property Currency() As String
        Get
            Return _strCurrency
        End Get
        Set(value As String)
            _strCurrency = value
        End Set
    End Property

    Property PaymentType() As String
        Get
            Return _strPaymentType
        End Get
        Set(value As String)
            _strPaymentType = value
        End Set
    End Property

    Property Interval() As String
        Get
            Return _strInterval
        End Get
        Set(value As String)
            _strInterval = value
        End Set
    End Property

    Property Status() As String
        Get
            Return _strStatus
        End Get
        Set(value As String)
            _strStatus = value
        End Set
    End Property

    Property SwiftRef() As String
        Get
            Return _strSwiftRef
        End Get
        Set(value As String)
            _strSwiftRef = value
        End Set
    End Property

    Public Sub LoadEditForm()

        Dim frmStandingOrderRequest As New FrmStandingOrderRequest
        With frmStandingOrderRequest
            ClsIMS.PopulateComboboxWithData(.cboInterval, "SELECT SI_ID, SI_Description AS [Interval] FROM DolfinPaymentStandingOrdersInterval ORDER BY SI_ID ASC")
            .cboInterval.Text = Interval
            ClsIMS.PopulateComboboxWithData(.cboStatus, "SELECT S_ID, S_Status FROM DolfinPaymentStatus WHERE S_ID IN (21, 32) ORDER BY S_Status ASC")
            .cboStatus.Text = Status
            .cboPayType.Text = PaymentType
            .cboPortfolio.Text = PortfolioName
            .cboTemplate.Text = TemplateName
            .dtpStartDate.Value = StartDate
            .dtpEndDate.Value = EndDate
            .cboCCY.Text = Currency
            .txtAmount.Text = Amount
            .txtSwiftRef.Text = SwiftRef
            .btnSave.Text = "Update"
            ._edit = True
            ._standingOrderId = StandingOrderId
            .ShowDialog()
        End With
    End Sub

End Class
