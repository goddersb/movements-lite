﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUsersPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmUsersPassword))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtResetPassword = New System.Windows.Forms.TextBox()
        Me.lblResetPassword = New System.Windows.Forms.Label()
        Me.CmdCancel = New System.Windows.Forms.Button()
        Me.cmdLogin = New System.Windows.Forms.Button()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBoxUser = New System.Windows.Forms.PictureBox()
        Me.ImageListUser = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageListButton = New System.Windows.Forms.ImageList(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBoxUser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtResetPassword)
        Me.GroupBox1.Controls.Add(Me.lblResetPassword)
        Me.GroupBox1.Controls.Add(Me.CmdCancel)
        Me.GroupBox1.Controls.Add(Me.cmdLogin)
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.txtUser)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.PictureBoxUser)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(468, 230)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtResetPassword
        '
        Me.txtResetPassword.Location = New System.Drawing.Point(199, 134)
        Me.txtResetPassword.Name = "txtResetPassword"
        Me.txtResetPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtResetPassword.Size = New System.Drawing.Size(256, 20)
        Me.txtResetPassword.TabIndex = 4
        Me.txtResetPassword.UseSystemPasswordChar = True
        Me.txtResetPassword.Visible = False
        '
        'lblResetPassword
        '
        Me.lblResetPassword.AutoSize = True
        Me.lblResetPassword.Location = New System.Drawing.Point(196, 118)
        Me.lblResetPassword.Name = "lblResetPassword"
        Me.lblResetPassword.Size = New System.Drawing.Size(90, 13)
        Me.lblResetPassword.TabIndex = 5
        Me.lblResetPassword.Text = "Reset Password :"
        Me.lblResetPassword.Visible = False
        '
        'CmdCancel
        '
        Me.CmdCancel.Location = New System.Drawing.Point(199, 178)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.Size = New System.Drawing.Size(93, 33)
        Me.CmdCancel.TabIndex = 2
        Me.CmdCancel.Text = "   Cancel"
        Me.CmdCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.CmdCancel.UseVisualStyleBackColor = True
        '
        'cmdLogin
        '
        Me.cmdLogin.Location = New System.Drawing.Point(362, 178)
        Me.cmdLogin.Name = "cmdLogin"
        Me.cmdLogin.Size = New System.Drawing.Size(93, 33)
        Me.cmdLogin.TabIndex = 1
        Me.cmdLogin.Text = "   Login"
        Me.cmdLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.cmdLogin.UseVisualStyleBackColor = True
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(199, 84)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(256, 20)
        Me.txtPassword.TabIndex = 0
        Me.txtPassword.UseSystemPasswordChar = True
        '
        'txtUser
        '
        Me.txtUser.Enabled = False
        Me.txtUser.Location = New System.Drawing.Point(199, 36)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(256, 20)
        Me.txtUser.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(196, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Password :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(196, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "User Name :"
        '
        'PictureBoxUser
        '
        Me.PictureBoxUser.InitialImage = CType(resources.GetObject("PictureBoxUser.InitialImage"), System.Drawing.Image)
        Me.PictureBoxUser.Location = New System.Drawing.Point(7, 20)
        Me.PictureBoxUser.Name = "PictureBoxUser"
        Me.PictureBoxUser.Size = New System.Drawing.Size(172, 191)
        Me.PictureBoxUser.TabIndex = 0
        Me.PictureBoxUser.TabStop = False
        '
        'ImageListUser
        '
        Me.ImageListUser.ImageStream = CType(resources.GetObject("ImageListUser.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListUser.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListUser.Images.SetKeyName(0, "password4.png")
        '
        'ImageListButton
        '
        Me.ImageListButton.ImageStream = CType(resources.GetObject("ImageListButton.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListButton.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListButton.Images.SetKeyName(0, "button_cancel16.png")
        Me.ImageListButton.Images.SetKeyName(1, "tick (1)16.png")
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(485, 7)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmUsersPassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(488, 254)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmUsersPassword"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FLOW Login"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBoxUser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents PictureBoxUser As PictureBox
    Friend WithEvents ImageListUser As ImageList
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents txtUser As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents CmdCancel As Button
    Friend WithEvents cmdLogin As Button
    Friend WithEvents ImageListButton As ImageList
    Friend WithEvents txtResetPassword As TextBox
    Friend WithEvents lblResetPassword As Label
    Friend WithEvents Button1 As Button
End Class
