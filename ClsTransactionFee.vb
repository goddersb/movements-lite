﻿Public Class ClsTransactionFee
    Private varID As Double = 0
    Private varTypeID As Double = 0
    Private varCCYEqvCode As Double = 0
    Private varTicketFee As Double = 0
    Private varTicketFeeCCY As Double = 0
    Private varTransFee As Double = 0
    Private varTransFeeCCY As Double = 0
    Private varFOPFee As Double = 0
    Private varFOPFeeCCY As Double = 0
    Private varCoverBrokerage As Double = 0
    Private varMinCharge As Double = 0
    Private varMaxCharge As Double = 0

    Property ID() As Double
        Get
            Return varID
        End Get
        Set(value As Double)
            varID = value
        End Set
    End Property

    Property TypeID() As Double
        Get
            Return varTypeID
        End Get
        Set(value As Double)
            varTypeID = value
        End Set
    End Property

    Property CCYEqvCode() As Double
        Get
            Return varCCYEqvCode
        End Get
        Set(value As Double)
            varCCYEqvCode = value
        End Set
    End Property

    Property TicketFee() As Double
        Get
            Return varTicketFee
        End Get
        Set(value As Double)
            varTicketFee = value
        End Set
    End Property

    Property TicketFeeCCY() As Double
        Get
            Return varTicketFeeCCY
        End Get
        Set(value As Double)
            varTicketFeeCCY = value
        End Set
    End Property

    Property TransFee() As Double
        Get
            Return varTransFee
        End Get
        Set(value As Double)
            varTransFee = value
        End Set
    End Property

    Property TransFeeCCY() As Double
        Get
            Return varTransFeeCCY
        End Get
        Set(value As Double)
            varTransFeeCCY = value
        End Set
    End Property

    Property FOPFee() As Double
        Get
            Return varFOPFee
        End Get
        Set(value As Double)
            varFOPFee = value
        End Set
    End Property

    Property FOPFeeCCY() As Double
        Get
            Return varFOPFeeCCY
        End Get
        Set(value As Double)
            varFOPFeeCCY = value
        End Set
    End Property

    Property CoverBrokerage() As Boolean
        Get
            Return varCoverBrokerage
        End Get
        Set(value As Boolean)
            varCoverBrokerage = value
        End Set
    End Property

    Property MinCharge() As Double
        Get
            Return varMinCharge
        End Get
        Set(value As Double)
            varMinCharge = value
        End Set
    End Property

    Property MaxCharge() As Double
        Get
            Return varMaxCharge
        End Get
        Set(value As Double)
            varMaxCharge = value
        End Set
    End Property
End Class
