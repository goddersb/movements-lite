﻿Public Class FrmOptions
    Private Sub FrmOptions_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dim UserOptConn As SqlClient.SqlConnection = ClsIMS.GetNewOpenConnection

        If IsNumeric(cboLocation.SelectedValue) Then
            ClsIMS.UserLocationCode = cboLocation.SelectedValue
            ClsIMS.UserLocation = cboLocation.Text
        End If

        ClsIMS.UserOptInactivity = CboOptInactivity.Text
        ClsIMS.UserOptRefresh = ChkOptRefresh.Checked

        If RadioButton1.Checked Then
            ClsIMS.UserOptGridSort = 0
        ElseIf RadioButton2.Checked Then
            ClsIMS.UserOptGridSort = 1
        End If

        Dim varSQL As String = "update vwDolfinPaymentUsers set U_OptAutoRefresh = " & IIf(ChkOptRefresh.Checked, 1, 0) & ",U_OptInactivity = " & CboOptInactivity.Text & ",U_OptGridSort = " & ClsIMS.UserOptGridSort & " where U_Username = '" & ClsIMS.FullUserName & "'"
        ClsIMS.ExecuteString(UserOptConn, varSQL)
        If UserOptConn.State = ConnectionState.Open Then
            UserOptConn.Close()
        End If
    End Sub

    Private Sub FrmOptions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateComboboxWithData(cboLocation, "select Branchcode,BranchLocation from vwDolfinPaymentBranches group by Branchcode,BranchLocation")
        cboLocation.SelectedValue = ClsIMS.UserLocationCode
        cboLocation.Text = ClsIMS.UserLocation

        CboOptInactivity.Items.Add("10")
        CboOptInactivity.Items.Add("15")
        CboOptInactivity.Items.Add("30")
        CboOptInactivity.Items.Add("60")
        CboOptInactivity.Items.Add("120")
        CboOptInactivity.Items.Add("600")

        CboOptInactivity.Text = ClsIMS.UserOptInActivity
        ChkOptRefresh.Checked = ClsIMS.UserOptRefresh

        If ClsIMS.UserOptGridSort = 0 Then
            RadioButton1.Checked = True
        ElseIf ClsIMS.UserOptGridSort = 1 Then
            RadioButton2.Checked = True
        End If
    End Sub

    Private Sub cboLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboLocation.SelectedIndexChanged
        If IsNumeric(cboLocation.SelectedValue) Then
            ClsIMS.UserLocationCode = cboLocation.SelectedValue
            ClsIMS.UserLocation = cboLocation.Text
        End If
    End Sub
End Class