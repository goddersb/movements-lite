﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml

Public Class FrmTRS

    Dim _dicTrsReportableTrades As Dictionary(Of String, String)
    Dim _dicTrsXmlExtract As Dictionary(Of String, String)
    Dim _dicTrsInsertFile As Dictionary(Of String, String)
    Dim _dicTrsInsertFileRec As Dictionary(Of String, String)
    Private _dicTrsDeleteFile As Dictionary(Of String, String)
    Dim _dstTrsDetails As New DataSet
    Dim _cboReportableColumn As DataGridViewComboBoxColumn
    Dim _cboTrader As DataGridViewComboBoxColumn
    Dim _cboInvDescPerson As DataGridViewComboBoxColumn
    Dim _btnRefreshRow As DataGridViewButtonColumn
    Dim _dgvdata As SqlDataAdapter
    Dim _conIMSPlus As SqlConnection
    Dim _blnFormLoaded As Boolean = False
    Dim _blnBypassRefresh As Boolean = False
    Dim _strLEI As String
    Dim _strXML As String
    Dim _intSequenceValue As Integer
    Dim _actualSequencenumber As Integer
    Dim _clsIMSData As ClsIMSData
    Dim _rowIndex As Integer
    Dim _refreshed As Boolean

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _clsIMSData = New ClsIMSData

    End Sub

    Private Sub FrmTRS_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            dtpReportDate.Value = SetReportDate() 'Date.Now 'New DateTime(2020, 2, 13)
            ModRMS.DoubleBuffered(dgvTrs, True)

            'Generate the Trs reportable data in the table _Dolfin_TrsTrades.
            _dicTrsReportableTrades = New Dictionary(Of String, String)
            _dicTrsReportableTrades.Add("ReportDate", dtpReportDate.Value.ToString)
            _dicTrsReportableTrades.Add("UserName", Environment.UserName)
            _dicTrsReportableTrades.Add("DocNo", String.Empty)
            _dicTrsReportableTrades.Add("BranchNo", ClsIMS.UserLocationCode)
            GenerateReportableTrades(_dicTrsReportableTrades)

            'Load the data grid with the Trs details.
            LoadGrid()

            'Set the form loaded as true.
            _blnFormLoaded = True

        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description} - error loading the trs details form.")
        End Try

    End Sub

    ''' <summary>
    ''' Set the Report date as the previous business day.
    ''' </summary>
    ''' <returns></returns>
    Private Function SetReportDate() As String

        If DateAdd(DateInterval.Day, -1, Date.Now).DayOfWeek.ToString = "Saturday" Then
            Return DateAdd(DateInterval.Day, -2, Date.Now).ToString
        ElseIf DateAdd(DateInterval.Day, -1, Date.Now).DayOfWeek.ToString = "Sunday" Then
            Return DateAdd(DateInterval.Day, -3, Date.Now).ToString
        Else
            Return DateAdd(DateInterval.Day, -1, Date.Now).ToString
        End If

    End Function


    Private Sub GenerateReportableTrades(ByVal dictionary As Dictionary(Of String, String))

        Try
            Cursor = Cursors.WaitCursor
            _clsIMSData.GenerateReportableTrades(dictionary)
            Cursor = Cursors.Default
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description}- Problem With GenerateReportableTrades.")
        End Try

    End Sub

    Private Sub LoadGrid()

        Dim strColSql As String
        Dim _ds As New DataSet

        Try
            _refreshed = True

            Cursor = Cursors.WaitCursor
            dgvTrs.DataSource = Nothing
            _conIMSPlus = ClsIMS.GetNewOpenConnection

            'Is Reportable Combo Box.
            strColSql = "SELECT 'Y' UNION SELECT 'N'"
            _dgvdata = New SqlDataAdapter(strColSql, _conIMSPlus)
            _dgvdata.Fill(_ds, "IsReportable")
            _cboReportableColumn = New DataGridViewComboBoxColumn
            _cboReportableColumn.DataSource = Nothing
            _cboReportableColumn.Items.Clear()
            _cboReportableColumn.HeaderText = "Reportable"
            _cboReportableColumn.DataPropertyName = "IsReportable"
            _cboReportableColumn.DataSource = _ds.Tables("IsReportable")
            _cboReportableColumn.ValueMember = _ds.Tables("IsReportable").Columns(0).ColumnName
            _cboReportableColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            _cboReportableColumn.FlatStyle = FlatStyle.Flat
            _cboReportableColumn.Width = 60
            dgvTrs.Columns.Insert(0, _cboReportableColumn)

            _dstTrsDetails = _clsIMSData.GetTrsDetails()
            dgvTrs.AutoGenerateColumns = True
            dgvTrs.DataSource = _dstTrsDetails.Tables("Trs")
            dgvTrs.MultiSelect = False
            tbpTRS.Text = $"TRS Details - {IIf(_dstTrsDetails.Tables("Trs").Rows.Count > 0, _dstTrsDetails.Tables("Trs").Rows.Count, 0)} {IIf(_dstTrsDetails.Tables("Trs").Rows.Count = 1, "transaction", "transactions")} pending"

            'Trader Combo Box.
            'strColSql = $"SELECT P_ID AS [Trader] FROM Persons WHERE P_IsTrader = 1 "
            strColSql = "SELECT	dbo._Dolfin_fnProperCase(CONCAT(P_FirstName, ' ', P_LastName)) As [Trader] FROM	Persons	WHERE P_IsTrader = 1"
            _dgvdata = New SqlDataAdapter(strColSql, _conIMSPlus)
            _dgvdata.Fill(_ds, "Trader")
            _cboTrader = New DataGridViewComboBoxColumn
            _cboTrader.DataSource = Nothing
            _cboTrader.HeaderText = "Trader"
            _cboTrader.DataPropertyName = "Trader"
            _cboTrader.DataSource = _ds.Tables("Trader")
            _cboTrader.ValueMember = _ds.Tables("Trader").Columns(0).ColumnName
            _cboTrader.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            _cboTrader.FlatStyle = FlatStyle.Flat
            _cboTrader.Width = 120
            dgvTrs.Columns.Insert(10, _cboTrader)

            'Investment Descision Person Combo Box.
            strColSql = $"SELECT '' AS [Investment Decision Person] UNION SELECT dbo._Dolfin_fnProperCase(CONCAT(P_FirstName, ' ', P_LastName)) FROM Persons WHERE P_ID IN (1005, 1011)"
            _dgvdata = New SqlDataAdapter(strColSql, _conIMSPlus)
            _dgvdata.Fill(_ds, "Investment Decision Person")
            _cboInvDescPerson = New DataGridViewComboBoxColumn
            _cboInvDescPerson.DataSource = Nothing
            _cboInvDescPerson.HeaderText = "Investment Decision Person"
            _cboInvDescPerson.DataPropertyName = "Investment Decision Person"
            _cboInvDescPerson.DataSource = _ds.Tables("Investment Decision Person")
            _cboInvDescPerson.ValueMember = _ds.Tables("Investment Decision Person").Columns(0).ColumnName
            _cboInvDescPerson.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            _cboInvDescPerson.FlatStyle = FlatStyle.Flat
            _cboInvDescPerson.Width = 120
            dgvTrs.Columns.Insert(11, _cboInvDescPerson)

            dgvTrs.Columns("Id").Visible = False
            dgvTrs.Columns("Trader").Visible = False
            dgvTrs.Columns("InvstmtDcsnPrsn").Visible = False
            dgvTrs.Columns("Investment Decision Person").Visible = False
            dgvTrs.Columns("TraderId").Visible = False
            dgvTrs.Columns("Portfolio").Width = 320
            dgvTrs.Columns("SubmissionType").Width = 70
            dgvTrs.Columns("SubmissionType").HeaderText = "Submission Type"
            dgvTrs.Columns("Buy/Sell").Visible = False
            dgvTrs.Columns("Document No").Width = 80
            dgvTrs.Columns("Instrument Name").Width = 150
            dgvTrs.Columns("TradeDateTime").Width = 80
            dgvTrs.Columns("TradeDateTime").HeaderText = "Trade Date"
            dgvTrs.Columns("TradeDateTime").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvTrs.Columns("Executing Entity").Width = 160
            dgvTrs.Columns("Submitting Entity").Width = 160
            dgvTrs.Columns("Buyer Client Code").Width = 160
            dgvTrs.Columns("Buyer Type").Width = 60
            dgvTrs.Columns("Buyer Decision Maker").Width = 160
            dgvTrs.Columns("Seller Decision Maker").Width = 160
            dgvTrs.Columns("Seller Client Code").Width = 160
            dgvTrs.Columns("Seller Type").Width = 60
            dgvTrs.Columns("Quantity").Width = 60
            dgvTrs.Columns("Quantity").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTrs.Columns("Quantity Ccy").Width = 60
            dgvTrs.Columns("Price Ccy").Width = 60
            dgvTrs.Columns("Price Money").Width = 70
            dgvTrs.Columns("Price Money").DefaultCellStyle.Format = "n2"
            dgvTrs.Columns("Price Money").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTrs.Columns("Price %").Width = 70
            dgvTrs.Columns("Price %").DefaultCellStyle.Format = "n2"
            dgvTrs.Columns("Price %").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTrs.Columns("Net Amount").Width = 80
            dgvTrs.Columns("Net Amount").DefaultCellStyle.Format = "n2"
            dgvTrs.Columns("Net Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            SetGridReadOnly(dgvTrs)

            _refreshed = False
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description}- Problem With viewing TRS details grid.")
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub SetGridReadOnly(ByVal _grid As DataGridView)

        Dim _column As DataGridViewColumn
        Try
            For Each _column In _grid.Columns
                If _column.Index = 0 Or _column.Index = 10 Or _column.Index = 11 Then
                    _column.ReadOnly = False
                Else
                    _column.ReadOnly = True
                End If
            Next
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description}- Problem With setting fields as read only.")
        End Try

    End Sub

    Private Sub dgvTrs_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTrs.RowLeave

        Dim intTrsRecId As Integer
        Dim intInvDescPerson As Integer
        Dim strManageType As String
        Dim strIsReportable As String
        Dim strTraderName As String
        Dim strInvDescPersonName As String
        Dim strSQL As String
        Dim _docNo As String = String.Empty

        Try
            If dgvTrs.EndEdit And _blnFormLoaded = True Then
                intTrsRecId = dgvTrs.Rows(e.RowIndex).Cells("Id").Value
                strTraderName = dgvTrs.Rows(e.RowIndex).Cells("Trader").Value
                _docNo = dgvTrs.Rows(e.RowIndex).Cells("Document No").Value
                strManageType = IIf(IsDBNull(dgvTrs.Rows(e.RowIndex).Cells("ManageType").Value), String.Empty, dgvTrs.Rows(e.RowIndex).Cells("ManageType").Value)
                intInvDescPerson = IIf(IsDBNull(dgvTrs.Rows(e.RowIndex).Cells("InvstmtDcsnPrsn").Value), 0, dgvTrs.Rows(e.RowIndex).Cells("InvstmtDcsnPrsn").Value)
                strInvDescPersonName = dgvTrs.Rows(e.RowIndex).Cells(11).Value
                strIsReportable = dgvTrs.Rows(e.RowIndex).Cells(0).Value

                If Not IsDBNull(intTrsRecId) And intTrsRecId <> 0 Then
                    If strManageType <> "Execution" Then
                        strSQL = $"UPDATE IMSPlus.dbo._Dolfin_TrsTrades SET Trader = (SELECT P_Id FROM persons WHERE dbo._Dolfin_fnProperCase(CONCAT(P_FirstName, ' ', P_LastName)) ='{strTraderName}')," &
                                $"TraderName = '{strTraderName}', InvstmtDcsnPrsnName = '{strInvDescPersonName}', " &
                                $"InvstmtDcsnPrsn = {intInvDescPerson} WHERE TrsRecID = {intTrsRecId}"
                    Else
                        strSQL = $"UPDATE IMSPlus.dbo._Dolfin_TrsTrades SET Trader = (SELECT P_Id FROM persons WHERE dbo._Dolfin_fnProperCase(CONCAT(P_FirstName, ' ', P_LastName)) ='{strTraderName}')," &
                                $"TraderName = '{strTraderName}' " &
                                $"WHERE TrsRecID = {intTrsRecId}"
                    End If
                    ClsIMS.ExecuteString(_conIMSPlus, strSQL)
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTrsReport, strSQL)
                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description} - Problem with updating TRS record for: {intTrsRecId}.")
        End Try

    End Sub

    Private Sub dgvTrs_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvTrs.RowHeaderMouseDoubleClick

        Dim strManageType As String
        Dim strIsReportable As String
        Dim strInvstmtDcsnPrsn As String
        Dim strMessage As String = ""
        Dim strTrader As String
        Dim strBuyerClientCode As String
        Dim strSellerClientCode As String

        Try
            strIsReportable = dgvTrs.Rows(e.RowIndex).Cells(1).Value
            If strIsReportable = "N" Then
                strManageType = IIf(IsDBNull(dgvTrs.Rows(e.RowIndex).Cells("ManageType").Value), vbNull, dgvTrs.Rows(e.RowIndex).Cells("ManageType").Value)
                strInvstmtDcsnPrsn = IIf(IsDBNull(dgvTrs.Rows(e.RowIndex).Cells("InvstmtDcsnPrsn").Value), String.Empty, dgvTrs.Rows(e.RowIndex).Cells("InvstmtDcsnPrsn").Value)
                strTrader = IIf(IsDBNull(dgvTrs.Rows(e.RowIndex).Cells("Trader").Value), String.Empty, dgvTrs.Rows(e.RowIndex).Cells("Trader").Value)
                strBuyerClientCode = IIf(IsDBNull(dgvTrs.Rows(e.RowIndex).Cells("Buyer Client Code").Value), String.Empty, dgvTrs.Rows(e.RowIndex).Cells("Buyer Client Code").Value)
                strSellerClientCode = IIf(IsDBNull(dgvTrs.Rows(e.RowIndex).Cells("Seller Client Code").Value), String.Empty, dgvTrs.Rows(e.RowIndex).Cells("Seller Client Code").Value)

                If (strManageType.Contains("Mandatory") Or strManageType.Contains("Discretionary")) And String.IsNullOrEmpty(strInvstmtDcsnPrsn) Then
                    strMessage = $"An Investment Designated Person must be supplied For a {strManageType} Management Type!"
                End If

                If String.IsNullOrEmpty(strTrader) Then
                    String.Concat(strMessage, $"{IIf(Not String.IsNullOrEmpty(strMessage), Chr(13), "")} Trader details are required in order to submit the file.")
                End If

                If Not String.IsNullOrEmpty(strMessage) Then
                    MsgBox(strMessage, MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Record not reportable alert")
                End If

            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, Err.Description & $" - Problem with viewing the not reportable warning message.")
        End Try

    End Sub

    Private Sub dgvTrs_CellContentClick(sender As System.Object, e As DataGridViewCellEventArgs) Handles dgvTrs.CellContentClick

        Dim stringDocNo As String
        Dim senderGrid = DirectCast(sender, DataGridView)
        Dim intResponse As Integer

        Try
            If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso
                    e.RowIndex >= 0 Then
                intResponse = OkayToRefresh()
                If intResponse = vbYes Then
                    stringDocNo = dgvTrs.Rows(e.RowIndex).Cells("Document No").Value
                    _dicTrsReportableTrades = New Dictionary(Of String, String)
                    _dicTrsReportableTrades.Add("ReportDate", dtpReportDate.Value.ToString)
                    _dicTrsReportableTrades.Add("UserName", Environment.UserName)
                    _dicTrsReportableTrades.Add("DocNo", stringDocNo)
                    _dicTrsReportableTrades.Add("BranchNo", ClsIMS.UserLocationCode)
                    GenerateReportableTrades(_dicTrsReportableTrades)

                    'Load the data grid with the Trs details.
                    LoadGrid()

                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, Err.Description & $" - Problem with refreshing the trs grid view for Doc No {stringDocNo}.")
        End Try

    End Sub

    ''' <summary>
    ''' Process any transactions that are to be set as non reportable.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub dgvTrs_CurrentCellDirtyStateChanged(sender As Object, e As EventArgs) Handles dgvTrs.CurrentCellDirtyStateChanged

        Dim _docNo As String = String.Empty
        Try
            If dgvTrs.CurrentCell.ColumnIndex = 0 Then
                Dim newValue As String = dgvTrs.CurrentCell.EditedFormattedValue
                If newValue = "N" And _refreshed = False And dgvTrs.IsCurrentCellDirty Then
                    _rowIndex = dgvTrs.CurrentCell.RowIndex
                    Dim _strSQL As String = String.Empty
                    _docNo = dgvTrs.Rows(_rowIndex).Cells("Document No").Value
                    Dim _trsRecId As Integer = dgvTrs.Rows(_rowIndex).Cells("Id").Value
                    Dim _response As Integer = MsgBox($"Are you sure you want to mark the record for DocNo {_docNo} as unreportable?", vbYesNo + vbQuestion, "Are you sure")
                    If _response = vbYes Then
                        _strSQL = $"UPDATE IMSPlus.dbo._Dolfin_TrsTrades SET IsReportable = 'N' WHERE TrsRecID = {_trsRecId}"
                        ClsIMS.ExecuteString(_conIMSPlus, _strSQL)

                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTrsReport, $"The DocNo {_docNo} has been set as unreportable. TrsRecID {_trsRecId}.")

                        InsertNonReportableDummyFile(_docNo)
                        btnRefreshGrid_Click(sender, e)
                    Else
                        dgvTrs.CurrentCell.Value = "Y"
                        SendKeys.Send("{ESC}")
                    End If
                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, Err.Description & $" - Problem with setting the Is Reportable value for Doc No {_docNo}.")
        End Try

    End Sub

    ''' <summary>
    ''' Insert the transaction details into the table _Dolfin_TrsFileRecMaltaNewFile with the default NonReportable file Id.
    ''' </summary>
    ''' <param name="_docNo"></param>
    Private Sub InsertNonReportableDummyFile(ByVal _docNo As String)

        Try
            'Insert the file details into the table _Dolfin_TrsFileRec.
            _dicTrsInsertFileRec = New Dictionary(Of String, String)
            _dicTrsInsertFileRec.Add("FileID", ClsIMS.FetchTRSFileId)
            _dicTrsInsertFileRec.Add("DocNo", _docNo)
            _clsIMSData.InsertTrsFileRec(_dicTrsInsertFileRec)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, Err.Description & $" - Problem inserting the non reportable transaction detials for Doc No {_docNo}.")
        End Try

    End Sub

    Private Sub btnRefreshGrid_Click(sender As Object, e As EventArgs) Handles btnRefreshGrid.Click

        Dim intResponse As Integer

        Try
            intResponse = OkayToRefresh()
            If intResponse = vbYes Then

                'Generate the Trs reportable data in the table _Dolfin_TrsTrades.
                _dicTrsReportableTrades = New Dictionary(Of String, String)
                _dicTrsReportableTrades.Add("ReportDate", dtpReportDate.Value.ToString)
                _dicTrsReportableTrades.Add("UserName", Environment.UserName)
                _dicTrsReportableTrades.Add("DocNo", String.Empty)
                _dicTrsReportableTrades.Add("BranchNo", ClsIMS.UserLocationCode)
                GenerateReportableTrades(_dicTrsReportableTrades)

                'Load the data grid with the Trs details.
                LoadGrid()

            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, Err.Description & $" - Problem with refreshing the trs grid view.")
        End Try

    End Sub

    Private Function OkayToRefresh() As Integer

        If _blnBypassRefresh = False And dgvTrs.Rows.Count > 0 Then
            Return MsgBox($"Refreshing the grid will revert all values back to their original, any changes that you made will be lost.  " &
                      $"Please confirm if you wish to proceed or cancel this refresh.", MsgBoxStyle.Information + MsgBoxStyle.YesNo, "Are you sure?")
        Else
            _blnBypassRefresh = False
            Return 6
        End If

    End Function

    Private Sub tcrTRS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tcrTRS.SelectedIndexChanged

        Dim indexOfSelectedTab As Integer = tcrTRS.SelectedIndex
        Dim tbpSelectedTab As TabPage = tcrTRS.SelectedTab

        Try
            If tbpSelectedTab.Text.Contains("TRS XML Preview") Then 'XML Preview tab.

                'Generate the File Name.
                txtFileName.Text = SetTrsFileName()

                'Load the XML Preview.
                LoadXMLPreview()

            Else 'Nothing here as of yet!

            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $" - Problem in tcrTRS_SelectedIndexChanged. Error message - {ex.Message}.")
        End Try

    End Sub

    Private Sub LoadXMLPreview()

        Try
            Cursor = Cursors.WaitCursor

            'Execute the stored procedure _DolfinPaymentsGetTrsXml to generate the XML File.
            _dicTrsXmlExtract = New Dictionary(Of String, String)
            _dicTrsXmlExtract.Add("LEI", _strLEI)
            _dicTrsXmlExtract.Add("FileName", txtFileName.Text)
            _strXML = _clsIMSData.GetTrsXmlExtract(_dicTrsXmlExtract)

            If _strXML.Contains("TxId") Then
                rtbXml.Text = DisplayFormattedXML(_strXML)
                txtFileName.Enabled = True
                btnSubmit.Enabled = True
                btnSetFileName.Enabled = True
            Else
                rtbXml.Text = "No current submissions located"
                txtFileName.Enabled = False
                btnSubmit.Enabled = False
                btnSetFileName.Enabled = False
            End If

        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $" - Problem in LoadXMLPreview. Error message - {ex.Message}.")
        Finally
            Cursor = Cursors.Default
        End Try

    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        Dim intResponse As Integer
        Dim xmdDocument As New XmlDocument
        Dim intNewId As Integer = 0
        Dim _Id As Integer

        Try
            intResponse = MsgBox($"Please confirm that you wish to proceed with the generation of the TRS Report - {txtFileName.Text}", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Are you sure?")
            If intResponse = vbYes Then
                sfdXml.FileName = txtFileName.Text
                sfdXml.Filter = "XML|*.xml"
                If sfdXml.ShowDialog() <> DialogResult.Cancel Then

                    _Id = Math.Abs(CInt((txtFileName.Text.Replace("RMSM_DATTRA_MT_", "").Replace(String.Concat("_", Strings.Right(CStr(Date.Now.Year), 2), ".xml"), ""))))

                    'Insert the file details into the table _Dolfin_TrsFile and return the new ID.
                    '_dicTrsInsertFile = New Dictionary(Of String, String)
                    '_dicTrsInsertFile.Add("FileName", txtCurrentFileName.Text)
                    '_dicTrsInsertFile.Add("FileSequenceNumber", _Id)
                    'intNewId = _clsIMSData.InsertTrsFile(_dicTrsInsertFile)

                    intNewId = _clsIMSData.FetchTRSCurrentFileId()

                    If intNewId <> 0 Then
                        'Insert the file details into the table _Dolfin_TrsFileRec.
                        _dicTrsInsertFileRec = New Dictionary(Of String, String)
                        _dicTrsInsertFileRec.Add("FileID", intNewId)
                        _dicTrsInsertFileRec.Add("SubmittedBy", Environment.UserName)
                        _clsIMSData.InsertTrsFileRec(_dicTrsInsertFileRec)

                        xmdDocument.LoadXml(_strXML)
                        xmdDocument.Save(sfdXml.FileName)

                        'Now we set the focus to the trs details tab.
                        tcrTRS.SelectedIndex = 0
                        _blnBypassRefresh = True
                        btnRefreshGrid_Click(sender, e)

                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTrsReport, $" Trs File details file saved to - {sfdXml.FileName}.  TRS XML file saved.")
                    Else
                        ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $" An error occured whilst inserting the Trs File details for - {txtFileName.Text}.  No valid New Id found.")
                    End If
                End If
            End If

        Catch ex As XmlException
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $" - Problem in btnSubmit_Click. Error message - {ex.Message}.")
        End Try

    End Sub

    Private Function DisplayFormattedXML(ByVal strXml As String) As String

        Dim strResponse As String
        Try
            'Format the displayed XML.
            Dim xmlDoc As New XmlDocument
            xmlDoc.LoadXml(strXml)
            Dim swrXML As New StringWriter()
            Dim xtwXML = New XmlTextWriter(swrXML)
            xtwXML.Formatting = Formatting.Indented
            xtwXML.Indentation = 4
            xmlDoc.WriteTo(xtwXML)
            strResponse = swrXML.ToString()
            Return strResponse

        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $" - Problem in DisplayFormattedXML. Error message - {ex.Message}.")
            Return String.Empty
        End Try

    End Function

    Private Function SetTrsFileName(Optional ByVal sequenceNumber As Integer = 0) As String

        Dim strFileName As String
        Dim clsIMSData As New ClsIMSData
        Dim strSQL As String
        'Dim intPadLength As Integer
        'Dim strPadString As String = ""

        Try
            'Construct the SQL.
            strSQL = "SELECT COALESCE(MAX(FileSequenceNumber), 0) AS [FileSequenceNumber] FROM _Dolfin_TrsFileMaltaNewFile WHERE 1 = 1 AND DATEPART(YEAR, [CreateDate]) = DATEPART(YEAR, GETDATE())"

            'Get the current Max File Sequence Number.
            _intSequenceValue = IIf(sequenceNumber = 0, clsIMSData.GetFileSequenceNumber(strSQL), sequenceNumber)

            ''Pad the string based on the Sequence number.
            'intPadLength = 6 - Len(_intSequenceValue.ToString)
            'strPadString = String.Concat(strPadString.PadRight(intPadLength, "0"), _intSequenceValue.ToString)

            'Create the file name.
            strFileName = CreateFileName(_intSequenceValue)

            Return strFileName

        Catch ex As Exception
            clsIMSData = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $" - Problem generating the TRS XML file name. Error message - {ex.Message}.")
            Return String.Empty
        End Try

    End Function

    Private Sub txtFileName_LostFocus(sender As Object, e As EventArgs) Handles txtFileName.LostFocus

        Dim intSequenceNumber As Integer
        Dim intPadLength As Integer
        Dim intPadLengthError As Integer
        Dim strPadString As String = String.Empty
        Dim strPadStringError As String = String.Empty
        Try
            If txtFileName.Text <> SetTrsFileName() Then

                intSequenceNumber = _intSequenceValue
                _actualSequencenumber = _intSequenceValue

                'Fetch the new sequence number as the file name has changed!
                _intSequenceValue = Math.Abs(CInt((txtFileName.Text.Replace("RMSM_DATTRA_MT_", "").Replace(String.Concat("_", Strings.Right(CStr(Date.Now.Year), 2), ".xml"), ""))))

                intPadLength = 6 - Len(_intSequenceValue.ToString)
                strPadString = String.Concat(strPadString.PadRight(intPadLength, "0"), _intSequenceValue.ToString)

                _actualSequencenumber = _actualSequencenumber - 2
                intPadLengthError = 6 - Len(_actualSequencenumber.ToString)
                strPadStringError = String.Concat(strPadStringError.PadRight(intPadLengthError, "0"), _actualSequencenumber.ToString)

                If _intSequenceValue < intSequenceNumber - 2 Then
                    MsgBox($"The sequeuce number {strPadString} cannot be less than {strPadStringError}, please select a value equal or greater than {strPadStringError}",
                           MsgBoxStyle.Information + MsgBoxStyle.OkOnly)
                    txtFileName.Text = SetTrsFileName()
                Else
                    txtFileName.Text = SetTrsFileName(_intSequenceValue)
                End If

            End If

            'Reload the preview.
            LoadXMLPreview()

            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTrsReport, $"Successfully modified the file sequence number from {intSequenceNumber} to {_intSequenceValue}")
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $" - Error attempting to set the file name - {ex.Message}.")
        End Try

    End Sub

    Private Sub btnSetFileName_Click(sender As Object, e As EventArgs) Handles btnSetFileName.Click

        Dim intNewId As Integer = 0

        Try
            'Delete any Files that may have been created but do not contain transactions.
            _dicTrsDeleteFile = New Dictionary(Of String, String)
            _dicTrsDeleteFile.Add("FileID", intNewId)
            _clsIMSData.DeleteOldTrsFile(_dicTrsDeleteFile)

            'Insert the file details into the table _Dolfin_TrsFile and return the new ID.
            _dicTrsInsertFile = New Dictionary(Of String, String)
            _dicTrsInsertFile.Add("FileName", CreateFileName(_intSequenceValue - 1))
            _dicTrsInsertFile.Add("FileSequenceNumber", _intSequenceValue - 1)
            intNewId = _clsIMSData.InsertTrsFile(_dicTrsInsertFile)

            MsgBox($"TRS file name has been set as {txtFileName.Text} with the File Id {intNewId}.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Set TRS File Name")

            'Fetch the Next valid file name.
            txtFileName.Text = SetTrsFileName()

            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTrsReport, $"Successfully set the current file name to {txtFileName.Text}")
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $" - Error attempting to set the file name - {ex.Message}.")
        End Try


    End Sub

    Private Function CreateFileName(ByVal _sequenceNumber As Integer)

        Dim strPadString As String = String.Empty
        Dim intPadLength As Integer
        Dim strFileName As String = String.Empty

        Try
            intPadLength = 6 - Len(_intSequenceValue.ToString)
            strPadString = String.Concat(strPadString.PadRight(intPadLength, "0"), _sequenceNumber.ToString)
            Return $"RMSM_DATTRA_MT_{strPadString}_{Strings.Right(CStr(Date.Now.Year), 2)}.xml"
        Catch ex As Exception

        End Try

    End Function

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click

        Cursor = Cursors.WaitCursor
        ExprtGridToExcelFrmForm(dgvTrs, 0)
        Cursor = Cursors.Default

    End Sub

    Private Sub dgvTrs_DataBindingComplete(sender As Object, e As DataGridViewBindingCompleteEventArgs) Handles dgvTrs.DataBindingComplete

        'Assign the LEI value to the variable _strLEI.
        If dgvTrs.Rows.Count > 0 Then
            _strLEI = IIf(dgvTrs.Rows.Count > 0, dgvTrs.Rows(0).Cells("Executing Entity").Value, String.Empty)
        Else
            _strLEI = String.Empty
        End If

    End Sub

    Private Sub btnGenerateXmlFile_Click(sender As Object, e As EventArgs) Handles btnGenerateXmlFile.Click

        Try
            NewTRSFileManagement = New FrmTrsFileManagement
            NewTRSFileManagement.ShowDialog()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, $"{Err.Description}- Problem With btnGenerateXmlFile_Click.")
        End Try

    End Sub

    Private Sub dgvTrs_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvTrs.DataError

        ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTrsReport, e.Exception.ToString & $" - Problem with refreshing the trs grid view.")

    End Sub

    Private Sub FrmTRS_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed

        _clsIMSData = Nothing
        If _conIMSPlus.State = ConnectionState.Open Then
            _conIMSPlus.Close()
        End If

    End Sub

End Class