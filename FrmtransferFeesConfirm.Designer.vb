﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmtransferFeesConfirm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Rb1 = New System.Windows.Forms.RadioButton()
        Me.Rb2 = New System.Windows.Forms.RadioButton()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.Rb3 = New System.Windows.Forms.RadioButton()
        Me.LblMsg = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblrateccy = New System.Windows.Forms.Label()
        Me.txtrate = New System.Windows.Forms.TextBox()
        Me.lblrate = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtTFCOrderRemainingBalance = New System.Windows.Forms.TextBox()
        Me.txtTFCOrderFee = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTFCOrderFeeEx = New System.Windows.Forms.TextBox()
        Me.txtTFCOrderPortfolio = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboTFCOrderAccount = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTFCOrderBalance = New System.Windows.Forms.TextBox()
        Me.txtTFCOrderCCY = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cboTFCBenPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboTFCBenAccount = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTFCBenBalance = New System.Windows.Forms.TextBox()
        Me.txtTFCBenCCY = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ChkTFIMS = New System.Windows.Forms.CheckBox()
        Me.ChkTFSwift = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(107, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(229, 24)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Transfer Fees Options?"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Rb1)
        Me.GroupBox1.Controls.Add(Me.Rb2)
        Me.GroupBox1.Controls.Add(Me.cmdContinue)
        Me.GroupBox1.Controls.Add(Me.Rb3)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 466)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(431, 147)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Options for transfer fee"
        '
        'Rb1
        '
        Me.Rb1.AutoSize = True
        Me.Rb1.Location = New System.Drawing.Point(15, 28)
        Me.Rb1.Name = "Rb1"
        Me.Rb1.Size = New System.Drawing.Size(60, 17)
        Me.Rb1.TabIndex = 0
        Me.Rb1.TabStop = True
        Me.Rb1.Text = "option1"
        Me.Rb1.UseVisualStyleBackColor = True
        '
        'Rb2
        '
        Me.Rb2.AutoSize = True
        Me.Rb2.Location = New System.Drawing.Point(15, 51)
        Me.Rb2.Name = "Rb2"
        Me.Rb2.Size = New System.Drawing.Size(60, 17)
        Me.Rb2.TabIndex = 1
        Me.Rb2.TabStop = True
        Me.Rb2.Text = "option2"
        Me.Rb2.UseVisualStyleBackColor = True
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(6, 115)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(419, 23)
        Me.cmdContinue.TabIndex = 9
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'Rb3
        '
        Me.Rb3.AutoSize = True
        Me.Rb3.Location = New System.Drawing.Point(15, 74)
        Me.Rb3.Name = "Rb3"
        Me.Rb3.Size = New System.Drawing.Size(60, 17)
        Me.Rb3.TabIndex = 5
        Me.Rb3.TabStop = True
        Me.Rb3.Text = "option3"
        Me.Rb3.UseVisualStyleBackColor = True
        '
        'LblMsg
        '
        Me.LblMsg.AutoSize = True
        Me.LblMsg.Location = New System.Drawing.Point(12, 22)
        Me.LblMsg.Name = "LblMsg"
        Me.LblMsg.Size = New System.Drawing.Size(379, 13)
        Me.LblMsg.TabIndex = 13
        Me.LblMsg.Text = "Transfer fee account has been found for this Portfolio to extract the transfer fe" &
    "e"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblrateccy)
        Me.GroupBox2.Controls.Add(Me.txtrate)
        Me.GroupBox2.Controls.Add(Me.lblrate)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtTFCOrderRemainingBalance)
        Me.GroupBox2.Controls.Add(Me.txtTFCOrderFee)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtTFCOrderFeeEx)
        Me.GroupBox2.Controls.Add(Me.txtTFCOrderPortfolio)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.cboTFCOrderAccount)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtTFCOrderBalance)
        Me.GroupBox2.Controls.Add(Me.txtTFCOrderCCY)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.LblMsg)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 117)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(431, 206)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Reconmended/Selected Account"
        '
        'lblrateccy
        '
        Me.lblrateccy.AutoSize = True
        Me.lblrateccy.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblrateccy.Location = New System.Drawing.Point(190, 129)
        Me.lblrateccy.Name = "lblrateccy"
        Me.lblrateccy.Size = New System.Drawing.Size(64, 15)
        Me.lblrateccy.TabIndex = 31
        Me.lblrateccy.Text = "GBPUSD"
        Me.lblrateccy.Visible = False
        '
        'txtrate
        '
        Me.txtrate.Location = New System.Drawing.Point(68, 126)
        Me.txtrate.Name = "txtrate"
        Me.txtrate.Size = New System.Drawing.Size(116, 20)
        Me.txtrate.TabIndex = 30
        Me.txtrate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblrate
        '
        Me.lblrate.AutoSize = True
        Me.lblrate.Location = New System.Drawing.Point(14, 129)
        Me.lblrate.Name = "lblrate"
        Me.lblrate.Size = New System.Drawing.Size(48, 13)
        Me.lblrate.TabIndex = 28
        Me.lblrate.Text = "Ex Rate:"
        Me.lblrate.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(138, 179)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(166, 13)
        Me.Label12.TabIndex = 27
        Me.Label12.Text = "Remaining Balance (inc pending):"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(279, 129)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(28, 13)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "Fee:"
        '
        'txtTFCOrderRemainingBalance
        '
        Me.txtTFCOrderRemainingBalance.Enabled = False
        Me.txtTFCOrderRemainingBalance.Location = New System.Drawing.Point(310, 176)
        Me.txtTFCOrderRemainingBalance.Name = "txtTFCOrderRemainingBalance"
        Me.txtTFCOrderRemainingBalance.Size = New System.Drawing.Size(106, 20)
        Me.txtTFCOrderRemainingBalance.TabIndex = 25
        Me.txtTFCOrderRemainingBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTFCOrderFee
        '
        Me.txtTFCOrderFee.Enabled = False
        Me.txtTFCOrderFee.Location = New System.Drawing.Point(310, 126)
        Me.txtTFCOrderFee.Name = "txtTFCOrderFee"
        Me.txtTFCOrderFee.Size = New System.Drawing.Size(106, 20)
        Me.txtTFCOrderFee.TabIndex = 24
        Me.txtTFCOrderFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(163, 153)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(141, 13)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Amount after exchange rate:"
        '
        'txtTFCOrderFeeEx
        '
        Me.txtTFCOrderFeeEx.Enabled = False
        Me.txtTFCOrderFeeEx.Location = New System.Drawing.Point(310, 150)
        Me.txtTFCOrderFeeEx.Name = "txtTFCOrderFeeEx"
        Me.txtTFCOrderFeeEx.Size = New System.Drawing.Size(106, 20)
        Me.txtTFCOrderFeeEx.TabIndex = 22
        Me.txtTFCOrderFeeEx.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTFCOrderPortfolio
        '
        Me.txtTFCOrderPortfolio.Enabled = False
        Me.txtTFCOrderPortfolio.Location = New System.Drawing.Point(68, 46)
        Me.txtTFCOrderPortfolio.Name = "txtTFCOrderPortfolio"
        Me.txtTFCOrderPortfolio.Size = New System.Drawing.Size(348, 20)
        Me.txtTFCOrderPortfolio.TabIndex = 21
        Me.txtTFCOrderPortfolio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Portfolio:"
        '
        'cboTFCOrderAccount
        '
        Me.cboTFCOrderAccount.FormattingEnabled = True
        Me.cboTFCOrderAccount.Location = New System.Drawing.Point(68, 73)
        Me.cboTFCOrderAccount.Name = "cboTFCOrderAccount"
        Me.cboTFCOrderAccount.Size = New System.Drawing.Size(348, 21)
        Me.cboTFCOrderAccount.TabIndex = 19
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(221, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Current Balance:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 103)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "CCY:"
        '
        'txtTFCOrderBalance
        '
        Me.txtTFCOrderBalance.Enabled = False
        Me.txtTFCOrderBalance.Location = New System.Drawing.Point(310, 100)
        Me.txtTFCOrderBalance.Name = "txtTFCOrderBalance"
        Me.txtTFCOrderBalance.Size = New System.Drawing.Size(106, 20)
        Me.txtTFCOrderBalance.TabIndex = 16
        Me.txtTFCOrderBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTFCOrderCCY
        '
        Me.txtTFCOrderCCY.Enabled = False
        Me.txtTFCOrderCCY.Location = New System.Drawing.Point(68, 100)
        Me.txtTFCOrderCCY.Name = "txtTFCOrderCCY"
        Me.txtTFCOrderCCY.Size = New System.Drawing.Size(65, 20)
        Me.txtTFCOrderCCY.TabIndex = 15
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 77)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Account:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboTFCBenPortfolio)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.cboTFCBenAccount)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.txtTFCBenBalance)
        Me.GroupBox3.Controls.Add(Me.txtTFCBenCCY)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 329)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(431, 131)
        Me.GroupBox3.TabIndex = 15
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Reconmended/Selected Account"
        '
        'cboTFCBenPortfolio
        '
        Me.cboTFCBenPortfolio.FormattingEnabled = True
        Me.cboTFCBenPortfolio.Location = New System.Drawing.Point(68, 47)
        Me.cboTFCBenPortfolio.Name = "cboTFCBenPortfolio"
        Me.cboTFCBenPortfolio.Size = New System.Drawing.Size(348, 21)
        Me.cboTFCBenPortfolio.TabIndex = 23
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(16, 51)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(48, 13)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Portfolio:"
        '
        'cboTFCBenAccount
        '
        Me.cboTFCBenAccount.FormattingEnabled = True
        Me.cboTFCBenAccount.Location = New System.Drawing.Point(68, 74)
        Me.cboTFCBenAccount.Name = "cboTFCBenAccount"
        Me.cboTFCBenAccount.Size = New System.Drawing.Size(348, 21)
        Me.cboTFCBenAccount.TabIndex = 19
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(156, 104)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Balance:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(31, 104)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(31, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "CCY:"
        '
        'txtTFCBenBalance
        '
        Me.txtTFCBenBalance.Enabled = False
        Me.txtTFCBenBalance.Location = New System.Drawing.Point(211, 101)
        Me.txtTFCBenBalance.Name = "txtTFCBenBalance"
        Me.txtTFCBenBalance.Size = New System.Drawing.Size(205, 20)
        Me.txtTFCBenBalance.TabIndex = 16
        Me.txtTFCBenBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTFCBenCCY
        '
        Me.txtTFCBenCCY.Enabled = False
        Me.txtTFCBenCCY.Location = New System.Drawing.Point(68, 101)
        Me.txtTFCBenCCY.Name = "txtTFCBenCCY"
        Me.txtTFCBenCCY.Size = New System.Drawing.Size(65, 20)
        Me.txtTFCBenCCY.TabIndex = 15
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(15, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Account:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(12, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(203, 13)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "RMS Transfer fee account to be paid into"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ChkTFIMS)
        Me.GroupBox4.Controls.Add(Me.ChkTFSwift)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 36)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(431, 75)
        Me.GroupBox4.TabIndex = 51
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Sending Options"
        '
        'ChkTFIMS
        '
        Me.ChkTFIMS.AutoSize = True
        Me.ChkTFIMS.Location = New System.Drawing.Point(343, 46)
        Me.ChkTFIMS.Name = "ChkTFIMS"
        Me.ChkTFIMS.Size = New System.Drawing.Size(73, 17)
        Me.ChkTFIMS.TabIndex = 41
        Me.ChkTFIMS.Text = "Send IMS"
        Me.ChkTFIMS.UseVisualStyleBackColor = True
        '
        'ChkTFSwift
        '
        Me.ChkTFSwift.AutoSize = True
        Me.ChkTFSwift.Location = New System.Drawing.Point(343, 21)
        Me.ChkTFSwift.Name = "ChkTFSwift"
        Me.ChkTFSwift.Size = New System.Drawing.Size(77, 17)
        Me.ChkTFSwift.TabIndex = 40
        Me.ChkTFSwift.Text = "Send Swift"
        Me.ChkTFSwift.UseVisualStyleBackColor = True
        '
        'FrmtransferFeesConfirm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(451, 631)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmtransferFeesConfirm"
        Me.Text = "Transfer Fee Options"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Rb1 As RadioButton
    Friend WithEvents Rb2 As RadioButton
    Friend WithEvents cmdContinue As Button
    Friend WithEvents Rb3 As RadioButton
    Friend WithEvents LblMsg As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cboTFCOrderAccount As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtTFCOrderBalance As TextBox
    Friend WithEvents txtTFCOrderCCY As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtTFCOrderPortfolio As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents cboTFCBenAccount As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtTFCBenBalance As TextBox
    Friend WithEvents txtTFCBenCCY As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtTFCOrderRemainingBalance As TextBox
    Friend WithEvents txtTFCOrderFee As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtTFCOrderFeeEx As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents lblrate As Label
    Friend WithEvents cboTFCBenPortfolio As ComboBox
    Friend WithEvents txtrate As TextBox
    Friend WithEvents lblrateccy As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents ChkTFIMS As CheckBox
    Friend WithEvents ChkTFSwift As CheckBox
End Class
