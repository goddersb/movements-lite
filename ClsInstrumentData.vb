﻿Public Class ClsInstrumentData
    Dim varTicker As String = ""
    Dim varRefSecurityClass As String = ""
    Dim varCCY As String = ""
    Dim _cpnCCY As String = ""
    Dim _tradeCCY As String = ""
    Dim varInPence As Boolean = False
    Dim varDescription As String = ""
    Dim varName As String = ""
    Dim varFundType As String = ""
    Dim varMarketSectorDescription As String = ""
    Dim varCountryISO As String = ""
    Dim varSecurityDescription As String = ""
    Dim varISIN As String = ""
    Dim varMIFIDIndicator As String = ""
    Dim varIssuer As String = ""
    Dim varPutable As String = ""
    Dim varCallable As String = ""
    Dim varConvertible As String = ""
    Dim varSinkable As String = ""
    Dim varCFICode As String = ""
    Dim varIssueDate As String = ""
    Dim varInitDate As String = ""
    Dim varMaturityDate As String = ""
    Dim varFutLastDate As String = ""
    Dim varCouponFrequency As String = ""
    Dim varInterest As String = ""
    Dim varCoupon As String = ""
    Dim varFaceValue As String = ""
    Dim varCusip As String = ""
    Dim varSedol As String = ""
    Dim varTickerAndExchangeCode As String = ""
    Dim varDayCount As String = ""
    Dim varCouponType As String = ""
    Dim varZeroCoupon As String = ""
    Dim varMinimumDenomination As String = ""
    Dim varMinimumTradingQuantity As String = ""
    Dim varRatingsMoodys As String = ""
    Dim varRatingsSP As String = ""
    Dim varRatingsFitch As String = ""
    Dim varFirstCouponDate As String = ""
    Dim varIndustry As String = ""
    Dim varAskPrice As String = ""
    Dim varBidPrice As String = ""
    Dim varOpenPrice As String = ""
    Dim varClosePrice As String = ""
    Dim varApproved As Boolean = True
    Dim varActive As Boolean = True
    Dim varListed As Boolean = True
    Dim varRecapitalised As Boolean = False
    Dim varFreefield1 As String = ""
    Dim varFreefield2 As String = ""
    Dim varFreefield3 As String = ""
    Dim varFreefield4 As String = ""
    Dim varLink1 As String = ""
    Dim varLink2 As String = ""
    Dim varLink3 As String = ""
    Dim varLink4 As String = ""
    Dim varLink5 As String = ""
    Dim varExportLink1 As String = ""
    Dim varExportLink2 As String = ""
    Dim varPutCall As String = ""
    Dim varStrike As String = ""
    Dim varTickSize As String = ""
    Dim _bbg_ID As String = ""
    Dim _companyToParentoRel As String = ""
    Dim _mfidComplexIndicator As String = ""
    Dim _deliveryType As String = ""
    Dim _closePrice As String = ""
    Dim _priceHigh As String = ""
    Dim _priceLow As String = ""
    Dim _derivativeDeliveryType As String = ""
    Dim _underlyingProduct As String = ""
    Dim _tFactor As Double
    Dim _style As String = ""
    Dim _firstTradingDate As Date
    Dim _settleDays As String
    Dim _exchangeSymbol As String
    Dim _posMultiplierFactor As String
    Dim _futureNoticeFirst As String
    Dim _ticker As String
    Dim _futureFirstTradeDate As String

    Property Future_First_Trade_Date() As String
        Get
            Return _futureFirstTradeDate
        End Get
        Set(value As String)
            _futureFirstTradeDate = value
        End Set
    End Property

    Property Ticker() As String
        Get
            Return _ticker
        End Get
        Set(value As String)
            _ticker = value
        End Set
    End Property

    Property Future_Notice_First_Date() As String
        Get
            Return _futureNoticeFirst
        End Get
        Set(value As String)
            _futureNoticeFirst = value
        End Set
    End Property

    Property POS_Multiplier_Factor() As String
        Get
            Return _posMultiplierFactor
        End Get
        Set(value As String)
            _posMultiplierFactor = value
        End Set
    End Property

    Property Exchange_Symbol() As String
        Get
            Return _exchangeSymbol
        End Get
        Set(value As String)
            _exchangeSymbol = value
        End Set
    End Property

    Property SETTLEMENT_DAYS() As String
        Get
            Return _settleDays
        End Get
        Set(value As String)
            _settleDays = value
        End Set
    End Property

    Property FIRST_TRADING_DATE() As String
        Get
            Return _firstTradingDate
        End Get
        Set(value As String)
            _firstTradingDate = value
        End Set
    End Property

    Property Style() As String
        Get
            Return _style
        End Get
        Set(value As String)
            _style = value
        End Set
    End Property

    Property T_Factor() As Double
        Get
            Return _tFactor
        End Get
        Set(value As Double)
            _tFactor = value
        End Set
    End Property

    Property Underlying_Product() As String
        Get
            Return _underlyingProduct
        End Get
        Set(value As String)
            _underlyingProduct = value
        End Set
    End Property

    Property Derivative_Delivery_Type() As String
        Get
            Return _derivativeDeliveryType
        End Get
        Set(value As String)
            _derivativeDeliveryType = value
        End Set
    End Property

    Property Close_Price() As String
        Get
            Return _closePrice
        End Get
        Set(value As String)
            _closePrice = value
        End Set
    End Property

    Property High_Price() As String
        Get
            Return _priceHigh
        End Get
        Set(value As String)
            _priceHigh = value
        End Set
    End Property

    Property Low_Price() As String
        Get
            Return _priceLow
        End Get
        Set(value As String)
            _priceLow = value
        End Set
    End Property

    Property Delivery_Type() As String
        Get
            Return _deliveryType
        End Get
        Set(value As String)
            _deliveryType = value
        End Set
    End Property

    Property MFID_Complex_Instr_Indicator() As String
        Get
            Return _mfidComplexIndicator
        End Get
        Set(value As String)
            _mfidComplexIndicator = value
        End Set
    End Property

    Property Company_To_Parent_Relationship() As String
        Get
            Return _companyToParentoRel
        End Get
        Set(value As String)
            _companyToParentoRel = value
        End Set
    End Property

    Property BBG_ID() As String
        Get
            Return _bbg_ID
        End Get
        Set(value As String)
            _bbg_ID = value
        End Set
    End Property

    Property ReferenceSecurityClass() As String
        Get
            Return varRefSecurityClass
        End Get
        Set(value As String)
            varRefSecurityClass = value
        End Set
    End Property

    Property CCY() As String
        Get
            Return varCCY
        End Get
        Set(value As String)
            varCCY = value
        End Set
    End Property

    Property CouponCCY() As String
        Get
            Return _cpnCCY
        End Get
        Set(value As String)
            _cpnCCY = value
        End Set
    End Property

    Property TradeCCY() As String
        Get
            Return _tradeCCY
        End Get
        Set(value As String)
            _tradeCCY = value
        End Set
    End Property

    Property InPence() As Boolean
        Get
            Return varInPence
        End Get
        Set(value As Boolean)
            varInPence = value
        End Set
    End Property

    Property Description() As String
        Get
            Return varDescription
        End Get
        Set(value As String)
            varDescription = value
        End Set
    End Property

    Property Name() As String
        Get
            Return varName
        End Get
        Set(value As String)
            varName = value
        End Set
    End Property

    Property FundType() As String
        Get
            Return varFundType
        End Get
        Set(value As String)
            varFundType = value
        End Set
    End Property

    Property MarketSectorDescription() As String
        Get
            Return varMarketSectorDescription
        End Get
        Set(value As String)
            varMarketSectorDescription = value
        End Set
    End Property

    Property CountryISO() As String
        Get
            Return varCountryISO
        End Get
        Set(value As String)
            varCountryISO = value
        End Set
    End Property

    Property SecurityDescription() As String
        Get
            Return varSecurityDescription
        End Get
        Set(value As String)
            varSecurityDescription = value
        End Set
    End Property

    Property ISIN() As String
        Get
            Return varISIN
        End Get
        Set(value As String)
            varISIN = value
        End Set
    End Property

    Property MIFIDIndicator() As String
        Get
            Return varMIFIDIndicator
        End Get
        Set(value As String)
            varMIFIDIndicator = value
        End Set
    End Property

    Property Issuer() As String
        Get
            Return varIssuer
        End Get
        Set(value As String)
            varIssuer = value
        End Set
    End Property

    Property Putable() As String
        Get
            Return varPutable
        End Get
        Set(value As String)
            varPutable = value
        End Set
    End Property

    Property Callable() As String
        Get
            Return varCallable
        End Get
        Set(value As String)
            varCallable = value
        End Set
    End Property

    Property Convertible() As String
        Get
            Return varConvertible
        End Get
        Set(value As String)
            varConvertible = value
        End Set
    End Property

    Property Sinkable() As String
        Get
            Return varSinkable
        End Get
        Set(value As String)
            varSinkable = value
        End Set
    End Property

    Property CFICode() As String
        Get
            Return varCFICode
        End Get
        Set(value As String)
            varCFICode = value
        End Set
    End Property

    Property IssueDate() As String
        Get
            Return varIssueDate
        End Get
        Set(value As String)
            varIssueDate = value
        End Set
    End Property

    Property InitDate() As String
        Get
            Return varInitDate
        End Get
        Set(value As String)
            varInitDate = value
        End Set
    End Property

    Property MaturityDate() As String
        Get
            Return varMaturityDate
        End Get
        Set(value As String)
            varMaturityDate = value
        End Set
    End Property

    Property FutLastDate() As String
        Get
            Return varFutLastDate
        End Get
        Set(value As String)
            varFutLastDate = value
        End Set
    End Property

    Property CouponFrequency() As String
        Get
            Return varCouponFrequency
        End Get
        Set(value As String)
            varCouponFrequency = value
        End Set
    End Property

    Property Interest() As String
        Get
            Return varInterest
        End Get
        Set(value As String)
            varInterest = value
        End Set
    End Property

    Property Coupon() As String
        Get
            Return varCoupon
        End Get
        Set(value As String)
            varCoupon = value
        End Set
    End Property

    Property FaceValue() As String
        Get
            Return varFaceValue
        End Get
        Set(value As String)
            varFaceValue = value
        End Set
    End Property

    Property Cusip() As String
        Get
            Return varCusip
        End Get
        Set(value As String)
            varCusip = value
        End Set
    End Property

    Property Sedol() As String
        Get
            Return varSedol
        End Get
        Set(value As String)
            varSedol = value
        End Set
    End Property

    Property TickerAndExchangeCode() As String
        Get
            Return varTickerAndExchangeCode
        End Get
        Set(value As String)
            varTickerAndExchangeCode = value
        End Set
    End Property

    Property DayCount() As String
        Get
            Return varDayCount
        End Get
        Set(value As String)
            varDayCount = value
        End Set
    End Property

    Property CouponType() As String
        Get
            Return varCouponType
        End Get
        Set(value As String)
            varCouponType = value
        End Set
    End Property

    Property ZeroCoupon() As String
        Get
            Return varZeroCoupon
        End Get
        Set(value As String)
            varZeroCoupon = value
        End Set
    End Property

    Property MinimumDenomination() As String
        Get
            Return varMinimumDenomination
        End Get
        Set(value As String)
            varMinimumDenomination = value
        End Set
    End Property

    Property MinimumTradingQuantity() As String
        Get
            Return varMinimumTradingQuantity
        End Get
        Set(value As String)
            varMinimumTradingQuantity = value
        End Set
    End Property

    Property RatingsMoodys() As String
        Get
            Return varRatingsMoodys
        End Get
        Set(value As String)
            varRatingsMoodys = value
        End Set
    End Property

    Property RatingsSP() As String
        Get
            Return varRatingsSP
        End Get
        Set(value As String)
            varRatingsSP = value
        End Set
    End Property

    Property RatingsFitch() As String
        Get
            Return varRatingsFitch
        End Get
        Set(value As String)
            varRatingsFitch = value
        End Set
    End Property

    Property FirstCouponDate() As String
        Get
            Return varFirstCouponDate
        End Get
        Set(value As String)
            varFirstCouponDate = value
        End Set
    End Property

    Property Industry() As String
        Get
            Return varIndustry
        End Get
        Set(value As String)
            varIndustry = value
        End Set
    End Property

    Property AskPrice() As String
        Get
            Return varAskPrice
        End Get
        Set(value As String)
            varAskPrice = value
        End Set
    End Property

    Property BidPrice() As String
        Get
            Return varBidPrice
        End Get
        Set(value As String)
            varBidPrice = value
        End Set
    End Property

    Property OpenPrice() As String
        Get
            Return varOpenPrice
        End Get
        Set(value As String)
            varOpenPrice = value
        End Set
    End Property

    Property ClosePrice() As String
        Get
            Return varClosePrice
        End Get
        Set(value As String)
            varClosePrice = value
        End Set
    End Property

    Property Approved() As Boolean
        Get
            Return varApproved
        End Get
        Set(value As Boolean)
            varApproved = value
        End Set
    End Property

    Property Active() As Boolean
        Get
            Return varActive
        End Get
        Set(value As Boolean)
            varActive = value
        End Set
    End Property

    Property Listed() As Boolean
        Get
            Return varListed
        End Get
        Set(value As Boolean)
            varListed = value
        End Set
    End Property

    Property Recapitalised() As Boolean
        Get
            Return varRecapitalised
        End Get
        Set(value As Boolean)
            varRecapitalised = value
        End Set
    End Property

    Property FreeField1() As String
        Get
            Return varFreefield1
        End Get
        Set(value As String)
            varFreefield1 = value
        End Set
    End Property

    Property FreeField2() As String
        Get
            Return varFreefield2
        End Get
        Set(value As String)
            varFreefield2 = value
        End Set
    End Property

    Property FreeField3() As String
        Get
            Return varFreefield3
        End Get
        Set(value As String)
            varFreefield3 = value
        End Set
    End Property

    Property FreeField4() As String
        Get
            Return varFreefield4
        End Get
        Set(value As String)
            varFreefield4 = value
        End Set
    End Property

    Property Link1() As String
        Get
            Return varLink1
        End Get
        Set(value As String)
            varLink1 = value
        End Set
    End Property

    Property Link2() As String
        Get
            Return varLink2
        End Get
        Set(value As String)
            varLink2 = value
        End Set
    End Property

    Property Link3() As String
        Get
            Return varLink3
        End Get
        Set(value As String)
            varLink3 = value
        End Set
    End Property

    Property Link4() As String
        Get
            Return varLink4
        End Get
        Set(value As String)
            varLink4 = value
        End Set
    End Property

    Property Link5() As String
        Get
            Return varLink5
        End Get
        Set(value As String)
            varLink5 = value
        End Set
    End Property

    Property ExportLink1() As String
        Get
            Return varExportLink1
        End Get
        Set(value As String)
            varExportLink1 = value
        End Set
    End Property

    Property ExportLink2() As String
        Get
            Return varExportLink2
        End Get
        Set(value As String)
            varExportLink2 = value
        End Set
    End Property

    Property PutCall() As String
        Get
            Return varPutCall
        End Get
        Set(value As String)
            varPutCall = value
        End Set
    End Property

    Property Strike() As String
        Get
            Return varStrike
        End Get
        Set(value As String)
            varStrike = value
        End Set
    End Property

    Property TickSize() As String
        Get
            Return varTickSize
        End Get
        Set(value As String)
            varTickSize = value
        End Set
    End Property
End Class
