﻿Public Class ClsGridView
    Private varOption As Double = 0
    Private varID As Double = 0
    Private varID2 As Double = 0
    Private varDate1 As Date = Now
    Private varDate2 As Date = Now

    Property GridOption() As Double
        Get
            Return varOption
        End Get
        Set(value As Double)
            varOption = value
        End Set
    End Property

    Property ID() As Double
        Get
            Return varID
        End Get
        Set(value As Double)
            varID = value
        End Set
    End Property

    Property ID2() As Double
        Get
            Return varID2
        End Get
        Set(value As Double)
            varID2 = value
        End Set
    End Property

    Property Date1() As Date
        Get
            Return varDate1
        End Get
        Set(value As Date)
            varDate1 = value
        End Set
    End Property

    Property Date2() As Date
        Get
            Return varDate2
        End Get
        Set(value As Date)
            varDate2 = value
        End Set
    End Property
End Class
