﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTrsFileManagement
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTrsFileManagement))
        Me.gpbTrsManagement = New System.Windows.Forms.GroupBox()
        Me.dgvFileRec = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblDeleteMessage = New System.Windows.Forms.Label()
        Me.cboSubType = New System.Windows.Forms.ComboBox()
        Me.lblSubmissionType = New System.Windows.Forms.Label()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.cboTrsFileName = New System.Windows.Forms.ComboBox()
        Me.lblFileName = New System.Windows.Forms.Label()
        Me.cboPriceCcy = New System.Windows.Forms.ComboBox()
        Me.lblPriceCcy = New System.Windows.Forms.Label()
        Me.cboQunatityCcy = New System.Windows.Forms.ComboBox()
        Me.lblCurr = New System.Windows.Forms.Label()
        Me.btnResetFilter = New System.Windows.Forms.Button()
        Me.lblPortfolio = New System.Windows.Forms.Label()
        Me.cboPortfolio = New System.Windows.Forms.ComboBox()
        Me.btnFilterGrid = New System.Windows.Forms.Button()
        Me.lblTrsFileId = New System.Windows.Forms.Label()
        Me.cboTrsFiles = New System.Windows.Forms.ComboBox()
        Me.lblTrsFileManagement = New System.Windows.Forms.Label()
        Me.gpbTrsManagement.SuspendLayout()
        CType(Me.dgvFileRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'gpbTrsManagement
        '
        Me.gpbTrsManagement.Controls.Add(Me.dgvFileRec)
        Me.gpbTrsManagement.Controls.Add(Me.GroupBox2)
        Me.gpbTrsManagement.Location = New System.Drawing.Point(12, 36)
        Me.gpbTrsManagement.Name = "gpbTrsManagement"
        Me.gpbTrsManagement.Size = New System.Drawing.Size(1702, 686)
        Me.gpbTrsManagement.TabIndex = 0
        Me.gpbTrsManagement.TabStop = False
        Me.gpbTrsManagement.Text = "TRS File Rec Submission History"
        '
        'dgvFileRec
        '
        Me.dgvFileRec.AllowUserToAddRows = False
        Me.dgvFileRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFileRec.Location = New System.Drawing.Point(0, 96)
        Me.dgvFileRec.Name = "dgvFileRec"
        Me.dgvFileRec.Size = New System.Drawing.Size(1690, 575)
        Me.dgvFileRec.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblDeleteMessage)
        Me.GroupBox2.Controls.Add(Me.cboSubType)
        Me.GroupBox2.Controls.Add(Me.lblSubmissionType)
        Me.GroupBox2.Controls.Add(Me.CmdExportToExcel)
        Me.GroupBox2.Controls.Add(Me.cboTrsFileName)
        Me.GroupBox2.Controls.Add(Me.lblFileName)
        Me.GroupBox2.Controls.Add(Me.cboPriceCcy)
        Me.GroupBox2.Controls.Add(Me.lblPriceCcy)
        Me.GroupBox2.Controls.Add(Me.cboQunatityCcy)
        Me.GroupBox2.Controls.Add(Me.lblCurr)
        Me.GroupBox2.Controls.Add(Me.btnResetFilter)
        Me.GroupBox2.Controls.Add(Me.lblPortfolio)
        Me.GroupBox2.Controls.Add(Me.cboPortfolio)
        Me.GroupBox2.Controls.Add(Me.btnFilterGrid)
        Me.GroupBox2.Controls.Add(Me.lblTrsFileId)
        Me.GroupBox2.Controls.Add(Me.cboTrsFiles)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1690, 80)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'lblDeleteMessage
        '
        Me.lblDeleteMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDeleteMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblDeleteMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeleteMessage.ForeColor = System.Drawing.Color.Red
        Me.lblDeleteMessage.Location = New System.Drawing.Point(1041, 15)
        Me.lblDeleteMessage.Name = "lblDeleteMessage"
        Me.lblDeleteMessage.Size = New System.Drawing.Size(598, 55)
        Me.lblDeleteMessage.TabIndex = 58
        Me.lblDeleteMessage.Text = resources.GetString("lblDeleteMessage.Text")
        Me.lblDeleteMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSubType
        '
        Me.cboSubType.FormattingEnabled = True
        Me.cboSubType.Location = New System.Drawing.Point(774, 19)
        Me.cboSubType.Name = "cboSubType"
        Me.cboSubType.Size = New System.Drawing.Size(79, 21)
        Me.cboSubType.TabIndex = 57
        '
        'lblSubmissionType
        '
        Me.lblSubmissionType.AutoSize = True
        Me.lblSubmissionType.Location = New System.Drawing.Point(678, 22)
        Me.lblSubmissionType.Name = "lblSubmissionType"
        Me.lblSubmissionType.Size = New System.Drawing.Size(90, 13)
        Me.lblSubmissionType.TabIndex = 56
        Me.lblSubmissionType.Text = "Submission Type:"
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(1645, 25)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 55
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'cboTrsFileName
        '
        Me.cboTrsFileName.FormattingEnabled = True
        Me.cboTrsFileName.Location = New System.Drawing.Point(339, 19)
        Me.cboTrsFileName.Name = "cboTrsFileName"
        Me.cboTrsFileName.Size = New System.Drawing.Size(272, 21)
        Me.cboTrsFileName.TabIndex = 11
        '
        'lblFileName
        '
        Me.lblFileName.AutoSize = True
        Me.lblFileName.Location = New System.Drawing.Point(251, 25)
        Me.lblFileName.Name = "lblFileName"
        Me.lblFileName.Size = New System.Drawing.Size(82, 13)
        Me.lblFileName.TabIndex = 10
        Me.lblFileName.Text = "TRS File Name:"
        '
        'cboPriceCcy
        '
        Me.cboPriceCcy.FormattingEnabled = True
        Me.cboPriceCcy.Location = New System.Drawing.Point(774, 49)
        Me.cboPriceCcy.Name = "cboPriceCcy"
        Me.cboPriceCcy.Size = New System.Drawing.Size(79, 21)
        Me.cboPriceCcy.TabIndex = 9
        '
        'lblPriceCcy
        '
        Me.lblPriceCcy.AutoSize = True
        Me.lblPriceCcy.Location = New System.Drawing.Point(713, 52)
        Me.lblPriceCcy.Name = "lblPriceCcy"
        Me.lblPriceCcy.Size = New System.Drawing.Size(55, 13)
        Me.lblPriceCcy.TabIndex = 8
        Me.lblPriceCcy.Text = "Price Ccy:"
        '
        'cboQunatityCcy
        '
        Me.cboQunatityCcy.FormattingEnabled = True
        Me.cboQunatityCcy.Location = New System.Drawing.Point(617, 49)
        Me.cboQunatityCcy.Name = "cboQunatityCcy"
        Me.cboQunatityCcy.Size = New System.Drawing.Size(79, 21)
        Me.cboQunatityCcy.TabIndex = 7
        '
        'lblCurr
        '
        Me.lblCurr.AutoSize = True
        Me.lblCurr.Location = New System.Drawing.Point(541, 52)
        Me.lblCurr.Name = "lblCurr"
        Me.lblCurr.Size = New System.Drawing.Size(70, 13)
        Me.lblCurr.TabIndex = 6
        Me.lblCurr.Text = "Quantity Ccy:"
        '
        'btnResetFilter
        '
        Me.btnResetFilter.Location = New System.Drawing.Point(938, 45)
        Me.btnResetFilter.Name = "btnResetFilter"
        Me.btnResetFilter.Size = New System.Drawing.Size(97, 26)
        Me.btnResetFilter.TabIndex = 5
        Me.btnResetFilter.Text = "Reset Filter"
        Me.btnResetFilter.UseVisualStyleBackColor = True
        '
        'lblPortfolio
        '
        Me.lblPortfolio.AutoSize = True
        Me.lblPortfolio.Location = New System.Drawing.Point(15, 52)
        Me.lblPortfolio.Name = "lblPortfolio"
        Me.lblPortfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblPortfolio.TabIndex = 4
        Me.lblPortfolio.Text = "Portfolio:"
        '
        'cboPortfolio
        '
        Me.cboPortfolio.FormattingEnabled = True
        Me.cboPortfolio.Location = New System.Drawing.Point(69, 49)
        Me.cboPortfolio.Name = "cboPortfolio"
        Me.cboPortfolio.Size = New System.Drawing.Size(461, 21)
        Me.cboPortfolio.TabIndex = 3
        '
        'btnFilterGrid
        '
        Me.btnFilterGrid.Location = New System.Drawing.Point(938, 15)
        Me.btnFilterGrid.Name = "btnFilterGrid"
        Me.btnFilterGrid.Size = New System.Drawing.Size(97, 26)
        Me.btnFilterGrid.TabIndex = 2
        Me.btnFilterGrid.Text = "Filter Grid"
        Me.btnFilterGrid.UseVisualStyleBackColor = True
        '
        'lblTrsFileId
        '
        Me.lblTrsFileId.AutoSize = True
        Me.lblTrsFileId.Location = New System.Drawing.Point(16, 25)
        Me.lblTrsFileId.Name = "lblTrsFileId"
        Me.lblTrsFileId.Size = New System.Drawing.Size(47, 13)
        Me.lblTrsFileId.TabIndex = 1
        Me.lblTrsFileId.Text = "Doc No:"
        '
        'cboTrsFiles
        '
        Me.cboTrsFiles.FormattingEnabled = True
        Me.cboTrsFiles.Location = New System.Drawing.Point(69, 22)
        Me.cboTrsFiles.Name = "cboTrsFiles"
        Me.cboTrsFiles.Size = New System.Drawing.Size(156, 21)
        Me.cboTrsFiles.TabIndex = 0
        '
        'lblTrsFileManagement
        '
        Me.lblTrsFileManagement.AutoSize = True
        Me.lblTrsFileManagement.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrsFileManagement.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblTrsFileManagement.Location = New System.Drawing.Point(607, 9)
        Me.lblTrsFileManagement.Name = "lblTrsFileManagement"
        Me.lblTrsFileManagement.Size = New System.Drawing.Size(318, 24)
        Me.lblTrsFileManagement.TabIndex = 46
        Me.lblTrsFileManagement.Text = "TRS File Rec Submission History"
        '
        'FrmTrsFileManagement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1726, 734)
        Me.Controls.Add(Me.lblTrsFileManagement)
        Me.Controls.Add(Me.gpbTrsManagement)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmTrsFileManagement"
        Me.Text = "TRS File Rec Management"
        Me.gpbTrsManagement.ResumeLayout(False)
        CType(Me.dgvFileRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents gpbTrsManagement As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lblTrsFileManagement As Label
    Friend WithEvents dgvFileRec As DataGridView
    Friend WithEvents cboTrsFiles As ComboBox
    Friend WithEvents lblTrsFileId As Label
    Friend WithEvents btnFilterGrid As Button
    Friend WithEvents lblPortfolio As Label
    Friend WithEvents cboPortfolio As ComboBox
    Friend WithEvents btnResetFilter As Button
    Friend WithEvents cboQunatityCcy As ComboBox
    Friend WithEvents lblCurr As Label
    Friend WithEvents cboPriceCcy As ComboBox
    Friend WithEvents lblPriceCcy As Label
    Friend WithEvents lblFileName As Label
    Friend WithEvents cboTrsFileName As ComboBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents cboSubType As ComboBox
    Friend WithEvents lblSubmissionType As Label
    Friend WithEvents lblDeleteMessage As Label
End Class
