﻿Public Class SwiftPayloadData

    Public Property P_PaymentId As Double 'Payment Id used in the request.
    Public Property P_ClientId As Integer 'New field Client id from the table DolfinPaymentApiClientDetails
    Public Property P_ClientSwiftCode As String 'Client specific code replaces select from vwDolfinPaymentDolfinAddress
    Public Property P_PaymentTypeID As Integer '@PaymentTypeID
    Public Property P_SettleDate As String '@SettleDate
    Public Property P_SettleDateOther As String 'New field to store settled date of related order.
    Public Property P_Amount As Double? '@Amount
    Public Property P_CCY As String '@CCY
    Public Property P_OrderRef As String '@OrderRef
    Public Property P_OrderRelatedRef As String 'New Field
    Public Property P_OrderSwift As String 'Broker Swift Code - New field Client id from the table DolfinPaymentApiClientDetails
    Public Property P_OrderAccountNumber As String '@OrderAccountNumber
    Public Property P_OrderIBAN As String '@OrderIBAN
    Public Property P_OrderOther As String '@OrderOther
    Public Property P_OrderVOCode As String '@OrderVOCode
    Public Property P_BenRef As String '@BenRef
    Public Property P_BenTypeID As Integer?
    Public Property P_BenFirstName As String
    Public Property P_BenMiddleName As String
    Public Property P_BenSurname As String
    Public Property P_BenName As String 'Concantenation of the various name fields.
    Public Property P_BenAddress1 As String '@BenAddress1
    Public Property P_BenAddress2 As String '@BenAddress2
    Public Property P_BenZip As String '@BenZip
    Public Property P_BenSwift As String '@BenSwift
    Public Property P_BenAccountNumber As String '@BenAccountNumber
    Public Property P_BenIBAN As String '@BenIBAN
    Public Property P_BenOther As String '@BenOther
    Public Property P_BenBIK As String '@BenBIK
    Public Property P_BenINN As String '@BenINN
    Public Property P_BenCCY As String '@BenCCY
    Public Property P_BenBrokerCode As Integer? '@BenBrokerCode
    Public Property P_BenSubBankName As String '@BenSubBankName
    Public Property P_BenSubBankAddress1 As String '@BenSubBankAddress1
    Public Property P_BenSubBankAddress2 As String '@BenSubBankAddress2
    Public Property P_BenSubBankZip As String '@BenSubBankZip
    Public Property P_BenSubBankSwift As String '@BenSubBankSwift
    Public Property P_BenSubBankIBAN As String '@BenSubBankIBAN
    Public Property P_BenSubBankOther As String '@BenSubBankOther
    Public Property P_BenSubBankBIK As String '@BenSubBankBIK
    Public Property P_BenSubBankINN As String '@BenSubBankINN
    Public Property P_BenBankName As String '@BenBankName
    Public Property P_BenBankAddress1 As String '@BenBankAddress1
    Public Property P_BenBankAddress2 As String '@BenBankAddress2
    Public Property P_BenBankZip As String '@BenBankZip
    Public Property P_BenBankSwift As String '@BenBankSwift
    Public Property P_BenBankIBAN As String '@BenBankIBAN
    Public Property P_BenBankBIK As String '@BenBankBIK
    Public Property P_BenBankINN As String '@BenBankINN
    Public Property P_SwiftUrgent As Boolean '@SwiftUrgent
    Public Property P_SwiftSendRef As Boolean '@SwiftSendRef
    Public Property P_ExchangeRate As Double? '@ExchangeRate
    Public Property P_OrderTransferMessTypeID As Integer '@OrderTransferMessTypeID
    Public Property P_Order3rdPartyMessTypeID As Integer '@Order3rdPartyMessTypeID
    Public Property P_OrderOtherMessTypeID As Integer '@OrderOtherMessTypeID
    Public Property P_Other_ID As Integer '@OtherID
    Public Property OTH_Type As String '@OTH_Type
    Public Property P_portfolioiscommission As Boolean '@p_portfolioiscommission
    Public Property P_Comments As String '@Comments
    Public Property P_IMSRef As String '@IMSRef
    Public Property P_OrderAmount As Double = 0 'New Field, used in MT920
    Public Property O_OrderName As String
    Public Property O_Address As String
    Public Property O_City As String
    Public Property O_PostCode As String
    Public Property A_ApiKey As String 'New Field
    Public Property E_Email As String

End Class