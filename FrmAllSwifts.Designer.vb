﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAllSwifts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.cmdRDRefresh = New System.Windows.Forms.Button()
        Me.CboFilterStatus = New System.Windows.Forms.ComboBox()
        Me.cboPayCheck = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.dgvSwift = New System.Windows.Forms.DataGridView()
        Me.ChkCashRefresh = New System.Windows.Forms.CheckBox()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.CmdAutoSendCSV = New System.Windows.Forms.Button()
        Me.ViewGroupBox.SuspendLayout()
        CType(Me.dgvSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(12, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(112, 24)
        Me.Label13.TabIndex = 42
        Me.Label13.Text = "Auto Swifts"
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.cmdRDRefresh)
        Me.ViewGroupBox.Controls.Add(Me.CboFilterStatus)
        Me.ViewGroupBox.Controls.Add(Me.cboPayCheck)
        Me.ViewGroupBox.Controls.Add(Me.Label10)
        Me.ViewGroupBox.Controls.Add(Me.Label23)
        Me.ViewGroupBox.Location = New System.Drawing.Point(16, 36)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(1065, 51)
        Me.ViewGroupBox.TabIndex = 41
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Movements Criteria"
        '
        'cmdRDRefresh
        '
        Me.cmdRDRefresh.Location = New System.Drawing.Point(21, 19)
        Me.cmdRDRefresh.Name = "cmdRDRefresh"
        Me.cmdRDRefresh.Size = New System.Drawing.Size(174, 23)
        Me.cmdRDRefresh.TabIndex = 65
        Me.cmdRDRefresh.Text = "Refresh Transactions"
        Me.cmdRDRefresh.UseVisualStyleBackColor = True
        '
        'CboFilterStatus
        '
        Me.CboFilterStatus.FormattingEnabled = True
        Me.CboFilterStatus.Location = New System.Drawing.Point(915, 19)
        Me.CboFilterStatus.Name = "CboFilterStatus"
        Me.CboFilterStatus.Size = New System.Drawing.Size(138, 21)
        Me.CboFilterStatus.TabIndex = 67
        '
        'cboPayCheck
        '
        Me.cboPayCheck.FormattingEnabled = True
        Me.cboPayCheck.Location = New System.Drawing.Point(703, 19)
        Me.cboPayCheck.Name = "cboPayCheck"
        Me.cboPayCheck.Size = New System.Drawing.Size(138, 21)
        Me.cboPayCheck.TabIndex = 69
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(847, 22)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 66
        Me.Label10.Text = "Filter Status"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(616, 22)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(81, 13)
        Me.Label23.TabIndex = 68
        Me.Label23.Text = "Payment Period"
        '
        'dgvSwift
        '
        Me.dgvSwift.AllowUserToAddRows = False
        Me.dgvSwift.AllowUserToDeleteRows = False
        Me.dgvSwift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSwift.Location = New System.Drawing.Point(16, 93)
        Me.dgvSwift.Name = "dgvSwift"
        Me.dgvSwift.ReadOnly = True
        Me.dgvSwift.Size = New System.Drawing.Size(1066, 444)
        Me.dgvSwift.TabIndex = 40
        '
        'ChkCashRefresh
        '
        Me.ChkCashRefresh.AutoSize = True
        Me.ChkCashRefresh.Checked = True
        Me.ChkCashRefresh.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCashRefresh.ForeColor = System.Drawing.Color.Maroon
        Me.ChkCashRefresh.Location = New System.Drawing.Point(16, 543)
        Me.ChkCashRefresh.Name = "ChkCashRefresh"
        Me.ChkCashRefresh.Size = New System.Drawing.Size(146, 17)
        Me.ChkCashRefresh.TabIndex = 71
        Me.ChkCashRefresh.Text = "Auto Refresh Grid On/Off"
        Me.ChkCashRefresh.UseVisualStyleBackColor = True
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Location = New System.Drawing.Point(931, 543)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(157, 24)
        Me.cmdSubmit.TabIndex = 70
        Me.cmdSubmit.Text = "Submit Transactions"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'CmdAutoSendCSV
        '
        Me.CmdAutoSendCSV.Location = New System.Drawing.Point(719, 544)
        Me.CmdAutoSendCSV.Name = "CmdAutoSendCSV"
        Me.CmdAutoSendCSV.Size = New System.Drawing.Size(156, 23)
        Me.CmdAutoSendCSV.TabIndex = 72
        Me.CmdAutoSendCSV.Text = "Auto Send Client CSV Files"
        Me.CmdAutoSendCSV.UseVisualStyleBackColor = True
        '
        'FrmAllSwifts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1099, 572)
        Me.Controls.Add(Me.CmdAutoSendCSV)
        Me.Controls.Add(Me.ChkCashRefresh)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.Controls.Add(Me.dgvSwift)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmAllSwifts"
        Me.Text = "FrmAllSwifts"
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        CType(Me.dgvSwift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label13 As Label
    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents dgvSwift As DataGridView
    Friend WithEvents cboPayCheck As ComboBox
    Friend WithEvents Label23 As Label
    Friend WithEvents CboFilterStatus As ComboBox
    Friend WithEvents cmdRDRefresh As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents ChkCashRefresh As CheckBox
    Friend WithEvents cmdSubmit As Button
    Friend WithEvents CmdAutoSendCSV As Button
End Class
