﻿Public Class MT920

    Public Property ClientSwiftCode As String
    Public Property OrderRef As String
    Public Property OrderSwift As String
    Public Property OrderCcy As String
    Public Property OrderAmount As Double
    Public Property OrderAccountNumber As String
    Public Property OrderType As String
    Public Property SwiftUrgent As Boolean
    Public Property Email As String
    Public Property Ftp As Boolean?

End Class