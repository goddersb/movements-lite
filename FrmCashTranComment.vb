﻿Imports System.Data.SqlClient

Public Class FrmCashTranComment
    Private TR_TDCode As Object
    Dim chkIsSelected As DataGridViewCheckBoxColumn
    Dim chkIsSelectedHeader As CheckBox
    Dim SqlString As String
    Dim dgvRow As Integer = 0
    Dim IsApprover As Boolean = False
    Dim SICConn As SqlClient.SqlConnection

    Private Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        cboPortfolio.Enabled = True
        dtpFromDate.Enabled = True
        dtpToDate.Enabled = True
        txtDocNo.Enabled = True
        txtDocNo.Text = ""
        txtComment.Text = ""
    End Sub

    Private Sub cboPortfolio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPortfolio.SelectedIndexChanged
        txtDocNo.Enabled = False
        txtDocNo.Text = ""
    End Sub

    Private Sub cmdFilter_Click(sender As Object, e As EventArgs) Handles cmdFilter.Click
        Dim SqlString As String

        SqlString = "SELECT
                           Comment
                           , Portfolio
                           , MyReference
                           , DocNo
                           , SetNo
                           , TransactionType
                           , TransactionDate
                           , SettleDate
                           , TotalValue
                           , TotalValueXCrncy
                           , CurrencyRate
                           , SenderBank
                           , SenderBankSWIFT
                           , SenderIBAN
                           , ReceiverBankSWIFT
                           , ReceiverBankIBAN
                       FROM
                           dbo.vwDolfinPaymentTransactionComments TR "

        If txtDocNo.Text = "" Then
            If cboPortfolio.Text = "" Then
                MsgBox("Please make sure you have selected a client!")
                Exit Sub
            End If

            If DateDiff(DateInterval.Day, dtpFromDate.Value, dtpToDate.Value) < 0 Then
                MsgBox("Please Select a valid Date. From Date must Not be later than the To date!")
                Exit Sub
            ElseIf DateDiff(DateInterval.Day, dtpFromDate.Value, dtpToDate.Value) > 365 Then
                MsgBox("Date range must be within 365 days!")
                Exit Sub
            End If

            SqlString = SqlString & " WHERE PF_Code = " & cboPortfolio.SelectedValue.ToString &
                               " AND TransactionDate BETWEEN '" & Format(dtpFromDate.Value, "yyyy-MM-dd").ToString & "' " & " AND '" & Format(dtpToDate.Value, "yyyy-MM-dd").ToString & "' " &
                               "ORDER BY TransactionDate "
        Else
            SqlString = SqlString & " WHERE  DocNo = '" & txtDocNo.Text & "'"
        End If

        dgvTransaction.DataSource = Nothing
        ' Debug.Print(SqlString)
        LoadTransactionData(SqlString)
    End Sub
    Private Sub FrmCashTranComment_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateComboboxWithData(cboPortfolio, "Select pf_Code,Portfolio from vwDolfinPaymentPortfolio where branchCode = " & ClsIMS.UserLocationCode & " order by Portfolio")
        ClsIMS.PopulateComboboxWithData(cboRecordType, "Select Code = 'All', String = 'All' 
                                                        UNION ALL Select Code = 'Approved', String = 'Approved'
                                                        UNION ALL Select Code = 'Cancelled', String = 'Cancelled'
                                                        UNION ALL Select Code = 'Outstanding', String = 'Outstanding'
                                                        UNION ALL Select Code = 'Rejected', String = 'Rejected'")
        cboPortfolio.Enabled = True
        dtpFromDate.Enabled = True
        dtpToDate.Enabled = True
        txtDocNo.Enabled = True
        dtpStartDate.Value = DateAdd(DateInterval.Day, -30, Now)
        dtpEndDate.Value = Now

        dtpFromDate.Value = DateAdd(DateInterval.Day, -60, Now)
        dtpFromDate.Enabled = True ' TRANSACTION FINDER
        dtpToDate.Enabled = True
        cboRecordType.SelectedIndex = 3
        cmdSubmit.Enabled = False

        If ClsIMS.CanUser(Environment.UserDomainName & "\" & Environment.UserName, "U_AuthoriseComments") Then
            IsApprover = True
        End If
    End Sub

    Private Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdSubmit.Click

        Dim InsertString As String
        Dim RowIndex As Integer

        If Trim(Me.txtComment.Text) = Trim(Me.dgvTransaction.Item(0, RowIndex).Value.ToString) _
                Or Me.txtComment.Text = "" _
                Or dgvTransaction Is Nothing Then
            Return
        Else
            RowIndex = dgvTransaction.CurrentRow.Index
            InsertString = "INSERT INTO dbo._Dolfin_TransactionCommentUpdateRequest (TCUR_DocNo, TCUR_SetNo, TCUR_CommentCurrent, TCUR_CommentNew, TCUR_RequestedBy)" &
                            "SELECT '" & dgvTransaction.Item(3, RowIndex).Value & "', " &
                                 "'" & dgvTransaction.Item(4, RowIndex).Value & "', " &
                                 "'" & dgvTransaction.Item(0, RowIndex).Value & "', " &
                                 "'" & Replace(Trim(Me.txtComment.Text), "'", "''") & "', " &
                                 "'" & Environment.UserDomainName & "\" & Environment.UserName & "' "

            SICConn = ClsIMS.GetNewOpenConnection
            ClsIMS.GetSQLDataNonQuery(InsertString)
            SICConn.Close()
            SICConn = Nothing
        End If
        dgvTransaction.ClearSelection()
        Me.txtComment.Text = ""
        Me.cmdSubmit.Enabled = False
    End Sub
    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Me.txtComment.Text = ""
        Me.cmdSubmit.Enabled = False
    End Sub
    Private Sub LoadRequestData()
        Dim SqlString As String = ""
        Me.dgvRequest.DataSource = Nothing
        Me.dgvRequest.Rows.Clear()
        Me.dgvRequest.Columns.Clear()

        SqlString = "SELECT [Doc No] = TCUR_DocNo
                        , [Set No] = TCUR_SetNo
                        , Status = TCUR_DecisionStatus
                        , [Current Comment] = TCUR_CommentCurrent
                        , [New Comment] = TCUR_CommentNew
                        , [Requested By] = TCUR_RequestedBy
                        , [Request Date] = TCUR_RequestedDate
                        , [Decided By] = TCUR_DecidedBy
                        , [Decision Date] = TCUR_DecisionDate
                    FROM dbo._Dolfin_TransactionCommentUpdateRequest
                    WHERE TCUR_RequestedDate >= '" & Format(dtpStartDate.Value, "yyyy-MM-dd") & "' AND TCUR_RequestedDate < DATEADD(DD, 1, '" & Format(dtpEndDate.Value, "yyyy-MM-dd") & "')"

        If cboRecordType.Text <> "All" Then
            SqlString += " AND TCUR_DecisionStatus = '" & cboRecordType.Text & "'"
        End If
        If IsApprover = False Then
            SqlString += " AND TCUR_RequestedBy = '" & Environment.UserDomainName & "\" & Environment.UserName & "'"
        End If

        Dim ds As DataSet
        ds = ClsIMS.GetTransactionData(SqlString)
        With Me.dgvRequest
            .AutoGenerateColumns = True
            .DataSource = ds.Tables(0)
            .CurrentCell = Nothing
            .Columns(0).Width = 70
            .Columns(1).Width = 70
            .Columns(2).Width = 80
            .Columns(3).Width = 300
            .Columns(4).Width = 300
        End With

    End Sub


    Private Sub dgvRequest_MouseClick(sender As Object, e As MouseEventArgs) Handles dgvRequest.MouseClick
        Dim edit_Menu As New ContextMenuStrip
        Dim mousePosition As Integer

        If e.Button = MouseButtons.Right Then
            mousePosition = dgvRequest.HitTest(e.X, e.Y).RowIndex
            dgvRow = mousePosition
            If mousePosition >= 0 Then
                edit_Menu.Items.Add("Show Comment").Name = "Show"
                If Me.dgvRequest.Item(2, dgvRow).Value.ToString.ToUpper = "OUTSTANDING" And IsApprover = True Then
                    edit_Menu.Items.Add("Approve").Name = "Approve"
                    edit_Menu.Items.Add("Reject").Name = "Reject"
                ElseIf IsApprover = False Then
                    edit_Menu.Items.Add("Cancel").Name = "Cancel"
                End If
                AddHandler edit_Menu.ItemClicked, AddressOf edit_Menu_ItemClicked
            Else
                Exit Sub
            End If

            edit_Menu.Show(dgvRequest, New Point(e.X, e.Y))
        End If
    End Sub
    Private Sub dtpEndDate_ValueChanged(sender As Object, e As EventArgs) Handles dtpEndDate.ValueChanged
        If CDate(dtpEndDate.Value) < CDate(dtpStartDate.Value) Then
            MsgBox("End date must be later or equal to the start date!!")
            dtpEndDate.Value = dtpStartDate.Value
        End If
    End Sub
    Private Sub txtDocNo_TextChanged(sender As Object, e As EventArgs) Handles txtDocNo.TextChanged
        If txtDocNo.Text <> "" Then
            cboPortfolio.Enabled = False
            dtpFromDate.Enabled = False
            dtpToDate.Enabled = False
        Else
            cboPortfolio.Enabled = True
            dtpFromDate.Enabled = True
            dtpToDate.Enabled = True
        End If
    End Sub
    Private Sub txtComment_TextChanged(sender As Object, e As EventArgs) Handles txtComment.TextChanged
        Dim RowIndex As Integer = 0

        If dgvTransaction.CurrentRow Is Nothing Then
            Exit Sub
        Else
            RowIndex = dgvTransaction.CurrentRow.Index
        End If

        If Trim(Me.txtComment.Text) = Trim(Me.dgvTransaction.Item(0, RowIndex).Value.ToString) Then
            Return
        Else
            Me.cmdSubmit.Enabled = True
        End If
    End Sub

    Private Sub dgvTransaction_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTransaction.CellContentDoubleClick
        If e.RowIndex = -1 Then
            Return
        End If

        Dim Result As SqlDataReader

        Result = ClsIMS.GetSQLData("SELECT * FROM dbo._Dolfin_TransactionCommentUpdateRequest WHERE TCUR_DecisionStatus = 'OUTSTANDING' AND TCUR_SetNo = " & Me.dgvTransaction.Item(4, e.RowIndex).Value.ToString)

        If Result.HasRows Then
            If MsgBox("There's an existing request for the transaction that needs to be processed. Do you want to cancel the existing one?", vbYesNo, "Existing Request") = vbYes Then
                ClsIMS.GetSQLDataNonQuery("UPDATE dbo._Dolfin_TransactionCommentUpdateRequest SET TCUR_DecisionStatus = 'Cancelled', TCUR_DecisionDate = GetDate(), TCUR_DecidedBy = '" & Environment.UserDomainName & "\" & Environment.UserName & "'  WHERE TCUR_SetNo = " & Me.dgvTransaction.Item(4, e.RowIndex).Value.ToString)
                Me.txtComment.Text = Me.dgvTransaction.Item(0, e.RowIndex).Value.ToString
            End If
        Else
            Me.txtComment.Text = Me.dgvTransaction.Item(0, e.RowIndex).Value.ToString
        End If
    End Sub
    Private Sub tbApproveComment_Enter(sender As Object, e As EventArgs) Handles tbApproveComment.Enter
        LoadRequestData()
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        If DateDiff(DateInterval.Day, dtpStartDate.Value, dtpEndDate.Value) < 0 Then
            MsgBox("Please select a valid date. From date must not be later than the to date!")
            Exit Sub
        ElseIf DateDiff(DateInterval.Day, dtpStartDate.Value, dtpEndDate.Value) > 365 Then
            MsgBox("Date range must be within 365 days!")
            Exit Sub
        End If
        LoadRequestData()
    End Sub

    Friend WithEvents editMenu As System.Windows.Forms.ToolStripMenuItem

    Private Sub edit_Menu_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles editMenu.Click
        ' Get the clicked menu strip and update its Text to the Label's Text.
        Dim toolStripItem = e.ClickedItem
        If toolStripItem.Text = "Show Comment" Then
            MsgBox("Current Comment: " & vbCrLf & vbTab & Me.dgvRequest.Item(3, dgvRow).Value.ToString & vbCrLf & "New Comment: " & vbCrLf & vbTab & Me.dgvRequest.Item(4, dgvRow).Value.ToString)
            Exit Sub
        ElseIf Me.dgvRequest.Item(2, dgvRow).Value.ToString.ToUpper <> "OUTSTANDING" Then
            Exit Sub
        End If

        If toolStripItem.Text = "Approve" Then ' This is done only by the approver
            ClsIMS.GetSQLDataNonQuery("EXEC dbo._DolfinUpdateTransactionComment '" & Me.dgvRequest.Item(0, dgvRow).Value.ToString & "', '" & Me.dgvRequest.Item(4, dgvRow).Value.ToString.Replace("'", "''") & "', 'Approved', '" & Environment.UserDomainName & "\" & Environment.UserName & "'")
        ElseIf toolStripItem.Text = "Reject" Then ' This is done only by the approver
            ClsIMS.GetSQLDataNonQuery("EXEC dbo._DolfinUpdateTransactionComment '" & Me.dgvRequest.Item(0, dgvRow).Value.ToString & "', '', 'Rejected', '" & Environment.UserDomainName & "\" & Environment.UserName & "'")
        ElseIf toolStripItem.Text = "Cancel" Then ' Done by the requester
            ClsIMS.GetSQLDataNonQuery("EXEC dbo._DolfinUpdateTransactionComment '" & Me.dgvRequest.Item(0, dgvRow).Value.ToString & "', '', 'Cancelled', '" & Environment.UserDomainName & "\" & Environment.UserName & "'")
        End If
        LoadRequestData()
    End Sub

    Private Sub LoadTransactionData(SqlString As String)
        Me.dgvTransaction.DataSource = Nothing
        Me.dgvTransaction.Rows.Clear()
        Me.dgvTransaction.Columns.Clear()

        Dim ds As DataSet
        ds = ClsIMS.GetTransactionData(SqlString)
        With Me.dgvTransaction
            .AutoGenerateColumns = True
            .DataSource = ds.Tables(0)
            ' .Columns("TR_Currency").HeaderText = "Currency 1"
            .CurrentCell = Nothing
        End With
        Me.cmdSubmit.Enabled = False

        With dgvTransaction
            .Columns(0).Width = 350
            .Columns(1).Width = 250
            .Columns(2).Width = 80
        End With
    End Sub


End Class