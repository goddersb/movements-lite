﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaymentAuthorise
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPaymentAuthorise))
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.Rb1 = New System.Windows.Forms.RadioButton()
        Me.Rb4 = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtBatchOtherID = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ChkIMSAck = New System.Windows.Forms.CheckBox()
        Me.ChkIMSReturned = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtIMSStatus = New System.Windows.Forms.TextBox()
        Me.ChkSwiftAck = New System.Windows.Forms.CheckBox()
        Me.ChkSwiftReturned = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSwiftStatus = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ChkIMS = New System.Windows.Forms.CheckBox()
        Me.ChkSwift = New System.Windows.Forms.CheckBox()
        Me.cmdSendControl = New System.Windows.Forms.Button()
        Me.CmdMail = New System.Windows.Forms.Button()
        Me.CmdMailAttach = New System.Windows.Forms.Button()
        Me.txtAuthID = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtAuthComments = New System.Windows.Forms.TextBox()
        Me.txtPortfolioFee = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtAuthPayType = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtAuthBenAccount = New System.Windows.Forms.TextBox()
        Me.lblauthbenaccount = New System.Windows.Forms.Label()
        Me.txtAuthOrderAccount = New System.Windows.Forms.TextBox()
        Me.lblauthorderaccount = New System.Windows.Forms.Label()
        Me.txtAuthPortfolio = New System.Windows.Forms.TextBox()
        Me.lblauthportfolio = New System.Windows.Forms.Label()
        Me.txtAuthCCY = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtAuthAmount = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblauthbenname = New System.Windows.Forms.Label()
        Me.txtAuthBen = New System.Windows.Forms.TextBox()
        Me.txtAuthOrder = New System.Windows.Forms.TextBox()
        Me.lblauthordername = New System.Windows.Forms.Label()
        Me.Rb3 = New System.Windows.Forms.RadioButton()
        Me.Rb2 = New System.Windows.Forms.RadioButton()
        Me.Rb5 = New System.Windows.Forms.RadioButton()
        Me.RB6 = New System.Windows.Forms.RadioButton()
        Me.RB7 = New System.Windows.Forms.RadioButton()
        Me.Rb8 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(9, 727)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(411, 23)
        Me.cmdContinue.TabIndex = 18
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'Rb1
        '
        Me.Rb1.AutoSize = True
        Me.Rb1.Location = New System.Drawing.Point(28, 521)
        Me.Rb1.Name = "Rb1"
        Me.Rb1.Size = New System.Drawing.Size(200, 17)
        Me.Rb1.TabIndex = 10
        Me.Rb1.Text = "Authorise Movement (Ready to send)"
        Me.Rb1.UseVisualStyleBackColor = True
        '
        'Rb4
        '
        Me.Rb4.AutoSize = True
        Me.Rb4.Location = New System.Drawing.Point(28, 591)
        Me.Rb4.Name = "Rb4"
        Me.Rb4.Size = New System.Drawing.Size(102, 17)
        Me.Rb4.TabIndex = 11
        Me.Rb4.Text = "Copy Movement"
        Me.Rb4.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(79, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(273, 24)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Selected Movement Options"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtBatchOtherID)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.ChkIMSAck)
        Me.GroupBox1.Controls.Add(Me.ChkIMSReturned)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtIMSStatus)
        Me.GroupBox1.Controls.Add(Me.ChkSwiftAck)
        Me.GroupBox1.Controls.Add(Me.ChkSwiftReturned)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtSwiftStatus)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtAuthComments)
        Me.GroupBox1.Controls.Add(Me.txtPortfolioFee)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtAuthPayType)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtAuthBenAccount)
        Me.GroupBox1.Controls.Add(Me.lblauthbenaccount)
        Me.GroupBox1.Controls.Add(Me.txtAuthOrderAccount)
        Me.GroupBox1.Controls.Add(Me.lblauthorderaccount)
        Me.GroupBox1.Controls.Add(Me.txtAuthPortfolio)
        Me.GroupBox1.Controls.Add(Me.lblauthportfolio)
        Me.GroupBox1.Controls.Add(Me.txtAuthCCY)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtAuthAmount)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblauthbenname)
        Me.GroupBox1.Controls.Add(Me.txtAuthBen)
        Me.GroupBox1.Controls.Add(Me.txtAuthOrder)
        Me.GroupBox1.Controls.Add(Me.lblauthordername)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 42)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(411, 469)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Summary Movement Details"
        '
        'txtBatchOtherID
        '
        Me.txtBatchOtherID.Enabled = False
        Me.txtBatchOtherID.Location = New System.Drawing.Point(100, 283)
        Me.txtBatchOtherID.Name = "txtBatchOtherID"
        Me.txtBatchOtherID.Size = New System.Drawing.Size(72, 20)
        Me.txtBatchOtherID.TabIndex = 60
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(11, 286)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 13)
        Me.Label6.TabIndex = 59
        Me.Label6.Text = "Batch/Other ID:"
        '
        'ChkIMSAck
        '
        Me.ChkIMSAck.AutoSize = True
        Me.ChkIMSAck.Enabled = False
        Me.ChkIMSAck.Location = New System.Drawing.Point(282, 364)
        Me.ChkIMSAck.Name = "ChkIMSAck"
        Me.ChkIMSAck.Size = New System.Drawing.Size(119, 17)
        Me.ChkIMSAck.TabIndex = 58
        Me.ChkIMSAck.Text = "IMS Acknowledged"
        Me.ChkIMSAck.UseVisualStyleBackColor = True
        '
        'ChkIMSReturned
        '
        Me.ChkIMSReturned.AutoSize = True
        Me.ChkIMSReturned.Enabled = False
        Me.ChkIMSReturned.Location = New System.Drawing.Point(100, 364)
        Me.ChkIMSReturned.Name = "ChkIMSReturned"
        Me.ChkIMSReturned.Size = New System.Drawing.Size(92, 17)
        Me.ChkIMSReturned.TabIndex = 57
        Me.ChkIMSReturned.Text = "IMS Returned"
        Me.ChkIMSReturned.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(28, 439)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 56
        Me.Label3.Text = "Swift Status:"
        '
        'txtIMSStatus
        '
        Me.txtIMSStatus.Enabled = False
        Me.txtIMSStatus.Location = New System.Drawing.Point(100, 387)
        Me.txtIMSStatus.Name = "txtIMSStatus"
        Me.txtIMSStatus.Size = New System.Drawing.Size(305, 20)
        Me.txtIMSStatus.TabIndex = 55
        '
        'ChkSwiftAck
        '
        Me.ChkSwiftAck.AutoSize = True
        Me.ChkSwiftAck.Enabled = False
        Me.ChkSwiftAck.Location = New System.Drawing.Point(282, 413)
        Me.ChkSwiftAck.Name = "ChkSwiftAck"
        Me.ChkSwiftAck.Size = New System.Drawing.Size(123, 17)
        Me.ChkSwiftAck.TabIndex = 54
        Me.ChkSwiftAck.Text = "Swift Acknowledged"
        Me.ChkSwiftAck.UseVisualStyleBackColor = True
        '
        'ChkSwiftReturned
        '
        Me.ChkSwiftReturned.AutoSize = True
        Me.ChkSwiftReturned.Enabled = False
        Me.ChkSwiftReturned.Location = New System.Drawing.Point(100, 413)
        Me.ChkSwiftReturned.Name = "ChkSwiftReturned"
        Me.ChkSwiftReturned.Size = New System.Drawing.Size(96, 17)
        Me.ChkSwiftReturned.TabIndex = 53
        Me.ChkSwiftReturned.Text = "Swift Returned"
        Me.ChkSwiftReturned.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 390)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 52
        Me.Label1.Text = "IMS Status:"
        '
        'txtSwiftStatus
        '
        Me.txtSwiftStatus.Enabled = False
        Me.txtSwiftStatus.Location = New System.Drawing.Point(100, 436)
        Me.txtSwiftStatus.Name = "txtSwiftStatus"
        Me.txtSwiftStatus.Size = New System.Drawing.Size(305, 20)
        Me.txtSwiftStatus.TabIndex = 51
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ChkIMS)
        Me.GroupBox2.Controls.Add(Me.ChkSwift)
        Me.GroupBox2.Controls.Add(Me.cmdSendControl)
        Me.GroupBox2.Controls.Add(Me.CmdMail)
        Me.GroupBox2.Controls.Add(Me.CmdMailAttach)
        Me.GroupBox2.Controls.Add(Me.txtAuthID)
        Me.GroupBox2.Location = New System.Drawing.Point(10, 19)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(392, 75)
        Me.GroupBox2.TabIndex = 50
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Options"
        '
        'ChkIMS
        '
        Me.ChkIMS.AutoSize = True
        Me.ChkIMS.Enabled = False
        Me.ChkIMS.Location = New System.Drawing.Point(101, 42)
        Me.ChkIMS.Name = "ChkIMS"
        Me.ChkIMS.Size = New System.Drawing.Size(73, 17)
        Me.ChkIMS.TabIndex = 56
        Me.ChkIMS.Text = "Send IMS"
        Me.ChkIMS.UseVisualStyleBackColor = True
        '
        'ChkSwift
        '
        Me.ChkSwift.AutoSize = True
        Me.ChkSwift.Enabled = False
        Me.ChkSwift.Location = New System.Drawing.Point(101, 19)
        Me.ChkSwift.Name = "ChkSwift"
        Me.ChkSwift.Size = New System.Drawing.Size(77, 17)
        Me.ChkSwift.TabIndex = 55
        Me.ChkSwift.Text = "Send Swift"
        Me.ChkSwift.UseVisualStyleBackColor = True
        '
        'cmdSendControl
        '
        Me.cmdSendControl.BackColor = System.Drawing.Color.White
        Me.cmdSendControl.ForeColor = System.Drawing.Color.DarkGreen
        Me.cmdSendControl.Image = CType(resources.GetObject("cmdSendControl.Image"), System.Drawing.Image)
        Me.cmdSendControl.Location = New System.Drawing.Point(184, 15)
        Me.cmdSendControl.Name = "cmdSendControl"
        Me.cmdSendControl.Size = New System.Drawing.Size(64, 53)
        Me.cmdSendControl.TabIndex = 50
        Me.cmdSendControl.UseVisualStyleBackColor = False
        '
        'CmdMail
        '
        Me.CmdMail.BackColor = System.Drawing.Color.White
        Me.CmdMail.Image = CType(resources.GetObject("CmdMail.Image"), System.Drawing.Image)
        Me.CmdMail.Location = New System.Drawing.Point(323, 15)
        Me.CmdMail.Name = "CmdMail"
        Me.CmdMail.Size = New System.Drawing.Size(61, 53)
        Me.CmdMail.TabIndex = 12
        Me.CmdMail.UseVisualStyleBackColor = False
        '
        'CmdMailAttach
        '
        Me.CmdMailAttach.BackColor = System.Drawing.Color.White
        Me.CmdMailAttach.ForeColor = System.Drawing.Color.DarkGreen
        Me.CmdMailAttach.Image = CType(resources.GetObject("CmdMailAttach.Image"), System.Drawing.Image)
        Me.CmdMailAttach.Location = New System.Drawing.Point(253, 15)
        Me.CmdMailAttach.Name = "CmdMailAttach"
        Me.CmdMailAttach.Size = New System.Drawing.Size(64, 53)
        Me.CmdMailAttach.TabIndex = 11
        Me.CmdMailAttach.UseVisualStyleBackColor = False
        '
        'txtAuthID
        '
        Me.txtAuthID.AcceptsReturn = True
        Me.txtAuthID.AcceptsTab = True
        Me.txtAuthID.BackColor = System.Drawing.SystemColors.Control
        Me.txtAuthID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAuthID.Enabled = False
        Me.txtAuthID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAuthID.Location = New System.Drawing.Point(6, 19)
        Me.txtAuthID.Multiline = True
        Me.txtAuthID.Name = "txtAuthID"
        Me.txtAuthID.Size = New System.Drawing.Size(89, 40)
        Me.txtAuthID.TabIndex = 49
        Me.txtAuthID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(35, 313)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 13)
        Me.Label10.TabIndex = 49
        Me.Label10.Text = "Comments:"
        '
        'txtAuthComments
        '
        Me.txtAuthComments.AcceptsReturn = True
        Me.txtAuthComments.AcceptsTab = True
        Me.txtAuthComments.Enabled = False
        Me.txtAuthComments.Location = New System.Drawing.Point(100, 313)
        Me.txtAuthComments.Multiline = True
        Me.txtAuthComments.Name = "txtAuthComments"
        Me.txtAuthComments.Size = New System.Drawing.Size(305, 45)
        Me.txtAuthComments.TabIndex = 48
        '
        'txtPortfolioFee
        '
        Me.txtPortfolioFee.Enabled = False
        Me.txtPortfolioFee.Location = New System.Drawing.Point(333, 286)
        Me.txtPortfolioFee.Name = "txtPortfolioFee"
        Me.txtPortfolioFee.Size = New System.Drawing.Size(72, 20)
        Me.txtPortfolioFee.TabIndex = 47
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(191, 286)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(136, 13)
        Me.Label11.TabIndex = 46
        Me.Label11.Text = "Portfolio Fee Movement ID:"
        '
        'txtAuthPayType
        '
        Me.txtAuthPayType.Enabled = False
        Me.txtAuthPayType.Location = New System.Drawing.Point(100, 100)
        Me.txtAuthPayType.Name = "txtAuthPayType"
        Me.txtAuthPayType.Size = New System.Drawing.Size(305, 20)
        Me.txtAuthPayType.TabIndex = 43
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 103)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(87, 13)
        Me.Label9.TabIndex = 42
        Me.Label9.Text = "Movement Type:"
        '
        'txtAuthBenAccount
        '
        Me.txtAuthBenAccount.Enabled = False
        Me.txtAuthBenAccount.Location = New System.Drawing.Point(100, 231)
        Me.txtAuthBenAccount.Name = "txtAuthBenAccount"
        Me.txtAuthBenAccount.Size = New System.Drawing.Size(305, 20)
        Me.txtAuthBenAccount.TabIndex = 38
        '
        'lblauthbenaccount
        '
        Me.lblauthbenaccount.AutoSize = True
        Me.lblauthbenaccount.Location = New System.Drawing.Point(22, 234)
        Me.lblauthbenaccount.Name = "lblauthbenaccount"
        Me.lblauthbenaccount.Size = New System.Drawing.Size(72, 13)
        Me.lblauthbenaccount.TabIndex = 37
        Me.lblauthbenaccount.Text = "Ben Account:"
        '
        'txtAuthOrderAccount
        '
        Me.txtAuthOrderAccount.Enabled = False
        Me.txtAuthOrderAccount.Location = New System.Drawing.Point(100, 178)
        Me.txtAuthOrderAccount.Name = "txtAuthOrderAccount"
        Me.txtAuthOrderAccount.Size = New System.Drawing.Size(305, 20)
        Me.txtAuthOrderAccount.TabIndex = 36
        '
        'lblauthorderaccount
        '
        Me.lblauthorderaccount.AutoSize = True
        Me.lblauthorderaccount.Location = New System.Drawing.Point(15, 181)
        Me.lblauthorderaccount.Name = "lblauthorderaccount"
        Me.lblauthorderaccount.Size = New System.Drawing.Size(79, 13)
        Me.lblauthorderaccount.TabIndex = 35
        Me.lblauthorderaccount.Text = "Order Account:"
        '
        'txtAuthPortfolio
        '
        Me.txtAuthPortfolio.Enabled = False
        Me.txtAuthPortfolio.Location = New System.Drawing.Point(100, 126)
        Me.txtAuthPortfolio.Name = "txtAuthPortfolio"
        Me.txtAuthPortfolio.Size = New System.Drawing.Size(305, 20)
        Me.txtAuthPortfolio.TabIndex = 34
        '
        'lblauthportfolio
        '
        Me.lblauthportfolio.AutoSize = True
        Me.lblauthportfolio.Location = New System.Drawing.Point(46, 129)
        Me.lblauthportfolio.Name = "lblauthportfolio"
        Me.lblauthportfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblauthportfolio.TabIndex = 33
        Me.lblauthportfolio.Text = "Portfolio:"
        '
        'txtAuthCCY
        '
        Me.txtAuthCCY.Enabled = False
        Me.txtAuthCCY.Location = New System.Drawing.Point(100, 152)
        Me.txtAuthCCY.Name = "txtAuthCCY"
        Me.txtAuthCCY.Size = New System.Drawing.Size(72, 20)
        Me.txtAuthCCY.TabIndex = 32
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(63, 155)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 31
        Me.Label5.Text = "CCY:"
        '
        'txtAuthAmount
        '
        Me.txtAuthAmount.Enabled = False
        Me.txtAuthAmount.Location = New System.Drawing.Point(254, 152)
        Me.txtAuthAmount.Name = "txtAuthAmount"
        Me.txtAuthAmount.Size = New System.Drawing.Size(151, 20)
        Me.txtAuthAmount.TabIndex = 30
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(202, 155)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "Amount:"
        '
        'lblauthbenname
        '
        Me.lblauthbenname.AutoSize = True
        Me.lblauthbenname.Location = New System.Drawing.Point(34, 260)
        Me.lblauthbenname.Name = "lblauthbenname"
        Me.lblauthbenname.Size = New System.Drawing.Size(60, 13)
        Me.lblauthbenname.TabIndex = 28
        Me.lblauthbenname.Text = "Ben Name:"
        '
        'txtAuthBen
        '
        Me.txtAuthBen.Enabled = False
        Me.txtAuthBen.Location = New System.Drawing.Point(100, 257)
        Me.txtAuthBen.Name = "txtAuthBen"
        Me.txtAuthBen.Size = New System.Drawing.Size(305, 20)
        Me.txtAuthBen.TabIndex = 27
        '
        'txtAuthOrder
        '
        Me.txtAuthOrder.Enabled = False
        Me.txtAuthOrder.Location = New System.Drawing.Point(100, 205)
        Me.txtAuthOrder.Name = "txtAuthOrder"
        Me.txtAuthOrder.Size = New System.Drawing.Size(305, 20)
        Me.txtAuthOrder.TabIndex = 26
        '
        'lblauthordername
        '
        Me.lblauthordername.AutoSize = True
        Me.lblauthordername.Location = New System.Drawing.Point(27, 208)
        Me.lblauthordername.Name = "lblauthordername"
        Me.lblauthordername.Size = New System.Drawing.Size(67, 13)
        Me.lblauthordername.TabIndex = 25
        Me.lblauthordername.Text = "Order Name:"
        '
        'Rb3
        '
        Me.Rb3.AutoSize = True
        Me.Rb3.Checked = True
        Me.Rb3.Location = New System.Drawing.Point(28, 568)
        Me.Rb3.Name = "Rb3"
        Me.Rb3.Size = New System.Drawing.Size(96, 17)
        Me.Rb3.TabIndex = 26
        Me.Rb3.TabStop = True
        Me.Rb3.Text = "Edit Movement"
        Me.Rb3.UseVisualStyleBackColor = True
        '
        'Rb2
        '
        Me.Rb2.AutoSize = True
        Me.Rb2.Location = New System.Drawing.Point(28, 545)
        Me.Rb2.Name = "Rb2"
        Me.Rb2.Size = New System.Drawing.Size(238, 17)
        Me.Rb2.TabIndex = 27
        Me.Rb2.Text = "Unauthorise Movement (Pending transaction)"
        Me.Rb2.UseVisualStyleBackColor = True
        '
        'Rb5
        '
        Me.Rb5.AutoSize = True
        Me.Rb5.Location = New System.Drawing.Point(28, 662)
        Me.Rb5.Name = "Rb5"
        Me.Rb5.Size = New System.Drawing.Size(206, 17)
        Me.Rb5.TabIndex = 28
        Me.Rb5.Text = "Delete Movement (Cancel transaction)"
        Me.Rb5.UseVisualStyleBackColor = True
        '
        'RB6
        '
        Me.RB6.AutoSize = True
        Me.RB6.Location = New System.Drawing.Point(28, 685)
        Me.RB6.Name = "RB6"
        Me.RB6.Size = New System.Drawing.Size(166, 17)
        Me.RB6.TabIndex = 29
        Me.RB6.Text = "Remove Movement From Grid"
        Me.RB6.UseVisualStyleBackColor = True
        '
        'RB7
        '
        Me.RB7.AutoSize = True
        Me.RB7.Location = New System.Drawing.Point(28, 614)
        Me.RB7.Name = "RB7"
        Me.RB7.Size = New System.Drawing.Size(279, 17)
        Me.RB7.TabIndex = 30
        Me.RB7.Text = "Re-Send Authorised Swift transaction (From Rejected)"
        Me.RB7.UseVisualStyleBackColor = True
        '
        'Rb8
        '
        Me.Rb8.AutoSize = True
        Me.Rb8.Location = New System.Drawing.Point(28, 637)
        Me.Rb8.Name = "Rb8"
        Me.Rb8.Size = New System.Drawing.Size(318, 17)
        Me.Rb8.TabIndex = 31
        Me.Rb8.Text = "Re-Send Authorised IMS transaction (Awaiting IMS, IMS Error)"
        Me.Rb8.UseVisualStyleBackColor = True
        '
        'FrmPaymentAuthorise
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(432, 762)
        Me.Controls.Add(Me.Rb8)
        Me.Controls.Add(Me.RB7)
        Me.Controls.Add(Me.RB6)
        Me.Controls.Add(Me.Rb5)
        Me.Controls.Add(Me.Rb2)
        Me.Controls.Add(Me.Rb3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdContinue)
        Me.Controls.Add(Me.Rb1)
        Me.Controls.Add(Me.Rb4)
        Me.Controls.Add(Me.Label2)
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmPaymentAuthorise"
        Me.Text = "Payment Options"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdContinue As System.Windows.Forms.Button
    Friend WithEvents Rb1 As System.Windows.Forms.RadioButton
    Friend WithEvents Rb4 As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAuthCCY As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtAuthAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblauthbenname As System.Windows.Forms.Label
    Friend WithEvents txtAuthBen As System.Windows.Forms.TextBox
    Friend WithEvents txtAuthOrder As System.Windows.Forms.TextBox
    Friend WithEvents lblauthordername As System.Windows.Forms.Label
    Friend WithEvents txtAuthOrderAccount As System.Windows.Forms.TextBox
    Friend WithEvents lblauthorderaccount As System.Windows.Forms.Label
    Friend WithEvents txtAuthPortfolio As System.Windows.Forms.TextBox
    Friend WithEvents lblauthportfolio As System.Windows.Forms.Label
    Friend WithEvents txtAuthBenAccount As System.Windows.Forms.TextBox
    Friend WithEvents lblauthbenaccount As System.Windows.Forms.Label
    Friend WithEvents Rb3 As System.Windows.Forms.RadioButton
    Friend WithEvents Rb2 As System.Windows.Forms.RadioButton
    Friend WithEvents txtAuthPayType As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtPortfolioFee As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtAuthComments As System.Windows.Forms.TextBox
    Friend WithEvents txtAuthID As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents CmdMailAttach As Button
    Friend WithEvents CmdMail As Button
    Friend WithEvents ChkSwiftAck As CheckBox
    Friend WithEvents ChkSwiftReturned As CheckBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtSwiftStatus As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtIMSStatus As TextBox
    Friend WithEvents ChkIMSAck As CheckBox
    Friend WithEvents ChkIMSReturned As CheckBox
    Friend WithEvents Rb5 As RadioButton
    Friend WithEvents RB6 As RadioButton
    Friend WithEvents txtBatchOtherID As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents RB7 As RadioButton
    Friend WithEvents cmdSendControl As Button
    Friend WithEvents ChkIMS As CheckBox
    Friend WithEvents ChkSwift As CheckBox
    Friend WithEvents Rb8 As RadioButton
End Class
