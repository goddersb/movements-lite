﻿Public Class FrmPaymentRequestFOPOption

    Private Function ValidateConfirm() As Boolean
        ValidateConfirm = True
        If (Rb3.Checked) And (ClsRD.StatusID = cStatusCancelled) Then
            MsgBox("You cannot action this transaction as it has already been cancelled", vbCritical, "Cannot edit")
            ValidateConfirm = False
        End If
    End Function

    Private Sub FrmPaymentRequestFOPOption_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtRDAuthType.Text = IIf(ClsRD.RecType = 1, "Receive Free", "Deliver Free")
        txtRDAuthID.Text = ClsRD.SelectedTransactionID
        txtRDAuthPortfolio.Text = ClsRD.PortfolioName
        txtRDAuthCCY.Text = ClsRD.CCYName
        txtRDAuthAmount.Text = ClsRD.Amount
        dtRDAuthTransDate.Value = ClsRD.TransDate
        dtRDAuthSettleDate.Value = ClsRD.SettleDate
        txtRDAuthInstrument.Text = ClsRD.InstrName
        txtRDAuthISIN.Text = ClsRD.InstrISIN
    End Sub

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Dim varResponse As Integer
        Dim varNotes As String = ""

        ClsRD.SelectedAuthoriseOption = 0
        If ValidateConfirm() Then
            If Rb1.Checked Then
                ClsRD = New ClsReceiveDeliver
                Dim NewPR = New FrmPaymentRequest
                ClsIMS.GetReaderItemIntoRDClass(txtRDAuthID.Text)
                ClsGV.GridOption = 0
                ClsGV.ID = ClsRD.SelectedTransactionID
                Dim FrmAuditView As New FrmGridView
                FrmAuditView.ShowDialog()
            ElseIf Rb2.Checked Then
                varResponse = MsgBox("Are you sure you want to copy this free of payment transaction?", vbQuestion + vbYesNo, "copy transaction")
                If varResponse = vbYes Then
                    ClsRD.SelectedAuthoriseOption = 2
                End If
            ElseIf Rb3.Checked Then
                varResponse = MsgBox("Are you sure you want to cancel this free of payment transaction (This might also send a cancellation SWIFT)?", vbQuestion + vbYesNo, "cancel transaction")
                If varResponse = vbYes Then
                    ClsRD.SelectedAuthoriseOption = 3
                End If
                'ClsRD.SelectedAuthoriseOption = 4 = manual confirmation
            ElseIf Rb4.Checked Then
                If ClsRD.StatusID = cStatusPendingCompliance Or ClsRD.StatusID = cStatusPendingPaymentsTeam Then
                    If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseCompliance) Or ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorisePaymentsTeam) Then
                        varResponse = MsgBox("Are you sure you want to authorise this free of payment transaction?", vbQuestion + vbYesNo, "Authorise Payment")
                        If varResponse = vbYes Then
                            ClsRD.SelectedAuthoriseOption = 5
                            ClsRD.CreateAndSendFOPEmail(False, NewConfirmReceipt.dgvFOP, NewConfirmReceipt.dgvFOP.SelectedRows.Item(0).Index, ClsRD.PortfolioName, ClsRD.SelectedTransactionID, ClsRD.Email, False, varNotes)
                        End If
                    Else
                        MsgBox("You do not have permissions to authorise this free of payment transaction", vbExclamation, "Cannot authorise payment")
                    End If
                Else
                    MsgBox("You can only authorise pending payments team free of payment transactions", vbExclamation, "Cannot authorise payment")
                End If
            ElseIf Rb5.Checked Then
                If ClsRD.StatusID = cStatusPendingCompliance Or ClsRD.StatusID = cStatusPendingPaymentsTeam Then
                    ClsRD.CreateAndSendFOPEmail(False, NewConfirmReceipt.dgvFOP, NewConfirmReceipt.dgvFOP.SelectedRows.Item(0).Index, ClsRD.PortfolioName, ClsRD.SelectedTransactionID, ClsRD.Email, True, varNotes)
                    MsgBox("Email has been sent", vbInformation, "Email sent")
                Else
                    MsgBox("You can only re-send pending payments team free of payment transactions", vbExclamation, "Cannot re-send payment")
                End If
            ElseIf Rb6.Checked Then
                If ClsRD.StatusID = cStatusPendingCompliance Or ClsRD.StatusID = cStatusPendingPaymentsTeam Then
                    MsgBox("You can only re-send non pending payments team free of payment transactions", vbExclamation, "Cannot re-send email")
                Else
                    ClsRD.CreateAndSendFOPEmail(False, NewConfirmReceipt.dgvFOP, NewConfirmReceipt.dgvFOP.SelectedRows.Item(0).Index, ClsRD.PortfolioName, ClsRD.SelectedTransactionID, ClsRD.Email, False, varNotes)
                    MsgBox("Email has been sent", vbInformation, "Email sent")
                End If
            End If
            If ClsRD.SelectedAuthoriseOption <> 0 Then
                ClsIMS.ActionMovementRD(ClsRD.SelectedAuthoriseOption, ClsRD.SelectedTransactionID, ClsIMS.FullUserName)
            End If

            If varResponse = vbYes Then
                Me.Close()
            End If
        End If
    End Sub



    Private Sub Rb4_CheckedChanged(sender As Object, e As EventArgs) Handles Rb4.CheckedChanged
        ColorRadio(Rb4)
    End Sub

    Private Sub Rb1_CheckedChanged(sender As Object, e As EventArgs) Handles Rb1.CheckedChanged
        ColorRadio(Rb1)
    End Sub

    Private Sub Rb2_CheckedChanged(sender As Object, e As EventArgs) Handles Rb2.CheckedChanged
        ColorRadio(Rb2)
    End Sub

    Private Sub Rb3_CheckedChanged(sender As Object, e As EventArgs) Handles Rb3.CheckedChanged
        ColorRadio(Rb3)
    End Sub

    Private Sub Rb5_CheckedChanged(sender As Object, e As EventArgs) Handles Rb5.CheckedChanged
        ColorRadio(Rb5)
    End Sub
End Class